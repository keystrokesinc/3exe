﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Windows.Forms;
using DataProcessor;

namespace UpdateManager
{
    public partial class Download : Form
    {
        public bool IsAskForUpdate = false;
        private readonly string _downloadFile = Path.Combine(Path.GetTempPath(), "SBTSync.msi");

        public Download()
        {
           
            InitializeComponent();
            cmdDownload.Text = ButtonCaption.Download;
            cmdClose.Text = ButtonCaption.Close;
            if (CheckForUpdate.IsUpdateAvailable())
            {
                Text = @"Update SBT QuickBooks Sync (from v" + GlobalValue.AppVersion + " to v" +
                        CheckForUpdate.ServerVersion.ToString(4) + ")";
                lblUpdateCaption.Text = @" An improved version of SBT Sync is now available. Click Download to Start Update process";
            }
            else
            {
                lblUpdateCaption.Text = @"You are running lastest version. Thank you for checking updates.";
                cmdDownload.Visible = false;
                lblSize.Visible = false;
                progressBar.Visible = false;
            }
        }


        private void cmdDownload_Click(object sender, EventArgs e)
        {
            if (cmdDownload.Text == ButtonCaption.Download)
            {
                WebClient webClient = new WebClient();
                webClient.DownloadFileCompleted += Completed;
                webClient.DownloadProgressChanged += ProgressChanged;
                webClient.DownloadFileAsync(new Uri(GlobalValue.UpdateDownloadPath), _downloadFile);
                cmdClose.Enabled = false;
                cmdDownload.Enabled = false;
            }

            if (cmdDownload.Text == ButtonCaption.Install)
            {
                Process.Start(_downloadFile);
                DialogResult = System.Windows.Forms.DialogResult.OK;
                Close();
            }
        }

        private void ProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;
            lblSize.Text = ((e.BytesReceived / 1024) / 1024) + @"MB / " + ((e.TotalBytesToReceive / 1024) / 1024) + @"MB";
        }

        private void Completed(object sender, AsyncCompletedEventArgs e)
        {
            cmdDownload.Text = ButtonCaption.Install;
            cmdClose.Enabled = true;
            cmdDownload.Enabled = true;
        }

        private void cmdClose_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }

        private static class ButtonCaption
        {
            public static string Download
            {
                get { return "Download"; }
            }

            public static string Close
            {
                get { return "Close"; }
            }

            public static string Install
            {
                get { return "Install"; }
            }
        }

        

        private void Download_Load(object sender, EventArgs e)
        {
           
            this.Width = this.Width + 10;
        }
    }
}