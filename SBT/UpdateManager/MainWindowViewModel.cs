﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Input;
using DataProcessor;

namespace UpdateManager
{
    public class MainWindowViewModel : PropertyChangedBase
    {
        private readonly string _downloadFile = Path.Combine(Path.GetTempPath(), "SBTSync.msi");

        public ICommand MinimizeWindowCommand { get; private set; }

        public ICommand CloseWindowCommand { get; private set; }

        public ICommand DownloadButtonCommand { get; private set; }

        public ICommand CloseButtonCommand { get; private set; }

        public ICommand InstallButtonCommand { get; private set; }

        private string _progressText;
        public string ProgressText
        {
            get { return _progressText; }
            set
            {
                _progressText = value;
                OnPropertyChanged("ProgressText");
            }
        }

        private string _updateAvailableText = "An application update is available";
        public string UpdateAvailableText
        {
            get { return _updateAvailableText; }
            set
            {
                _updateAvailableText = value;
                OnPropertyChanged("UpdateAvailableText");
            }
        }

        private int _progress;
        public int Progress
        {
            get { return _progress; }
            set
            {
                _progress = value;
                OnPropertyChanged("Progress");
            }
        }

        private bool _uiEnabled = true;
        public bool UiEnabled
        {
            get { return _uiEnabled; }
            set
            {
                _uiEnabled = value;
                OnPropertyChanged("UiEnabled");
            }
        }

        private bool _updateAvailable = true;
        public bool UpdateAvailable
        {
            get { return _updateAvailable; }
            set
            {
                _updateAvailable = value;
                OnPropertyChanged("UpdateAvailable");
            }
        }

        private bool _installAvailable = false;
        public bool InstallAvailable
        {
            get { return _installAvailable; }
            set
            {
                _installAvailable = value;
                OnPropertyChanged("InstallAvailable");
            }
        }

        public MainWindowViewModel()
        {
            if (!DesignerProperties.GetIsInDesignMode(new DependencyObject()))
            {
                UpdateAvailable = CheckForUpdate.IsUpdateAvailable();
            }
            else
            {
                ProgressText = "0MB / 0MB";
                Progress = 50;
            }

            UpdateAvailableText = UpdateAvailable
                ? @"An improved version of SBT Sync is now available. Click Download to Start Update process."
                : @"You are running lastest version. Thank you for checking updates.";

            SetCommands();
        }

        private void SetCommands()
        {
            MinimizeWindowCommand = new Command(o => Application.Current.MainWindow.WindowState = WindowState.Minimized);

            CloseWindowCommand = new Command(o =>
            {
                var window = Application.Current.MainWindow;

                window.DialogResult = false;

                if (window != null)
                {
                    window.Close();
                }
            });

            CloseButtonCommand = CloseWindowCommand;
            DownloadButtonCommand = new Command(o =>
            {
                UiEnabled = false;

                UpdateAvailableText = @"Update SBT QuickBooks Sync (from v" + GlobalValue.AppVersion + " to v" +
                                      CheckForUpdate.ServerVersion.ToString(4) + ")";

                WebClient webClient = new WebClient();
                webClient.DownloadFileCompleted += Completed;
                webClient.DownloadProgressChanged += ProgressChanged;
                webClient.DownloadFileAsync(new Uri(GlobalValue.UpdateDownloadPath), _downloadFile);
            });

            InstallButtonCommand = new Command(o =>
            {
                Process.Start(_downloadFile);
                Application.Current.MainWindow.DialogResult = true;
            });
        }

        private void ProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            Progress = e.ProgressPercentage;
            ProgressText = (e.BytesReceived / Math.Pow(1024d, 2)).ToString("#0.##") + @"MB / "
                + (e.TotalBytesToReceive / Math.Pow(1024d, 2)).ToString("#0.##") + @"MB";
        }

        private void Completed(object sender, AsyncCompletedEventArgs e)
        {
            UiEnabled = true;
            InstallAvailable = true;
        }
    }
}
