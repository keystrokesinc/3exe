﻿using System.ComponentModel;

namespace UpdateManager
{
    public abstract class PropertyChangedBase : INotifyPropertyChanged, IDataErrorInfo
    {
        public bool WindowLoaded = false;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public virtual string this[string columnName] { get { return string.Empty; } }

        public string Error
        {
            get { return string.Empty; }
        }
    }
}
