﻿using System;
using System.Xml;
using DataProcessor;

namespace UpdateManager
{
    public static class CheckForUpdate
    {
        public static Version ServerVersion = new Version(99, 99, 99, 99);

        private static void GetUpdateXml()
        {
            XmlTextReader xmlTextReader = new XmlTextReader(GlobalValue.UpdateXmlURL);
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(xmlTextReader);

            XmlNodeList xmlNodeList = xmlDocument.SelectNodes("//SBTpdate");
            if (xmlNodeList != null)
            {
                XmlNodeList selectNodes = xmlNodeList[0].SelectNodes("//AppUpdate");
                if (selectNodes != null)
                {
                    XmlAttributeCollection xmlAttributeCollection = selectNodes[0].Attributes;
                    if (xmlAttributeCollection != null)
                    {
                        ServerVersion = new Version(xmlAttributeCollection["CurrentVersion"].Value);
                        GlobalValue.UpdateDownloadPath = xmlAttributeCollection["URL"].Value;
                    }
                }
            }
        }

        public static bool IsUpdateAvailable()
        {
            GetUpdateXml();

            Version currentVersion = new Version(GlobalValue.AppVersion);

            if (currentVersion < ServerVersion)
            {
                return true;
            }
            return false;
        }
    }
}