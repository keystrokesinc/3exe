﻿using System;
using System.Threading;
using SBTSync.Properties;

namespace SBTSync
{
    static class Program
    {
        static readonly Mutex Mutex = new Mutex(true, "{59C51C24-9ED7-4148-A505-8EAC03B2C2E7}");

        [STAThread]
        static void Main()
        {
            if (!Mutex.WaitOne(TimeSpan.Zero, true))
            {
                return;
            }

            try
            {
                if (Settings.Default.SettingUpgradeRequired)
                {
                    Settings.Default.Upgrade();
                    DataProcessor.Properties.Settings.Default.Upgrade();
                    Settings.Default.SettingUpgradeRequired = false;
                    Settings.Default.Save();
                    DataProcessor.Properties.Settings.Default.Save();
                }

                new App().Run();
            }
            catch (Exception ex)
            {
                LogWriter.WriteToLog("Inside Program");
                LogWriter.WriteToLog(ex.Message.ToString());
            }
            finally
            {
                Mutex.ReleaseMutex();
            }

        }
    }
}
