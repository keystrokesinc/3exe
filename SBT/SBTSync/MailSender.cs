﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using Microsoft.Exchange.WebServices.Data;
using SBTSync.Properties;

namespace SBTSync
{
    public class MailSender
    {
        private static readonly SmtpClient Client;

        static MailSender()
        {
            Client = new SmtpClient(Settings.Default.MAILHOST, Settings.Default.MAILPORT)
            {
                EnableSsl = Settings.Default.MAILENABLESSL,

                Credentials = new NetworkCredential
                {
                    UserName = Settings.Default.MAILUSERNAME,
                    Password = Settings.Default.MAILPASSWORD
                }
            };
        }

        public static void Send(string to, string subject, string body, params System.Net.Mail.Attachment[] attachments)
        {
            ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2010);
            service.Credentials = new WebCredentials("admin@keystrokesinc.net", "GONE4gold!");
            //service.Credentials = new WebCredentials("info@simplebilltracking.com", "Simple2014!");
            service.Url = new Uri("https://outlook.office365.com/EWS/Exchange.asmx");
            EmailMessage email = new EmailMessage(service);
            email.ToRecipients.Add(to);
            email.Subject = subject;
            email.Body = new MessageBody(body);
            foreach (var attachment in attachments)
            {
                email.Attachments.AddFileAttachment(AppDomain.CurrentDomain.BaseDirectory + "LOG\\ERRORLOG\\" + attachment.Name);
            }
            email.SendAndSaveCopy();

            //OLD CODE
            //var message = new MailMessage("noreply@simplebilltracking.com", to)
            //{
            //    Subject = subject,
            //    Body = body
            //};

            //foreach (var attachment in attachments)
            //{
            //    message.Attachments.Add(attachment);
            //}
            //Client.Send(message);
        }
    }
}
