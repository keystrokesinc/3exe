using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace SBTSync
{
	
	partial class SqlServerConfiguration : Form
	{

		//Form overrides dispose to clean up the component list.
		[DebuggerNonUserCode()]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private IContainer components;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[DebuggerStepThrough()]
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SqlServerConfiguration));
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.Label6 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtTime = new System.Windows.Forms.TextBox();
            this.txtQbfileLoc = new System.Windows.Forms.TextBox();
            this.txtLicenseKey = new System.Windows.Forms.TextBox();
            this.chkINvoice = new System.Windows.Forms.CheckBox();
            this.chkIsPaymentReceived = new System.Windows.Forms.CheckBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.chkTimer = new System.Windows.Forms.CheckBox();
            this.chkLog = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.GroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.button1);
            this.GroupBox1.Controls.Add(this.Label6);
            this.GroupBox1.Controls.Add(this.Label2);
            this.GroupBox1.Controls.Add(this.btnClose);
            this.GroupBox1.Controls.Add(this.btnSave);
            this.GroupBox1.Controls.Add(this.txtTime);
            this.GroupBox1.Controls.Add(this.txtQbfileLoc);
            this.GroupBox1.Controls.Add(this.txtLicenseKey);
            this.GroupBox1.Controls.Add(this.chkINvoice);
            this.GroupBox1.Controls.Add(this.chkIsPaymentReceived);
            this.GroupBox1.Controls.Add(this.Label1);
            this.GroupBox1.Controls.Add(this.chkTimer);
            this.GroupBox1.Controls.Add(this.chkLog);
            this.GroupBox1.Location = new System.Drawing.Point(12, 12);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(458, 380);
            this.GroupBox1.TabIndex = 0;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Configuration Setting";
            this.GroupBox1.Enter += new System.EventHandler(this.GroupBox1_Enter);
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(31, 80);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(167, 17);
            this.Label6.TabIndex = 44;
            this.Label6.Text = "QuickBooks File Location";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(73, 34);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(85, 17);
            this.Label2.TabIndex = 42;
            this.Label2.Text = "License Key";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(364, 324);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 41;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(274, 324);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 40;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtTime
            // 
            this.txtTime.Location = new System.Drawing.Point(174, 218);
            this.txtTime.Name = "txtTime";
            this.txtTime.Size = new System.Drawing.Size(265, 22);
            this.txtTime.TabIndex = 38;
            this.txtTime.Text = "1200";
            this.txtTime.Visible = false;
            // 
            // txtQbfileLoc
            // 
            this.txtQbfileLoc.Location = new System.Drawing.Point(174, 73);
            this.txtQbfileLoc.Name = "txtQbfileLoc";
            this.txtQbfileLoc.Size = new System.Drawing.Size(265, 22);
            this.txtQbfileLoc.TabIndex = 36;
            // 
            // txtLicenseKey
            // 
            this.txtLicenseKey.Location = new System.Drawing.Point(174, 34);
            this.txtLicenseKey.Name = "txtLicenseKey";
            this.txtLicenseKey.Size = new System.Drawing.Size(265, 22);
            this.txtLicenseKey.TabIndex = 39;
            // 
            // chkINvoice
            // 
            this.chkINvoice.AutoSize = true;
            this.chkINvoice.Location = new System.Drawing.Point(174, 145);
            this.chkINvoice.Name = "chkINvoice";
            this.chkINvoice.Size = new System.Drawing.Size(88, 21);
            this.chkINvoice.TabIndex = 32;
            this.chkINvoice.Text = "Is Invoice";
            this.chkINvoice.UseVisualStyleBackColor = true;
            this.chkINvoice.Visible = false;
            // 
            // chkIsPaymentReceived
            // 
            this.chkIsPaymentReceived.AutoSize = true;
            this.chkIsPaymentReceived.Location = new System.Drawing.Point(174, 168);
            this.chkIsPaymentReceived.Name = "chkIsPaymentReceived";
            this.chkIsPaymentReceived.Size = new System.Drawing.Size(164, 21);
            this.chkIsPaymentReceived.TabIndex = 31;
            this.chkIsPaymentReceived.Text = "IS Payment Received";
            this.chkIsPaymentReceived.UseVisualStyleBackColor = true;
            this.chkIsPaymentReceived.Visible = false;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(42, 225);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(124, 17);
            this.Label1.TabIndex = 30;
            this.Label1.Text = "Time in Milisecond";
            this.Label1.Visible = false;
            // 
            // chkTimer
            // 
            this.chkTimer.AutoSize = true;
            this.chkTimer.Location = new System.Drawing.Point(174, 195);
            this.chkTimer.Name = "chkTimer";
            this.chkTimer.Size = new System.Drawing.Size(114, 21);
            this.chkTimer.TabIndex = 23;
            this.chkTimer.Text = "Enable Timer";
            this.chkTimer.UseVisualStyleBackColor = true;
            this.chkTimer.Visible = false;
            // 
            // chkLog
            // 
            this.chkLog.AutoSize = true;
            this.chkLog.Location = new System.Drawing.Point(174, 122);
            this.chkLog.Name = "chkLog";
            this.chkLog.Size = new System.Drawing.Size(102, 21);
            this.chkLog.TabIndex = 22;
            this.chkLog.Text = "Enable Log";
            this.chkLog.UseVisualStyleBackColor = true;
            this.chkLog.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(88, 324);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(161, 23);
            this.button1.TabIndex = 45;
            this.button1.Text = "Send log to support";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // SqlServerConfiguration
            // 
            this.ClientSize = new System.Drawing.Size(496, 404);
            this.Controls.Add(this.GroupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SqlServerConfiguration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configuration Setting";
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.ResumeLayout(false);

		}
		internal GroupBox GroupBox1;
		internal Button btnClose;
		internal Button btnSave;
        internal TextBox txtTime;
        internal TextBox txtQbfileLoc;
		internal TextBox txtLicenseKey;
		internal CheckBox chkINvoice;
		internal CheckBox chkIsPaymentReceived;
		internal Label Label1;
		internal CheckBox chkTimer;
        internal CheckBox chkLog;
        internal Label Label6;
		internal Label Label2;
		public SqlServerConfiguration()
		{
			Load += SQLServerConfiguration_Load;
			InitializeComponent();
		}

        private Button button1;
	}
}
