using System;
using DataProcessor;

namespace SBTSync.Views
{
    public class CleanItem : WorkItem
    {
        private readonly Func<GlobalValue.Reponse> _work;
        public GlobalValue.Reponse Response { get; private set; }
        public CleanItem(string title, Func<GlobalValue.Reponse> work) : base(title)
          
        {
            _work = work;
        }

        public override void DoWork()
        {
            Response = _work();
        }
    }
}