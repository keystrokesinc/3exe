﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Threading;
using DataProcessor;
using DataProcessor.QB;
using SBTSync.DataBase;
using SBTSync.DataBase.DAL;
using SBTSync.Properties;
using System.Configuration;
using Application = System.Windows.Application;
using MessageBox = System.Windows.Forms.MessageBox;

namespace SBTSync.Views
{
    public class MainWindowViewModel : PropertyChangedBase
    {
        private readonly SqlConnection _conn = new SqlConnection();
        Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        public ICommand MinimizeWindowCommand { get; private set; }

        public ICommand RestoreWindowCommand { get; private set; }

        public ICommand MaximizeWindowCommand { get; private set; }

        public ICommand CloseWindowCommand { get; private set; }

        public ICommand SelectSyncCommand { get; private set; }
        public ICommand SelectSFCommand { get; private set; }

        public ICommand SelectCleanCommand { get; private set; }

        public ICommand SelectSettingsCommand { get; private set; }

        public ICommand SyncCommand { get; private set; }

        public ICommand CleanCommand { get; private set; }

        public ICommand SaveSettingsCommand { get; private set; }

        public ICommand SendLogsToSupportCommand { get; private set; }
        public ICommand PullSalesforceCommand { get; private set; }

        public ICommand OpenFileDialogCommand { get; private set; }

       // public List<SyncItem> SyncItems { get; private set; }
        private List<SyncItem> _SyncItems;
        public List<SyncItem> SyncItems
        {
            get { return _SyncItems; }
            set
            {
                _SyncItems = value;
                OnPropertyChanged("SyncItems");
            }
        }

        public List<CleanItem> CleanItems { get; private set; }

        private string _header = string.Format("QuickBooks Sync v.{0}", GlobalValue.AppVersion);
        public string Header
        {
            get { return _header; }
            set
            {
                _header = value;
                OnPropertyChanged("Header");
            }
        }
        private string _cheader = string.Format("SBT - ");
        public string CHeader
        {
            get { return _cheader; }
            set
            {
                _cheader = value;
                OnPropertyChanged("CHeader");
            }
        }


        private Tabs _selectedTab;
        public Tabs SelectedTab
        {
            get { return _selectedTab; }
            set
            {
                _selectedTab = value;
                OnPropertyChanged("SelectedTab");

                WindowHeader = _selectedTab.ToString();
            }
        }

        private string _licenseKey;
        public string LicenseKey
        {
            get { return _licenseKey; }
            set
            {
                _licenseKey = value;
                OnPropertyChanged("LicenseKey");
            }
        }
        private string _batchName;
        public string BatchName
        {
            get { return _batchName; }
            set
            {
                _batchName = value;
                OnPropertyChanged("BatchName");
            }
        }
        private DateTime _startDate = DateTime.Now;
        public DateTime StartDate
        {
            get { return _startDate; }
            set
            {
                _startDate = value;
                OnPropertyChanged("StartDate");
            }
        }
        private DateTime _endDate = DateTime.Now;
        public DateTime EndDate
        {
            get { return _endDate; }
            set
            {
                _endDate = value;
                OnPropertyChanged("EndDate");
            }
        }

        private string _syncOutput = string.Empty;
        public string SyncOutput
        {
            get { return _syncOutput; }
            set
            {
                _syncOutput = value;
                OnPropertyChanged("SyncOutput");
            }
        }

        private string _syncProgress;
        public string SyncProgress
        {
            get { return _syncProgress; }
            set
            {
                _syncProgress = value;
                OnPropertyChanged("SyncProgress");
            }
        }

        private string _cleanOutput = string.Empty;
        public string CleanOutput
        {
            get { return _cleanOutput; }
            set
            {
                _cleanOutput = value;
                OnPropertyChanged("CleanOutput");
            }
        }

        private string _qbFileLocation;
        public string QBFileLocation
        {
            get { return _qbFileLocation; }
            set
            {
                _qbFileLocation = value;
                OnPropertyChanged("QBFileLocation");
            }
        }

        //private bool _enableLog;
        //public bool EnableLog
        //{
        //    get { return _enableLog; }
        //    set
        //    {
        //        _enableLog = value;
        //        OnPropertyChanged("EnableLog");
        //    }
        //}
        private string _visibleSF;
        public string VisibleSF
        {
            get { return _visibleSF; }
            set
            {
                _visibleSF = value;
                OnPropertyChanged("VisibleSF");
            }
        }
        private string _visibleMOW;
        public string VisibleMOW
        {
            get { return _visibleMOW; }
            set
            {
                _visibleMOW = value;
                OnPropertyChanged("VisibleMOW");
            }
        }

        private bool _isInvoice;
        public bool IsInvoice
        {
            get { return _isInvoice; }
            set
            {
                _isInvoice = value;
                OnPropertyChanged("IsInvoice");
            }
        }

        private bool _isPaymentReceived;
        public bool IsPaymentReceived
        {
            get { return _isPaymentReceived; }
            set
            {
                _isPaymentReceived = value;
                OnPropertyChanged("IsPaymentReceived");
            }
        }

        private bool _enableTimer;
        public bool EnableTimer
        {
            get { return _enableTimer; }
            set
            {
                _enableTimer = value;
                OnPropertyChanged("EnableTimer");
            }
        }

        private int _timeInMilliseconds;
        public int TimeInMilliseconds
        {
            get { return _timeInMilliseconds; }
            set
            {
                _timeInMilliseconds = value;
                OnPropertyChanged("TimeInMilliseconds");
            }
        }

        private string _windowHeader = "Simple Bill Tracking";
        public string WindowHeader
        {
            get { return _windowHeader; }
            set
            {
                _windowHeader = value;
                OnPropertyChanged("WindowHeader");
            }
        }

        private bool _uiEnaled = true;
        public bool UiEnabled
        {
            get { return _uiEnaled; }
            set
            {
                _uiEnaled = value;
                OnPropertyChanged("UiEnabled");
            }
        }

        public MainWindowViewModel()
        {
            SelectedTab = Tabs.Sync;
            SetCleanItems();
            SetSyncItems();
            SetCommands();
            SetSettings();           
        }

        private void SetSettings()
        {
            try
            {
                LicenseKey = config.AppSettings.Settings["LICENSEKEY"].Value.ToString();//Settings.Default.LICENSEKEY;
                QBFileLocation = config.AppSettings.Settings["QBPATH"].Value.ToString();
                CHeader = config.AppSettings.Settings["CompanyName"].Value.ToString();
                if (string.IsNullOrEmpty(CHeader))
                    CHeader = "SBT - ";//Settings.Default.QBPATH;
                TimeInMilliseconds = Settings.Default.INTERVAL;                
                // EnableLog = Settings.Default.LOGENABLE;
                EnableTimer = Settings.Default.TIMERENABLE;
                IsInvoice = Settings.Default.ISINVOICE;
                IsPaymentReceived = Settings.Default.ISPAYMENTRECEIVED;
                if (Settings.Default.SFEnable)
                    VisibleSF = "Visible";
                else
                    VisibleSF = "Collapsed";

                if (Settings.Default.MOWSFEnable)
                    VisibleMOW = "Visible";
                else
                    VisibleMOW = "Collapsed";
                
            }
            catch (Exception ex)
            {
                GlobalValue.Eventlogupdate("SetSettings: " + ex.Message, null, "Error");
                MessageBox.Show(@"Unable to Save Settings, please contact Administrator", @"Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
        }

        private void AppendToSyncOutput(string s)
        {
            SyncOutput += string.Format("[{0}] {1}{2}", DateTime.Now.ToString("T"), s, Environment.NewLine);
        }

        private void AppendCleanOutput(string s)
        {
            CleanOutput += string.Format("[{0}] {1}{2}", DateTime.Now.ToString("T"), s, Environment.NewLine);
        }

        private void SetCommands()
        {
            MinimizeWindowCommand = new Command(o => Application.Current.MainWindow.WindowState = WindowState.Minimized);
            RestoreWindowCommand = new Command(o => Application.Current.MainWindow.WindowState = WindowState.Normal);
            MaximizeWindowCommand = new Command(o => Application.Current.MainWindow.WindowState = WindowState.Maximized);
            CloseWindowCommand = new Command(o => Application.Current.Shutdown());

            SelectSyncCommand = new Command(o => {
                SelectedTab = Tabs.Sync;
                SetSyncItems();
            }
            );
            SelectCleanCommand = new Command(o => SelectedTab = Tabs.Clean);
            SelectSettingsCommand = new Command(o => SelectedTab = Tabs.Settings);
            SelectSFCommand = new Command(o =>
            {
                SelectedTab = Tabs.SF;
                SetSyncItems();
            });

            SyncCommand = new Command(o => ThreadPool.QueueUserWorkItem(state =>
            {
                UiEnabled = false;

                try
                {
                    Sync();
                }
                finally
                {
                    UiEnabled = true;
                }
            }));

            CleanCommand = new Command(o => ThreadPool.QueueUserWorkItem(state =>
            {
                UiEnabled = false;

                try
                {
                    Clean();
                }
                finally
                {
                    UiEnabled = true;
                }
            }));

            SaveSettingsCommand = new Command(o => ThreadPool.QueueUserWorkItem(state =>
            {
                UiEnabled = false;

                try
                {
                    SaveSettings();
                }
                finally
                {
                    UiEnabled = true;
                }

            }));

            SendLogsToSupportCommand = new Command(o => SendLogs());
            PullSalesforceCommand = new Command(o => PullSalesForce());

            OpenFileDialogCommand = new Command(o =>
            {
                var dialog = new OpenFileDialog();
                var result = dialog.ShowDialog();

                if (result == DialogResult.OK)
                {
                    QBFileLocation = dialog.FileName;
                }
            });
        }

        private void SetSyncItems()
        {
            if (SyncItems != null)
                SyncItems.Clear();
            if (SelectedTab == Tabs.SF)
            {
                SyncItems = new List<SyncItem>
                    {
                        new SyncItem("Pull Invoice from SF", SFInvoice.PullSFInvoice, UpdateSyncProgress),
                        new SyncItem("Pull SalesReceipt from SF", SFSalesReceipt.PullSFSalesReceipt, UpdateSyncProgress),
                        new SyncItem("Pull Payment from SF", SFPayment.PullSFPayment, UpdateSyncProgress),
                        new SyncItem("Push Invoice to QB", SFInvoice.ProcessSFInvoice, UpdateSyncProgress),
                        new SyncItem("Push SalesReceipt to QB", SFSalesReceipt.ProcessSFSalesReceipt, UpdateSyncProgress),
                        new SyncItem("Push Payment to QB", SFPayment.ProcessSFPayment, UpdateSyncProgress),
                    };
            }
            if (SelectedTab == Tabs.Sync)
            {
                if (SyncItems != null)
                    SyncItems.Clear();
                if (Settings.Default.SFEnable)
                {
                    SyncItems = new List<SyncItem>
                    {
                        new SyncItem("Account(s)", Account.ProcessAccount, UpdateSyncProgress),
                        new SyncItem("Class", Class.ProcessClass, UpdateSyncProgress),
                        new SyncItem("Item(s)", Items.ProcessItems, UpdateSyncProgress),
                        new SyncItem("Customer/Job", Customer.ProcessCustomer, UpdateSyncProgress),
                        new SyncItem("Vendor(s)", Vendor.ProcessVendor, UpdateSyncProgress),
                        new SyncItem("Check/Bill", Bill.ProcessBill, UpdateSyncProgress),
                        new SyncItem("Pull Invoice from SF", SFInvoice.PullSFInvoice, UpdateSyncProgress),
                        new SyncItem("Push Invoice to QB", SFInvoice.ProcessSFInvoice, UpdateSyncProgress),                        
                    };
                }
                else if (Settings.Default.MOWSFEnable)
                {
                    SyncItems = new List<SyncItem>
                    {
                        new SyncItem("Account(s)", Account.ProcessAccount, UpdateSyncProgress),
                        new SyncItem("Class", Class.ProcessClass, UpdateSyncProgress),
                        new SyncItem("Item(s)", Items.ProcessItems, UpdateSyncProgress),
                        new SyncItem("Customer/Job", Customer.ProcessCustomer, UpdateSyncProgress),
                        new SyncItem("Vendor(s)", Vendor.ProcessVendor, UpdateSyncProgress),
                        new SyncItem("Check/Bill", Bill.ProcessBill, UpdateSyncProgress),
                        new SyncItem("Pull Invoice from SF", MWSFInvoice.PullSFInvoice, UpdateSyncProgress),
                        new SyncItem("Push Invoice to QB", MWSFInvoice.ProcessSFInvoice, UpdateSyncProgress),
                    };
                }
                else if (Settings.Default.LOGEnable)
                {
                    SyncItems = new List<SyncItem>
                    {
                        new SyncItem("Account(s)", Account.ProcessAccount, UpdateSyncProgress),
                        new SyncItem("Class", Class.ProcessClass, UpdateSyncProgress),
                        new SyncItem("Item(s)", Items.ProcessItems, UpdateSyncProgress),
                        new SyncItem("Customer/Job", Customer.ProcessCustomer, UpdateSyncProgress),
                        new SyncItem("Vendor(s)", Vendor.ProcessVendor, UpdateSyncProgress),
                        new SyncItem("Check/Bill", Bill.ProcessBill, UpdateSyncProgress),
                        new SyncItem("Log(s)", Logs.ProcessLogs, UpdateSyncProgress),                        
                    };
                }
                else
                {
                    SyncItems = new List<SyncItem>
                    {
                        new SyncItem("Account(s)", Account.ProcessAccount, UpdateSyncProgress),
                        new SyncItem("Class", Class.ProcessClass, UpdateSyncProgress),
                        new SyncItem("Item(s)", Items.ProcessItems, UpdateSyncProgress),
                        new SyncItem("Customer/Job", Customer.ProcessCustomer, UpdateSyncProgress),
                        new SyncItem("Vendor(s)", Vendor.ProcessVendor, UpdateSyncProgress),
                        new SyncItem("Check/Bill", Bill.ProcessBill, UpdateSyncProgress),                        
                    };
                }
            }
            //Carbonlite
            //SyncItems = new List<SyncItem>
            //        {
            //            new SyncItem("Vendor(s)", Vendor.ProcessVendor, UpdateSyncProgress),
            //            new SyncItem("Bill(s)", Bill.ProcessBills, UpdateSyncProgress),
            //            new SyncItem("Bill Payment(s)", BillPayments.BillPaymentsPull, UpdateSyncProgress),
            //            new SyncItem("AgingReport", AgingReport.ProcessAgingReport, UpdateSyncProgress),
            //            new SyncItem("Vendor Credit", VendorCredit.ProcessVendorCredit, UpdateSyncProgress),
            //        };
        }

        private void UpdateSyncProgress(ProgressEventArgs args)
        {
            SyncProgress = args.Completed
                         ? string.Empty
                         : string.Format("Syncing {0} of {1}", args.Current, args.Total);
        }

        private void SetCleanItems()
        {
            CleanItems = new List<CleanItem>
            {
                // new CleanItem("Account(s)", Account.DeleteAccount),
                // new CleanItem("Cost Center(s)", Class.DeleteClass),
                // new CleanItem("Fund(s)", Customer.DeleteCustomer),
                // new CleanItem("Vendor(s)", Vendor.DeleteVendor)
            };
        }

        private void Sync()
        {
            Settings.Default.BatchName = BatchName;
            if(Settings.Default.SFEnable)
            {
                Settings.Default.StartDate = StartDate.ToString("dd-MM-yyyy");
                Settings.Default.EndDate = EndDate.ToString("dd-MM-yyyy");
            }            
            else
            {
                Settings.Default.StartDate = StartDate.ToString("yyyy-MM-dd");
                Settings.Default.EndDate = EndDate.ToString("yyyy-MM-dd");
            }
            var selectedItems = SyncItems.Where(item => item.IsSelected).ToList();

            if (selectedItems.Count == 0)
            {
                MessageBox.Show(@"Nothing Selected");
                return;
            }

            SyncOutput = string.Empty;

            AppendToSyncOutput("Sync started...");

            if (!Credentials.Update())
            {
                AppendToSyncOutput("Sync failed");
                return;
            }

            try
            {
                AppendToSyncOutput("Initializing connection....");

                QBXConnector.GetQBConnector();

             if(Company.ValidateCompany()==0)
                {
                    AppendToSyncOutput("Company EIN Validation failed. Unable to Sync");
                    return;
                }
               
              

                foreach (var item in selectedItems)
                {
                    AppendToSyncOutput(string.Format("Processing " + item.Title + " ....."));
                    item.DoWork();
                    AppendToSyncOutput(string.Format("{0}.", item.Response.Message));
                }

                AppendToSyncOutput("Data fetch completed.....");

                QBXConnector.GetQBConnector().CloseConnection();

                AppendToSyncOutput("Connection closed.....");
            }
            catch (Exception exx)
            {
                GlobalValue.Eventlogupdate("Sync: " + exx.Message, null, "Error");
                AppendToSyncOutput(exx.Message);

                try
                {
                    QBXConnector.GetQBConnector().CloseConnection();
                    AppendToSyncOutput("Connection closed.....");
                }
                catch (Exception ex)
                {
                    GlobalValue.Eventlogupdate("Sync: " + ex.Message, null, "Error");
                    AppendToSyncOutput("Connection couldn't close....." + ex.Message);
                }
            }
        }

        private void Clean()
        {
            var selectedItems = CleanItems.Where(item => item.IsSelected).ToList();

            if (selectedItems.Count == 0)
            {
                MessageBox.Show(@"Nothing Selected");
                return;
            }

            CleanOutput = string.Empty;

            AppendCleanOutput("Clean started...");

            if (!Credentials.Update())
            {
                AppendCleanOutput("Clean failed");
                return;
            }

            try
            {
                AppendCleanOutput("Initializing connection.....");

                QBXConnector.GetQBConnector();

                foreach (var item in selectedItems)
                {
                    AppendCleanOutput("Processing " + item.Title + " .....");
                    item.DoWork();
                    AppendCleanOutput(string.Format("{0} .", item.Response.Message));
                }

                AppendCleanOutput("Cleaning completed.....");
            }
            catch (Exception exx)
            {
                GlobalValue.Eventlogupdate("Clean: " + exx.Message, null, "Error");
                AppendCleanOutput(exx.Message);
            }
            finally
            {
                try
                {
                    QBXConnector.GetQBConnector().CloseConnection();
                    AppendCleanOutput("Connection closed.....");
                }
                catch (Exception ex)
                {
                    GlobalValue.Eventlogupdate("Clean Close connetion: " + ex.Message, null, "Error");
                    AppendCleanOutput("Connection couldn't close....." + ex.Message);
                }
            }
        }

        private void SaveSettings()
        {
            try
            {
                if (string.IsNullOrEmpty(LicenseKey) || string.IsNullOrEmpty(QBFileLocation))//QBFileLocation
                {
                    MessageBox.Show(@"Please Enter All The Fields Before Saving!", @"Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                    return;
                }
                config.AppSettings.Settings["QBPATH"].Value = QBFileLocation;
                config.AppSettings.Settings["LICENSEKEY"].Value = LicenseKey;
                config.AppSettings.Settings["CompanyName"].Value = CHeader;
                config.Save(ConfigurationSaveMode.Modified);
                //Settings.Default.LICENSEKEY = LicenseKey;
                //Settings.Default.QBPATH = QBFileLocation;
                //Settings.Default.Save();
                if (!Credentials.Update())
                {
                    return;
                }

                string tempConnection = Credentials.Get.ConnectionString;
                CHeader = Credentials.Get.companyName;
                config.AppSettings.Settings["CompanyName"].Value = CHeader;
                config.Save(ConfigurationSaveMode.Modified);
                var isvalidcon = false;

                try
                {
                    _conn.ConnectionString = tempConnection;
                    _conn.Open();

                    if (_conn.State == ConnectionState.Open)
                    {
                        isvalidcon = true;
                        _conn.Close();
                    }
                }
                catch (Exception ex)
                {
                    _conn.Close();
                    isvalidcon = false;
                }
                finally
                {
                    _conn.Close();
                }

                if (isvalidcon)
                {                   
                    var isTimerEnable = false;                  

                    if (EnableTimer)
                    {
                        isTimerEnable = true;
                    }
                    
                   // Settings.Default.QBPATH = QBFileLocation;
                    Settings.Default.INTERVAL = TimeInMilliseconds;
                    Settings.Default.TIMERENABLE = isTimerEnable;
                    //Settings.Default.LOGENABLE = EnableLog;                    
                   
                    Settings.Default.Save();
                    config.AppSettings.Settings["LICENSEKEY"].Value = LicenseKey;
                    config.AppSettings.Settings["QBPATH"].Value = QBFileLocation;
                    config.Save(ConfigurationSaveMode.Modified);
                    DataProcessor.Properties.Settings.Default.QBPATH = QBFileLocation;
                    DataProcessor.Properties.Settings.Default.ERRORLOGENABLE = Settings.Default.ERRORLOGENABLE;
                    DataProcessor.Properties.Settings.Default.Save();

                    System.Windows.Forms.MessageBox.Show(@"Config Settings Have Been Saved Successfully.", "", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);

                    SelectedTab = Tabs.Sync;
                }
                else
                {

                    MessageBox.Show(@"Could't connect to DB");
                }
            }
            catch (Exception ex)
            {
                GlobalValue.Eventlogupdate("Could't save config. Error: " + ex.Message, null, "Error");
            }
        }

        private void SendLogs()
        {
            try
            {
                var attachments =
                    new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory)
                        .Parent
                        .GetDirectories()
                        .SelectMany(i => i.GetDirectories()
                                          .Where(info => info.Name.ToUpper() == "LOG"))
                        .SelectMany(i => i.GetDirectories()
                                          .Where(info => info.Name.ToUpper() == "ERRORLOG"))
                        .SelectMany(i => i.GetFiles()
                                          .Select(fi => new Attachment(fi.FullName)))
                        .ToArray();

                if (attachments.Any())
                {
                    string company = DBBase.GetCompanyName(Credentials.Get.CompanyId);
                    if (Credentials.Get.CompanyId <= 0 || string.IsNullOrEmpty(company))
                    {
                        MessageBox.Show("No company found. Sending log failed", "QB Sync");
                        return;
                    }

                    if (string.IsNullOrEmpty(SyncOutput))
                    {
                        MessageBox.Show("No log to send", "QB Sync");
                        return;
                    }

                    MailSender.Send(Settings.Default.MAILTO, "QBSync logs - " + company, "logs in attached files", attachments);
                    MessageBox.Show("Logs sent successfully");
                    SelectedTab = Tabs.Sync;
                }
                else
                {
                    MessageBox.Show("Log files not found");
                }
            }
            catch (Exception ex)
            {
                GlobalValue.Eventlogupdate("SendLogs: " + ex.Message, null, "Error");
                MessageBox.Show("Logs not sent");
            }
        }

        private void PullSalesForce()
        {
            //SelectedTab = Tabs.Sync;
            if (!Credentials.Update())
            {                
                return;
            }
            string response = GetAllSFRecords.GetRecords();           
            AppendToSyncOutput(response);

        }

        }
    }
