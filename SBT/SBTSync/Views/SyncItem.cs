using System;
using DataProcessor;
using SBTSync.DataBase;

namespace SBTSync.Views
{
    public class SyncItem : WorkItem
    {
        private readonly Func<Action<ProgressEventArgs>, GlobalValue.Reponse> _work;
        private readonly Action<ProgressEventArgs> _onProgressChanged; 


        public GlobalValue.Reponse Response { get; private set; }

        public SyncItem(string title, 
                        Func<Action<ProgressEventArgs>, GlobalValue.Reponse> work,
                        Action<ProgressEventArgs> onProgressChanged) : base(title)
        {
            _work = work;
            _onProgressChanged = onProgressChanged;
        }

        public override void DoWork()
        {
            Response = _work(_onProgressChanged);
        }
    }
}