﻿namespace SBTSync.Views
{
    public abstract class WorkItem : PropertyChangedBase
    {
        public string Title { get; private set; }

        private bool _IsSelected;
        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                _IsSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        protected WorkItem(string title)
        {
            Title = title;
        }

        public abstract void DoWork();
    }
}