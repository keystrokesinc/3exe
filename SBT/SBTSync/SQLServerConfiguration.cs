using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Windows.Forms;
using DataProcessor;
using SBTSync.DataBase;
using SBTSync.Properties;

namespace SBTSync
{
    public partial class SqlServerConfiguration
    {
      //  private readonly SimpleAES.SimpleAES _clsEncryption = new SimpleAES.SimpleAES();
        private readonly SqlConnection _conn = new SqlConnection();

        private void SQLServerConfiguration_Load(object sender, EventArgs e)
        {
            try
            {
                //If Not String.IsNullOrEmpty(gblConnectionString) Then
                //    Dim dbclass As New QBAUTOUPDATEDataContext(gblConnectionString)
                //    With lkpAccountPayable.Properties
                //        .DataSource = dbclass.Accounts_Select(Nothing, gblCompanyID)
                //        .DisplayMember = "AccountFullName"
                //        .ValueMember = "ListID"
                //        .Columns.Add(New LookUpColumnInfo("AccountFullName", "Account Name", 50))
                //    End With
                //End If

                FormLoadSettings();
            }
            catch (Exception)
            {
            }
        }

        private void FormLoadSettings()
        {
            //Dim doc As XmlDocument = New XmlDocument()
            //Dim sFilePath As String = "Config.dll"
            //doc.Load(sFilePath)
            //Dim nodelist As XmlNodeList = doc.SelectNodes("SQL")

            try
            {

                txtLicenseKey.Text = Settings.Default.LICENSEKEY;
                // clsEncryption.DecryptString(doc.SelectSingleNode("SQL/SERVERNAME").InnerText)
                // clsEncryption.DecryptString(doc.SelectSingleNode("SQL/DATABASENAME").InnerText)
                // clsEncryption.DecryptString(doc.SelectSingleNode("SQL/USERID").InnerText)
                // clsEncryption.DecryptString(doc.SelectSingleNode("SQL/PASSWORD").InnerText)
                txtQbfileLoc.Text = Settings.Default.QBPATH;
                //clsEncryption.DecryptString(doc.SelectSingleNode("SQL/QBPATH").InnerText)
                txtTime.Text = Settings.Default.INTERVAL.ToString();
                //clsEncryption.DecryptString(doc.SelectSingleNode("SQL/INTERVAL").InnerText)
                //clsEncryption.DecryptString(doc.SelectSingleNode("SQL/COMPANY").InnerText)
                //If Not doc.SelectSingleNode("SQL/APACCOUNT").InnerText = "NO" Then
                //    lkpAccountPayable.EditValue = doc.SelectSingleNode("SQL/APACCOUNT").InnerText
                //End If
                bool islogstatus = Settings.Default.ERRORLOGENABLE;
                // clsEncryption.DecryptString(doc.SelectSingleNode("SQL/LOGENABLE").InnerText)
                chkLog.CheckState = islogstatus ? CheckState.Checked : CheckState.Unchecked;

                bool isTimestatus = Settings.Default.TIMERENABLE;
                //clsEncryption.DecryptString(doc.SelectSingleNode("SQL/TIMERENABLE").InnerText)
                chkTimer.CheckState = isTimestatus ? CheckState.Checked : CheckState.Unchecked;
                chkINvoice.Checked = Settings.Default.ISINVOICE;
                chkIsPaymentReceived.Checked = Settings.Default.ISPAYMENTRECEIVED;
            }
            catch (Exception)
            {
                MessageBox.Show(@"Unable to Save Settings, please contact Administrator", Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtLicenseKey.Text) &&  string.IsNullOrEmpty(txtQbfileLoc.Text))
                {
                    MessageBox.Show(@"Please Enter All The Fields Before Saving!", Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                    txtLicenseKey.Focus();
                    return;
                }

                Settings.Default.LICENSEKEY = txtLicenseKey.Text;

                if (!Credentials.Update())
                {
                    return;
                }

                string tempConnection = Credentials.Get.ConnectionString;
                
                var isvalidcon = false;

                try
                {
                    _conn.ConnectionString = tempConnection;
                    _conn.Open();

                    if (_conn.State == ConnectionState.Open)
                    {
                        isvalidcon = true;
                        _conn.Close();
                    }
                }
                catch (Exception ex)
                {
                    _conn.Close();
                    isvalidcon = false;
                }
                finally
                {
                    _conn.Close();
                }
                if (isvalidcon)
                {
                    var islogEnable = false;
                    var isTimerEnable = false;
                    if (chkLog.CheckState == CheckState.Checked)
                    {
                        islogEnable = true;
                    }
                    if (chkTimer.CheckState == CheckState.Checked)
                    {
                        isTimerEnable = true;
                    }
                    Settings.Default.QBPATH = txtQbfileLoc.Text;
                    Settings.Default.INTERVAL = Convert.ToInt32(txtTime.Text);
                    Settings.Default.TIMERENABLE = isTimerEnable;
                    Settings.Default.ERRORLOGENABLE = islogEnable;
                    Settings.Default.ISINVOICE = chkINvoice.Checked;
                    Settings.Default.ISPAYMENTRECEIVED = chkIsPaymentReceived.Checked;
                    Settings.Default.LICENSEKEY = txtLicenseKey.Text;
                    Settings.Default.Save();

                   DataProcessor.Properties.Settings.Default.QBPATH = Settings.Default.QBPATH;
                   DataProcessor.Properties.Settings.Default.ERRORLOGENABLE = Settings.Default.ERRORLOGENABLE;
                   DataProcessor.Properties.Settings.Default.Save();

                    MessageBox.Show(@"Config Settings Has Been Saved Successfully.", "", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    Close();
                }
                else
                {

                    MessageBox.Show(@"License is not valid");
                }
            }
            catch (Exception ex)
            {
                GlobalValue.Eventlogupdate("Could't save config. Error: " + ex.Message, null, "Error");
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        //private void CreateConfigFile(string SERVERNAME, string DATABASENAME, string USERID, string PASSWORD,
        //    string QBPATH, int INTERVAL, bool TIMERENABLE, bool LOGENABLE, int CompanyID)
        //{
        //    try
        //    {
        //        var settings = new XmlWriterSettings();
        //        settings.Indent = true;
        //        using (XmlWriter writer = XmlWriter.Create(Application.StartupPath + "\\Config.dll", settings))
        //        {
        //            writer.WriteStartDocument();
        //            writer.WriteStartElement("SQL");
        //            writer.WriteElementString("SERVERNAME", _clsEncryption.EncryptToString(SERVERNAME));
        //            writer.WriteElementString("DATABASENAME", _clsEncryption.EncryptToString(DATABASENAME));
        //            writer.WriteElementString("USERID", _clsEncryption.EncryptToString(USERID));
        //            writer.WriteElementString("PASSWORD", _clsEncryption.EncryptToString(PASSWORD));
        //            writer.WriteElementString("QBPATH", _clsEncryption.EncryptToString(QBPATH));
        //            writer.WriteElementString("INTERVAL", _clsEncryption.EncryptToString(INTERVAL.ToString()));
        //            writer.WriteElementString("TIMERENABLE", _clsEncryption.EncryptToString(TIMERENABLE.ToString()));
        //            writer.WriteElementString("LOGENABLE", _clsEncryption.EncryptToString(LOGENABLE.ToString()));
        //            writer.WriteElementString("COMPANY", _clsEncryption.EncryptToString(CompanyID.ToString()));
        //            // writer.WriteElementString("APACCOUNT", APACCOUNT)
        //            writer.WriteEndElement();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var attachments = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory)
                    .Parent
                    .GetDirectories()
                    .SelectMany(i => i.GetDirectories()
                                      .Where(info => info.Name.ToUpper() == "LOG"))
                    .SelectMany(i => i.GetDirectories()
                                      .Where(info => info.Name.ToUpper() == "ERRORLOG"))
                    .SelectMany(i => i.GetFiles()
                                      .Select(fi => new Attachment(fi.FullName)))
                    .ToArray();

                if (attachments.Any())
                {
                    MailSender.Send(Settings.Default.MAILTO, "QBSync logs", "logs in attached files", attachments);
                    MessageBox.Show("Logs sent successfully");
                    Close();
                }
                else
                {
                    MessageBox.Show("Log files not found");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show("Logs not sent");
            }
        }
    }
}