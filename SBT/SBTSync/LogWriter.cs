﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SBTSync
{
    public static class LogWriter
    {
        public static void WriteToLog(string message)
        {
            DateTime todaydate = DateTime.Now.AddMilliseconds(9);
            string datePart = todaydate.Date.ToString();
            string timePart = todaydate.TimeOfDay.ToString();
            string hourOnly = todaydate.Hour.ToString();
            string dayonly = todaydate.Day.ToString();
            string monthOnly = todaydate.Month.ToString();
            string yearOnly = todaydate.Year.ToString();

            string logtime = yearOnly + "-" + monthOnly + "-" + dayonly + ".txt";
            string logDirPath = AppDomain.CurrentDomain.BaseDirectory + "LOG\\ERRORLOG\\" + logtime;
            if (!Directory.Exists(Directory.GetParent(logDirPath).FullName))
                Directory.CreateDirectory(Directory.GetParent(logDirPath).FullName);
            StreamWriter sw = new StreamWriter(logDirPath, true);
            sw.WriteLine(message);
            sw.Close();
        }
    }
}
