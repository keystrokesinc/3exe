﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Markup;
using SBTSync.Views;
using Application = System.Windows.Application;
using System.IO;

namespace SBTSync
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

            FrameworkElement.LanguageProperty.OverrideMetadata(
                typeof(FrameworkElement),
                new FrameworkPropertyMetadata(XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));

            base.OnStartup(e);

            CheckUpdates();

            var window = new MainWindow();

            MainWindow = window;

            window.Show();
        }

        public App()
        {
            InitializeComponent();
        }

        private void CheckUpdates()
        {
            try
            {
                //if (UpdateManager.CheckForUpdate.IsUpdateAvailable())
                //{
                //    bool? showDialog = (new UpdateManager.MainWindow()).ShowDialog();

                //    if (showDialog == true)
                //    {
                //        Process[] processesByName = Process.GetProcessesByName("SBTSync");
                //        if (processesByName.Length > 0)
                //        {
                //            processesByName[0].Kill();
                //        }
                //        Current.Shutdown();
                //    }
                //}
            }
            catch (Exception ex)
            {
                LogWriter.WriteToLog("Inside Update Call" + DataProcessor.GlobalValue.GetAllFootprints(ex));
                System.Windows.Forms.MessageBox.Show(ex.Message.ToString());
                LogWriter.WriteToLog(ex.Message.ToString());
            }
        }
    }
}
