﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Linq;
using DataProcessor;
using DataProcessor.QB.XMLProcessor;
using DataProcessor.Sync.ModelRet;
using SBTSync.Properties;
using DataProcessor.Sync.ModelAdd;
using DataProcessor.Sync;
using SBTSync.DataBase.DAL;
using SBTSync.DataBase;

namespace SBTSync.DataBase.DAL
{
    public class PreferencesReport : DBBase
    {
        private static string errMsg = string.Empty;
        private static int dateParamCount = 11;
        public static GlobalValue.Reponse ProcessPreferencesReport(Action<ProgressEventArgs> onProgressChanged)
        {            
            GlobalValue.Reponse responses = new GlobalValue.Reponse();            
            errMsg = string.Empty;            
            QBXPreferenceReport gDReport = new QBXPreferenceReport();
            //DateTime dtClosing = Convert.ToDateTime("09-30-2020");
            //var closeDate = dtClosing.ToString("yyyy-MM-dd");
            IList<PreferencesRet> prefReports = gDReport.GetPreferencegReport();
            if(prefReports.Count >0)
            {
                foreach (PreferencesRet prReport in prefReports)
                {
                    DataContext.PreferencesInsert(Credentials.Get.CompanyId, prReport.AccountingPreferences!= null ? prReport.AccountingPreferences.IsUsingAccountNumbers : "",
              prReport.AccountingPreferences != null ? prReport.AccountingPreferences.IsRequiringAccounts :"",
              prReport.AccountingPreferences != null ? prReport.AccountingPreferences.IsUsingClassTracking : "",
              prReport.AccountingPreferences != null ? prReport.AccountingPreferences.IsUsingAuditTrail : "",
              prReport.AccountingPreferences != null ? prReport.AccountingPreferences.IsAssigningJournalEntryNumbers : "",
              prReport.AccountingPreferences != null ? prReport.AccountingPreferences.ClosingDate : "",
              prReport.FinanceChargePreferences != null ? prReport.FinanceChargePreferences.AnnualInterestRate : "",
              prReport.FinanceChargePreferences != null ? prReport.FinanceChargePreferences.MinFinanceCharge : "",
              prReport.FinanceChargePreferences != null ? prReport.FinanceChargePreferences.GracePeriod : "",
              prReport.FinanceChargePreferences != null ? prReport.FinanceChargePreferences.IsAssessingForOverdueCharges : "",
              prReport.FinanceChargePreferences != null ? prReport.FinanceChargePreferences.CalculateChargesFrom : "",
              prReport.FinanceChargePreferences != null ? prReport.FinanceChargePreferences.IsMarkedToBePrinted : "",
              prReport.JobsAndEstimatesPreferences != null ? prReport.JobsAndEstimatesPreferences.IsUsingEstimates : "",
              prReport.JobsAndEstimatesPreferences != null ? prReport.JobsAndEstimatesPreferences.IsUsingProgressInvoicing : "",
              prReport.JobsAndEstimatesPreferences != null ? prReport.JobsAndEstimatesPreferences.IsPrintingItemsWithZeroAmounts : "",
             prReport.MultiCurrencyPreferences != null ? prReport.MultiCurrencyPreferences.IsMultiCurrencyOn : "",
             prReport.MultiLocationInventoryPreferences != null ? prReport.MultiLocationInventoryPreferences.IsMultiLocationInventoryAvailable : "",
             prReport.MultiLocationInventoryPreferences != null ? prReport.MultiLocationInventoryPreferences.IsMultiLocationInventoryEnabled : "",
            prReport.PurchasesAndVendorsPreferences != null ? prReport.PurchasesAndVendorsPreferences.IsUsingInventory : "",
             prReport.PurchasesAndVendorsPreferences != null ? prReport.PurchasesAndVendorsPreferences.DaysBillsAreDue : "",
             prReport.PurchasesAndVendorsPreferences != null ? prReport.PurchasesAndVendorsPreferences.IsAutomaticallyUsingDiscounts : "",
             prReport.ReportsPreferences != null ? prReport.ReportsPreferences.AgingReportBasis : "",
             prReport.ReportsPreferences != null ? prReport.ReportsPreferences.SummaryReportBasis : "",
             prReport.SalesAndCustomersPreferences != null ? prReport.SalesAndCustomersPreferences.PriceLevels.IsUsingPriceLevels : "",
            prReport.SalesAndCustomersPreferences != null ? prReport.SalesAndCustomersPreferences.PriceLevels.IsRoundingSalesPriceUp : "",
             prReport.SalesAndCustomersPreferences != null ? prReport.SalesAndCustomersPreferences.IsTrackingReimbursedExpensesAsIncome : "",
             prReport.SalesAndCustomersPreferences != null ? prReport.SalesAndCustomersPreferences.IsAutoApplyingPayments : "",
             prReport.TimeTrackingPreferences != null ? prReport.TimeTrackingPreferences.FirstDayOfWeek : "",
             prReport.CurrentAppAccessRights != null ? prReport.CurrentAppAccessRights.IsAutomaticLoginAllowed : "",
             prReport.CurrentAppAccessRights != null ? prReport.CurrentAppAccessRights.AutomaticLoginUserName : "",
            prReport.CurrentAppAccessRights != null ? prReport.CurrentAppAccessRights.IsPersonalDataAccessAllowed : "",
             prReport.ItemsAndInventoryPreferences != null ? prReport.ItemsAndInventoryPreferences.EnhancedInventoryReceivingEnabled : "",
            prReport.ItemsAndInventoryPreferences != null ? prReport.ItemsAndInventoryPreferences.IsTrackingSerialOrLotNumber : "",
              prReport.ItemsAndInventoryPreferences != null ? prReport.ItemsAndInventoryPreferences.FIFOEnabled : "",
              prReport.ItemsAndInventoryPreferences != null ? prReport.ItemsAndInventoryPreferences.IsRSBEnabled : "",
              prReport.ItemsAndInventoryPreferences != null ? prReport.ItemsAndInventoryPreferences.IsBarcodeEnabled : "");
                }
                   
            }           
             
            onProgressChanged(new ProgressEventArgs
            {
                Completed = true
            });           
            responses.Message += errMsg;         
             return responses;
        }





    }
}
