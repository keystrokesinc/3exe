﻿using System;
using System.Collections.Generic;
using DataProcessor;
using DataProcessor.QB.XMLProcessor;
using DataProcessor.Sync.ModelRet;
using System.Data.Linq;
using System.Data.SqlClient;
using DataProcessor.Sync.ModelAdd;
using DataProcessor.Sync;
using SBTSync.Properties;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using Newtonsoft.Json;
using System.Data;
using System.Linq;
using DataProcessor.Ref;

namespace SBTSync.DataBase.DAL
{
    public class VendorCredit : DBBase
    {

        public static int insertBillCount = 0;
        public static GlobalValue.Reponse ProcessVendorCredit(Action<ProgressEventArgs> onProgressChanged)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            try
            {
                QBXVendorCredit qbxVC = new QBXVendorCredit();
                ISingleResult<BillSelect> billsSelectResults = DataContext.GetBillDetails();
                List<BillSelect> selectResults = new List<BillSelect>(billsSelectResults);
                if (selectResults.Count > 0)
                {
                    foreach (BillSelect billSelectResult in selectResults)
                    {
                        VendorCreditAdd vc = new VendorCreditAdd
                        {
                            VendorRef = new VendorRef
                            {
                                FullName = billSelectResult.Vendor
                            },
                            APAccountRef = new DataProcessor.Sync.ModelAdd.APAccountRef
                            {
                                ListID = billSelectResult.APAccount_ListID
                            },
                            TxnDate = billSelectResult.TxnDate,
                            RefNumber = billSelectResult.RefNumber,
                        };
                        ExpenseLineAdd lineCM = new ExpenseLineAdd();
                        lineCM.AccountRef = new DataProcessor.Ref.AccountRef
                        {
                            FullName = "AP_Clearance"
                        };
                        lineCM.Amount = billSelectResult.Amount;

                        vc.ExpenseLineAdd = lineCM;

                        List<GlobalValue.Reponse> saveVC = qbxVC.SaveVC(vc);
                        if (saveVC.Count > 0 && !saveVC[0].IsError)
                        {
                            var checkSerializeXml = SyncConstant.SerializeXml<VendorCreditRet>(saveVC[0].EntityObject.ToString(), QueryXML.VendorCredit, false);
                            if (checkSerializeXml.Count > 0)
                            {
                                BillPaymentCheckAdd billPaymentCheckAdd = new BillPaymentCheckAdd
                                {
                                    BankAccountRef = new BankAccountRef
                                    {
                                        FullName = "Bank Leumi - Operating"
                                    },
                                    TxnDate = String.Format("{0:yyyy-MM-dd}", checkSerializeXml[0].TxnDate),
                                    RefNumber = "ACH",//checkSerializeXml[0].RefNumber,
                                    PayeeEntityRef = new DataProcessor.Sync.ModelAdd.PayeeEntityRef
                                    {
                                        FullName = billSelectResult.Vendor//Vendor Name
                                    },
                                    AppliedToTxnAdd = new AppliedToTxnAdd
                                    {
                                        TxnID = billSelectResult.TxnID,//TxnID of the Bill
                                                                   //PaymentAmount = checkSerializeXml[0].CreditAmount.ToString(),
                                        SetCredit = new SetCredit
                                        {
                                            CreditTxnID = checkSerializeXml[0].TxnID, //TxnID of Vendor Credit
                                            AppliedAmount = billSelectResult.Amount
                                        }
                                    }
                                };
                                List<GlobalValue.Reponse> applyCredit = qbxVC.ApplyCredit(billPaymentCheckAdd);
                                if (applyCredit.Count > 0 && !applyCredit[0].IsError)
                                {
                                    insertBillCount++;
                                    DataContext.UpdateVendorCreditTxnID(billSelectResult.TxnID, checkSerializeXml[0].TxnID);                                    
                                    responses.IsError = false;
                                    //responses.Message = checkSerializeXml[0].TxnID;
                                }
                            }
                        }
                        onProgressChanged(new ProgressEventArgs
                        {
                            Total = selectResults.Count,
                            Current = insertBillCount
                        });
                    }
                }
                

            }
            catch (Exception ex)
            {
                responses.IsError = true;
                responses.Message += ex.Message + "\n";
                GlobalValue.Eventlogupdate(responses.Message, null, "Error");
            }
            onProgressChanged(new ProgressEventArgs
            {
                Completed = true
            });

            return responses;
        }
        public static GlobalValue.Reponse ProcessVendorCreditHardcoded(Action<ProgressEventArgs> onProgressChanged)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            try
            {                
                QBXVendorCredit qbxVC = new QBXVendorCredit();
             
                VendorCreditAdd vc = new VendorCreditAdd
                {
                    VendorRef = new VendorRef
                    {
                        FullName = "Dynisco Instruments LLC"
                    },
                    APAccountRef = new DataProcessor.Sync.ModelAdd.APAccountRef
                    {
                        ListID = "8000001F-1393517911"
                    },
                    TxnDate = "2021-02-04",
                    RefNumber = "020421",                    
                };                
                ExpenseLineAdd lineCM = new ExpenseLineAdd();
                lineCM.AccountRef = new DataProcessor.Ref.AccountRef
                {
                    FullName = "AP_Clearance"
                };
                lineCM.Amount = "537.59";                          
                
                vc.ExpenseLineAdd = lineCM;

                List<GlobalValue.Reponse> saveVC = qbxVC.SaveVC(vc);
                if (saveVC.Count > 0 && !saveVC[0].IsError)
                {
                    var checkSerializeXml = SyncConstant.SerializeXml<VendorCreditRet>(saveVC[0].EntityObject.ToString(), QueryXML.VendorCredit, false);
                    if (checkSerializeXml.Count > 0)
                    {
                        BillPaymentCheckAdd billPaymentCheckAdd = new BillPaymentCheckAdd
                        {
                            BankAccountRef = new BankAccountRef
                            {
                                FullName = "Bank Leumi - Operating"
                            },
                            TxnDate = String.Format("{0:yyyy-MM-dd}", checkSerializeXml[0].TxnDate),
                            RefNumber = checkSerializeXml[0].RefNumber,                            
                            PayeeEntityRef = new DataProcessor.Sync.ModelAdd.PayeeEntityRef
                            {
                                FullName = "Dynisco Instruments LLC"//Vendor Name
                            },
                            AppliedToTxnAdd = new AppliedToTxnAdd
                            {
                                TxnID = "80668-1614954001",//TxnID of the Bill
                                //PaymentAmount = checkSerializeXml[0].CreditAmount.ToString(),
                                SetCredit = new SetCredit
                                {
                                    CreditTxnID = checkSerializeXml[0].TxnID, //TxnID of Vendor Credit
                                    AppliedAmount = "537.59"
                                }
                            }
                        };                        
                        List<GlobalValue.Reponse> applyCredit = qbxVC.ApplyCredit(billPaymentCheckAdd);
                        if (applyCredit.Count > 0 && !applyCredit[0].IsError)
                        {
                        }
                    }
                }                                 

            }
            catch (Exception ex)
            {
                responses.IsError = true;                
                responses.Message +=  ex.Message + "\n";                
                GlobalValue.Eventlogupdate(responses.Message, null, "Error");
            }
            onProgressChanged(new ProgressEventArgs
            {
                Completed = true
            });            

            return responses;
        }
       

    }
}
