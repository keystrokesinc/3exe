﻿

using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Linq;
using DataProcessor;
using DataProcessor.QB.XMLProcessor;
using DataProcessor.Sync.ModelRet;
using SBTSync.Properties;

namespace SBTSync.DataBase.DAL
{
    public class Account : DBBase
    {
        private static string errMsg = "";

        public static GlobalValue.Reponse ProcessAccount(Action<ProgressEventArgs> onProgressChanged)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            responses.Message = "";
            QBXAccount account = new QBXAccount();

            DateTime? qbAccountLastMod = GetLastModified("Account");
            int insertAccountCount = 0;
            int UpdateAccountCount = 0;
            errMsg = "";

            List<AccountRet> accountRets = account.GetAllAccounts(null);
            GlobalValue.Eventlogupdate(" Total accounts from QB " + accountRets.Count,null);
            GlobalValue.Eventlogupdate(" Active accounts from QB " + accountRets.Count(e => e.IsActive == true), null);
            GlobalValue.Eventlogupdate(" In Active accounts from QB " + accountRets.Count(e => e.IsActive == false), null);

            foreach (AccountRet accountRet in accountRets)  //(selectResults.Any(item => item.VendorRefListID == null)
            {
                try
                {
                    ISingleResult<Accounts_SelectResult> accountSelectResults = DataContext.Accounts_Select(accountRet.ListID, Credentials.Get.CompanyId);
                    List<Accounts_SelectResult> selectResults = new List<Accounts_SelectResult>(accountSelectResults);
                    if (accountRet.AccountNumber == null) accountRet.AccountNumber = "";

                    if (selectResults.Count == 0)
                    {
                        DataContext.Accounts_Insert(Credentials.Get.CompanyId, accountRet.ListID, accountRet.TimeCreated, accountRet.TimeModified,
                            accountRet.AccountNumber, accountRet.FullName, accountRet.AccountType, accountRet.Desc, Convert.ToBoolean(accountRet.IsActive) );
                        GlobalValue.Eventlogupdate("Account having Name " + accountRet.Name + " Inserted", null);
                        insertAccountCount++;
                    }
                    else
                    {
                        DataContext.Accounts_Update(Credentials.Get.CompanyId, accountRet.ListID, accountRet.TimeCreated, accountRet.TimeModified,
                             accountRet.AccountNumber, accountRet.FullName, accountRet.AccountType, accountRet.Desc,Convert.ToBoolean(accountRet.IsActive));
                        GlobalValue.Eventlogupdate("Account having Name " + accountRet.Name + " Updated.Active Status:"+ accountRet.IsActive, null);
                        UpdateAccountCount++;
                    }

                    onProgressChanged(new ProgressEventArgs
                    {
                        Total = accountRets.Count,
                        Current = insertAccountCount + UpdateAccountCount
                    });
                }
                catch (Exception ex)
                {
                    errMsg += ex.Message + Environment.NewLine;
                    GlobalValue.Eventlogupdate(ex, null, "Error");
                }
            }

            onProgressChanged(new ProgressEventArgs
            {
                Completed = true
            });

            if (errMsg.Length > 0)
                responses.Message += errMsg + ".\n";

            if (insertAccountCount > 0)
                responses.Message += insertAccountCount + " Account(s) added to database.\n";
            if (UpdateAccountCount > 0)
                responses.Message += UpdateAccountCount + " Account(s) updated in database.\n";

            responses.Message += DeleteAccount(accountRets).Message;
            if (!string.IsNullOrEmpty(errMsg))
            {
                MailSender.Send(Settings.Default.MAILTO, Credentials.Get.CompanyId + ": Error in Account Sync " + Credentials.SyncDateTime.ToString("MM-dd-yyyy HH:mm"), errMsg);
            }
            else
            {
                DataContext.Settings_InsertUpdate("Account", DateTime.UtcNow, Credentials.Get.CompanyId);
            }
            return responses;
        }

        public static GlobalValue.Reponse DeleteAccount(List<AccountRet> accountRets = null)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();

            try
            {
                accountRets = accountRets ?? (new QBXAccount()).GetAllAccounts(null);

                ISingleResult<Accounts_SelectResult> accountSelectResults = DataContext.Accounts_Select(null, Credentials.Get.CompanyId);
                List<Accounts_SelectResult> dbAccount = new List<Accounts_SelectResult>(accountSelectResults);


                foreach (Accounts_SelectResult accountsSelectResult in dbAccount)
                {
                    AccountRet classR = accountRets.FirstOrDefault(c => c.ListID == accountsSelectResult.ListID);
                    if (classR == null)
                    {

                        GlobalValue.Eventlogupdate("Setting inactive Account record having name " + accountsSelectResult.AccountFullName, null);
                        DataContext.Accounts_Update(Credentials.Get.CompanyId, accountsSelectResult.ListID, accountsSelectResult.TimeCreated, accountsSelectResult.TimeModified, accountsSelectResult.AccountNumber, accountsSelectResult.AccountFullName,
                            accountsSelectResult.AccounType, accountsSelectResult.Description, false);
                        responses.RecordCount++;
                    }
                }

            }
            catch (Exception ex)
            {
                errMsg += ex.Message + Environment.NewLine;
                GlobalValue.Eventlogupdate("Account Inactive: " + ex.Message, null, "Error");
            }

            responses.Message = responses.RecordCount.ToString() + " Account(s) set  to inactive status .";
            return responses;
        }
    }

}
