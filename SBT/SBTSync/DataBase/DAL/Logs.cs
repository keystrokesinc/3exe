﻿using DataProcessor;
using DataProcessor.QB.XMLProcessor;
using DataProcessor.Sync.ModelRet;
using SBTSync.Properties;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;

namespace SBTSync.DataBase.DAL
{
  public  class Logs : DBBase
    {    
        
        public static GlobalValue.Reponse ProcessLogs(Action<ProgressEventArgs> onProgressChanged)
        {
              int insertInvoiceCount = 0;
              int SFinsertInvoiceCount = 0;
              int SFinsertSalesReceiptCount = 0;
              int insertSalesReceiptCount = 0;
              int insertPaymentCount = 0;

              int TotalCount = 0;   int LoopCount = 0;


            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            QBXInvoice invoice = new QBXInvoice();
            DateTime? qbInvoiceLastMod = GetLastModified("Invoice");
            GlobalValue.Eventlogupdate("Getting Invoices", null);
            ISingleResult<Logs_SelectResult> invoiceSelectResults = DataContext.Invoice_Select_AddQuickBooks(Credentials.Get.CompanyId);
            List<Logs_SelectResult> InvResults = new List<Logs_SelectResult>(invoiceSelectResults);

            ISingleResult<Logs_SelectResult> salesReceiptSelectResults = DataContext.SalesReceipt_Select_AddQuickBooks(Credentials.Get.CompanyId);
            List<Logs_SelectResult> salesResults = new List<Logs_SelectResult>(salesReceiptSelectResults);

            ISingleResult<Logs_SelectResult> paymentSelectResults = DataContext.Payment_Select_AddQuickBooks(Credentials.Get.CompanyId);
            List<Logs_SelectResult> PayResults = new List<Logs_SelectResult>(paymentSelectResults);

            ISingleResult<Logs_SelectResult> invoiceCombSelectResults = DataContext.InvoiceComb_Select_AddQuickBooks(Credentials.Get.CompanyId);
            List<Logs_SelectResult> InvCombResults = new List<Logs_SelectResult>(invoiceCombSelectResults);

            ISingleResult<Logs_SelectResult> salesReceiptCombSelectResults = DataContext.SalesReceiptComb_Select_AddQuickBooks(Credentials.Get.CompanyId);
            List<Logs_SelectResult> salesCombResults = new List<Logs_SelectResult>(salesReceiptCombSelectResults);
            
            //**Sales force Invoice and Sales Receipt**//
            ISingleResult<Logs_SelectResult> SFinvoiceSelectResults = DataContext.SFInvoice_Select_AddQuickBooks(Credentials.Get.CompanyId);
            List<Logs_SelectResult> SFInvResults = new List<Logs_SelectResult>(SFinvoiceSelectResults);

            ISingleResult<Logs_SelectResult> SFsalesReceiptSelectResults = DataContext.SFSalesReceipt_Select_AddQuickBooks(Credentials.Get.CompanyId);
            List<Logs_SelectResult> SFsalesResults = new List<Logs_SelectResult>(SFsalesReceiptSelectResults);

            TotalCount = SFInvResults.Count + SFsalesResults.Count + InvResults.Count + salesResults.Count + PayResults.Count+ InvCombResults.Count + salesCombResults.Count;

            List<int> payID = new List<int>();


            insertInvoiceCount = 0;
            QBXSalesReceipt salesReceipt = new QBXSalesReceipt();
            QBXCustomer qBXCustomer = new QBXCustomer();
            responses.Message = "";
            string errMsg = "";

            if (InvResults.Count > 0)
            {
                foreach (Logs_SelectResult invoiceSelectResult in InvResults)
                {
                    GlobalValue.Reponse res = new GlobalValue.Reponse();
                    res.IsError = false;
                    try
                    {
                        if (invoiceSelectResult.CustomerID == null)
                        {
                            string custID = Customer.insertCustomerToQB(invoiceSelectResult);
                            if (custID.Contains("Error"))
                            {
                                res.IsError = true;
                                res.Message = "- Invoice for customer " + invoiceSelectResult.Customer + ", Log ID " +
                                 invoiceSelectResult.LogPaymentID + ", form number " +
                                 invoiceSelectResult.FormNumber + ", with amount $" +
                                 invoiceSelectResult.Amount + ", was not added to QuickBooks because: " + custID;
                            }
                            else
                                invoiceSelectResult.CustomerID = custID;
                        }
                        else//SearchCustomer
                        {
                            string customerID = Customer.searchCustomer(invoiceSelectResult);
                            if (customerID.Contains("Error"))
                            {
                                res.IsError = true;
                                res.Message = "- Invoice for customer " + invoiceSelectResult.Customer + ", Log ID " +
                                 invoiceSelectResult.LogPaymentID + ", form number " +
                                 invoiceSelectResult.FormNumber + ", with amount $" +
                                 invoiceSelectResult.Amount + ", was not added to QuickBooks because: " + customerID;
                            }
                        }

                        if (!res.IsError)
                            res = Invoice.SaveInvoice(invoiceSelectResult, invoice);

                        if (!res.IsError)
                        {
                            insertInvoiceCount++;
                        }
                        else
                        {
                            responses.Message += $" Error in Invoice sync - " + res.Message.ToString() + "\n";
                            errMsg += $" Error in Invoice sync - " + res.Message.ToString() + Environment.NewLine;
                        }
                        if (payID.Contains(invoiceSelectResult.LogPaymentID) == false)
                        {
                            payID.Add(invoiceSelectResult.LogPaymentID);
                        }

                    }
                    catch (Exception ex)
                    {
                        GlobalValue.Eventlogupdate(ex, null, "Error");
                        responses.Message += $" Error in Invoice sync - " + ex.Message.ToString() + "\n";
                        errMsg += $" Error in Invoice sync - " + ex.Message.ToString() + Environment.NewLine;
                    }
                    LoopCount++;

                    onProgressChanged(new ProgressEventArgs
                    {
                        Total = TotalCount,
                        Current = LoopCount
                    });
                }
            }



            // Sales Receipt


            insertSalesReceiptCount = 0;
            if (salesResults.Count > 0)
            {
                foreach (Logs_SelectResult invoiceSelectResult in salesResults)
                {

                    GlobalValue.Reponse res = new GlobalValue.Reponse();
                    res.IsError = false;
                    try
                    {
                        if (invoiceSelectResult.CustomerID == null)
                        {
                            string custID = Customer.insertCustomerToQB(invoiceSelectResult);
                            if (custID.Contains("Error"))
                            {
                                res.IsError = true;
                                res.Message = "- Sales Receipt for customer " + invoiceSelectResult.Customer + ", Log ID " +
                                 invoiceSelectResult.LogPaymentID + ", form number " +
                                 invoiceSelectResult.FormNumber + ", with amount $" +
                                 invoiceSelectResult.Amount + ", was not added to QuickBooks because: " + custID;
                            }
                            else
                                invoiceSelectResult.CustomerID = custID;
                        }
                        else//SearchCustomer
                        {
                            string customerID = Customer.searchCustomer(invoiceSelectResult);
                            if (customerID.Contains("Error"))
                            {
                                res.IsError = true;
                                res.Message = "- Invoice for customer " + invoiceSelectResult.Customer + ", Log ID " +
                                 invoiceSelectResult.LogPaymentID + ", form number " +
                                 invoiceSelectResult.FormNumber + ", with amount $" +
                                 invoiceSelectResult.Amount + ", was not added to QuickBooks because: " + customerID;
                            }
                        }

                        if (!res.IsError)
                            res = SalesReceipt.SaveSalesReceipt(invoiceSelectResult, salesReceipt);

                        if (!res.IsError)
                        {

                            insertSalesReceiptCount++;
                        }
                        else
                        {
                            responses.Message += $" Error in Sales Receipt sync - " + res.Message.ToString() + "\n";
                            errMsg += $" Error in Sales Receipt sync - " + res.Message.ToString() + Environment.NewLine;
                        }
                        if (payID.Contains(invoiceSelectResult.LogPaymentID) == false)
                        {
                            payID.Add(invoiceSelectResult.LogPaymentID);
                        }
                    }
                    catch (Exception ex)
                    {
                        responses.Message += $" Error in Sales Receipt sync - " + ex.Message.ToString() + "\n";
                        errMsg += $" Error in Sales Receipt sync - " + ex.Message.ToString() + Environment.NewLine;
                        GlobalValue.Eventlogupdate(ex, null, "Error");
                    }
                    LoopCount++;

                    onProgressChanged(new ProgressEventArgs
                    {
                        Total = TotalCount,
                        Current = LoopCount
                    });
                }
            }


            //Payment
            QBXPaymentMethod payment = new QBXPaymentMethod();

            insertPaymentCount = 0;
            if (PayResults.Count > 0)
            {
                foreach (Logs_SelectResult invoiceSelectResult in PayResults)
                {

                    GlobalValue.Reponse res = new GlobalValue.Reponse();
                    try
                    {
                        if (invoiceSelectResult.CustomerID == null)
                        {
                            string custID = Customer.insertCustomerToQB(invoiceSelectResult);
                            if (custID.Contains("Error"))
                            {
                                res.IsError = true;
                                res.Message = "- Payment for customer " + invoiceSelectResult.Customer + ", Log ID " +
                                 invoiceSelectResult.LogPaymentID + ", form number " +
                                 invoiceSelectResult.FormNumber + ", with amount $" +
                                 invoiceSelectResult.Amount + ", was not added to QuickBooks because: " + custID;
                            }
                            else
                                invoiceSelectResult.CustomerID = custID;
                        }
                        else//SearchCustomer
                        {
                            string customerID = Customer.searchCustomer(invoiceSelectResult);
                            if (customerID.Contains("Error"))
                            {
                                res.IsError = true;
                                res.Message = "- Invoice for customer " + invoiceSelectResult.Customer + ", Log ID " +
                                 invoiceSelectResult.LogPaymentID + ", form number " +
                                 invoiceSelectResult.FormNumber + ", with amount $" +
                                 invoiceSelectResult.Amount + ", was not added to QuickBooks because: " + customerID;
                            }
                        }

                        if (!res.IsError)
                            res = Payment.SavePayment(invoiceSelectResult, payment);

                        if (!res.IsError)
                        {
                            insertPaymentCount++;
                        }
                        else
                        {
                            responses.Message += $" Error in Payment sync - " + res.Message.ToString() + "\n";
                            errMsg += $" Error in Payment sync - " + res.Message.ToString() + Environment.NewLine;
                        }
                        if (payID.Contains(invoiceSelectResult.LogPaymentID) == false)
                        {
                            payID.Add(invoiceSelectResult.LogPaymentID);
                        }
                    }
                    catch (Exception ex)
                    {
                        responses.Message += $" Error in Payment sync - " + ex.Message.ToString() + "\n";
                        errMsg += $" Error in Payment sync - " + ex.Message.ToString() + Environment.NewLine;
                        GlobalValue.Eventlogupdate(ex, null, "Error");
                    }
                    LoopCount++;

                    onProgressChanged(new ProgressEventArgs
                    {
                        Total = TotalCount,
                        Current = LoopCount
                    });
                }
            }

            if (insertPaymentCount > 0)
                responses.Message += $"{insertPaymentCount} Payment(s) added to quickbooks\n";

            //Combined Invoice 
            int CombinedID = 0;
            if (InvCombResults.Count > 0)
            {
                var combList = InvCombResults.GroupBy(test => test.CombinedID)
                                .Select(grp => grp.First());

                foreach (var key in combList)
                {

                    CombinedID = key.CombinedID;
                    var result = from myRow in InvCombResults
                                 where myRow.CombinedID == CombinedID
                                 select myRow;
                    if (result.Count() > 0)
                    {
                        var custRes = result.ElementAt(0);
                        GlobalValue.Reponse res = new GlobalValue.Reponse();
                        try
                        {
                            if (custRes.CustomerID == null)
                            {
                                string custID = Customer.insertCustomerToQB(custRes);
                                if (custID.Contains("Error"))
                                {
                                    res.IsError = true;
                                    res.Message = "- combined Invoice for customer " + custRes.Customer + ", Log ID " +
                                     custRes.LogPaymentID + ", form number " +
                                     custRes.FormNumber + ", with amount $" +
                                     custRes.Amount + ", was not added to QuickBooks because: " + custID;
                                }
                                else
                                    custRes.CustomerID = custID;
                            }
                            else//SearchCustomer
                            {
                                string customerID = Customer.searchCustomer(custRes);
                                if (customerID.Contains("Error"))
                                {
                                    res.IsError = true;
                                    res.Message = "- Invoice for customer " + custRes.Customer + ", Log ID " +
                                     custRes.LogPaymentID + ", form number " +
                                     custRes.FormNumber + ", with amount $" +
                                     custRes.Amount + ", was not added to QuickBooks because: " + customerID;
                                }
                            }

                            res = Invoice.SaveCombInvoice(result.ToList(), invoice);
                            if (!res.IsError)
                            {
                                insertInvoiceCount = insertInvoiceCount + result.Count();
                            }
                            else
                            {
                                responses.Message += $" Error in  combined Invoice sync - " + res.Message.ToString() + "\n";
                                errMsg += $" Error in combined Invoice sync - " + res.Message.ToString() + Environment.NewLine;
                            }
                            if (payID.Contains(custRes.LogPaymentID) == false)
                            {
                                payID.Add(custRes.LogPaymentID);
                            }
                        }

                        catch (Exception ex)
                        {
                            responses.Message += $" Error in combined Invoice sync - " + ex.Message.ToString() + "\n";
                            errMsg += $" Error in combined Invoice sync - " + ex.Message.ToString() + Environment.NewLine;
                            GlobalValue.Eventlogupdate(ex, null, "Error");
                        }
                        LoopCount = LoopCount + result.Count();
                    }
                }

                onProgressChanged(new ProgressEventArgs
                {
                    Total = TotalCount,
                    Current = LoopCount
                });
            }

            if (insertInvoiceCount > 0)
                responses.Message += $"{insertInvoiceCount} Invoice(s) added to quickbooks\n";

            //Comb Sales 
            if (salesCombResults.Count > 0)
            {
                var combList = salesCombResults.GroupBy(test => test.CombinedID)
                                .Select(grp => grp.First());

                foreach (var key in combList)
                {

                    CombinedID = key.CombinedID;
                    var result = from myRow in salesCombResults
                                 where myRow.CombinedID == CombinedID
                                 select myRow;
                    if (result.Count() > 0)
                    {
                        var custRes = result.ElementAt(0);
                        GlobalValue.Reponse res = new GlobalValue.Reponse();
                        try
                        {
                            if (custRes.CustomerID == null)
                            {
                                string custID = Customer.insertCustomerToQB(custRes);
                                if (custID.Contains("Error"))
                                {
                                    res.IsError = true;
                                    res.Message = "- combined Sales receipt for customer " + custRes.Customer + ", Log ID " +
                                     custRes.LogPaymentID + ", form number " +
                                     custRes.FormNumber + ", with amount $" +
                                     custRes.Amount + ", was not added to QuickBooks because: " + custID;
                                }
                                else
                                    custRes.CustomerID = custID;
                            }
                            else//SearchCustomer
                            {
                                string customerID = Customer.searchCustomer(custRes);
                                if (customerID.Contains("Error"))
                                {
                                    res.IsError = true;
                                    res.Message = "- Invoice for customer " + custRes.Customer + ", Log ID " +
                                     custRes.LogPaymentID + ", form number " +
                                     custRes.FormNumber + ", with amount $" +
                                     custRes.Amount + ", was not added to QuickBooks because: " + customerID;
                                }
                            }

                            res = SalesReceipt.SaveCombSalesReceipt(result.ToList(), salesReceipt);
                            if (!res.IsError)
                            {
                                insertSalesReceiptCount = insertSalesReceiptCount + result.Count();
                            }
                            else
                            {
                                responses.Message += $" Error in  combined sales receipt sync - " + res.Message.ToString() + "\n";
                                errMsg += $" Error in combined sales receipt sync - " + res.Message.ToString() + Environment.NewLine;
                            }
                            if (payID.Contains(custRes.LogPaymentID) == false)
                            {
                                payID.Add(custRes.LogPaymentID);
                            }
                        }

                        catch (Exception ex)
                        {
                            responses.Message += $" Error in combined sales receipt sync - " + ex.Message.ToString() + "\n";
                            errMsg += $" Error in combined sales receipt sync - " + ex.Message.ToString() + Environment.NewLine;
                            GlobalValue.Eventlogupdate(ex, null, "Error");
                        }
                        LoopCount = LoopCount + result.Count();
                    }
                }

                onProgressChanged(new ProgressEventArgs
                {
                    Total = TotalCount,
                    Current = LoopCount
                });
            }



            if (insertSalesReceiptCount > 0)
                responses.Message += $"{insertSalesReceiptCount} Sales Receipt(s) added to quickbooks\n";

            //**Salesforce Invoice and Sales Receipt **//
            if (SFInvResults.Count > 0)
            {
                SFinsertInvoiceCount = 0;
                foreach (Logs_SelectResult SFinvoiceSelectResult in SFInvResults)
                {
                    GlobalValue.Reponse res = new GlobalValue.Reponse();
                    res.IsError = false;
                    try
                    {
                        if (SFinvoiceSelectResult.CustomerID == null)
                        {
                            string custID = "";
                            IList<CustomerRet> customerRets = qBXCustomer.SearchCustomer(SFinvoiceSelectResult.Customer);
                            if (customerRets?.Count > 0)
                                custID = customerRets[0].ListID;
                            else
                                custID = Customer.insertCustomerToQB(SFinvoiceSelectResult);
                            if (custID.Contains("Error"))
                            {
                                res.IsError = true;
                                res.Message = "- SF Invoice for customer " + SFinvoiceSelectResult.Customer + ", Log ID " +
                                 SFinvoiceSelectResult.LogPaymentID + ", form number " +
                                 SFinvoiceSelectResult.FormNumber + ", with amount $" +
                                 SFinvoiceSelectResult.Amount + ", was not added to QuickBooks because: " + custID;
                            }
                            else
                                SFinvoiceSelectResult.CustomerID = custID;
                        }

                        if (!res.IsError)
                            res = SFInvoice.SaveInvoice(SFinvoiceSelectResult, invoice);

                        if (!res.IsError)
                        {
                            SFinsertInvoiceCount++;
                        }
                        else
                        {
                            responses.Message += $" Error in SF Invoice sync - " + res.Message.ToString() + "\n";
                            errMsg += $" Error in SF Invoice sync - " + res.Message.ToString() + Environment.NewLine;
                        }
                        if (payID.Contains(SFinvoiceSelectResult.LogPaymentID) == false)
                        {
                            payID.Add(SFinvoiceSelectResult.LogPaymentID);
                        }

                    }
                    catch (Exception ex)
                    {
                        GlobalValue.Eventlogupdate(ex, null, "Error");
                        responses.Message += $" Error in SF Invoice sync - " + ex.Message.ToString() + "\n";
                        errMsg += $" Error in SF Invoice sync - " + ex.Message.ToString() + Environment.NewLine;
                    }
                    LoopCount++;

                    onProgressChanged(new ProgressEventArgs
                    {
                        Total = TotalCount,
                        Current = LoopCount
                    });
                }
            }
            if (SFinsertInvoiceCount > 0)
                responses.Message += $"{SFinsertInvoiceCount} SF Invoice(s) added to quickbooks\n";
            SFinsertSalesReceiptCount = 0;
            if (SFsalesResults.Count > 0)
            {
                foreach (Logs_SelectResult invoiceSelectResult in SFsalesResults)
                {

                    GlobalValue.Reponse res = new GlobalValue.Reponse();
                    res.IsError = false;
                    try
                    {
                        if (invoiceSelectResult.CustomerID == null)
                        {
                            string custID = "";
                            IList<CustomerRet> customerRets = qBXCustomer.SearchCustomer(invoiceSelectResult.Customer);
                            if (customerRets?.Count > 0)
                                custID = customerRets[0].ListID;
                            else
                                custID = Customer.insertCustomerToQB(invoiceSelectResult);
                            if (custID.Contains("Error"))
                            {
                                res.IsError = true;
                                res.Message = "- SF Sales Receipt for customer " + invoiceSelectResult.Customer + ", Log ID " +
                                 invoiceSelectResult.LogPaymentID + ", form number " +
                                 invoiceSelectResult.FormNumber + ", with amount $" +
                                 invoiceSelectResult.Amount + ", was not added to QuickBooks because: " + custID;
                            }
                            else
                                invoiceSelectResult.CustomerID = custID;
                        }

                        if (!res.IsError)
                            res = SFSalesReceipt.SaveSalesReceipt(invoiceSelectResult, salesReceipt);

                        if (!res.IsError)
                        {

                            SFinsertSalesReceiptCount++;
                        }
                        else
                        {
                            responses.Message += $" Error in SF Sales Receipt sync - " + res.Message.ToString() + "\n";
                            errMsg += $" Error in SF Sales Receipt sync - " + res.Message.ToString() + Environment.NewLine;
                        }
                        if (payID.Contains(invoiceSelectResult.LogPaymentID) == false)
                        {
                            payID.Add(invoiceSelectResult.LogPaymentID);
                        }
                    }
                    catch (Exception ex)
                    {
                        responses.Message += $" Error in SF Sales Receipt sync - " + ex.Message.ToString() + "\n";
                        errMsg += $" Error in SF Sales Receipt sync - " + ex.Message.ToString() + Environment.NewLine;
                        GlobalValue.Eventlogupdate(ex, null, "Error");
                    }
                    LoopCount++;

                    onProgressChanged(new ProgressEventArgs
                    {
                        Total = TotalCount,
                        Current = LoopCount
                    });
                }
            }
            if (SFinsertSalesReceiptCount > 0)
                responses.Message += $"{SFinsertSalesReceiptCount} SF Sales Receipt(s) added to quickbooks\n";
            foreach (int payId in payID)
            {
                try
                {
                    DataContext.Log_UpdateStatus(payId);
                }

                catch (Exception ex)
                {
                    responses.Message += $" Error in Update Log Status - " + ex.Message.ToString() + "\n";
                    errMsg += $" Error in  Update Log Status - " + ex.Message.ToString() + Environment.NewLine;
                    GlobalValue.Eventlogupdate(ex, null, "Error");
                }

            }

            onProgressChanged(new ProgressEventArgs
            {
                Completed = true
            });

            if (errMsg.Length>1)
            {
                responses.Message += "Error: " + errMsg + Environment.NewLine;
                MailSender.Send(Settings.Default.MAILTO, Credentials.Get.CompanyId + " : Error in Logs Sync " + Credentials.SyncDateTime.ToString("MM-dd-yyyy HH:mm"), errMsg);
            }

            return responses;
        }

    }
}
