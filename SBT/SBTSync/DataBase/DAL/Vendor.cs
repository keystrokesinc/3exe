﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using DataProcessor;
using DataProcessor.QB.XMLProcessor;
using DataProcessor.Sync;
using DataProcessor.Sync.ModelAdd;
using DataProcessor.Sync.ModelRet;
using SBTSync.Properties;
using DataProcessor.Ref;

namespace SBTSync.DataBase.DAL
{
    public class Vendor : DBBase
    {
        private static string errMsg;

        public static GlobalValue.Reponse ProcessVendor(Action<ProgressEventArgs> onProgressChanged)
        {
            errMsg = "";
            GlobalValue.Reponse addUpdateOnDataBase = AddUpdateOnDataBase(onProgressChanged);

            GlobalValue.Reponse addNewVendor = AddNewVendor();

            GlobalValue.Reponse deletenewVendor = DeleteVendor();

            //File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + "\\currentTime.txt", DateTime.Now.ToString());
            if (!string.IsNullOrEmpty(errMsg))
            {
                MailSender.Send(Settings.Default.MAILTO, Credentials.Get.CompanyId + ": Error in Vendor Sync " + Credentials.SyncDateTime.ToString("MM-dd-yyyy HH:mm"), errMsg);
            }
            return new GlobalValue.Reponse
            {
                Message = addUpdateOnDataBase.Message + "\n" + addNewVendor.Message + "\n" + deletenewVendor.Message

            };

        }

        public static GlobalValue.Reponse AddVendor()
        {

            GlobalValue.Reponse addNewVendor = AddNewVendor();

            return new GlobalValue.Reponse
            {
                Message = addNewVendor.Message + "\n",

            };

        }
        private static GlobalValue.Reponse AddUpdateOnDataBase(Action<ProgressEventArgs> onProgressChanged)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            QBXVendor vendor = new QBXVendor();

            DateTime? qbAccountLastMod = GetLastModified("Vendor");


            List<VendorRet> vendorRets = vendor.GetAllVendor(null);
            // to find a single vendor using vendor list ID
            //  List<VendorRet> vendorRets = vendor.GetVendorQuery("80000385-14138848621");

            int insertVendorCount = 0;
            int updateVendorCount = 0;

            //Disable after test
            //vendorRets = vendorRets.Where(p => p.Name == "NewVendor28Jan").ToList();
            bool InsertRecord = false;
            List<Vendor_SelectResult> selectResults;
            foreach (VendorRet vendorRet in vendorRets)
            {
                try
                {
                    ISingleResult<Vendor_SelectResult> vendorSelectResults = DataContext.Vendor_Select(vendorRet.ListID, false, Credentials.Get.CompanyId);
                    selectResults = new List<Vendor_SelectResult>(vendorSelectResults);

                    string addr1 = ""; string addr2 = ""; string addr3 = ""; string addr4 = ""; string addr5 = "";
                    if (vendorRet.VendorAddress != null)
                    {
                        addr1 = vendorRet.VendorAddress.Addr1;

                        addr2 = vendorRet.VendorAddress.Addr2;

                        addr3 = vendorRet.VendorAddress.Addr3;

                        addr4 = vendorRet.VendorAddress.Addr4;

                        addr5 = vendorRet.VendorAddress.City + "," + vendorRet.VendorAddress.State + "," + vendorRet.VendorAddress.PostalCode;
                    }
                    if (selectResults.Count == 0)
                    {
                        InsertRecord = true;
                        DataContext.Vendor_Insert(Credentials.Get.CompanyId, vendorRet.ListID, vendorRet.TimeCreated, vendorRet.TimeModified, vendorRet.Name, addr1,
                            addr2, addr3, addr4, addr5, null, 0, null, vendorRet.IsActive, string.Empty);
                        GlobalValue.Eventlogupdate("Vendor  " + vendorRet.Name + " added to SBT", null);
                        insertVendorCount++;
                    }
                    else
                    {
                        InsertRecord = false;
                        DataContext.Vendor_Update(Credentials.Get.CompanyId, vendorRet.ListID, vendorRet.TimeCreated, vendorRet.TimeModified, vendorRet.Name, addr1,
                            addr2, addr3, addr4, addr5, false, null, 0, null, vendorRet.IsActive, string.Empty, selectResults[0].VendorID);
                        GlobalValue.Eventlogupdate("Vendor  " + vendorRet.Name + " Updated on SBT", null);
                        updateVendorCount++;
                    }

                    onProgressChanged(new ProgressEventArgs
                    {
                        Total = vendorRets.Count,
                        Current = updateVendorCount + insertVendorCount
                    });
                }
                catch (Exception ex)
                {
                    GlobalValue.Eventlogupdate(ex.Message + " : Record " + InsertRecord.ToString() + " " + Credentials.Get.CompanyId + ", " + vendorRet.ListID + ", " + vendorRet.TimeCreated + ", " + vendorRet.TimeModified + ", " + vendorRet.Name + ",'','','','','',false, null, 0, null," + vendorRet.IsActive + ",'', ListID: " + vendorRet.ListID + ")", null, "Error");
                    errMsg += ex.Message + Environment.NewLine;
                }
            }

            onProgressChanged(new ProgressEventArgs
            {
                Completed = true
            });
            if (errMsg.Length > 1)
                responses.Message += errMsg + ".\n";
            if (insertVendorCount > 0)
            {
                responses.Message += insertVendorCount + " Vendor(s) added to database.\n";
            }

            if (updateVendorCount > 0)
            {
                responses.Message += updateVendorCount + " \nVendor(s) updated in database.\n";
            }

            return responses;
        }

        private static GlobalValue.Reponse AddNewVendor()
        {
            ISingleResult<Vendor_Select_AddQuickbookResult> vendorSelectAddQuickbookResults = DataContext.Vendor_Select_AddQuickbook(Credentials.Get.CompanyId);
            List<Vendor_Select_AddQuickbookResult> vendorList = new List<Vendor_Select_AddQuickbookResult>(vendorSelectAddQuickbookResults);

            GlobalValue.Eventlogupdate(vendorList.Count + " Vendor(s) found to add to QuickBooks.", null);

            // all active vendors in Quickbooks and compare 
            QBXVendor vendor = new QBXVendor();
            List<VendorRet> vendorRets = vendor.GetAllVendor(null);

            // Finding all the vendors we have in DB that we need to update the listID for.
            var objectsExits = vendorRets.Join(vendorList,
                  c => c.Name,
                  o => o.Name,
                  (c, o) => new
                  {
                      c,
                      VendorId = o.Vendorid

                  }).ToList();


            foreach (var ob in objectsExits)
            {
                DataContext.Vendor_Update(Credentials.Get.CompanyId, ob.c.ListID, ob.c.TimeCreated,
                        ob.c.TimeModified, ob.c.Name, ob.c.VendorAddress?.Addr1,
                        ob.c.VendorAddress?.Addr2, ob.c.VendorAddress?.Addr3, ob.c.VendorAddress?.Addr4,
                        ob.c.VendorAddress?.City + "," + ob.c.VendorAddress?.State + "," +
                        ob.c.VendorAddress?.PostalCode, false, null, 0, null, ob.c.IsActive, string.Empty, ob.VendorId);

            }

            // we will filter from all teh vendors and the insert only the ones that are not in QB
            List<VendorAdd> vendors = vendorList.Where(x => vendorRets.All(svendor => x.Name != svendor.Name)).Select(v => new VendorAdd
            {
                VendorAddress = new BillAddress
                {

                    Addr1 = v.Addr1,
                    Addr2 = v.Addr2,
                    Addr3 = v.Addr3,
                    Addr4 = v.Addr4,
                    Addr5 = v.City
                },
                IsActive = v.IsActive,
                Name = v.Name,
                CompanyName = v.Name,
                Email = v.MainEmail,
                Phone = v.MainPhone,
                AccountNumber = v.AccountNumber,
                VendorTaxIdent = v.TaxID
            }).ToList();

            QBXVendor xvendor = new QBXVendor();

            List<GlobalValue.Reponse> saveVendor = xvendor.SaveVendor(vendors);

            foreach (GlobalValue.Reponse reponse in saveVendor)
            {
                if (!reponse.IsError)
                {
                    List<VendorRet> serializeXml = SyncConstant.SerializeXml<VendorRet>(reponse.EntityObject.ToString(), QueryXML.Vendor, false);
                    if (serializeXml.Count > 0)
                        DataContext.Vendor_UpdateListID(serializeXml[0].ListID, serializeXml[0].Name, Credentials.Get.CompanyId);
                }
                else
                {
                    errMsg += reponse.Message + Environment.NewLine;
                }
            }

            return new GlobalValue.Reponse
            {
                Message = saveVendor.Count + " Vendor(s) added in QuickBooks",
            };
        }


        public static GlobalValue.Reponse DeleteVendor()
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            //GlobalValue.Reponse addNewVendor = AddNewVendor();
            int deleteCount = 0;
            try
            {

                GlobalValue.Eventlogupdate("DELETE OPERATION STARTED FOR VENDORS", null);
                List<VendorRet> vendorRets = (new QBXVendor()).GetAllVendor();

                ISingleResult<Vendor_SelectResult> vendorSelectResults = DataContext.Vendor_Select(null, false, Credentials.Get.CompanyId);
                List<Vendor_SelectResult> dbVendor = new List<Vendor_SelectResult>(vendorSelectResults);


                foreach (Vendor_SelectResult VendorSelectResult in dbVendor)
                {
                    VendorRet vendorR = vendorRets.FirstOrDefault(v => v.ListID == VendorSelectResult.ListID);
                    if (vendorR == null)
                    {
                        GlobalValue.Eventlogupdate(" " + VendorSelectResult.Name + " set to inactive status ", null);
                        DataContext.Vendor_Update(Credentials.Get.CompanyId, VendorSelectResult.ListID, VendorSelectResult.TimeCreated, VendorSelectResult.TimeModified, VendorSelectResult.Name, null, null, null, null, null, true, null, null, null, false, null, VendorSelectResult.VendorID);
                        deleteCount++;
                    }
                }


                GlobalValue.Eventlogupdate("Clean operation ended for vendor(s). " + deleteCount + " record(s) set to inactive status ", null);
            }
            catch (Exception ex)
            {
                errMsg += ex.Message + Environment.NewLine;
                GlobalValue.Eventlogupdate("Vendor clean: " + ex.Message, null, "Error");
            }

            responses.Message = deleteCount.ToString() + " Vendor(s) set  to inactive status .";
            return responses;
        }
    }
}
