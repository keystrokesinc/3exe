﻿using System;
using System.Collections.Generic;
using DataProcessor;
using DataProcessor.QB.XMLProcessor;
using DataProcessor.Sync.ModelRet;
using System.Data.Linq;
using System.Data.SqlClient;
using DataProcessor.Sync.ModelAdd;
using DataProcessor.Sync;
using SBTSync.Properties;

namespace SBTSync.DataBase.DAL
{
    public class Invoice : DBBase
    {
      
        public static int insertInvoiceCount = 0;
        public static GlobalValue.Reponse ProcessInvoice(Action<ProgressEventArgs> onProgressChanged)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            QBXInvoice invoice = new QBXInvoice();
            DateTime? qbInvoiceLastMod = GetLastModified("Invoice");
            GlobalValue.Eventlogupdate("Getting Invoices", null);
            ISingleResult<Logs_SelectResult> invoiceSelectResults = DataContext.Invoice_Select_AddQuickBooks(Credentials.Get.CompanyId);
            List<Logs_SelectResult> selectResults = new List<Logs_SelectResult>(invoiceSelectResults);
            insertInvoiceCount = 0;
            if (selectResults.Count > 0)
            {
                foreach (Logs_SelectResult invoiceSelectResult in selectResults)
                {
                    GlobalValue.Reponse res = new GlobalValue.Reponse();
                    try
                    {
                        if (invoiceSelectResult.CustomerID == null)
                            invoiceSelectResult.CustomerID = Customer.insertCustomerToQB(invoiceSelectResult); //error  need to handle

                        res = SaveInvoice(invoiceSelectResult, invoice);
                    }
                    catch (Exception ex)
                    {
                        GlobalValue.Eventlogupdate(ex, null, "Error");
                    }
                }
            }
            onProgressChanged(new ProgressEventArgs
            {
                Completed = true
            });
            if (insertInvoiceCount > 0)           
                responses.Message = $"{insertInvoiceCount} Invoice(s) added to quickbooks\n";

            responses.Message += SalesReceipt.ProcessSalesReceipt(onProgressChanged).Message;
            responses.Message += Payment.ProcessPayment(onProgressChanged).Message;
            return responses;
        }
      public static GlobalValue.Reponse SaveInvoice(Logs_SelectResult invoiceSelectAddQuickBooksResult, QBXInvoice invoice)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();            
            try
            {
                var classRef = new DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRetClassRef();//new DataProcessor.Sync.ModelAdd.InvoiceRetClassRef();
                if (!string.IsNullOrEmpty(invoiceSelectAddQuickBooksResult.ClassID)) {
                    classRef.ListID = invoiceSelectAddQuickBooksResult.ClassID;
                    classRef.FullName = invoiceSelectAddQuickBooksResult.ClassName;
                }
                else                
                    classRef = null;
                var templateRef = new DataProcessor.Sync.ModelAdd.InvoiceRetTemplateRef();
                if (!string.IsNullOrEmpty(invoiceSelectAddQuickBooksResult.TemplateListID))
                {
                    templateRef.ListID = invoiceSelectAddQuickBooksResult.TemplateListID;
                    templateRef.FullName = invoiceSelectAddQuickBooksResult.TemplateName;
                }
                else
                    templateRef = null;

                InvoiceAdd invoiceaddRq = new InvoiceAdd
                {
                    CustomerRef = new DataProcessor.Sync.ModelAdd.InvoiceRetCustomerRef
                    {
                        ListID = invoiceSelectAddQuickBooksResult.CustomerID,
                        FullName = invoiceSelectAddQuickBooksResult.Customer
                    },
                    TemplateRef = templateRef,          
                   
            
                };
                List<DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRet> lstLine = new List<DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRet>();
                DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRet line = new DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRet
                {
                    ItemRef = new DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRetItemRef
                    {
                        ListID = invoiceSelectAddQuickBooksResult.ItemID,
                        FullName = invoiceSelectAddQuickBooksResult.Item
                    },
                    Amount = string.Format("{0:0.00}", invoiceSelectAddQuickBooksResult.Amount),
                    Desc = invoiceSelectAddQuickBooksResult.ItemDescription,
                    ClassRef = classRef,

                };
                lstLine.Add(line);
                invoiceaddRq.InvoiceLineAdd = lstLine.ToArray();
                //if (!string.IsNullOrEmpty(invoiceSelectAddQuickBooksResult.Memo))
                //    invoiceaddRq.Memo = invoiceSelectAddQuickBooksResult.Memo + " " + invoiceSelectAddQuickBooksResult.PaymentID;
                if (!string.IsNullOrEmpty(invoiceSelectAddQuickBooksResult.FormNumber))
                    invoiceaddRq.RefNumber = invoiceSelectAddQuickBooksResult.FormNumber;
                if (!string.IsNullOrEmpty(invoiceSelectAddQuickBooksResult.MoneyVoucherDate))
                    invoiceaddRq.TxnDate = invoiceSelectAddQuickBooksResult.MoneyVoucherDate;
                if (invoiceSelectAddQuickBooksResult.LogPaymentID > 0)
                    invoiceaddRq.Memo = invoiceSelectAddQuickBooksResult.LogPaymentID.ToString();

                GlobalValue.Eventlogupdate("Sending Inv " + invoiceaddRq.RefNumber + " amt " + line.Amount.ToString(),null,"Info");

              List <GlobalValue.Reponse> saveInvoice = invoice.SaveInvoice(invoiceaddRq);

                
                if (saveInvoice.Count > 0 && !saveInvoice[0].IsError)
                {
                    insertInvoiceCount++;
                    var checkSerializeXml = SyncConstant.SerializeXml<InvoiceRet>(saveInvoice[0].EntityObject.ToString(), QueryXML.Invoice, false);
                    if (checkSerializeXml.Count > 0)
                    {
                        DataContext.UpdateLogPayment_afterSync(invoiceSelectAddQuickBooksResult.ID, Convert.ToInt32(checkSerializeXml[0].TxnNumber));
                        GlobalValue.Eventlogupdate("Invoice having TxnID " + checkSerializeXml[0].TxnNumber + " inserted to database", null);
                        responses.IsError = false;
                        responses.Message =  checkSerializeXml[0].TxnID;                       
                    }                    
                }


                if (saveInvoice[0].IsError)
                {
                    responses.IsError = true;
                    string resMsg = "Error!- Invoice for customer " + invoiceSelectAddQuickBooksResult.Customer +", Log ID "+
                                 invoiceSelectAddQuickBooksResult.LogPaymentID  + ", form number " +
                                 invoiceSelectAddQuickBooksResult.FormNumber + ", with amount $" +
                                 invoiceSelectAddQuickBooksResult.Amount + ", was not added to QuickBooks because: ";
                    responses.Message +=resMsg+saveInvoice[0].Message + "\n";
                    GlobalValue.Eventlogupdate(responses.Message, null, "Error");
                    //MailSender.Send(Settings.Default.MAILTO, Credentials.Get.CompanyId + ": Error in Invoice Sync " + Credentials.SyncDateTime.ToString("MM-dd-yyyy HH:mm"), responses.Message);
                    return responses;

                }
            }
            catch (Exception ex)
            {
                responses.IsError = true;
                string resMsg = "Error!- Invoice for customer " + invoiceSelectAddQuickBooksResult.Customer + ", Log ID " +
                               invoiceSelectAddQuickBooksResult.LogPaymentID + ", form number " +
                               invoiceSelectAddQuickBooksResult.FormNumber + ", with amount $" +
                               invoiceSelectAddQuickBooksResult.Amount + ", was not added to QuickBooks because: ";
                responses.Message += resMsg + ex.Message + "\n";
                GlobalValue.Eventlogupdate(responses.Message, null, "Error");
            }            
            return responses;
        }

        public static GlobalValue.Reponse SaveCombInvoice(List<Logs_SelectResult> invList, QBXInvoice invoice)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            var invItem = invList[0];
            try
            {
                var templateRef = new DataProcessor.Sync.ModelAdd.InvoiceRetTemplateRef();
                if (!string.IsNullOrEmpty(invItem.TemplateListID))
                {
                    templateRef.ListID = invItem.TemplateListID;
                    templateRef.FullName = invItem.TemplateName;
                }
                else
                    templateRef = null;

                InvoiceAdd invoiceaddRq = new InvoiceAdd
                {
                    CustomerRef = new DataProcessor.Sync.ModelAdd.InvoiceRetCustomerRef
                    {
                        ListID = invItem.CustomerID,
                        FullName = invItem.Customer
                    },
                    TemplateRef = templateRef,
                };
                List<DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRet> lstLine = new List<DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRet>();
                foreach (var invIns in invList)
                {
                    DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRet line = new DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRet
                    {
                        ItemRef = new DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRetItemRef
                        {
                            ListID = invIns.ItemID,
                            FullName = invIns.Item
                        },
                        Amount = string.Format("{0:0.00}", invIns.Amount),
                        Desc = invIns.ItemDescription,
                        ClassRef = new DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRetClassRef
                        {
                            ListID = invIns.ClassID,
                            FullName = invIns.ClassName
                        },

                };
                    lstLine.Add(line);
                }
                invoiceaddRq.InvoiceLineAdd = lstLine.ToArray();
                //if (!string.IsNullOrEmpty(invItem.Memo))
                //    invoiceaddRq.Memo = invItem.Memo + " " + invItem.PaymentID;
                if (!string.IsNullOrEmpty(invItem.FormNumber))
                    invoiceaddRq.RefNumber = invItem.FormNumber;
                if (!string.IsNullOrEmpty(invItem.MoneyVoucherDate))
                    invoiceaddRq.TxnDate = invItem.MoneyVoucherDate;
                if (invItem.LogPaymentID > 0)
                    invoiceaddRq.Memo = invItem.LogPaymentID.ToString();

                List<GlobalValue.Reponse> saveInvoice = invoice.SaveInvoice(invoiceaddRq);
                insertInvoiceCount++;
                if (saveInvoice.Count > 0 && !saveInvoice[0].IsError)
                {
                    var checkSerializeXml = SyncConstant.SerializeXml<InvoiceRet>(saveInvoice[0].EntityObject.ToString(), QueryXML.Invoice, false);
                    if (checkSerializeXml.Count > 0)
                    {
                        DataContext.updateCombLog_AfterSync(invItem.CombinedID, Convert.ToInt32(checkSerializeXml[0].TxnNumber));
                        GlobalValue.Eventlogupdate("Invoice having TxnID " + checkSerializeXml[0].TxnNumber + " inserted to database", null);
                        responses.IsError = false;
                        responses.Message = checkSerializeXml[0].TxnID;
                    }
                }


                if (saveInvoice[0].IsError)
                {
                    responses.IsError = true;
                    string resMsg = "Error!- Invoice for customer " + invItem.Customer + ", Log ID " +
                               invItem.LogPaymentID + ",\n form number " +
                               invItem.FormNumber + ", with amount $" +
                               invItem.Amount + ", was not added to QuickBooks because: ";
                    responses.Message += resMsg + saveInvoice[0].Message;
                    GlobalValue.Eventlogupdate(responses.Message, null, "Error");
                //    MailSender.Send(Settings.Default.MAILTO, Credentials.Get.CompanyId + ": Error in Combined Invoice Sync " + Credentials.SyncDateTime.ToString("MM-dd-yyyy HH:mm"), responses.Message);
                    return responses;

                }
            }
            catch (Exception ex)
            {
                responses.IsError = true;
                string resMsg = "Error!- Invoice for customer " + invItem.Customer + ", Log ID " +
                               invItem.LogPaymentID + ",\n form number " +
                               invItem.FormNumber + ", with amount $" +
                               invItem.Amount + ", was not added to QuickBooks because: ";
                responses.Message += resMsg + ex.Message;
                GlobalValue.Eventlogupdate(responses.Message, null, "Error");
                responses.Message = ex.Message;
            }
            return responses;
        }


    }
}
