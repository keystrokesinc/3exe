﻿using System;
using System.Collections.Generic;
using DataProcessor;
using DataProcessor.QB.XMLProcessor;
using DataProcessor.Sync.ModelRet;
using System.Data.Linq;
using System.Data.SqlClient;
using DataProcessor.Sync.ModelAdd;
using DataProcessor.Sync;
using SBTSync.Properties;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using Newtonsoft.Json;
using System.Data;

namespace SBTSync.DataBase.DAL
{
    public class SFInvoice : DBBase
    {
      
        public static int insertInvoiceCount = 0;
        public static GlobalValue.Reponse ProcessSFInvoice(Action<ProgressEventArgs> onProgressChanged)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            QBXInvoice invoice = new QBXInvoice();
            DateTime? qbInvoiceLastMod = GetLastModified("Invoice");
            GlobalValue.Eventlogupdate("Getting Invoices", null);
            ISingleResult<Logs_SelectResult> invoiceSelectResults = DataContext.SFInvoice_Select_AddQuickBooks(Credentials.Get.CompanyId);
            List<Logs_SelectResult> selectResults = new List<Logs_SelectResult>(invoiceSelectResults);
            insertInvoiceCount = 0;
            string batchName = Settings.Default.BatchName;
            if (selectResults.Count > 0)
            {
                foreach (Logs_SelectResult invoiceSelectResult in selectResults)
                {
                    GlobalValue.Reponse res = new GlobalValue.Reponse();
                    try
                    {
                        if (batchName != null)
                        {
                            if (batchName == invoiceSelectResult.Memo)
                            {
                                if (invoiceSelectResult.CustomerID == null)
                                {
                                    QBXCustomer qBXCustomer = new QBXCustomer();
                                    IList<CustomerRet> customerRets = qBXCustomer.SearchCustomer(invoiceSelectResult.Customer);
                                    if (customerRets?.Count > 0)
                                        invoiceSelectResult.CustomerID = customerRets[0].ListID;
                                    else
                                        invoiceSelectResult.CustomerID = Customer.insertCustomerToQB(invoiceSelectResult); //error need to handle
                                }

                                res = SaveInvoice(invoiceSelectResult, invoice);
                            }
                        }
                        else
                        {
                            if (invoiceSelectResult.CustomerID == null)
                            {
                                QBXCustomer qBXCustomer = new QBXCustomer();
                                IList<CustomerRet> customerRets = qBXCustomer.SearchCustomer(invoiceSelectResult.Customer);
                                if (customerRets?.Count > 0)
                                    invoiceSelectResult.CustomerID = customerRets[0].ListID;
                                else
                                    invoiceSelectResult.CustomerID = Customer.insertCustomerToQB(invoiceSelectResult); //error need to handle
                            }

                            res = SaveInvoice(invoiceSelectResult, invoice);
                        }                        
                    }
                    catch (Exception ex)
                    {
                        GlobalValue.Eventlogupdate(ex, null, "Error");
                    }
                }
            }
            onProgressChanged(new ProgressEventArgs
            {
                Completed = true
            });
            if (insertInvoiceCount > 0)           
                responses.Message = $"{insertInvoiceCount} Invoice(s) added to quickbooks\n";

            responses.Message += SalesReceipt.ProcessSalesReceipt(onProgressChanged).Message;
            responses.Message += Payment.ProcessPayment(onProgressChanged).Message;
            return responses;
        }
      public static GlobalValue.Reponse SaveInvoice(Logs_SelectResult invoiceSelectAddQuickBooksResult, QBXInvoice invoice)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();            
            try
            {
                var classRef = new DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRetClassRef();
                if (!string.IsNullOrEmpty(invoiceSelectAddQuickBooksResult.ClassName)) {
                   // classRef.ListID = invoiceSelectAddQuickBooksResult.ClassID;
                    classRef.FullName = invoiceSelectAddQuickBooksResult.ClassName;
                }
                else                
                    classRef = null;
                var templateRef = new DataProcessor.Sync.ModelAdd.InvoiceRetTemplateRef();
                if (!string.IsNullOrEmpty(invoiceSelectAddQuickBooksResult.TemplateListID))
                {
                    templateRef.ListID = invoiceSelectAddQuickBooksResult.TemplateListID;
                    templateRef.FullName = invoiceSelectAddQuickBooksResult.TemplateName;
                }
                else
                    templateRef = null;

                InvoiceAdd invoiceaddRq = new InvoiceAdd
                {
                    CustomerRef = new DataProcessor.Sync.ModelAdd.InvoiceRetCustomerRef
                    {
                        ListID = invoiceSelectAddQuickBooksResult.CustomerID,
                        FullName = invoiceSelectAddQuickBooksResult.Customer
                    },
                    TemplateRef = templateRef,          
                   
            
                };
                List<DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRet> lstLine = new List<DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRet>();
                DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRet line = new DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRet
                {
                    ItemRef = new DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRetItemRef
                    {                       
                        FullName = invoiceSelectAddQuickBooksResult.Item
                    },
                    Amount = string.Format("{0:0.00}", invoiceSelectAddQuickBooksResult.Amount),
                    Desc = invoiceSelectAddQuickBooksResult.ItemDescription,
                    ClassRef = classRef,

                };
                lstLine.Add(line);
                invoiceaddRq.InvoiceLineAdd = lstLine.ToArray();                
                invoiceaddRq.RefNumber = invoiceSelectAddQuickBooksResult.ID.ToString();
                string txnDate = invoiceSelectAddQuickBooksResult.TxnDate.ToString();
                invoiceaddRq.TxnDate = invoiceSelectAddQuickBooksResult.DueDate.ToString() != null ? invoiceSelectAddQuickBooksResult.DueDate.ToString("yyyy-MM-dd") : Convert.ToDateTime(txnDate).ToString("yyyy-MM-dd");                
                invoiceaddRq.Memo = invoiceSelectAddQuickBooksResult.SFReferance.ToString();
                GlobalValue.Eventlogupdate("Sending Inv " + invoiceaddRq.RefNumber + " amt " + line.Amount.ToString(),null,"Info");
                List <GlobalValue.Reponse> saveInvoice = invoice.SaveInvoice(invoiceaddRq);                
                if (saveInvoice.Count > 0 && !saveInvoice[0].IsError)
                {
                    insertInvoiceCount++;
                    var checkSerializeXml = SyncConstant.SerializeXml<InvoiceRet>(saveInvoice[0].EntityObject.ToString(), QueryXML.Invoice, false);
                    if (checkSerializeXml.Count > 0)
                    {
                        DataContext.UpdateLogPayment_afterSync(invoiceSelectAddQuickBooksResult.ID, Convert.ToInt32(checkSerializeXml[0].TxnNumber));
                        DataContext.Log_UpdateStatus(invoiceSelectAddQuickBooksResult.LogPaymentID);
                        //QBID updating to Salesforce
                        UpdateSalesforceData(invoiceSelectAddQuickBooksResult.SFReferance, checkSerializeXml[0].TxnID, "");
                        GlobalValue.Eventlogupdate("Invoice having TxnID " + checkSerializeXml[0].TxnNumber + " inserted to database", null);
                        responses.IsError = false;
                        responses.Message +=  checkSerializeXml[0].TxnID;                       
                    }                    
                }
                if (saveInvoice[0].IsError)
                {
                    //QB Error message updating to Salesforce
                   UpdateSalesforceData(invoiceSelectAddQuickBooksResult.SFReferance,"",saveInvoice[0].Message);
                    responses.IsError = true;
                    string resMsg = "Error!- Invoice for customer " + invoiceSelectAddQuickBooksResult.Customer +", Log ID "+
                                 invoiceSelectAddQuickBooksResult.LogPaymentID  + ", form number " +
                                 invoiceSelectAddQuickBooksResult.FormNumber + ", with amount $" +
                                 invoiceSelectAddQuickBooksResult.Amount + ", was not added to QuickBooks because: ";
                    responses.Message +=resMsg+saveInvoice[0].Message + "\n";
                    GlobalValue.Eventlogupdate(responses.Message, null, "Error");
                    MailSender.Send(Settings.Default.MAILTO, Credentials.Get.CompanyId + ": Error in Invoice Sync " + Credentials.SyncDateTime.ToString("MM-dd-yyyy HH:mm"), responses.Message);
                    return responses;
                }
            }
            catch (Exception ex)
            {
                responses.IsError = true;
                string resMsg = "Error!- Invoice for customer " + invoiceSelectAddQuickBooksResult.Customer + ", Log ID " +
                               invoiceSelectAddQuickBooksResult.LogPaymentID + ", form number " +
                               invoiceSelectAddQuickBooksResult.FormNumber + ", with amount $" +
                               invoiceSelectAddQuickBooksResult.Amount + ", was not added to QuickBooks because: ";
                responses.Message += resMsg + ex.Message + "\n";
                MailSender.Send(Settings.Default.MAILTO, Credentials.Get.CompanyId + ": Error in Invoice Sync " + Credentials.SyncDateTime.ToString("MM-dd-yyyy HH:mm"), responses.Message);
                GlobalValue.Eventlogupdate(responses.Message, null, "Error");
            }            
            return responses;
        }
        class ApiSendData
        {
            public string grant_type { get; set; }
            public string client_id { get; set; }
            public string client_secret { get; set; }
            public string username { get; set; }
            public string password { get; set; }
        }
        public class LoginResponse
        {
            public string access_token { get; set; }
            public string instance_url { get; set; }
            public string id { get; set; }
            public string token_type { get; set; }
            public string issued_at { get; set; }
            public string signature { get; set; }
        }
        class UpdateSFClass
        {
            public string sfId { get; set; }
            public string qbMemoid { get; set; }
            public string qbError { get; set; }
        }
        private static void UpdateSalesforceData(string SFReferance, string QBID, string QBErrorMsg)
        {
            LoginResponse res = null;
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("https://login.salesforce.com/services/oauth2/token?");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //**LIVE SALES FORCE LOGIN**//
                    var datatobeSent = new ApiSendData()
                    {
                        grant_type = "password",
                        client_id = "3MVG9oNqAtcJCF.EGnjKYNbXn57OdNsZyw8kuhVd0x__LMZvRMaHMIDxWGSqS.FzMFNs2_tVu6N74OZvwB3j4",
                        client_secret = "F106A47159B0AD459C7378BB43D21D2AA2BE92155B3C5B1DD1C0A571DD0AE1C3",
                        username = "dharris@parkinsonvoiceproject.org",
                        password = "Park@@321rT6YfmG6YlY3liA6P12EQtClJ"
                    };

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    var response = client.PostAsJsonAsync("https://login.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9oNqAtcJCF.EGnjKYNbXn57OdNsZyw8kuhVd0x__LMZvRMaHMIDxWGSqS.FzMFNs2_tVu6N74OZvwB3j4&client_secret=F106A47159B0AD459C7378BB43D21D2AA2BE92155B3C5B1DD1C0A571DD0AE1C3&username=dharris@parkinsonvoiceproject.org&password=Park@@321rT6YfmG6YlY3liA6P12EQtClJ", datatobeSent).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        res = response.Content.ReadAsAsync<LoginResponse>().Result;
                    }
                }

                //Update Salesforce
                using (var client = new HttpClient())
                {

                    client.BaseAddress = new Uri("https://pvp.my.salesforce.com/services/apexrest/updatememoid");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(res.token_type, res.access_token);
                    var datatobeSent = new UpdateSFClass()
                    {
                        sfId = SFReferance,
                        qbMemoid = QBID,
                        qbError = QBErrorMsg
                    };
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;                    
                    var response = client.PostAsJsonAsync("https://pvp.my.salesforce.com/services/apexrest/updatememoid", datatobeSent).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        
                    }
                }
                }
            catch(Exception ex)
            {

            }
        }

        //**PULL Invoice from SF
        public static GlobalValue.Reponse PullSFInvoice(Action<ProgressEventArgs> onProgressChanged)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            string connetionString = Credentials.Get.ConnectionString;
            SqlConnection cnn = new SqlConnection(connetionString);
            try
            {
                LoginResponse res = null;
                cnn.Open();
                //string loginURL = "https://pvp--qbtesting.my.salesforce.com/services/oauth2/token?";
                //string loginFullURL = "https://pvp--qbtesting.my.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9Nc1qcZ7BbZ0a4Q2fhHoqlj9I2wvpEGk0two3SD9maCoL.iFSnSEUBEwTrPvsRaRdZYNz0FGeFLudcZWL&client_secret=42BC9E36CDD596425B7177AEFEDD0668BCCCDE01C91A2319B1D57E97FCDC92A9&username=dharris@parkinsonvoiceproject.org.qbtesting&password=@!~Park123";
                //string instanceURL = "https://pvp--qbtesting.my.salesforce.com/services/apexrest/";

                //**LIVE URL**//
                string loginURL = "https://login.salesforce.com/services/oauth2/token?";
                string loginFullURL = "https://login.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9oNqAtcJCF.EGnjKYNbXn57OdNsZyw8kuhVd0x__LMZvRMaHMIDxWGSqS.FzMFNs2_tVu6N74OZvwB3j4&client_secret=F106A47159B0AD459C7378BB43D21D2AA2BE92155B3C5B1DD1C0A571DD0AE1C3&username=dharris@parkinsonvoiceproject.org&password=Park@@321rT6YfmG6YlY3liA6P12EQtClJ";
                string instanceURL = "https://pvp.my.salesforce.com/services/apexrest/";
                string startDate = Settings.Default.StartDate != null ? Settings.Default.StartDate.ToString() : DateTime.Now.ToString("dd-MM-yyyy");
                string endDate = Settings.Default.EndDate != null ? Settings.Default.EndDate.ToString() : DateTime.Now.ToString("dd-MM-yyyy");
                string batchName = Settings.Default.BatchName;
                responses.Message += "Initializing connection....\n";

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(loginURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    // HTTP POST Sales Force Login
                    var datatobeSent = new ApiSendData()
                    {
                        //grant_type = "password",
                        //client_id = "3MVG9Nc1qcZ7BbZ0a4Q2fhHoqlj9I2wvpEGk0two3SD9maCoL.iFSnSEUBEwTrPvsRaRdZYNz0FGeFLudcZWL",
                        //client_secret = "42BC9E36CDD596425B7177AEFEDD0668BCCCDE01C91A2319B1D57E97FCDC92A9",
                        //username = "dharris@parkinsonvoiceproject.org.qbtesting",
                        //password = "@!~Park123"

                        //**FOR LIVE **//
                        grant_type = "password",
                        client_id = "3MVG9oNqAtcJCF.EGnjKYNbXn57OdNsZyw8kuhVd0x__LMZvRMaHMIDxWGSqS.FzMFNs2_tVu6N74OZvwB3j4",
                        client_secret = "F106A47159B0AD459C7378BB43D21D2AA2BE92155B3C5B1DD1C0A571DD0AE1C3",
                        username = "dharris@parkinsonvoiceproject.org",
                        password = "Park@@321rT6YfmG6YlY3liA6P12EQtClJ"
                    };

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    var response = client.PostAsJsonAsync(loginFullURL, datatobeSent).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        res = response.Content.ReadAsAsync<LoginResponse>().Result;
                    }
                    responses.Message += "SF connection established....\n";
                }
                using (var client = new HttpClient())
                {
                    responses.Message += "SF sync started....\n";                   
                    client.BaseAddress = new Uri(instanceURL + "invoices?SearchBy=all&filterBy=cd&fd" + startDate + "&td=" + endDate);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(res.token_type, res.access_token);

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    var response = client.GetAsync(instanceURL + "invoices?SearchBy=all&filterBy=cd&fd" + startDate + "&td=" + endDate).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var jsonString = response.Content.ReadAsStringAsync();
                        var objData = JsonConvert.DeserializeObject<SFInvoiceAdd.RootObject>(jsonString.Result);
                        if (objData.invoices?.Count > 0)
                        {
                            foreach (var transaction in objData.invoices)
                            {
                                if (transaction.ClassRefFullName != null)
                                {
                                    if (transaction.ClassRefFullName.Contains("  "))
                                        transaction.ClassRefFullName = transaction.ClassRefFullName.Replace("  ", " ");
                                    int InvoiceID = 0;
                                    using (SqlCommand cmd = new SqlCommand("SFInvoice_Insert", cnn))
                                    {
                                        try
                                        {
                                            cmd.CommandType = CommandType.StoredProcedure;
                                            cmd.Parameters.Add("CustomerRefListID", SqlDbType.VarChar).Value = transaction.CustomerRefListID;
                                            cmd.Parameters.Add("CustomerRefFullName", SqlDbType.VarChar).Value = transaction.CustomerRefFullName;
                                            cmd.Parameters.Add("ClassRefListID", SqlDbType.VarChar).Value = transaction.ClassRefListID.Contains("\t") ? transaction.ClassRefListID.Replace("\t", " ") : transaction.ClassRefListID;
                                            cmd.Parameters.Add("ClassRefFullName", SqlDbType.VarChar).Value = transaction.ClassRefFullName.Contains("\t") ? transaction.ClassRefFullName.Replace("\t", " ") : transaction.ClassRefFullName;
                                            cmd.Parameters.Add("IitemLIstID", SqlDbType.VarChar).Value = transaction.IitemLIstID;
                                            cmd.Parameters.Add("ItemRefFullName", SqlDbType.VarChar).Value = transaction.ItemRefFullName;
                                            cmd.Parameters.Add("TxnDate", SqlDbType.VarChar).Value = transaction.TxnDate;
                                            cmd.Parameters.Add("RefNumber", SqlDbType.VarChar).Value = transaction.RefNumber;
                                            cmd.Parameters.Add("DueDate", SqlDbType.VarChar).Value = transaction.DueDate;
                                            cmd.Parameters.Add("Amount", SqlDbType.VarChar).Value = transaction.Amount;
                                            cmd.Parameters.Add("PaidInstallments", SqlDbType.VarChar).Value = transaction.PaidInstallments;
                                            cmd.Parameters.Add("PaidAmount", SqlDbType.VarChar).Value = transaction.PaidAmount;
                                            cmd.Parameters.Add("TxnID", SqlDbType.VarChar).Value = transaction.TxnID;
                                            cmd.Parameters.Add("SFReferance", SqlDbType.VarChar).Value = transaction.SFReferance;
                                            cmd.Parameters.Add("CompanyID", SqlDbType.Int).Value = Credentials.Get.CompanyId;
                                            InvoiceID = (int)cmd.ExecuteScalar();
                                            responses.Message += "Invoice for customer " + transaction.CustomerRefFullName + ", SFReferance " + transaction.SFReferance + ", with amount $" + transaction.Amount + " inserted into DB.\n";                                            
                                        }
                                        catch(Exception ex)
                                        {
                                            responses.Message += "Invoice for customer " + transaction.CustomerRefFullName + ", SFReferance " + transaction.SFReferance + ", with amount $" + transaction.Amount + " was not added to DB because: "+ ex.Message.ToString() + "\n"; 
                                            MailSender.Send(Settings.Default.MAILTO, Credentials.Get.CompanyId + ": Error in SF Invoice Pull " + Credentials.SyncDateTime.ToString("MM-dd-yyyy HH:mm"), responses.Message);
                                        }
                                    }                                    
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                responses.Message += ex.Message.ToString();
                MailSender.Send(Settings.Default.MAILTO, Credentials.Get.CompanyId + ": Error in SF Invoice Pull " + Credentials.SyncDateTime.ToString("MM-dd-yyyy HH:mm"), responses.Message);
            }
            return responses;
        }
    }
}
