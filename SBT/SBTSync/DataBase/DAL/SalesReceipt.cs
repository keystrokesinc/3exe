﻿using System;
using System.Collections.Generic;
using DataProcessor;
using DataProcessor.QB.XMLProcessor;
using DataProcessor.Sync.ModelRet;
using System.Data.Linq;
using System.Data.SqlClient;
using DataProcessor.Sync.ModelAdd;
using DataProcessor.Sync;
using SBTSync.Properties;

namespace SBTSync.DataBase.DAL
{
    public class SalesReceipt : DBBase
    {
      
        public static int insertSalesReceiptCount = 0;
        public static GlobalValue.Reponse ProcessSalesReceipt(Action<ProgressEventArgs> onProgressChanged )
        {

            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            QBXSalesReceipt salesReceipt = new QBXSalesReceipt();

            DateTime? qbInvoiceLastMod = GetLastModified("SalesReceipt");

            GlobalValue.Eventlogupdate("Getting SalesReceipt", null);
            

            ISingleResult<Logs_SelectResult> salesReceiptSelectResults = DataContext.SalesReceipt_Select_AddQuickBooks(Credentials.Get.CompanyId);
            List<Logs_SelectResult> selectResults = new List<Logs_SelectResult>(salesReceiptSelectResults);
            insertSalesReceiptCount = 0;
            if (selectResults.Count > 0)
            {
                foreach (Logs_SelectResult invoiceSelectResult in selectResults)
                {

                    GlobalValue.Reponse res = new GlobalValue.Reponse();
                    try
                    {
                        if (invoiceSelectResult.CustomerID == null)
                        {
                            invoiceSelectResult.CustomerID = Customer.insertCustomerToQB(invoiceSelectResult); //error need to handle
                        }
                     
                        res = SaveSalesReceipt(invoiceSelectResult, salesReceipt);
                    }
                    catch (Exception ex)
                    {
                        GlobalValue.Eventlogupdate(ex, null, "Error");
                    }
                }
            }
            onProgressChanged(new ProgressEventArgs
            {
                Completed = true
            });
            if (insertSalesReceiptCount > 0)
                responses.Message =  $"{insertSalesReceiptCount} Sales Receipt(s) added to quickbooks\n";
            return responses;
        }
       public static GlobalValue.Reponse SaveSalesReceipt(Logs_SelectResult saleSelectAddQuickBooksResult, QBXSalesReceipt salesReceipt)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();            
            try
            {
                var classRef = new SalesReceiptRetSalesReceiptLineRetClassRef();//DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRetClassRef();//new DataProcessor.Sync.ModelAdd.InvoiceRetClassRef();
                if (!string.IsNullOrEmpty(saleSelectAddQuickBooksResult.ClassID))
                {
                    classRef.ListID = saleSelectAddQuickBooksResult.ClassID;
                    classRef.FullName = saleSelectAddQuickBooksResult.ClassName;
                }
                else
                    classRef = null;
                var templateRef = new DataProcessor.Sync.ModelAdd.InvoiceRetTemplateRef();
                if (!string.IsNullOrEmpty(saleSelectAddQuickBooksResult.TemplateListID))
                {
                    templateRef.ListID = saleSelectAddQuickBooksResult.TemplateListID;
                    templateRef.FullName = saleSelectAddQuickBooksResult.TemplateName;
                }
                else
                    templateRef = null;
                var taxRef = new SalesReceiptRetSalesReceiptLineRetSalesTaxCodeRef();// DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRetSalesTaxCodeRef();
                taxRef = null;
                SalesReceiptAdd salesReceiptaddRq = new SalesReceiptAdd
                {
                    CustomerRef = new DataProcessor.Sync.ModelAdd.InvoiceRetCustomerRef
                    {
                        ListID = saleSelectAddQuickBooksResult.CustomerID,
                        FullName = saleSelectAddQuickBooksResult.Customer
                    },                              
                    TemplateRef = templateRef,
                };
                //List<DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRet> lstLine = new List<DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRet>();
                //DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRet line = new DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRet
                List<SalesReceiptRetSalesReceiptLineRet> lstLine = new List<SalesReceiptRetSalesReceiptLineRet>();
                SalesReceiptRetSalesReceiptLineRet line = new SalesReceiptRetSalesReceiptLineRet
                {
                    ItemRef = new SalesReceiptRetSalesReceiptLineRetItemRef
                    {
                        ListID = saleSelectAddQuickBooksResult.ItemID,
                        FullName = saleSelectAddQuickBooksResult.Item
                    },
                    Rate = saleSelectAddQuickBooksResult.Amount,
                    Amount = saleSelectAddQuickBooksResult.Amount,                    
                    Desc = saleSelectAddQuickBooksResult.ItemDescription,
                    ClassRef =  classRef,
                    SalesTaxCodeRef =  taxRef,
                };
                lstLine.Add(line);
                salesReceiptaddRq.SalesReceiptLineAdd = lstLine.ToArray();
                if (!string.IsNullOrEmpty(saleSelectAddQuickBooksResult.PaymentID))
                    salesReceiptaddRq.CheckNumber =  saleSelectAddQuickBooksResult.PaymentID.ToString();//saleSelectAddQuickBooksResult.Memo + " " +
                if (!string.IsNullOrEmpty(saleSelectAddQuickBooksResult.FormNumber))
                    salesReceiptaddRq.RefNumber = saleSelectAddQuickBooksResult.FormNumber;
                if (!string.IsNullOrEmpty(saleSelectAddQuickBooksResult.MoneyVoucherDate))
                    salesReceiptaddRq.TxnDate = saleSelectAddQuickBooksResult.MoneyVoucherDate;
                if (saleSelectAddQuickBooksResult.LogPaymentID > 0)
                    salesReceiptaddRq.Memo = saleSelectAddQuickBooksResult.LogPaymentID.ToString();
                List<GlobalValue.Reponse> saveSalesReceipt = salesReceipt.SaveSalesReceipt(salesReceiptaddRq);
                insertSalesReceiptCount++;
                if (saveSalesReceipt.Count > 0 && !saveSalesReceipt[0].IsError)
                {                   
                    var checkSerializeXml = SyncConstant.SerializeXml<SalesReceiptRet>(saveSalesReceipt[0].EntityObject.ToString(), QueryXML.SalesReceipt, false);
                    if (checkSerializeXml.Count > 0)
                    {
                        DataContext.UpdateLogPayment_afterSync(saleSelectAddQuickBooksResult.ID, Convert.ToInt32(checkSerializeXml[0].TxnNumber));
                        GlobalValue.Eventlogupdate("Sales Receipt having TxnID " + checkSerializeXml[0].TxnNumber + " inserted to database", null);                       
                        responses.IsError = false;
                        responses.Message =  checkSerializeXml[0].TxnID;                        
                    }                    
                }


                if (saveSalesReceipt[0].IsError)
                {
                    responses.IsError = true;
                    string resMsg = "Error!- Sales Receipt for customer " + saleSelectAddQuickBooksResult.Customer + ", Log ID " +
                               saleSelectAddQuickBooksResult.LogPaymentID + ",\n Form number " +
                               saleSelectAddQuickBooksResult.FormNumber + ", with amount $" +
                               saleSelectAddQuickBooksResult.Amount + ", was not added to QuickBooks because: ";
                    responses.Message += resMsg + saveSalesReceipt[0].Message + "\n";
                    GlobalValue.Eventlogupdate(responses.Message, null, "Error");
                    //MailSender.Send(Settings.Default.MAILTO, Credentials.Get.CompanyId + ": Error in Sales Receipt Sync " + Credentials.SyncDateTime.ToString("MM-dd-yyyy HH:mm"), responses.Message);
                    return responses;

                }
            }
            catch (Exception ex)
            {
                responses.IsError = true;
                string resMsg = "Error!- Sales Receipt for customer " + saleSelectAddQuickBooksResult.Customer + ", Log ID " +
                              saleSelectAddQuickBooksResult.LogPaymentID + ",\n Form number " +
                              saleSelectAddQuickBooksResult.FormNumber + ", with amount $" +
                              saleSelectAddQuickBooksResult.Amount + ", was not added to QuickBooks because: ";
                responses.Message += resMsg + ex.Message + "\n";
                GlobalValue.Eventlogupdate(responses.Message, null, "Error");
            }
            return responses;
        }

        public static GlobalValue.Reponse SaveCombSalesReceipt(List<Logs_SelectResult> saleList, QBXSalesReceipt salesReceipt)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            var saleItem = saleList[0];
            try
            {
                var templateRef = new DataProcessor.Sync.ModelAdd.InvoiceRetTemplateRef();
                if (!string.IsNullOrEmpty(saleItem.TemplateListID))
                {
                    templateRef.ListID = saleItem.TemplateListID;
                    templateRef.FullName = saleItem.TemplateName;
                }
                else
                    templateRef = null;
                SalesReceiptAdd salesReceiptaddRq = new SalesReceiptAdd
                {
                    CustomerRef = new DataProcessor.Sync.ModelAdd.InvoiceRetCustomerRef
                    {
                        ListID = saleItem.CustomerID,
                        FullName = saleItem.Customer
                    },                    
                    TemplateRef = templateRef,

                };
                //List<DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRet> lstLine = new List<DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRet>();
                List<SalesReceiptRetSalesReceiptLineRet> lstLine = new List<SalesReceiptRetSalesReceiptLineRet>();
                foreach (var salesItems in saleList)
                {
                    SalesReceiptRetSalesReceiptLineRet line = new SalesReceiptRetSalesReceiptLineRet
                    {
                        ItemRef = new SalesReceiptRetSalesReceiptLineRetItemRef
                        {
                            ListID = salesItems.ItemID,
                            FullName = salesItems.Item
                        },
                        Amount =  salesItems.Amount,
                        Desc = salesItems.ItemDescription,
                        ClassRef = new SalesReceiptRetSalesReceiptLineRetClassRef
                        {
                            ListID = salesItems.ClassID,
                            FullName = salesItems.ClassName
                        },                 

                    };
                    lstLine.Add(line);
                }
                salesReceiptaddRq.SalesReceiptLineAdd = lstLine.ToArray();
                if (!string.IsNullOrEmpty(saleItem.PaymentID))
                    salesReceiptaddRq.CheckNumber =  saleItem.PaymentID;
                if (!string.IsNullOrEmpty(saleItem.FormNumber))
                    salesReceiptaddRq.RefNumber = saleItem.FormNumber;
                if (!string.IsNullOrEmpty(saleItem.MoneyVoucherDate))
                    salesReceiptaddRq.TxnDate = saleItem.MoneyVoucherDate;
                if (saleItem.LogPaymentID > 0)
                salesReceiptaddRq.Memo = saleItem.LogPaymentID.ToString();

                List<GlobalValue.Reponse> saveSalesReceipt = salesReceipt.SaveSalesReceipt(salesReceiptaddRq);
                insertSalesReceiptCount++;
                if (saveSalesReceipt.Count > 0 && !saveSalesReceipt[0].IsError)
                {
                    var checkSerializeXml = SyncConstant.SerializeXml<SalesReceiptRet>(saveSalesReceipt[0].EntityObject.ToString(), QueryXML.SalesReceipt, false);
                    if (checkSerializeXml.Count > 0)
                    {
                        DataContext.updateCombLog_AfterSync(saleItem.CombinedID, Convert.ToInt32(checkSerializeXml[0].TxnNumber));
                        GlobalValue.Eventlogupdate("Sales Receipt having TxnID " + checkSerializeXml[0].TxnNumber + " inserted to database", null);
                        responses.IsError = false;
                        responses.Message = checkSerializeXml[0].TxnID;
                    }
                }


                if (saveSalesReceipt[0].IsError)
                {
                    responses.IsError = true;
                    string resMsg = "Error!- Sales Receipt for customer " + saleItem.Customer + ", Log ID " +
                               saleItem.LogPaymentID + ",\n Form number " +
                               saleItem.FormNumber + ", with amount $" +
                               saleItem.Amount + ", was not added to QuickBooks because: ";
                    responses.Message += resMsg + saveSalesReceipt[0].Message + "\n";
                    GlobalValue.Eventlogupdate(responses.Message, null, "Error");
                    //MailSender.Send(Settings.Default.MAILTO, Credentials.Get.CompanyId + ": Error in Combined Sales Receipt Sync " + Credentials.SyncDateTime.ToString("MM-dd-yyyy HH:mm"), responses.Message);
                    return responses;

                }
            }
            catch (Exception ex)
            {
                responses.IsError = true;
                string resMsg = "Error!- Sales Receipt for customer " + saleItem.Customer + ", Log ID " +
                               saleItem.LogPaymentID + ",\n Form number " +
                               saleItem.FormNumber + ", with amount $" +
                               saleItem.Amount + ", was not added to QuickBooks because: ";
                responses.Message += resMsg + ex.Message +"\n";
                GlobalValue.Eventlogupdate(responses.Message, null, "Error");
            }
            return responses;
        }

    }
}
