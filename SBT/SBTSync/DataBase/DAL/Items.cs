﻿using DataProcessor;
using DataProcessor.QB.XMLProcessor;
using DataProcessor.Sync;
using DataProcessor.Sync.ModelRet;
using SBTSync.Properties;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace SBTSync.DataBase.DAL
{
    public class Items : DBBase
    {

        private static string errMsg;

        public static GlobalValue.Reponse ProcessItems(Action<ProgressEventArgs> onProgressChanged)
        {
            int itemsave = 0;
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            responses.Message = "";
            QBXItems qbxItem = new QBXItems();
            errMsg = string.Empty;
            DateTime? qbAccountLastMod = GetLastModified("Item");
            var t = qbxItem.GetQBSyncItems(true).Where(a => a.Value.Count() > 0);
            foreach (KeyValuePair<QueryXML, List<object>> item in t)
            {
                try
                {
                    switch (item.Key.ToString())
                    {
                        case "ItemService":
                            List<ItemServiceRet> servicelist = item.Value.Cast<ItemServiceRet>().ToList();
                            foreach (ItemServiceRet service in servicelist)
                            {
                                Items_save(service);
                                itemsave++;
                            }
                            break;
                        case "ItemInventory":
                            List<ItemInventoryRet> inventorylist = item.Value.Cast<ItemInventoryRet>().ToList();
                            foreach (ItemInventoryRet service in inventorylist)
                            {
                                Items_save(service);
                                itemsave++;
                            }
                            break;
                        case "ItemNonInventory":
                            List<ItemNonInventoryRet> nonInventorylist = item.Value.Cast<ItemNonInventoryRet>().ToList();
                            foreach (ItemNonInventoryRet nonInventory in nonInventorylist)
                            {
                                Items_save(nonInventory);
                                itemsave++;
                            }
                            break;
                        case "ItemDiscount":
                            List<ItemDiscountRet> itemDiscountlist = item.Value.Cast<ItemDiscountRet>().ToList();
                            foreach (ItemDiscountRet discount in itemDiscountlist)
                            {
                                Items_save(discount);
                                itemsave++;
                            }
                            break;
                        case "ItemFixedAsset":
                            List<ItemFixedAssetRet> itemFixedAssetRetlist = item.Value.Cast<ItemFixedAssetRet>().ToList();
                            foreach (ItemFixedAssetRet fixedAsset in itemFixedAssetRetlist)
                            {
                                Items_save(fixedAsset);
                                itemsave++;
                            }
                            break;
                        case "ItemInventoryAssembly":
                            List<ItemInventoryAssemblyRet> itemInventoryAssemblyRetlist = item.Value.Cast<ItemInventoryAssemblyRet>().ToList();
                            foreach (ItemInventoryAssemblyRet itemInventoryAssembly in itemInventoryAssemblyRetlist)
                            {
                                Items_save(itemInventoryAssembly);
                                itemsave++;
                            }
                            break;
                        case "ItemOtherCharge":
                            List<ItemOtherChargeRet> itemOtherChargeslist = item.Value.Cast<ItemOtherChargeRet>().ToList();
                            foreach (ItemOtherChargeRet itemOtherCharges in itemOtherChargeslist)
                            {
                                Items_save(itemOtherCharges);
                                itemsave++;
                            }
                            break;
                        case "ItemPayment":
                            List<ItemPaymentRet> itemPaymentslist = item.Value.Cast<ItemPaymentRet>().ToList();
                            foreach (ItemPaymentRet itemPayment in itemPaymentslist)
                            {
                                Items_save(itemPayment);
                                itemsave++;
                            }
                            break;
                        default:
                            break;
                    }
                }
                catch (Exception ex)
                {
                    GlobalValue.Eventlogupdate(ex, null, "Error");
                    errMsg += ex.Message + Environment.NewLine;
                }
                onProgressChanged(new ProgressEventArgs
                {
                    Total = item.Value.Count(),
                    Current = itemsave
                });
            }

            onProgressChanged(new ProgressEventArgs
            {
                Completed = true
            });

            if (errMsg.Length > 0)
                responses.Message += errMsg + "\n";

            responses.Message += $"{itemsave} Item(s) added to database.";
            if (!string.IsNullOrEmpty(errMsg))
            {
                MailSender.Send(Settings.Default.MAILTO, Credentials.Get.CompanyId + ": Error in Items Sync " + Credentials.SyncDateTime.ToString("MM-dd-yyyy HH:mm"), errMsg);
            }
            else { DataContext.Settings_InsertUpdate("Item", DateTime.UtcNow, Credentials.Get.CompanyId); }
            return responses;
        }

        public static void Items_save(ItemInventoryRet inventoryItem)
        {
            try
            {
                DataContext.Items_save(Credentials.Get.CompanyId, inventoryItem.ListID, inventoryItem.TimeCreated, inventoryItem.TimeModified, inventoryItem.EditSequence, inventoryItem.Name, inventoryItem.FullName,
                                       inventoryItem.BarCodeValue, inventoryItem.IsActive, inventoryItem.ClassRef?.ListID, inventoryItem.ClassRef?.FullName, inventoryItem.ParentRef?.ListID,
                                       inventoryItem.ParentRef?.FullName, inventoryItem.Sublevel, string.Empty, inventoryItem.ManufacturerPartNumber, inventoryItem.UnitOfMeasureSetRef?.ListID,
                                       inventoryItem.UnitOfMeasureSetRef?.FullName, inventoryItem.SalesTaxCodeRef?.ListID, inventoryItem.SalesTaxCodeRef?.FullName, inventoryItem.PurchaseDesc,
                                       inventoryItem.PurchaseCost, 0, string.Empty, string.Empty, inventoryItem.SalesDesc, Convert.ToDecimal(inventoryItem.SalesPrice), inventoryItem.IncomeAccountRef?.ListID,
                                       inventoryItem.IncomeAccountRef?.FullName, string.Empty, 0, string.Empty, string.Empty, inventoryItem.PrefVendorRef?.ListID,
                                       inventoryItem.PrefVendorRef?.FullName, string.Empty, inventoryItem.ExternalGUID, inventoryItem.COGSAccountRef?.ListID, inventoryItem.COGSAccountRef?.FullName,
                                       inventoryItem.AssetAccountRef?.ListID, inventoryItem.AssetAccountRef?.FullName, inventoryItem.ReorderPoint, inventoryItem.Max, inventoryItem.QuantityOnHand,
                                       inventoryItem.AverageCost, inventoryItem.QuantityOnOrder, inventoryItem.QuantityOnSalesOrder, string.Empty, "ItemInventory", null, null);
            }
            catch (Exception ex)
            {
                GlobalValue.Eventlogupdate(ex, null, "Error");
                errMsg = ex.Message + Environment.NewLine;
            }
        }

        public static void Items_save(ItemServiceRet serviceItem)
        {
            try
            {
                DataContext.Items_save(Credentials.Get.CompanyId, serviceItem.ListID, serviceItem.TimeCreated, serviceItem.TimeModified, serviceItem.EditSequence, serviceItem.Name, serviceItem.FullName,
                                      serviceItem.BarCodeValue, serviceItem.IsActive, serviceItem.ClassRef?.ListID, serviceItem.ClassRef?.FullName, serviceItem.ParentRef?.ListID,
                                      serviceItem.ParentRef?.FullName, serviceItem.Sublevel, string.Empty, string.Empty, serviceItem.UnitOfMeasureSetRef?.ListID,
                                      serviceItem.UnitOfMeasureSetRef?.FullName, serviceItem.SalesTaxCodeRef?.ListID, serviceItem.SalesTaxCodeRef?.FullName, serviceItem.SalesOrPurchase?.Desc,
                                      Convert.ToDecimal(serviceItem.SalesOrPurchase?.Price), Convert.ToDecimal(serviceItem.SalesOrPurchase?.PricePercent), serviceItem.SalesOrPurchase?.AccountRef?.ListID,
                                      serviceItem.SalesOrPurchase?.AccountRef?.FullName, serviceItem.SalesAndPurchase?.SalesDesc, Convert.ToDecimal(serviceItem.SalesAndPurchase?.SalesPrice),
                                      serviceItem.SalesAndPurchase?.IncomeAccountRef?.ListID, serviceItem.SalesAndPurchase?.IncomeAccountRef?.FullName, serviceItem.SalesAndPurchase?.PurchaseDesc,
                                      serviceItem.SalesAndPurchase?.PurchaseCost, serviceItem.SalesAndPurchase?.ExpenseAccountRef?.ListID, serviceItem.SalesAndPurchase?.ExpenseAccountRef?.FullName,
                                      serviceItem.SalesAndPurchase?.PrefVendorRef?.ListID, serviceItem.SalesAndPurchase?.PrefVendorRef?.FullName, string.Empty, serviceItem.ExternalGUID, string.Empty, string.Empty,
                                      string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, 0, string.Empty, string.Empty, string.Empty, "ItemService", null, null);
            }
            catch (Exception ex)
            {
                GlobalValue.Eventlogupdate(ex, null, "Error");
                errMsg = ex.Message + Environment.NewLine;
            }
        }

        public static void Items_save(ItemNonInventoryRet nonInventoryItem)
        {
            try
            {
                DataContext.Items_save(Credentials.Get.CompanyId, nonInventoryItem.ListID, nonInventoryItem.TimeCreated, nonInventoryItem.TimeModified, nonInventoryItem.EditSequence,
                                       nonInventoryItem.Name, nonInventoryItem.FullName, nonInventoryItem.BarCodeValue, nonInventoryItem.IsActive,
                                       nonInventoryItem.ClassRef?.ListID, nonInventoryItem.ClassRef?.FullName, nonInventoryItem.ParentRef?.ListID,
                                       nonInventoryItem.ParentRef?.FullName, nonInventoryItem.Sublevel, string.Empty, nonInventoryItem.ManufacturerPartNumber,
                                       nonInventoryItem.UnitOfMeasureSetRef?.ListID, nonInventoryItem.UnitOfMeasureSetRef?.FullName, nonInventoryItem.SalesTaxCodeRef?.ListID,
                                       nonInventoryItem.SalesTaxCodeRef?.FullName, nonInventoryItem.SalesOrPurchase?.Desc, nonInventoryItem.SalesOrPurchase?.Price,
                                       nonInventoryItem.SalesOrPurchase?.PricePercent, nonInventoryItem.SalesOrPurchase?.AccountRef?.ListID, nonInventoryItem.SalesOrPurchase?.AccountRef?.FullName,
                                       nonInventoryItem.SalesAndPurchase?.SalesDesc, nonInventoryItem.SalesAndPurchase?.SalesPrice, nonInventoryItem.SalesAndPurchase?.IncomeAccountRef?.ListID,
                                       nonInventoryItem.SalesAndPurchase?.IncomeAccountRef?.FullName, nonInventoryItem.SalesAndPurchase?.PurchaseDesc,
                                       nonInventoryItem.SalesAndPurchase?.PurchaseCost, nonInventoryItem.SalesAndPurchase?.ExpenseAccountRef?.ListID,
                                       nonInventoryItem.SalesAndPurchase?.ExpenseAccountRef?.FullName, nonInventoryItem.SalesAndPurchase?.PrefVendorRef?.ListID,
                                       nonInventoryItem?.SalesAndPurchase?.PrefVendorRef?.FullName, string.Empty, nonInventoryItem.ExternalGUID, string.Empty,
                                       string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty,
                                       string.Empty, "ItemNonInventory", null, null);
            }
            catch (Exception ex)
            {
                GlobalValue.Eventlogupdate(ex, null, "Error");
                errMsg = ex.Message + Environment.NewLine;
            }
        }

        public static void Items_save(ItemDiscountRet discountItems)
        {
          
            try
            {
                DataContext.Items_save(Credentials.Get.CompanyId, discountItems.ListID, discountItems.TimeCreated, discountItems.TimeModified, discountItems.EditSequence, discountItems.Name, discountItems.FullName,
                                       discountItems.BarCodeValue, discountItems.IsActive, discountItems.ClassRef?.ListID, discountItems.ClassRef?.FullName, discountItems.ParentRef?.ListID,
                                       discountItems.ParentRef?.FullName, discountItems.Sublevel, discountItems.ItemDesc, string.Empty, string.Empty, string.Empty, discountItems.SalesTaxCodeRef?.ListID, discountItems.SalesTaxCodeRef?.FullName,
                                       string.Empty, null, null, string.Empty, string.Empty , string.Empty, null, string.Empty, string.Empty, string.Empty, 0, string.Empty, discountItems.AccountRef?.FullName, string.Empty, string.Empty,
                                       string.Empty, discountItems.ExternalGUID, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty,
                                       null, string.Empty, string.Empty, string.Empty, "ItemDiscount", discountItems.DiscountRate, discountItems.DiscountRatePercent);
            }
            catch (Exception ex)
            {
                GlobalValue.Eventlogupdate(ex, null, "Error");
                errMsg = ex.Message + Environment.NewLine;
            }
        }

        public static void Items_save(ItemFixedAssetRet itemFixedAsset)
        {
             string SalesORPurchasePriceAccountRefName = ""; 
            
            try
            {
                if (itemFixedAsset.AssetAccountRef != null)
                    SalesORPurchasePriceAccountRefName = itemFixedAsset.AssetAccountRef.FullName;

            }
            catch (Exception ex)
            {
                GlobalValue.Eventlogupdate(ex, null, "Error");

            }

            try
            {
                DataContext.Items_save(Credentials.Get.CompanyId, itemFixedAsset.ListID, itemFixedAsset.TimeCreated, itemFixedAsset.TimeModified, itemFixedAsset.EditSequence,
                                       itemFixedAsset.Name, string.Empty, itemFixedAsset.BarCodeValue, itemFixedAsset.IsActive,
                                       itemFixedAsset.ClassRef?.ListID, itemFixedAsset.ClassRef?.FullName, string.Empty,
                                       string.Empty, null, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                                       string.Empty, string.Empty, null, null, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty,
                                       string.Empty, 0, string.Empty, string.Empty, string.Empty, SalesORPurchasePriceAccountRefName, string.Empty, itemFixedAsset.ExternalGUID, string.Empty,
                                       string.Empty, string.Empty, string.Empty, string.Empty, null, string.Empty, null, string.Empty, string.Empty, string.Empty,
                                       "ItemFixedAsset", null, null);
            }
            catch (Exception ex)
            {
                GlobalValue.Eventlogupdate(ex, null, "Error");
                errMsg = ex.Message + Environment.NewLine;
            }
        }

        public static void Items_save(ItemInventoryAssemblyRet itemInventoryAssembly)
        {
            try
            {
                 string SalesORPurchasePriceAccountRefName = ""; 
                
                try
                {
                    if (itemInventoryAssembly.IncomeAccountRef != null)
                        SalesORPurchasePriceAccountRefName = itemInventoryAssembly.IncomeAccountRef.FullName;
                   
                }
                catch (Exception ex)
                {
                    GlobalValue.Eventlogupdate(ex, null, "Error");

                }

                DataContext.Items_save(Credentials.Get.CompanyId, itemInventoryAssembly.ListID, itemInventoryAssembly.TimeCreated, itemInventoryAssembly.TimeModified, itemInventoryAssembly.EditSequence,
                                       itemInventoryAssembly.Name, itemInventoryAssembly.FullName, itemInventoryAssembly.BarCodeValue, itemInventoryAssembly.IsActive,
                                       itemInventoryAssembly.ClassRef?.ListID, itemInventoryAssembly.ClassRef?.FullName, itemInventoryAssembly.ParentRef?.ListID,
                                       itemInventoryAssembly.ParentRef?.FullName, itemInventoryAssembly.Sublevel, string.Empty, itemInventoryAssembly.ManufacturerPartNumber, itemInventoryAssembly.UnitOfMeasureSetRef?.ListID,
                                       itemInventoryAssembly.UnitOfMeasureSetRef?.FullName, itemInventoryAssembly.SalesTaxCodeRef?.ListID, itemInventoryAssembly.SalesTaxCodeRef?.FullName,
                                       string.Empty, null, null, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty,
                                       string.Empty, 0, string.Empty, string.Empty, string.Empty, SalesORPurchasePriceAccountRefName , string.Empty, itemInventoryAssembly.ExternalGUID, itemInventoryAssembly.COGSAccountRef?.ListID,
                                       itemInventoryAssembly.COGSAccountRef?.FullName, itemInventoryAssembly.AssetAccountRef?.ListID, itemInventoryAssembly.AssetAccountRef?.FullName,
                                       null, itemInventoryAssembly.Max, itemInventoryAssembly.QuantityOnHand, itemInventoryAssembly.AverageCost, itemInventoryAssembly.QuantityOnOrder, itemInventoryAssembly.QuantityOnSalesOrder, itemInventoryAssembly.BuildPoint,
                                       "ItemInventoryAssembly", null, null);
            }
            catch (Exception ex)
            {
                GlobalValue.Eventlogupdate(ex, null, "Error");
                errMsg = ex.Message + Environment.NewLine;
            }
        }

        public static void Items_save(ItemOtherChargeRet itemOtherCharges)
        {
            try
            {
                string SalesAndPurchaseExpenceAccountRefName = "";string SalesORPurchasePriceAccountRefName = ""; 
                
                try
                {
                    if (itemOtherCharges.SalesAndPurchase != null)
                        SalesAndPurchaseExpenceAccountRefName = itemOtherCharges.SalesAndPurchase.IncomeAccountRef.FullName;
                    if (itemOtherCharges.SalesOrPurchase != null)
                        SalesORPurchasePriceAccountRefName = itemOtherCharges.SalesOrPurchase.AccountRef.FullName;
                }
                catch(Exception ex)
                {
                    GlobalValue.Eventlogupdate(ex, null, "Error");

                }


                DataContext.Items_save(Credentials.Get.CompanyId, itemOtherCharges.ListID, itemOtherCharges.TimeCreated, itemOtherCharges.TimeModified, itemOtherCharges.EditSequence,
                                       itemOtherCharges.Name, itemOtherCharges.FullName, itemOtherCharges.BarCodeValue, itemOtherCharges.IsActive,
                                       itemOtherCharges.ClassRef?.ListID, itemOtherCharges.ClassRef?.FullName, itemOtherCharges.ParentRef?.ListID,
                                       itemOtherCharges.ParentRef?.FullName, itemOtherCharges.Sublevel, string.Empty, string.Empty, string.Empty,
                                       string.Empty, itemOtherCharges.SalesTaxCodeRef?.ListID, itemOtherCharges.SalesTaxCodeRef?.FullName,
                                       string.Empty, null, null, string.Empty, SalesORPurchasePriceAccountRefName, string.Empty, null, string.Empty, string.Empty,
                                       string.Empty, 0, string.Empty, SalesAndPurchaseExpenceAccountRefName , string.Empty, string.Empty, string.Empty, itemOtherCharges.ExternalGUID, string.Empty,
                                       string.Empty, string.Empty, string.Empty, null, null, string.Empty, null, string.Empty, string.Empty, string.Empty,
                                       "ItemOtherCharge", null, null);
            }
            catch (Exception ex)
            {
                GlobalValue.Eventlogupdate(ex, null, "Error");
                errMsg = ex.Message + Environment.NewLine;
            }
        }

        public static void Items_save(ItemPaymentRet itemPayments)
        {
            try
            {
                DataContext.Items_save(Credentials.Get.CompanyId, itemPayments.ListID, itemPayments.TimeCreated, itemPayments.TimeModified,
                                       itemPayments.EditSequence,
                                       itemPayments.Name, null, itemPayments.BarCodeValue, itemPayments.IsActive,
                                       itemPayments.ClassRef?.ListID, itemPayments.ClassRef?.FullName, string.Empty,
                                       string.Empty, null, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                                       string.Empty, null, null, string.Empty, string.Empty, string.Empty, null, string.Empty, string.Empty,
                                       string.Empty, 0, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                                       itemPayments.ExternalGUID, string.Empty, string.Empty, string.Empty, string.Empty, null, null,
                                       string.Empty, null, string.Empty, string.Empty, string.Empty, "ItemPayment", null, null);
            }
            catch (Exception ex)
            {
                GlobalValue.Eventlogupdate(ex, null, "Error");
                errMsg = ex.Message + Environment.NewLine;
            }
        }

        public void Items_delete(String ListID)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Items_delete";
            cmd.Parameters.AddWithValue("@ListID", ListID);
        }

    }
}
