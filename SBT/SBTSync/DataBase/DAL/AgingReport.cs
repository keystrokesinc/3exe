﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Linq;
using DataProcessor;
using DataProcessor.QB.XMLProcessor;
using DataProcessor.Sync.ModelRet;
using SBTSync.Properties;
using DataProcessor.Sync.ModelAdd;
using DataProcessor.Sync;
using SBTSync.DataBase.DAL;
using SBTSync.DataBase;

namespace SBTSync.DataBase.DAL
{
    public class AgingReport : DBBase
    {
        private static string errMsg = string.Empty;
        private static int dateParamCount = 11;
        public static GlobalValue.Reponse ProcessAgingReport(Action<ProgressEventArgs> onProgressChanged)
        {            
            GlobalValue.Reponse responses = new GlobalValue.Reponse();            
            errMsg = string.Empty;            
            QBXAgingReport qbxReport = new QBXAgingReport();           
            IList<ReportRet> agReports = qbxReport.GetAgingReport();
            int insertReportCount = 0;
            foreach (ReportRet agReportData in agReports)
            {
                try
                {
                    DataContext.InsertAGReports(agReportData.ReportTitle, agReportData.ReportSubtitle, agReportData.ReportBasis, agReportData.NumRows,
                    agReportData.NumColumns, agReportData.NumColTitleRows);

                    //details
                    foreach (ReportData rdata in agReportData.ReportData)
                    {
                        foreach (DataRow row in rdata.DataRow)
                        {
                            var col1 = "";
                            var col2 = "";
                            var col3 = "";
                            var col4 = "";
                            var col5 = "";
                            var col6 = "";
                            var col7 = "";
                            var col8 = "";                           
                            
                            try
                            {
                                foreach (ColData col in row.ColData)
                                {
                                    if (col.colID == 1)
                                        col1 = col.value;
                                    if (col.colID == 2)
                                        col2 = col.value;
                                    if (col.colID == 3)
                                        col3 = col.value;
                                    if (col.colID == 4)
                                        col4 = col.value;
                                    if (col.colID == 5)
                                        col5 = col.value;
                                    if (col.colID == 6)
                                        col6 = col.value;
                                    if (col.colID == 7)
                                        col7 = col.value;
                                    if (col.colID == 8)
                                        col8 = col.value;                                   

                                }
                                insertReportCount++;
                                DataContext.InsertAGReportDetails(1, col1, col2, col3, col4, col5, col6, col7, col8);
                            }
                            catch (Exception ex)
                            {

                            }

                        }
                        foreach (SubtotalRow srow in rdata.SubtotalRow)
                        {
                            var col1 = "";
                            var col8 = "";
                            var col11 = "";
                            var col12 = "";
                            try
                            {
                                foreach (ColData col in srow.ColData)
                                {

                                    if (col.colID == 1)
                                        col1 = col.value != null ? col.value : "";
                                    if (col.colID == 8)
                                        col8 = col.value != null ? col.value : "";                                   

                                }
                                DataContext.InsertAGReportSummaryData(1,col1 != null ? col1 : "",
                                            col8 != null ? col8 : "");
                            }

                            catch (Exception ex)
                            {

                            }

                        }
                        foreach (TotalRow srow in rdata.TotalRow)
                        {
                            var col1 = "";
                            var col8 = "";
                            var col11 = "";
                            var col12 = "";
                            try
                            {
                                foreach (ColData col in srow.ColData)
                                {

                                    if (col.colID == 1)
                                        col1 = col.value != null ? col.value : "";
                                    if (col.colID == 8)
                                        col8 = col.value != null ? col.value : "";

                                }
                                DataContext.InsertAGReportSummaryData(1, col1 != null ? col1 : "",
                                            col8 != null ? col8 : "");
                            }

                            catch (Exception ex)
                            {

                            }

                        }
                    }

                    onProgressChanged(new ProgressEventArgs
                    {
                        Total = agReports.Count,
                        Current = insertReportCount
                    });


                }
                catch (Exception ex)
                {
                    GlobalValue.Eventlogupdate(ex, null, "Error");
                    errMsg = ex.Message + Environment.NewLine;
                }
            }

            onProgressChanged(new ProgressEventArgs
            {
                Completed = true
            });           
            responses.Message += errMsg;         
             return responses;
        }





    }
}
