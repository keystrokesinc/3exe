﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Linq;
using DataProcessor;
using DataProcessor.QB.XMLProcessor;
using SBTSync.Properties;

namespace SBTSync.DataBase.DAL
{
    public class Class : DBBase
    {
        private static string errMsg;

        public static GlobalValue.Reponse ProcessClass(Action<ProgressEventArgs> onProgressChanged)
        {

            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            responses.Message = "";
            QBXClass Class = new QBXClass();
            errMsg = "";

            DateTime? qbClassLastMod = null; //GetLastModified("Class");
            int insertCCCount = 0;
            int UpdateCCCount = 0;

            List<ClassRet> classRets = Class.GetAllClass(qbClassLastMod);

            foreach (ClassRet classRet in classRets)
            {
                try
                {
                    ISingleResult<CostCenters_SelectResult> classSelectResults = DataContext.CostCenters_Select(classRet.ListID, Credentials.Get.CompanyId);
                    List<CostCenters_SelectResult> selectResults = new List<CostCenters_SelectResult>(classSelectResults);

                    if (selectResults.Count == 0)
                    {
                        DataContext.CostCenters_Insert(Credentials.Get.CompanyId, classRet.ListID, classRet.TimeCreated, classRet.TimeModified, classRet.FullName, classRet.IsActive);
                        GlobalValue.Eventlogupdate("Class having Name " + classRet.Name + " Inserted", null);
                        insertCCCount++;
                    }
                    else
                    {
                        DataContext.CostCenters_Update(Credentials.Get.CompanyId, classRet.ListID, classRet.TimeCreated, classRet.TimeModified, classRet.FullName, classRet.IsActive);
                        GlobalValue.Eventlogupdate("Class having Name " + classRet.Name + " updated", null);
                        UpdateCCCount++;
                    }

                    onProgressChanged(new ProgressEventArgs
                    {
                        Current = insertCCCount + UpdateCCCount,
                        Total = classRets.Count
                    });
                }
                catch (Exception ex)
                {
                    GlobalValue.Eventlogupdate(ex, null, "Error");
                    errMsg += ex.Message + Environment.NewLine;
                }
            }

            onProgressChanged(new ProgressEventArgs
            {
                Completed = true
            });

            if (errMsg.Length > 0)
                responses.Message += errMsg + "\n";

            if (insertCCCount > 0)
                responses.Message += insertCCCount + " Cost Center(s) added to database.\n";
            if (UpdateCCCount > 0)
                responses.Message += UpdateCCCount + " Cost Center(s) updated in database.\n";
            responses.Message += DeleteClass(classRets).Message;

            if (!string.IsNullOrEmpty(errMsg))
            {
                MailSender.Send(Settings.Default.MAILTO, Credentials.Get.CompanyId + ": Error in Class Sync " + Credentials.SyncDateTime.ToString("MM-dd-yyyy HH:mm"), errMsg);
            }
            return responses;
        }

        public static GlobalValue.Reponse DeleteClass(IList<ClassRet> classRets = null)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            try
            {

                GlobalValue.Eventlogupdate("DELETE OPERATION STARTED FOR CLASS", null);
                classRets = classRets ?? (new QBXClass()).GetAllClass(null);

                ISingleResult<CostCenters_SelectResult> customerSelectResults = DataContext.CostCenters_Select(null, Credentials.Get.CompanyId);
                List<CostCenters_SelectResult> dbClass = new List<CostCenters_SelectResult>(customerSelectResults);


                foreach (CostCenters_SelectResult costCentersSelectResult in dbClass)
                {
                    ClassRet classR = classRets.FirstOrDefault(c => c.ListID == costCentersSelectResult.ListID);
                    if (classR == null)
                    {

                        GlobalValue.Eventlogupdate("Setting inactive class record having name " + costCentersSelectResult.FullName, null);
                        DataContext.CostCenters_Update(Credentials.Get.CompanyId, costCentersSelectResult.ListID, costCentersSelectResult.TimeCreated, costCentersSelectResult.TimeModified, costCentersSelectResult.FullName, false);
                        responses.RecordCount++;
                    }
                }

                GlobalValue.Eventlogupdate("DELETE OPERATION ENDED FOR CLASS. " + responses.RecordCount + " CLASS RECORD DELETED.", null);
            }
            catch (Exception ex)
            {
                GlobalValue.Eventlogupdate("CLASS DELETE: " + ex.Message, null, "Error");
                errMsg += ex.Message + Environment.NewLine;
            }
            responses.Message = responses.RecordCount + " Cost Center(s) set to inactive status.";
            return responses;
        }
    }
}
