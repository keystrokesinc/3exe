﻿using System;
using System.Collections.Generic;
using DataProcessor;
using DataProcessor.QB.XMLProcessor;
using DataProcessor.Sync.ModelRet;
using System.Data.Linq;
using System.Data.SqlClient;
using DataProcessor.Sync.ModelAdd;
using DataProcessor.Sync;
using SBTSync.Properties;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using Newtonsoft.Json;
using System.Data;

namespace SBTSync.DataBase.DAL
{
    public class MWSFInvoice : DBBase
    {
      
        public static int insertInvoiceCount = 0;
        public static GlobalValue.Reponse ProcessSFInvoice(Action<ProgressEventArgs> onProgressChanged)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            QBXInvoice invoice = new QBXInvoice();
            DateTime? qbInvoiceLastMod = GetLastModified("Invoice");
            GlobalValue.Eventlogupdate("Getting Invoices", null);
            ISingleResult<Logs_SelectResult> invoiceSelectResults = DataContext.SFInvoice_Select_AddQuickBooksMOW();
            List<Logs_SelectResult> selectResults = new List<Logs_SelectResult>(invoiceSelectResults);
            insertInvoiceCount = 0;
            string batchName = Settings.Default.BatchName;
            if (selectResults.Count > 0)
            {
                foreach (Logs_SelectResult invoiceSelectResult in selectResults)
                {
                    GlobalValue.Reponse res = new GlobalValue.Reponse();
                    try
                    {
                        if (batchName != null)
                        {
                            if (batchName == invoiceSelectResult.Memo)
                            {
                                if (invoiceSelectResult.CustomerID == null)
                                {
                                    QBXCustomer qBXCustomer = new QBXCustomer();
                                    IList<CustomerRet> customerRets = qBXCustomer.SearchCustomer(invoiceSelectResult.Customer);
                                    if (customerRets?.Count > 0)
                                        invoiceSelectResult.CustomerID = customerRets[0].ListID;
                                    else
                                        invoiceSelectResult.CustomerID = Customer.insertCustomerToQB(invoiceSelectResult); //error need to handle
                                }

                                res = SaveInvoice(invoiceSelectResult, invoice);
                            }
                        }
                        else
                        {
                            if (invoiceSelectResult.CustomerID == null && invoiceSelectResult.RowType =="S")
                            {
                                QBXCustomer qBXCustomer = new QBXCustomer();
                                IList<CustomerRet> customerRets = qBXCustomer.SearchCustomer(invoiceSelectResult.Customer);
                                if (customerRets?.Count > 0)
                                    invoiceSelectResult.CustomerID = customerRets[0].ListID;
                                else
                                    invoiceSelectResult.CustomerID = Customer.insertCustomerToQB(invoiceSelectResult); //error need to handle
                            }
                            if(invoiceSelectResult.GLID != null)
                                res = SaveInvoice(invoiceSelectResult, invoice);
                        }                        
                    }
                    catch (Exception ex)
                    {
                        GlobalValue.Eventlogupdate(ex, null, "Error");
                    }
                }
            }
            onProgressChanged(new ProgressEventArgs
            {
                Completed = true
            });
            if (insertInvoiceCount > 0)           
                responses.Message = $"{insertInvoiceCount} Invoice(s) added to quickbooks\n";
           
            return responses;
        }
      public static GlobalValue.Reponse SaveInvoice(Logs_SelectResult invoiceSelectAddQuickBooksResult, QBXInvoice invoice)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();            
            try
            {
                var customerRef = new DataProcessor.Sync.ModelAdd.InvoiceRetCustomerRef();
                if (invoiceSelectAddQuickBooksResult.RowType == "S")
                {
                    customerRef.ListID = invoiceSelectAddQuickBooksResult.CustomerID;
                    customerRef.FullName = invoiceSelectAddQuickBooksResult.Customer;
                }
                else
                    customerRef.FullName = "01 SalesForce Database";
                
                InvoiceAdd invoiceaddRq = new InvoiceAdd
                {
                    CustomerRef = customerRef,
                    ClassRef = new DataProcessor.Sync.ModelAdd.InvoiceRetClassRef
                    {
                        FullName = "Dev Parent:85Development"
                    },
                    TemplateRef = new DataProcessor.Sync.ModelAdd.InvoiceRetTemplateRef
                    {
                        FullName = "Intuit Service Invoice"
                    }          
                   
            
                };               
                List<DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRet> lstLine = new List<DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRet>();
                DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRet line = new DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRet
                {                    
                    ItemRef = new DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRetItemRef
                    {                       
                        FullName = invoiceSelectAddQuickBooksResult.ItemName
                    },
                    Amount = string.Format("{0:0.00}", invoiceSelectAddQuickBooksResult.TotalAmount),
                    Desc = invoiceSelectAddQuickBooksResult.ItemDescription,
                    ClassRef = new DataProcessor.Sync.ModelAdd.InvoiceRetInvoiceLineRetClassRef
                    {
                        FullName = "Dev Parent:85Development"
                    },

                };
                lstLine.Add(line);
                invoiceaddRq.InvoiceLineAdd = lstLine.ToArray();                
                invoiceaddRq.RefNumber = invoiceSelectAddQuickBooksResult.RefNumber.ToString();
                string txnDate = invoiceSelectAddQuickBooksResult.TxnDate.ToString();
                invoiceaddRq.TxnDate = invoiceSelectAddQuickBooksResult.DueDate.ToString() != null ? invoiceSelectAddQuickBooksResult.DueDate.ToString("yyyy-MM-dd") : Convert.ToDateTime(txnDate).ToString("yyyy-MM-dd");                
                invoiceaddRq.Memo = invoiceSelectAddQuickBooksResult.RowType == "G" ? invoiceSelectAddQuickBooksResult.Memo +"$"+ invoiceSelectAddQuickBooksResult.TotalAmount.ToString() + " Batch Deposit" : "";
                GlobalValue.Eventlogupdate("Sending Inv " + invoiceaddRq.RefNumber + " amt " + line.Amount.ToString(),null,"Info");
                List <GlobalValue.Reponse> saveInvoice = invoice.SaveInvoice(invoiceaddRq);                
                if (saveInvoice.Count > 0 && !saveInvoice[0].IsError)
                {
                    insertInvoiceCount++;
                    var checkSerializeXml = SyncConstant.SerializeXml<InvoiceRet>(saveInvoice[0].EntityObject.ToString(), QueryXML.Invoice, false);
                    if (checkSerializeXml.Count > 0)
                    {
                        if (invoiceSelectAddQuickBooksResult.RowType == "G")
                        {
                            var GLIDArr = invoiceSelectAddQuickBooksResult.GLID.Split(',');
                            if (GLIDArr.Length > 0)
                            {
                                for (int i = 0; i < GLIDArr.Length; i++)
                                {
                                    DataContext.UpdateLogPayment_afterSync(Convert.ToInt16(GLIDArr[i]), Convert.ToInt32(checkSerializeXml[0].TxnNumber));
                                    DataContext.Log_UpdateStatus(invoiceSelectAddQuickBooksResult.LogPaymentID);
                                }
                            }
                        }
                        else
                        { 
                            DataContext.UpdateLogPayment_afterSync(invoiceSelectAddQuickBooksResult.ID, Convert.ToInt32(checkSerializeXml[0].TxnNumber));
                            DataContext.Log_UpdateStatus(invoiceSelectAddQuickBooksResult.LogPaymentID);
                        }
                      
                        GlobalValue.Eventlogupdate("Invoice having TxnID " + checkSerializeXml[0].TxnNumber + " inserted to database", null);
                        responses.IsError = false;
                        responses.Message +=  checkSerializeXml[0].TxnID;                       
                    }                    
                }
                if (saveInvoice[0].IsError)
                {                                  
                    responses.IsError = true;
                    string resMsg = "Error!- Invoice for customer " + invoiceSelectAddQuickBooksResult.Customer +", Log ID "+
                                 invoiceSelectAddQuickBooksResult.LogPaymentID  + ", form number " +
                                 invoiceSelectAddQuickBooksResult.FormNumber + ", with amount $" +
                                 invoiceSelectAddQuickBooksResult.Amount + ", was not added to QuickBooks because: ";
                    responses.Message +=resMsg+saveInvoice[0].Message + "\n";
                    GlobalValue.Eventlogupdate(responses.Message, null, "Error");
                    //MailSender.Send(Settings.Default.MAILTO, Credentials.Get.CompanyId + ": Error in Invoice Sync " + Credentials.SyncDateTime.ToString("MM-dd-yyyy HH:mm"), responses.Message);
                    //return responses;
                }
            }
            catch (Exception ex)
            {
                responses.IsError = true;
                string resMsg = "Error!- Invoice for customer " + invoiceSelectAddQuickBooksResult.Customer + ", Log ID " +
                               invoiceSelectAddQuickBooksResult.LogPaymentID + ", form number " +
                               invoiceSelectAddQuickBooksResult.FormNumber + ", with amount $" +
                               invoiceSelectAddQuickBooksResult.Amount + ", was not added to QuickBooks because: ";
                responses.Message += resMsg + ex.Message + "\n";
                MailSender.Send(Settings.Default.MAILTO, Credentials.Get.CompanyId + ": Error in Invoice Sync " + Credentials.SyncDateTime.ToString("MM-dd-yyyy HH:mm"), responses.Message);
                GlobalValue.Eventlogupdate(responses.Message, null, "Error");
            }            
            return responses;
        }

        private static string getItemName(string item)
        {
            string ItemName = "";
            if (item.Contains("4050"))
                ItemName = "CONTRIBUTIONS:4050 Fundraising Mail Campaigns";
            else if (item.Contains("4010"))
                ItemName = "CONTRIBUTIONS:4010 PUBLIC CONT";
            else if (item.Contains("4015"))
                ItemName = "CONTRIBUTIONS:4015 Client Monthly Newsletter";
            else if (item.Contains("4020"))
                ItemName = "CONTRIBUTIONS:4020 EMPLOYEE GIVING";
            else if (item.Contains("Grants"))
                ItemName = "GRANTS - Charitable:Charitable Grant";
            return ItemName;
        }
        class ApiSendData
        {
            public string grant_type { get; set; }
            public string client_id { get; set; }
            public string client_secret { get; set; }
            public string refresh_token { get; set; }
            //public string username { get; set; }
            //public string password { get; set; }
        }
        public class LoginResponse
        {
            public string access_token { get; set; }
            public string instance_url { get; set; }
            public string id { get; set; }
            public string token_type { get; set; }
            public string issued_at { get; set; }
            public string signature { get; set; }
        }
        class UpdateSFClass
        {
            public string sfId { get; set; }
            public string qbMemoid { get; set; }
            public string qbError { get; set; }
        }
        private static void UpdateSalesforceData(string SFReferance, string QBID, string QBErrorMsg)
        {
            LoginResponse res = null;
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("https://login.salesforce.com/services/oauth2/token?");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //**LIVE SALES FORCE LOGIN**//
                    var datatobeSent = new ApiSendData()
                    {
                        grant_type = "password",
                        client_id = "3MVG9oNqAtcJCF.EGnjKYNbXn57OdNsZyw8kuhVd0x__LMZvRMaHMIDxWGSqS.FzMFNs2_tVu6N74OZvwB3j4",
                        client_secret = "F106A47159B0AD459C7378BB43D21D2AA2BE92155B3C5B1DD1C0A571DD0AE1C3",
                        //username = "dharris@parkinsonvoiceproject.org",
                        //password = "Park@@321rT6YfmG6YlY3liA6P12EQtClJ"
                    };

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    var response = client.PostAsJsonAsync("https://login.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9oNqAtcJCF.EGnjKYNbXn57OdNsZyw8kuhVd0x__LMZvRMaHMIDxWGSqS.FzMFNs2_tVu6N74OZvwB3j4&client_secret=F106A47159B0AD459C7378BB43D21D2AA2BE92155B3C5B1DD1C0A571DD0AE1C3&username=dharris@parkinsonvoiceproject.org&password=Park@@321rT6YfmG6YlY3liA6P12EQtClJ", datatobeSent).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        res = response.Content.ReadAsAsync<LoginResponse>().Result;
                    }
                }

                //Update Salesforce
                using (var client = new HttpClient())
                {

                    client.BaseAddress = new Uri("https://pvp.my.salesforce.com/services/apexrest/updatememoid");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(res.token_type, res.access_token);
                    var datatobeSent = new UpdateSFClass()
                    {
                        sfId = SFReferance,
                        qbMemoid = QBID,
                        qbError = QBErrorMsg
                    };
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;                    
                    var response = client.PostAsJsonAsync("https://pvp.my.salesforce.com/services/apexrest/updatememoid", datatobeSent).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        
                    }
                }
                }
            catch(Exception ex)
            {

            }
        }

        //**PULL Invoice from SF
        public static GlobalValue.Reponse PullSFInvoice(Action<ProgressEventArgs> onProgressChanged)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            string connetionString = Credentials.Get.ConnectionString;
            SqlConnection cnn = new SqlConnection(connetionString);
            try
            {
                LoginResponse res = null;
                cnn.Open();              

                //**LIVE URL**//
                string loginURL = "https://login.salesforce.com/services/oauth2/token";
                string loginFullURL = "https://login.salesforce.com/services/oauth2/token?grant_type=refresh_token&client_id=3MVG9xOCXq4ID1uFUvyxVwYbT_vbXMml0_IGyQ9mYVT.MgdEZOcX63n8RN6TwGu16Au0WRXVADGTjNUqqY1WG&client_secret=320BB58BC987CD609841493CED0D0E78BB72182A54382E58A3F5FB21992FE17C&refresh_token=5Aep861LNDQReieQSK2DomJjQh4bXtJn0WBPxlmwwB3aRmwlKRbOKxAsGBMB2Jhsop5ii.lKnueJixA9047QUg3";
                string instanceURL = "https://na40.salesforce.com/services/data/v51.0/query/";                
                responses.Message += "Initializing connection....\n";

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(loginURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    // HTTP POST Sales Force Login
                    var datatobeSent = new ApiSendData()
                    {

                        //**FOR LIVE **//
                        grant_type = "refresh_token",
                        client_id = "3MVG9xOCXq4ID1uFUvyxVwYbT_vbXMml0_IGyQ9mYVT.MgdEZOcX63n8RN6TwGu16Au0WRXVADGTjNUqqY1WG",
                        client_secret = "320BB58BC987CD609841493CED0D0E78BB72182A54382E58A3F5FB21992FE17C",
                        refresh_token = "5Aep861LNDQReieQSK2DomJjQh4bXtJn0WBPxlmwwB3aRmwlKRbOKxAsGBMB2Jhsop5ii.lKnueJixA9047QUg3"

                    };

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    var response = client.PostAsJsonAsync(loginFullURL, datatobeSent).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        res = response.Content.ReadAsAsync<LoginResponse>().Result;
                    }
                    responses.Message += "SF connection established....\n";
                }
                using (var client = new HttpClient())
                {
                    responses.Message += "SF sync started....\n";                   
                    client.BaseAddress = new Uri(instanceURL + "?q=select  Id, name, Account.name, account.id, Account.BillingStreet,Account.BillingCity,Account.BillingState,Account.BillingPostalCode,Account.BillingCountry,Description,StageName, amount,closedate,Campaign.id, Campaign.name,FiscalQuarter, fiscal, npe01__Number_of_Payments__c,npe01__Payments_Made__c,Campaign_Code__c,bde__Batch__c,npsp__Batch__c, Donation_Type__c,Deposit_Amount__c, Probability,isclosed,iswon from Opportunity where CloseDate >=  2021-04-01  and CloseDate <= 2021-04-30  and isclosed=true and iswon=true order by CloseDate Asc");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(res.token_type, res.access_token);

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    var response = client.GetAsync(instanceURL + "?q=select  Id, name, Account.name, account.id, Account.BillingStreet,Account.BillingCity,Account.BillingState,Account.BillingPostalCode,Account.BillingCountry,Description,StageName, amount,closedate,Campaign.id, Campaign.name,FiscalQuarter, fiscal, npe01__Number_of_Payments__c,npe01__Payments_Made__c,Campaign_Code__c,bde__Batch__c,npsp__Batch__c, Donation_Type__c,Deposit_Amount__c, Probability,isclosed,iswon from Opportunity where CloseDate >=  2021-04-01  and CloseDate <= 2021-04-30  and isclosed=true and iswon=true order by CloseDate Asc").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var jsonString = response.Content.ReadAsStringAsync();
                        var objData = JsonConvert.DeserializeObject<Root>(jsonString.Result);
                        if (objData.records?.Count > 0)
                        {
                            foreach (var transaction in objData.records)
                            {                               
                                    int InvoiceID = 0;
                                    using (SqlCommand cmd = new SqlCommand("SFInvoice_Insert_MOW", cnn))
                                    {
                                        try
                                        {
                                            cmd.CommandType = CommandType.StoredProcedure;
                                            cmd.Parameters.Add("CustomerRefListID", SqlDbType.VarChar).Value = transaction.Account.Id;
                                            cmd.Parameters.Add("CustomerRefFullName", SqlDbType.VarChar).Value = transaction.Account.Name;                                            
                                            cmd.Parameters.Add("IitemLIstID", SqlDbType.VarChar).Value = transaction.Campaign.Id;
                                            cmd.Parameters.Add("ItemRefFullName", SqlDbType.VarChar).Value = transaction.Campaign.Name;
                                            cmd.Parameters.Add("TxnDate", SqlDbType.VarChar).Value = transaction.CloseDate;
                                            cmd.Parameters.Add("RefNumber", SqlDbType.VarChar).Value = transaction.Description;
                                            cmd.Parameters.Add("Memo", SqlDbType.VarChar).Value = transaction.Donation_Type__c;
                                            cmd.Parameters.Add("DueDate", SqlDbType.VarChar).Value = transaction.CloseDate;
                                            cmd.Parameters.Add("Amount", SqlDbType.VarChar).Value = transaction.Amount;
                                            cmd.Parameters.Add("TotalAmount", SqlDbType.VarChar).Value = transaction.Deposit_Amount__c;
                                            cmd.Parameters.Add("BillAddressState", SqlDbType.VarChar).Value = transaction.Account.BillingState;
                                            cmd.Parameters.Add("BillAddressPostalCode", SqlDbType.VarChar).Value = transaction.Account.BillingPostalCode;
                                            cmd.Parameters.Add("BillAddressCountry", SqlDbType.VarChar).Value = transaction.Account.BillingCountry;
                                            cmd.Parameters.Add("BillAddressCity", SqlDbType.VarChar).Value = transaction.Account.BillingCity;
                                            cmd.Parameters.Add("BillAddressAddr1", SqlDbType.VarChar).Value = transaction.Account.BillingStreet;                                       
                                            cmd.Parameters.Add("SFReferance", SqlDbType.VarChar).Value = transaction.Id;
                                            cmd.Parameters.Add("CompanyID", SqlDbType.Int).Value = Credentials.Get.CompanyId;
                                            InvoiceID = (int)cmd.ExecuteScalar();
                                            responses.Message += "Invoice for customer " + transaction.Account.Name + ", SFReferance " + transaction.Id + ", with amount $" + transaction.Amount + " inserted into DB.\n";                                            
                                        }
                                        catch(Exception ex)
                                        {
                                            responses.Message += "Invoice for customer " + transaction.Account.Name + ", SFReferance " + transaction.Id + ", with amount $" + transaction.Amount + " was not added to DB because: "+ ex.Message.ToString() + "\n"; 
                                            MailSender.Send(Settings.Default.MAILTO, Credentials.Get.CompanyId + ": Error in SF Invoice Pull " + Credentials.SyncDateTime.ToString("MM-dd-yyyy HH:mm"), responses.Message);
                                        }
                                    }                                    
                               
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                responses.Message += ex.Message.ToString();
                MailSender.Send(Settings.Default.MAILTO, Credentials.Get.CompanyId + ": Error in SF Invoice Pull " + Credentials.SyncDateTime.ToString("MM-dd-yyyy HH:mm"), responses.Message);
            }
            return responses;
        }


        public class Attributes
        {
            public string type { get; set; }
            public string url { get; set; }
        }

        public class Account
        {
            public Attributes attributes { get; set; }
            public string Name { get; set; }
            public string Id { get; set; }
            public string BillingStreet { get; set; }
            public string BillingCity { get; set; }
            public string BillingState { get; set; }
            public string BillingPostalCode { get; set; }
            public object BillingCountry { get; set; }
        }

        public class Campaign
        {
            public Attributes attributes { get; set; }
            public string Id { get; set; }
            public string Name { get; set; }
        }
        
        public class Record
        {
            public Attributes attributes { get; set; }
            public string Id { get; set; }
            public string Name { get; set; }
            public Account Account { get; set; }
            public string Description { get; set; }
            public string StageName { get; set; }
            public double Amount { get; set; }
            public string CloseDate { get; set; }
            public Campaign Campaign { get; set; }
            public int FiscalQuarter { get; set; }
            public string Fiscal { get; set; }
            public double npe01__Number_of_Payments__c { get; set; }
            public double npe01__Payments_Made__c { get; set; }
            public object bde__Batch__c { get; set; }
            public string Campaign_Code__c { get; set; }
            public object npsp__Batch__c { get; set; }
            public object Donation_Type__c { get; set; }
           
            public double? Deposit_Amount__c { get; set; }
            public double Probability { get; set; }
            public bool IsClosed { get; set; }
            public bool IsWon { get; set; }
        }

        public class Root
        {
            public int totalSize { get; set; }
            public bool done { get; set; }
            public List<Record> records { get; set; }
        }
    }
}
