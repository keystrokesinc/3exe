﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.SqlClient;
using DataProcessor;
using DataProcessor.QB.XMLProcessor;
using DataProcessor.Sync.ModelRet;
using SBTSync.Properties;

namespace SBTSync.DataBase.DAL
{
 public   class ReceivePayment:DBBase
    {
     public static GlobalValue.Reponse ProcessReceivePayment(Action<ProgressEventArgs> onProgressChanged)
     {
         
         GlobalValue.Reponse responses = new GlobalValue.Reponse();
         QBXReceivePayment receivePayment = new QBXReceivePayment();

         DateTime? qbReceivePaymentLastMod = GetLastModified("RECPAY");

         GlobalValue.Eventlogupdate("Getting ReceivePayments", null);
         List<ReceivePaymentRet> receivePaymentRets = receivePayment.GetReceivePayment(qbReceivePaymentLastMod);

         foreach (ReceivePaymentRet receivePaymentRet in receivePaymentRets )
         {
             try
             {
                    ISingleResult<ReceivePayment_SelectResult> receivePaymentSelectResults = DataContext.ReceivePayment_Select(receivePaymentRet.TxnID);
                    List<ReceivePayment_SelectResult> selectResults = new List<ReceivePayment_SelectResult>(receivePaymentSelectResults);

                 if (selectResults.Count == 0)
                 {
                     DataContext.ReceivePayment_Insert(receivePaymentRet.TxnID, receivePaymentRet.TimeCreated, receivePaymentRet.TimeModified,
                         receivePaymentRet.CustomerRef.ListID, receivePaymentRet.ARAccountRef.ListID, receivePaymentRet.TxnDate, receivePaymentRet.RefNumber,
                         receivePaymentRet.TotalAmount, (receivePaymentRet.PaymentMethodRef != null ? receivePaymentRet.PaymentMethodRef.ListID : ""), receivePaymentRet.Memo, receivePaymentRet.DepositToAccountRef.ListID,
                         receivePaymentRet.UnusedPayment, receivePaymentRet.UnusedCredits, Credentials.Get.CompanyId);
                     GlobalValue.Eventlogupdate("ReceivePayment having RefNumber " + receivePaymentRet.RefNumber + " Inserted", null);

                     foreach (ReceivePaymentRetAppliedToTxnRet receivePaymentRetExpenseLineRet in receivePaymentRet.AppliedToTxnRet)
                     {
                         DataContext.ReceivePayment_AppliedTransaction_Insert(receivePaymentRet.TxnID, receivePaymentRetExpenseLineRet.TxnID, 
                             receivePaymentRetExpenseLineRet.TxnType, receivePaymentRetExpenseLineRet.TxnDate, receivePaymentRetExpenseLineRet.RefNumber, 
                             receivePaymentRetExpenseLineRet.BalanceRemaining,
                             receivePaymentRetExpenseLineRet.Amount);
                     }
                     GlobalValue.Eventlogupdate("ReceivePayment having Child Records count: " + receivePaymentRet.AppliedToTxnRet.Length + " Inserted", null);
                 }
                 else
                 {
                     DataContext.ReceivePayment_Insert(receivePaymentRet.TxnID, receivePaymentRet.TimeCreated, receivePaymentRet.TimeModified,
                           receivePaymentRet.CustomerRef.ListID, receivePaymentRet.ARAccountRef.ListID, receivePaymentRet.TxnDate, receivePaymentRet.RefNumber,
                           receivePaymentRet.TotalAmount, (receivePaymentRet.PaymentMethodRef != null ? receivePaymentRet.PaymentMethodRef.ListID : ""), receivePaymentRet.Memo, receivePaymentRet.DepositToAccountRef.ListID,
                           receivePaymentRet.UnusedPayment, receivePaymentRet.UnusedCredits, Credentials.Get.CompanyId);
                     GlobalValue.Eventlogupdate("ReceivePayment having RefNumber " + receivePaymentRet.RefNumber + " Inserted", null);

                     foreach (ReceivePaymentRetAppliedToTxnRet receivePaymentRetExpenseLineRet in receivePaymentRet.AppliedToTxnRet)
                     {
                         DataContext.ReceivePayment_AppliedTransaction_Insert(receivePaymentRet.TxnID, receivePaymentRetExpenseLineRet.TxnID,
                             receivePaymentRetExpenseLineRet.TxnType, receivePaymentRetExpenseLineRet.TxnDate, receivePaymentRetExpenseLineRet.RefNumber,
                             receivePaymentRetExpenseLineRet.BalanceRemaining,
                             receivePaymentRetExpenseLineRet.Amount);
                     }
                     GlobalValue.Eventlogupdate("ReceivePayment having Child Records count: " + receivePaymentRet.AppliedToTxnRet.Length + " Updated", null);
                 }

                 responses.RecordCount++;
             }
             catch (Exception ex)
             {
                    GlobalValue.Eventlogupdate(ex, null, "Error");
                    //   richTextBox1.AppendText(ex.Message + "\n");
                }
         }
         return responses;
     }

     public static void DeletePayment()
     {
         
     }
    }
}
