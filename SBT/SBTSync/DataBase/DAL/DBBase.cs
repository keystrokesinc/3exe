﻿using System;
using DataProcessor;
using SBTSync.Properties;
using System.Data.Linq;

namespace SBTSync.DataBase.DAL
{
    public class DBBase
    {
        protected static readonly QBAUTOUPDATEDataContext DataContext = new QBAUTOUPDATEDataContext(Credentials.Get.ConnectionString);

        protected static DateTime? GetLastModified(string entity)
        {
            GlobalValue.Eventlogupdate("Getting last modifed date", null);

            //  return null;
            DateTime? qbAccountLastMod = DataContext.QBAccount_LastMod(entity, Credentials.Get.CompanyId);

            GlobalValue.Eventlogupdate("Last modifed date of " + entity + ":" + (qbAccountLastMod.HasValue ? qbAccountLastMod.Value.ToString() : "NULL"), null);

            return qbAccountLastMod;
        }

        public static string GetCompanyName(int companyId)
        {
            ISingleResult<Company_SelectResult> company = DataContext.Company_Select(companyId);
            foreach (var i in company)
                return i.CompanyName;
            return "";
        }
        public static Company_SelectResult GetCompanyInfo(int companyId)
        {
            ISingleResult<Company_SelectResult> company = DataContext.Company_Select(companyId);
            foreach (var i in company)
                return i;

            return null;

        }
    }


}
