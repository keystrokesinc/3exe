﻿using System;
using System.Collections.Generic;
using DataProcessor;
using DataProcessor.QB.XMLProcessor;
using DataProcessor.Sync.ModelRet;
using System.Data.Linq;
using System.Data.SqlClient;
using DataProcessor.Sync.ModelAdd;
using DataProcessor.Sync;
using SBTSync.Properties;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using Newtonsoft.Json;
using System.Data;

namespace SBTSync.DataBase.DAL
{
    public class SFPayment : DBBase
    {

        public static int insertPaymentCount = 0;
        public static GlobalValue.Reponse ProcessSFPayment(Action<ProgressEventArgs> onProgressChanged)
        {

            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            QBXPaymentMethod payment = new QBXPaymentMethod();

            DateTime? qbInvoiceLastMod = GetLastModified("Payment");

            GlobalValue.Eventlogupdate("Getting Payments", null);


            ISingleResult<Logs_SelectResult> paymentSelectResults = DataContext.SFPayment_Select_AddQuickBooks(Credentials.Get.CompanyId);
            List<Logs_SelectResult> selectResults = new List<Logs_SelectResult>(paymentSelectResults);
            insertPaymentCount = 0;
            if (selectResults.Count > 0)
            {
                foreach (Logs_SelectResult invoiceSelectResult in selectResults)
                {

                    GlobalValue.Reponse res = new GlobalValue.Reponse();
                    try
                    {
                        if (invoiceSelectResult.CustomerID == null)
                        {
                            QBXCustomer qBXCustomer = new QBXCustomer();
                            IList<CustomerRet> customerRets = qBXCustomer.SearchCustomer(invoiceSelectResult.Customer);
                            if (customerRets?.Count > 0)
                                invoiceSelectResult.CustomerID = customerRets[0].ListID;
                            else
                                invoiceSelectResult.CustomerID = Customer.insertCustomerToQB(invoiceSelectResult); //error need to handle
                        }
                        res = SavePayment(invoiceSelectResult, payment);
                    }
                    catch (Exception ex)
                    {
                        GlobalValue.Eventlogupdate(ex, null, "Error");
                    }
                }
            }
            onProgressChanged(new ProgressEventArgs
            {
                Completed = true
            });
            if (insertPaymentCount > 0)
                responses.Message = $"{insertPaymentCount} Payment(s) added to quickbooks\n";
            return responses;
        }
        public static GlobalValue.Reponse SavePayment(Logs_SelectResult paySelectAddQuickBooksResult, QBXPaymentMethod payment)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            try
            {
                ReceivePaymentAdd paymentaddRq = new ReceivePaymentAdd
                {
                    CustomerRef = new DataProcessor.Sync.ModelAdd.ReceivePaymentRetCustomerRef
                    {
                        //ListID = paySelectAddQuickBooksResult.CustomerID,
                        FullName = paySelectAddQuickBooksResult.Customer
                    },
                    TotalAmount = string.Format("{0:0.00}", paySelectAddQuickBooksResult.Amount),
                    IsAutoApply = false,


                };
                if (!string.IsNullOrEmpty(paySelectAddQuickBooksResult.Memo))
                    paymentaddRq.Memo = paySelectAddQuickBooksResult.Memo + " " + paySelectAddQuickBooksResult.PaymentID;
                if (!string.IsNullOrEmpty(paySelectAddQuickBooksResult.RefNumber))
                    paymentaddRq.RefNumber = paySelectAddQuickBooksResult.RefNumber;
                if (!string.IsNullOrEmpty(paySelectAddQuickBooksResult.TxnDate))
                    paymentaddRq.TxnDate = paySelectAddQuickBooksResult.TxnDate;
                List<GlobalValue.Reponse> savePayment = payment.SavePayment(paymentaddRq);
                insertPaymentCount++;
                if (savePayment.Count > 0 && !savePayment[0].IsError)
                {
                    var checkSerializeXml = SyncConstant.SerializeXml<ReceivePaymentRet>(savePayment[0].EntityObject.ToString(), QueryXML.ReceivePayment, false);
                    if (checkSerializeXml.Count > 0)
                    {
                        DataContext.UpdateLogPayment_afterSync(paySelectAddQuickBooksResult.ID, Convert.ToInt32(checkSerializeXml[0].TxnNumber));
                        GlobalValue.Eventlogupdate("Payment having TxnNumber " + checkSerializeXml[0].TxnNumber + " inserted to database", null);
                        responses.IsError = false;
                        responses.Message = checkSerializeXml[0].TxnID;
                    }
                }


                if (savePayment[0].IsError)
                {
                    responses.IsError = true;
                    string resMsg = "Error!- Payment for customer " + paySelectAddQuickBooksResult.Customer + ",\n PaymentID " +
                              paySelectAddQuickBooksResult.PaymentID + ", with amount $" +
                              paySelectAddQuickBooksResult.Amount + ", was not added to QuickBooks because: ";
                    responses.Message += resMsg + savePayment[0].Message + "\n";
                    GlobalValue.Eventlogupdate(responses.Message, null, "Error");
                    //  MailSender.Send(Settings.Default.MAILTO, Credentials.Get.CompanyId + ": Error in Payment Sync " + Credentials.SyncDateTime.ToString("MM-dd-yyyy HH:mm"), responses.Message);
                    return responses;

                }
            }
            catch (Exception ex)
            {
                responses.IsError = true;
                string resMsg = "Error!- Payment for customer " + paySelectAddQuickBooksResult.Customer + ",\n PaymentID " +
                              paySelectAddQuickBooksResult.PaymentID + ", with amount $" +
                              paySelectAddQuickBooksResult.Amount + ", was not added to QuickBooks because: ";
                responses.Message += resMsg + ex.Message + "\n";
                GlobalValue.Eventlogupdate(responses.Message, null, "Error");

            }
            return responses;
        }
        class ApiSendData
        {
            public string grant_type { get; set; }
            public string client_id { get; set; }
            public string client_secret { get; set; }
            public string username { get; set; }
            public string password { get; set; }
        }
        public class LoginResponse
        {
            public string access_token { get; set; }
            public string instance_url { get; set; }
            public string id { get; set; }
            public string token_type { get; set; }
            public string issued_at { get; set; }
            public string signature { get; set; }
        }
        class UpdateSFClass
        {
            public string sfId { get; set; }
            public string qbMemoid { get; set; }
            public string qbError { get; set; }
        }
        private static string UpdateSalesforceData(string SFReferance, string QBID, string QBErrorMsg)
        {
            LoginResponse res = null;
            string resStr = "";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("https://login.salesforce.com/services/oauth2/token?");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //**LIVE SALES FORCE LOGIN**//
                    var datatobeSent = new ApiSendData()
                    {
                        grant_type = "password",
                        client_id = "3MVG9oNqAtcJCF.EGnjKYNbXn57OdNsZyw8kuhVd0x__LMZvRMaHMIDxWGSqS.FzMFNs2_tVu6N74OZvwB3j4",
                        client_secret = "F106A47159B0AD459C7378BB43D21D2AA2BE92155B3C5B1DD1C0A571DD0AE1C3",
                        username = "dharris@parkinsonvoiceproject.org",
                        password = "Park@@321rT6YfmG6YlY3liA6P12EQtClJ"
                    };

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    var response = client.PostAsJsonAsync("https://login.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9oNqAtcJCF.EGnjKYNbXn57OdNsZyw8kuhVd0x__LMZvRMaHMIDxWGSqS.FzMFNs2_tVu6N74OZvwB3j4&client_secret=F106A47159B0AD459C7378BB43D21D2AA2BE92155B3C5B1DD1C0A571DD0AE1C3&username=dharris@parkinsonvoiceproject.org&password=Park@@321rT6YfmG6YlY3liA6P12EQtClJ", datatobeSent).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        res = response.Content.ReadAsAsync<LoginResponse>().Result;
                    }
                }

                //Update Salesforce
                using (var client = new HttpClient())
                {

                    client.BaseAddress = new Uri("https://pvp.my.salesforce.com/services/apexrest/updatememoid");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(res.token_type, res.access_token);
                    var datatobeSent = new UpdateSFClass()
                    {
                        sfId = SFReferance,
                        qbMemoid = QBID,
                        qbError = QBErrorMsg
                    };
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    var response = client.PostAsJsonAsync("https://pvp.my.salesforce.com/services/apexrest/updatememoid", datatobeSent).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        resStr = "QBID: " + QBID + "updated to Salesforce.";
                    }
                }
                return resStr;
            }
            catch (Exception ex)
            {
                resStr = ex.Message;
                return resStr;
            }
        }


        //**PULL Payment Data from SF
        public static GlobalValue.Reponse PullSFPayment(Action<ProgressEventArgs> onProgressChanged)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            string connetionString = Credentials.Get.ConnectionString;
            SqlConnection cnn = new SqlConnection(connetionString);
            try
            {
                LoginResponse res = null;
                cnn.Open();
                string loginURL = "https://pvp--qbtesting.my.salesforce.com/services/oauth2/token?";
                string loginFullURL = "https://pvp--qbtesting.my.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9Nc1qcZ7BbZ0a4Q2fhHoqlj9I2wvpEGk0two3SD9maCoL.iFSnSEUBEwTrPvsRaRdZYNz0FGeFLudcZWL&client_secret=42BC9E36CDD596425B7177AEFEDD0668BCCCDE01C91A2319B1D57E97FCDC92A9&username=dharris@parkinsonvoiceproject.org.qbtesting&password=@!~Park123";
                string instanceURL = "https://pvp--qbtesting.my.salesforce.com/services/apexrest/";

                //**LIVE URL**//
                //string loginURL = "https://login.salesforce.com/services/oauth2/token?";
                //string loginFullURL = "https://login.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9oNqAtcJCF.EGnjKYNbXn57OdNsZyw8kuhVd0x__LMZvRMaHMIDxWGSqS.FzMFNs2_tVu6N74OZvwB3j4&client_secret=F106A47159B0AD459C7378BB43D21D2AA2BE92155B3C5B1DD1C0A571DD0AE1C3&username=dharris@parkinsonvoiceproject.org&password=Park@@321rT6YfmG6YlY3liA6P12EQtClJ";
                //string instanceURL = "https://pvp.my.salesforce.com/services/apexrest/";
                string startDate = Settings.Default.StartDate != null ? Settings.Default.StartDate.ToString() : DateTime.Now.ToString("dd-MM-yyyy");
                string endDate = Settings.Default.EndDate != null ? Settings.Default.EndDate.ToString() : DateTime.Now.ToString("dd-MM-yyyy");
                string batchName = Settings.Default.BatchName;
                responses.Message += "Initializing connection....\n";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(loginURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    // HTTP POST Sales Force Login
                    var datatobeSent = new ApiSendData()
                    {
                        grant_type = "password",
                        client_id = "3MVG9Nc1qcZ7BbZ0a4Q2fhHoqlj9I2wvpEGk0two3SD9maCoL.iFSnSEUBEwTrPvsRaRdZYNz0FGeFLudcZWL",
                        client_secret = "42BC9E36CDD596425B7177AEFEDD0668BCCCDE01C91A2319B1D57E97FCDC92A9",
                        username = "dharris@parkinsonvoiceproject.org.qbtesting",
                        password = "@!~Park123"

                        //**FOR LIVE **//
                        //grant_type = "password",
                        //client_id = "3MVG9oNqAtcJCF.EGnjKYNbXn57OdNsZyw8kuhVd0x__LMZvRMaHMIDxWGSqS.FzMFNs2_tVu6N74OZvwB3j4",
                        //client_secret = "F106A47159B0AD459C7378BB43D21D2AA2BE92155B3C5B1DD1C0A571DD0AE1C3",
                        //username = "dharris@parkinsonvoiceproject.org",
                        //password = "Park@@321rT6YfmG6YlY3liA6P12EQtClJ"
                    };

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    var response = client.PostAsJsonAsync(loginFullURL, datatobeSent).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        res = response.Content.ReadAsAsync<LoginResponse>().Result;
                    }
                    responses.Message += "SF connection established....\n";
                }
                using (var client = new HttpClient())
                {
                    responses.Message += "SF sync started....\n";
                    client.BaseAddress = new Uri(instanceURL + "customerTransactions?SearchBy=all&filterBy=cd&fd=" + startDate + "&td=" + endDate);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(res.token_type, res.access_token);

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    var response1 = client.GetAsync(instanceURL + "customerTransactions?SearchBy=all&filterBy=cd&fd=" + startDate + "&td=" + endDate).Result;
                    if (response1.IsSuccessStatusCode)
                    {
                        var jsonString = response1.Content.ReadAsStringAsync();
                        var objData = JsonConvert.DeserializeObject<SFSalesReceiptAdd.RootObject1>(jsonString.Result);
                        foreach (var row in objData.invoiceLinkedTxns)
                        {
                            if (row.ClassRefFullName != null && row.TxnID == null)//&& row.donationImportbatch == "2019_09_12 Checks"
                            {
                                if (row.ClassRefFullName.Contains("  "))
                                    row.ClassRefFullName = row.ClassRefFullName.Replace("  ", " ");                                
                                        using (SqlCommand cmd = new SqlCommand("SFPayment_Insert", cnn))
                                        {
                                            try
                                            {
                                                cmd.CommandType = CommandType.StoredProcedure;
                                                cmd.Parameters.Add("CustomerRefListID", SqlDbType.VarChar).Value = row.CustomerRefListID != null ? row.CustomerRefListID : null;
                                                cmd.Parameters.Add("CustomerRefFullName", SqlDbType.VarChar).Value = row.CustomerRefFullName != null ? row.CustomerRefFullName : null;
                                                cmd.Parameters.Add("ClassRefListID", SqlDbType.VarChar).Value = row.ClassRefListID.Contains("\t") ? row.ClassRefListID.Replace("\t", " ") : row.ClassRefListID;
                                                cmd.Parameters.Add("ClassRefFullName", SqlDbType.VarChar).Value = row.ClassRefFullName.Contains("\t") ? row.ClassRefFullName.Replace("\t", " ") : row.ClassRefFullName;
                                                cmd.Parameters.Add("DepositToAccountRefListID", SqlDbType.VarChar).Value = row.ARAccountRefListID != null ? row.ARAccountRefListID : null;
                                                cmd.Parameters.Add("DepositToAccountRefFullName", SqlDbType.VarChar).Value = row.ARAccountRefFullName != null ? row.ARAccountRefFullName : null;
                                                cmd.Parameters.Add("TxnDate", SqlDbType.VarChar).Value = row.TxnDate != null ? row.TxnDate : null;
                                                cmd.Parameters.Add("RefNumber", SqlDbType.VarChar).Value = row.RefNumber != null ? row.RefNumber : null;
                                                cmd.Parameters.Add("SFReferance", SqlDbType.VarChar).Value = row.SFReferance != null ? row.SFReferance : null;
                                                cmd.Parameters.Add("TotalAmount", SqlDbType.VarChar).Value = row.TotalAmount != null ? row.TotalAmount : null;
                                                cmd.Parameters.Add("RecordtypeName", SqlDbType.VarChar).Value = row.RecordtypeName != null ? row.RecordtypeName : null;                                                
                                                cmd.Parameters.Add("TxnID", SqlDbType.VarChar).Value = row.TxnID != null ? row.TxnID : null;
                                                cmd.Parameters.Add("OpportunityStage", SqlDbType.VarChar).Value = row.OpportunityStage != null ? row.OpportunityStage : null;
                                                cmd.Parameters.Add("DateOfDonation", SqlDbType.VarChar).Value = row.DateOfDonation != null ? row.DateOfDonation : null;
                                                cmd.Parameters.Add("CustSFReferance", SqlDbType.VarChar).Value = row.customer.SFReferance != null ? row.customer.SFReferance : null;
                                                cmd.Parameters.Add("Salutation", SqlDbType.VarChar).Value = row.customer.Salutation != null ? row.customer.Salutation : null;
                                                cmd.Parameters.Add("Name", SqlDbType.VarChar).Value = row.customer.Name != null ? row.customer.Name : null;
                                                cmd.Parameters.Add("LastName", SqlDbType.VarChar).Value = row.customer.LastName != null ? row.customer.LastName : null;
                                                cmd.Parameters.Add("FirstName", SqlDbType.VarChar).Value = row.customer.FirstName != null ? row.customer.FirstName : null;
                                                cmd.Parameters.Add("BillAddressState", SqlDbType.VarChar).Value = row.customer.BillAddressState != null ? row.customer.BillAddressState : null;
                                                cmd.Parameters.Add("BillAddressPostalCode", SqlDbType.VarChar).Value = row.customer.BillAddressPostalCode != null ? row.customer.BillAddressPostalCode : null;
                                                cmd.Parameters.Add("BillAddressCountry", SqlDbType.VarChar).Value = row.customer.BillAddressCountry != null ? row.customer.BillAddressCountry : null;
                                                cmd.Parameters.Add("BillAddressCity", SqlDbType.VarChar).Value = row.customer.BillAddressCity != null ? row.customer.BillAddressCity : null;
                                                cmd.Parameters.Add("BillAddressAddr1", SqlDbType.VarChar).Value = row.customer.BillAddressAddr1 != null ? row.customer.BillAddressAddr1 : null;
                                                cmd.Parameters.Add("CompanyID", SqlDbType.Int).Value = Credentials.Get.CompanyId;
                                                cmd.Parameters.Add("LinktoTxnID1", SqlDbType.VarChar).Value = row.LinktoTxnID1 != null ? row.LinktoTxnID1 : null;
                                                cmd.Parameters.Add("InvoiceId", SqlDbType.VarChar).Value = row.InvoiceId != null ? row.InvoiceId : null;
                                                cmd.Parameters.Add("Memo", SqlDbType.VarChar).Value = row.QuickbooksmemoId != null ? row.QuickbooksmemoId : null;
                                                cmd.Parameters.Add("PaymentMethod", SqlDbType.VarChar).Value = row.paymentMethod != null ? row.paymentMethod : null;
                                                cmd.ExecuteNonQuery();
                                                responses.Message += "Payment for customer " + row.customer.FirstName + " " + row.customer.LastName + ", SFReferance " + row.SFReferance + ", with amount $" + row.TotalAmount + " inserted into DB.\n";
                                                // responses.Message += row.customer.FirstName + " " + row.customer.LastName + " inserted into DB.\n";

                                            }
                                            catch (Exception ex)
                                            {
                                                responses.Message += "Payment for customer " + row.customer.FirstName + " " + row.customer.LastName + ", SFReferance " + row.SFReferance + ", with amount $" + row.TotalAmount + " , was not added to DB because:" + ex.Message + "\n";
                                                MailSender.Send(Settings.Default.MAILTO, "Error in SF Payment Pull " + Credentials.SyncDateTime.ToString("MM-dd-yyyy HH:mm"), responses.Message);
                                            }
                                        }                                    
                                }
                               

                            }
                        }
                    }
                
            }
            catch (Exception ex)
            {
                responses.Message += ex.Message.ToString();
            }
            return responses;
        }

    }
}
