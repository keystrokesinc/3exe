﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Linq;
using DataProcessor;
using DataProcessor.QB.XMLProcessor;
using DataProcessor.Sync;
using DataProcessor.Sync.ModelRet;
using SBTSync.Properties;
using System.Text.RegularExpressions;
using DataProcessor.Ref;
using DataProcessor.Sync.ModelAdd;

namespace SBTSync.DataBase.DAL
{
    public class Bill : DBBase
    {
        private static string errMsg;

        public static GlobalValue.Reponse ProcessBill(Action<ProgressEventArgs> onProgressChanged)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            responses.Message = "";
            QBXBill bill = new QBXBill();
            QBXCheque cheque = new QBXCheque();
            QBXBillPayment billPayment = new QBXBillPayment();
            QBXCreditCard qbxCreditCard = new QBXCreditCard();
            //DateTime? qbBillLastMod = GetLastModified("Bill");
            // List<BillRet> billRets = bill.GetAllBills(qbBillLastMod);

            var addCount = 0;
            var updateCount = 0;
            errMsg = "";

            {
                try
                {
                    //List<BillRet> billRets = (new QBXBill()).GetAllBills(null);
                    //List<CheckRet> checkRets = (new QBXCheque()).GetAllChecks(null);

                    ISingleResult<Bill_Select_AddQuickBooksResult> billSelectResults = DataContext.Bill_Select_AddQuickBooks(Credentials.Get.CompanyId);
                    List<Bill_Select_AddQuickBooksResult> selectResults = new List<Bill_Select_AddQuickBooksResult>(billSelectResults);

                    ISingleResult<Check_Select_AddQuickBooksResult> checkSelectResults = DataContext.Check_Select_AddQuickBooks(Credentials.Get.CompanyId);
                    List<Check_Select_AddQuickBooksResult> lstChecks = new List<Check_Select_AddQuickBooksResult>(checkSelectResults);

                    List<Check_Select_AddQuickBooksResult> lstCreditCards = lstChecks.Where(c => c.RefNumber.ToLower() == "creditcard").ToList();
                    lstChecks = lstChecks.Where(c => c.RefNumber.ToLower() != "creditcard").ToList();

  
                    //Need to Check Bills and see if any vendors needed to be added, if it finds any it will add the vendor first and then add the bills
                    if ((selectResults.Any(item => item.VendorRefListID == null)) ||
                        (lstChecks.Any(item => item.VendorRefListID == null)) || (lstCreditCards.Any(item => item.VendorRefListID == null)))
                    {
                        responses.Message = Vendor.AddVendor().Message;
                        billSelectResults.Dispose();
                        billSelectResults = DataContext.Bill_Select_AddQuickBooks(Credentials.Get.CompanyId);
                        selectResults = new List<Bill_Select_AddQuickBooksResult>(billSelectResults);
                        checkSelectResults.Dispose();
                        checkSelectResults = DataContext.Check_Select_AddQuickBooks(Credentials.Get.CompanyId);
                        lstChecks = new List<Check_Select_AddQuickBooksResult>(checkSelectResults);
                        lstCreditCards = lstChecks.Where(c => c.RefNumber.ToLower() == "creditcard").ToList();
                        lstChecks = lstChecks.Where(c => c.RefNumber.ToLower() != "creditcard").ToList();
                    }

                    foreach (Check_Select_AddQuickBooksResult checkSelectAddQuickBooksResult in lstChecks)
                    {
                        GlobalValue.Reponse res = new GlobalValue.Reponse();
                        try
                        {
                            res = SaveCheck(checkSelectAddQuickBooksResult, bill, billPayment);

                            if (!res.IsError)
                            {
                                DataContext.Bill_UPDATE_TXNID(res.Message, checkSelectAddQuickBooksResult.BillID.ToString(), Credentials.Get.CompanyId, true);
                                GlobalValue.Eventlogupdate("Bill having Bill ID  " + checkSelectAddQuickBooksResult.BillID.ToString() + " Updated", null);
                                updateCount++;
                            }
                            else
                            {
                                errMsg += res.Message + Environment.NewLine;
                            }
                        }
                        catch (Exception ex)
                        {
                            GlobalValue.Eventlogupdate("Bill_UPDATE_TXNID: " + ex.Message, null, "Error");
                            errMsg += ex.Message + Environment.NewLine;
                        }
                    }

                    foreach (Check_Select_AddQuickBooksResult checkSelectAddQuickBooksResult in lstCreditCards)
                    {
                        GlobalValue.Reponse res = new GlobalValue.Reponse();
                        try
                        {
                            res = SaveCreditCard(checkSelectAddQuickBooksResult, qbxCreditCard);

                            if (!res.IsError)
                            {
                                DataContext.Bill_UPDATE_TXNID(res.Message, checkSelectAddQuickBooksResult.BillID.ToString(), Credentials.Get.CompanyId, true);
                                GlobalValue.Eventlogupdate("Bill having Bill ID  " + checkSelectAddQuickBooksResult.BillID.ToString() + " Updated", null);
                                updateCount++;
                            }
                            else
                            {
                                errMsg += res.Message + Environment.NewLine;
                            }
                        }
                        catch (Exception ex)
                        {
                            GlobalValue.Eventlogupdate("Bill_UPDATE_TXNID: " + ex.Message, null, "Error");
                            errMsg += ex.Message + Environment.NewLine;
                        }
                    }


                    foreach (Bill_Select_AddQuickBooksResult billSelectAddQuickBooksResult in selectResults)
                    {

                        GlobalValue.Reponse res = new GlobalValue.Reponse();
                        try
                        {
                            res = SaveBill(billSelectAddQuickBooksResult, bill);
                            if (!res.IsError)
                            {
                                DataContext.Bill_UPDATE_TXNID(res.Message, billSelectAddQuickBooksResult.BillID.ToString(), Credentials.Get.CompanyId, false);
                                GlobalValue.Eventlogupdate("Bill having Bill ID  " + billSelectAddQuickBooksResult.BillID.ToString() + " Updated", null);
                                updateCount++;
                            }
                            else
                            {
                                errMsg += res.Message + Environment.NewLine;
                            }
                        }
                        catch (Exception ex)
                        {
                            GlobalValue.Eventlogupdate("Bill_UPDATE_TXNID: " + ex.Message, null, "Error");
                            errMsg += ex.Message + Environment.NewLine;
                        }
                    }


                    responses.RecordCount++;

                    onProgressChanged(new ProgressEventArgs
                    {
                        Total = selectResults.Count,
                        Current = addCount + updateCount
                    });
                }
                catch (SqlException ex)
                {
                    GlobalValue.Eventlogupdate(ex, null, "Error");
                    errMsg += ex.Message + Environment.NewLine;
                }
            }

            onProgressChanged(new ProgressEventArgs
            {
                Completed = true
            });


            if (!string.IsNullOrEmpty(errMsg))
            {
                responses.Message += "Error: " + errMsg + Environment.NewLine ;
                MailSender.Send(Settings.Default.MAILTO, Credentials.Get.CompanyId + " : Error in Bill Sync " + Credentials.SyncDateTime.ToString("MM-dd-yyyy HH:mm"), errMsg);
            }
            else
            {
                DataContext.Settings_InsertUpdate("Bill", DateTime.UtcNow, Credentials.Get.CompanyId);
            }

              responses.Message += updateCount + " Bill(s) updated in database.";

            return responses;
        }
        private static GlobalValue.Reponse SaveBill(Bill_Select_AddQuickBooksResult billSelectAddQuickBooksResult, QBXBill bill)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            try
            {
                BillAdd billadd = new BillAdd
                {
                    VendorRef = new BillAddVendorRef
                    {
                        ListID = billSelectAddQuickBooksResult.VendorRefListID,
                        FullName = billSelectAddQuickBooksResult.VendorName,
                    },
                    APAccountRef = new BillAddAPAccountRef { ListID = billSelectAddQuickBooksResult.APAccountRefListID, FullName = billSelectAddQuickBooksResult.APAccountName },
                    TxnDate = String.Format("{0:yyyy-MM-dd}", billSelectAddQuickBooksResult.TxnDate),
                    DueDate = billSelectAddQuickBooksResult.DueDate,
                    RefNumber = billSelectAddQuickBooksResult.RefNumber,
                    Memo = Regex.Replace(billSelectAddQuickBooksResult.Memo + " REF:" + billSelectAddQuickBooksResult.BillID, "[^a-zA-Z0-9_.-]+", " ", RegexOptions.Compiled)
                };
                ISingleResult<BillLineItem_selectResult> billLineItemSelectResults = DataContext.BillLineItem_select(null,
                    billSelectAddQuickBooksResult.BillID.ToString(), billSelectAddQuickBooksResult.CompanyID);
                List<BillLineItem_selectResult> billLineItemResults =
                    new List<BillLineItem_selectResult>(billLineItemSelectResults);

                List<BillAddExpenseLineAdd> expenses = new List<BillAddExpenseLineAdd>();
                //Add Line Item
                foreach (BillLineItem_selectResult billLineItemSelectResult in billLineItemResults)
                {
                    BillAddExpenseLineAdd billExpense = new BillAddExpenseLineAdd();
                    billExpense.AccountRef = new BillAddExpenseLineAddAccountRef
                    {
                        ListID = billLineItemSelectResult.ExpenseLineAccountRefListID,
                        FullName = billLineItemSelectResult.AccountName,

                    };
                    billExpense.Amount = billLineItemSelectResult.ExpenseLineAmount;
                    billExpense.Memo = Regex.Replace(billLineItemSelectResult.ExpenseLineMemo, "[^a-zA-Z0-9_.-]+", " ", RegexOptions.Compiled);

                    if (!string.IsNullOrEmpty(billLineItemSelectResult.ExpenseLineCustomerListID))
                    {
                        billExpense.CustomerRef = new BillAddExpenseLineAddCustomerRef
                        {
                            ListID = billLineItemSelectResult.ExpenseLineCustomerListID,
                            FullName = billLineItemSelectResult.FundName,
                        };
                        // billExpense.BillableStatus = "NotBillable";
                    }

                    if (!string.IsNullOrEmpty(billLineItemSelectResult.ExpenseLineClassListID))
                    {
                        billExpense.ClassRef = new DataProcessor.Ref.ClassRef
                        {
                            ListID = billLineItemSelectResult.ExpenseLineClassListID,
                            FullName = billLineItemSelectResult.CostCenterName,

                        };
                    }

                    expenses.Add(billExpense);
                }


                billadd.ExpenseLineAdd = expenses.ToArray();

                List<GlobalValue.Reponse> saveBill = bill.SaveBill(billadd);


                if (saveBill.Count > 0 && !saveBill[0].IsError)
                {
                    List<BillRet> serializeXml = SyncConstant.SerializeXml<BillRet>(saveBill[0].EntityObject.ToString(),
                        QueryXML.Vendor, false);
                    if (serializeXml.Count > 0)
                    {
                        responses.IsError = false;
                        responses.Message = serializeXml[0].TxnID;
                        return responses;
                    }

                }
                if (saveBill[0].IsError)
                {
                    string errMsg = saveBill[0].Message.ToString();
                    string id = null;
                    responses.IsError = true;
                    string resMsg = "Bill not  added with Ref Number " + billadd.RefNumber + ",\n for Vendor " +
                                    billSelectAddQuickBooksResult.VendorName + ", with amount $" +
                                    billSelectAddQuickBooksResult.AmountDue + ", was not added to QuickBooks because: ";
                    if (errMsg.Contains("invalid reference to QuickBooks AP Account"))
                    {
                        if (errMsg.IndexOf('\"') > -1)
                        {
                            id = errMsg.Substring(errMsg.IndexOf('\"') + 1,
                                errMsg.LastIndexOf('\"') - errMsg.IndexOf('\"') - 1);
                        }
                        if (billadd.APAccountRef.ListID == id)
                            responses.Message += resMsg + "The default Account Payables " + billadd.APAccountRef.FullName +
                                                 " is incorrect, Please change the default Account Payable.\n";
                    }
                    else
                    {
                        if (errMsg.IndexOf('\"') > -1)
                        {
                            id = errMsg.Substring(errMsg.IndexOf('\"') + 1,
                                errMsg.LastIndexOf('\"') - errMsg.IndexOf('\"') - 1);
                        }
                        if (billadd.APAccountRef != null && billadd.APAccountRef.ListID == id)
                        {
                            responses.Message += resMsg + "the Account " + billadd.APAccountRef.FullName +
                                       ", does not exist in QuickBooks.\n";
                            GlobalValue.Eventlogupdate("Bill not  added with ref " + billadd.RefNumber + "\n for billID " +
                          responses.Message, null, "Error");
                            //return responses;
                        }
                        if (billadd.VendorRef.ListID == id)
                        {
                            responses.Message += resMsg + "the Vendor " + billadd.VendorRef.FullName +
                                       ", does not exist in QuickBooks.\n";
                            GlobalValue.Eventlogupdate("Bill not  added with ref " + billadd.RefNumber + "\n for billID " +
                          responses.Message, null, "Error");
                            //return responses;
                        }
                        foreach (BillAddExpenseLineAdd exp in billadd.ExpenseLineAdd)
                        {
                            if (exp.AccountRef.ListID == id)
                            {
                                responses.Message += resMsg + "the Account " + exp.AccountRef.FullName +
                                           " , does not exist in QuickBooks.\n";
                                GlobalValue.Eventlogupdate("Bill not  added with ref " + billadd.RefNumber + "\n for billID " +
                            responses.Message, null, "Error");
                                //return responses;
                            }
                            if (exp.ClassRef.ListID == id)
                            {
                                responses.Message += resMsg + "the Class " + exp.ClassRef.FullName +
                                           ", does not exist in QuickBooks.\n";
                                GlobalValue.Eventlogupdate("Bill not  added with ref " + billadd.RefNumber + "\n for billID " +
                            responses.Message, null, "Error");
                                //return responses;
                            }
                            if (exp.CustomerRef.ListID == id)
                            {
                                responses.Message += resMsg + "The Fund " + exp.CustomerRef.FullName +
                                           ", does not exist in QuickBooks.\n";
                                GlobalValue.Eventlogupdate("Bill not  added with ref " + billadd.RefNumber + "\n for billID " +
                            responses.Message, null, "Error");
                                //return responses;
                            }
                        }
                        if (errMsg != "")
                        {
                            responses.Message += resMsg + errMsg;
                            GlobalValue.Eventlogupdate(responses.Message, null, "Error");
                           // return responses;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                GlobalValue.Eventlogupdate(ex, null, "Error");
                responses.IsError = true;
                responses.Message = ex.Message;
            }
            return responses;
        }
        public static void DeleteBill()
        {
            try
            {
                List<BillRet> billRets = (new QBXBill()).GetAllBills(null);

                ISingleResult<Bill_SelectResult> billSelectResults = DataContext.Bill_Select(null, false, Credentials.Get.CompanyId);
                List<Bill_SelectResult> dbBill = new List<Bill_SelectResult>(billSelectResults);


                foreach (Bill_SelectResult billRet in dbBill)
                {
                    BillRet classR = billRets.FirstOrDefault(c => c.TxnID == billRet.TxnID);
                    if (classR == null)
                    {
                        GlobalValue.Eventlogupdate("Setting inactive Bill record having name " + billRet.RefNumber, null);
                        DataContext.Bill_Insert_Update(billRet.TxnID, billRet.TimeCreated, billRet.TimeModified,
                     billRet.TxnNumber, billRet.VendorRefListID, billRet.APAccountRefListID, billRet.TxnDate, billRet.DueDate, billRet.AmountDue,
                     billRet.RefNumber, billRet.Memo, billRet.IsPaid, true, Credentials.Get.CompanyId);
                    }
                }

            }
            catch (Exception ex)
            {
                GlobalValue.Eventlogupdate("BILL DELETE: " + ex.Message, null, "Error");
            }


        }

        private static GlobalValue.Reponse SaveCheck(Check_Select_AddQuickBooksResult checkSelectAddQuickBooksResult, QBXBill bill, QBXBillPayment billPayment)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            try
            {

                BillAdd billadd = new BillAdd
                {
                    VendorRef = new BillAddVendorRef
                    {
                        ListID = checkSelectAddQuickBooksResult.VendorRefListID,
                        FullName = checkSelectAddQuickBooksResult.VendorName,
                    },
                    // APAccountRef = new BillAddAPAccountRef { ListID = checkSelectAddQuickBooksResult.PaidBankRefListID, FullName = checkSelectAddQuickBooksResult.BankAccountName },
                    TxnDate = String.Format("{0:yyyy-MM-dd}", checkSelectAddQuickBooksResult.TxnDate),
                    RefNumber = checkSelectAddQuickBooksResult.RefNumber,
                    Memo = Regex.Replace(checkSelectAddQuickBooksResult.Memo, "[^a-zA-Z0-9_.-]+", " ", RegexOptions.Compiled)
                };



                ISingleResult<BillLineItem_selectResult> billLineItemSelectResults = DataContext.BillLineItem_select(null,
                    checkSelectAddQuickBooksResult.BillID.ToString(), checkSelectAddQuickBooksResult.CompanyID);
                List<BillLineItem_selectResult> billLineItemResults =
                    new List<BillLineItem_selectResult>(billLineItemSelectResults);

                List<BillAddExpenseLineAdd> expenses = new List<BillAddExpenseLineAdd>();
                //Add Line Item
                foreach (BillLineItem_selectResult billLineItemSelectResult in billLineItemResults)
                {
                    BillAddExpenseLineAdd billExpense = new BillAddExpenseLineAdd();
                    billExpense.AccountRef = new BillAddExpenseLineAddAccountRef
                    {
                        ListID = billLineItemSelectResult.ExpenseLineAccountRefListID,
                        FullName = billLineItemSelectResult.AccountName,

                    };
                    billExpense.Amount = billLineItemSelectResult.ExpenseLineAmount;
                    billExpense.Memo = Regex.Replace(billLineItemSelectResult.ExpenseLineMemo, "[^a-zA-Z0-9_.-]+", " ", RegexOptions.Compiled);

                    if (!string.IsNullOrEmpty(billLineItemSelectResult.ExpenseLineCustomerListID))
                    {
                        billExpense.CustomerRef = new BillAddExpenseLineAddCustomerRef
                        {
                            ListID = billLineItemSelectResult.ExpenseLineCustomerListID,
                            FullName = billLineItemSelectResult.FundName,
                        };
                        // billExpense.BillableStatus = "NotBillable";
                    }

                    if (!string.IsNullOrEmpty(billLineItemSelectResult.ExpenseLineClassListID))
                    {
                        billExpense.ClassRef = new DataProcessor.Ref.ClassRef
                        {
                            ListID = billLineItemSelectResult.ExpenseLineClassListID,
                            FullName = billLineItemSelectResult.CostCenterName,

                        };
                    }

                    expenses.Add(billExpense);
                }


                billadd.ExpenseLineAdd = expenses.ToArray();

                //Saving the Bill 
                List<GlobalValue.Reponse> saveBill = bill.SaveBill(billadd);
                if (saveBill.Count > 0 && !saveBill[0].IsError)
                {
                    List<BillRet> serializeXml = SyncConstant.SerializeXml<BillRet>(saveBill[0].EntityObject.ToString(),
                        QueryXML.Vendor, false);
                    if (serializeXml.Count > 0)
                    {
                        responses.IsError = false;
                        responses.Message = serializeXml[0].TxnID;
                        //        List<BillRet> listBill = bill.GetBillByRefNumber(checkSelectAddQuickBooksResult.RefNumber);
                        //        BillRet billRet = listBill.FirstOrDefault(b => b.Memo.Trim() == checkSelectAddQuickBooksResult.Memo.Trim());
                        //    if (billRet != null)
                        //   {
                        BillPaymentCheckAdd billPaymentCheckAdd = new BillPaymentCheckAdd
                        {
                            BankAccountRef = new BankAccountRef { ListID = checkSelectAddQuickBooksResult.PaidBankRefListID, FullName = checkSelectAddQuickBooksResult.BankAccountName },
                            TxnDate = String.Format("{0:yyyy-MM-dd}", checkSelectAddQuickBooksResult.DueDate),
                            RefNumber = serializeXml[0].RefNumber,
                            Memo = Regex.Replace(serializeXml[0].Memo, "[^a-zA-Z0-9_.-]+", " ", RegexOptions.Compiled),
                            PayeeEntityRef = new DataProcessor.Sync.ModelAdd.PayeeEntityRef
                            {
                                ListID = checkSelectAddQuickBooksResult.VendorRefListID,
                                FullName = checkSelectAddQuickBooksResult.VendorName,
                            },
                            AppliedToTxnAdd = new AppliedToTxnAdd
                            {
                                TxnID = serializeXml[0].TxnID,
                                PaymentAmount = serializeXml[0].AmountDue.ToString()
                            }
                        };

                        List<GlobalValue.Reponse> saveBillPayment = billPayment.SaveBillPayment(billPaymentCheckAdd);
                        if (saveBillPayment.Count > 0 && !saveBillPayment[0].IsError)
                        {
                            var checkSerializeXml = SyncConstant.SerializeXml<BillPaymentCheckRet>(saveBillPayment[0].EntityObject.ToString(), QueryXML.BillPaymentCheckQuery, false);
                            if (checkSerializeXml.Count > 0)
                            {
                                responses.IsError = false;
                                responses.Message = serializeXml[0].TxnID + "|" + checkSerializeXml[0].TxnID;
                                return responses;
                            }
                        }
                        if (saveBillPayment[0].IsError)
                        {
                            string errMsg = saveBillPayment[0].Message.ToString();
                            string id = null;
                            responses.IsError = true;
                            string resMsg = "Bill not added with check number " + billPaymentCheckAdd.RefNumber + ",\n for Vendor " +
                                            checkSelectAddQuickBooksResult.VendorName + ", with amount $" +
                                            checkSelectAddQuickBooksResult.AmountDue + ", was not added to QuickBooks because: ";
                            if (errMsg.Contains("invalid reference to QuickBooks AP Account"))
                            {
                                if (errMsg.IndexOf('\"') > -1)
                                {
                                    id = errMsg.Substring(errMsg.IndexOf('\"') + 1,
                                        errMsg.LastIndexOf('\"') - errMsg.IndexOf('\"') - 1);
                                }
                                if (billPaymentCheckAdd.APAccountRef.ListID == id)
                                    responses.Message += resMsg + "The default Account Payables " + billPaymentCheckAdd.APAccountRef.FullName +
                                                         " is incorrect, Please change the default Account Payable.\n";
                            }
                            else
                            {
                                if (errMsg.IndexOf('\"') > -1)
                                {
                                    id = errMsg.Substring(errMsg.IndexOf('\"') + 1,
                                        errMsg.LastIndexOf('\"') - errMsg.IndexOf('\"') - 1);

                                    if (billPaymentCheckAdd.APAccountRef.ListID == id)
                                    {
                                        responses.Message += resMsg + "the Account " + billPaymentCheckAdd.APAccountRef.FullName +
                                                   ", does not exist in QuickBooks.\n";
                                        GlobalValue.Eventlogupdate("Bill not  added with ref " + billPaymentCheckAdd.RefNumber + "\n for billID " +
                                      responses.Message, null, "Error");
                                        //return responses;
                                    }
                                    if (billPaymentCheckAdd.PayeeEntityRef.ListID == id)
                                    {
                                        responses.Message += resMsg + "the Vendor " + billPaymentCheckAdd.PayeeEntityRef.FullName +
                                                   ", does not exist in QuickBooks.\n";
                                        GlobalValue.Eventlogupdate("Bill not  added with ref " + billPaymentCheckAdd.RefNumber + "\n for billID " +
                                      responses.Message, null, "Error");
                                       // return responses;
                                    }
                                }
                                else
                                {
                                    responses.Message = resMsg + " " + errMsg;
                                }
                            }

                            if (bill.DeleteByTxnId("Bill", serializeXml[0].TxnID))
                            {
                                responses.Message += Environment.NewLine + "\nAdded bill[TxnId: " + serializeXml[0].TxnID + "] is rolled back.";
                            }
                            else
                            {
                                responses.Message += "\nUnable to rollback added bill[TxnId: " + serializeXml[0].TxnID + "].";
                            }
                        }
                        //}
                    }
                }
                if (saveBill[0].IsError)
                {
                    string errMsg = saveBill[0].Message.ToString();
                    string id = null;
                    responses.IsError = true;
                    string resMsg = "Bill not added with Ref Number " + billadd.RefNumber + ",\n for Vendor " +
                                    checkSelectAddQuickBooksResult.VendorName + ", with amount $" +
                                    checkSelectAddQuickBooksResult.AmountDue + ", was not added to QuickBooks because: ";
                    if (errMsg.Contains("invalid reference to QuickBooks AP Account"))
                    {
                        if (errMsg.IndexOf('\"') > -1)
                        {
                            id = errMsg.Substring(errMsg.IndexOf('\"') + 1,
                                errMsg.LastIndexOf('\"') - errMsg.IndexOf('\"') - 1);
                        }
                        if (billadd.APAccountRef.ListID == id)
                            responses.Message += resMsg + "The default Account Payables " + billadd.APAccountRef.FullName +
                                                 " is incorrect, Please change the default Account Payable.\n";
                    }
                    else
                    {
                        if (errMsg.IndexOf('\"') > -1)
                        {
                            id = errMsg.Substring(errMsg.IndexOf('\"') + 1,
                                errMsg.LastIndexOf('\"') - errMsg.IndexOf('\"') - 1);
                        }
                        if (billadd.APAccountRef != null && billadd.APAccountRef.ListID == id)
                        {
                            responses.Message += resMsg + "the Account " + billadd.APAccountRef.FullName +
                                       ", does not exist in QuickBooks.\n";
                            GlobalValue.Eventlogupdate("Bill not  added with ref " + billadd.RefNumber + "\n for billID " +
                          responses.Message, null, "Error");
                            //return responses;
                        }
                        if (billadd.VendorRef.ListID == id)
                        {
                            responses.Message += resMsg + "the Vendor " + billadd.VendorRef.FullName +
                                       ", does not exist in QuickBooks.\n";
                            GlobalValue.Eventlogupdate("Bill not  added with ref " + billadd.RefNumber + "\n for billID " +
                          responses.Message, null, "Error");
                            //return responses;
                        }
                     
                        foreach (BillAddExpenseLineAdd exp in billadd.ExpenseLineAdd)
                        {
                            if (exp.AccountRef.ListID == id)
                            {
                                responses.Message += resMsg + "the Account " + exp.AccountRef.FullName +
                                           " , does not exist in QuickBooks.\n";
                                GlobalValue.Eventlogupdate("Bill not  added with ref " + billadd.RefNumber + "\n for billID " +
                            responses.Message, null, "Error");
                                //return responses;
                            }
                            if (exp.ClassRef.ListID == id)
                            {
                                responses.Message += resMsg + "the Class " + exp.ClassRef.FullName +
                                           ", does not exist in QuickBooks.\n";
                                GlobalValue.Eventlogupdate("Bill not  added with ref " + billadd.RefNumber + "\n for billID " +
                            responses.Message, null, "Error");
                                //return responses;
                            }
                            if (exp.CustomerRef.ListID == id)
                            {
                                responses.Message += resMsg + "The Fund " + exp.CustomerRef.FullName +
                                           ", does not exist in QuickBooks.\n";
                                GlobalValue.Eventlogupdate("Bill not  added with ref " + billadd.RefNumber + "\n for billID " +
                            responses.Message, null, "Error");
                                //return responses;
                            }
                        }
                        if(errMsg!="")
                        {
                            responses.Message += resMsg + errMsg;
                            GlobalValue.Eventlogupdate(responses.Message, null, "Error");
                            //return responses;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                GlobalValue.Eventlogupdate(ex, null, "Error");
                responses.IsError = true;
                responses.Message = ex.Message;
            }
            return responses;
        }

        private static GlobalValue.Reponse SaveCreditCard(Check_Select_AddQuickBooksResult ccSelectAddQuickBooksResult, QBXCreditCard qbxCC)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            try
            {
                CreditCardChargeAdd ccChargeAdd = new CreditCardChargeAdd
                {
                    PayeeEntityRef = new DataProcessor.Sync.ModelAdd.PayeeEntityRef
                    {
                        ListID = ccSelectAddQuickBooksResult.VendorRefListID,
                        FullName = ccSelectAddQuickBooksResult.VendorName,
                    },
                    AccountRef = new DataProcessor.Ref.AccountRef { ListID = ccSelectAddQuickBooksResult.PaidBankRefListID, FullName = ccSelectAddQuickBooksResult.BankAccountName },
                    TxnDate = String.Format("{0:yyyy-MM-dd}", ccSelectAddQuickBooksResult.TxnDate),
                    RefNumber = ccSelectAddQuickBooksResult.RefNumber,
                    Memo = Regex.Replace(ccSelectAddQuickBooksResult.Memo, "[^a-zA-Z0-9_.-]+", " ", RegexOptions.Compiled)
                };
                ISingleResult<BillLineItem_selectResult> billLineItemSelectResults = DataContext.BillLineItem_select(null,
                    ccSelectAddQuickBooksResult.BillID.ToString(), ccSelectAddQuickBooksResult.CompanyID);
                List<BillLineItem_selectResult> billLineItemResults =
                    new List<BillLineItem_selectResult>(billLineItemSelectResults);

                List<ExpenseLineAdd> expenses = new List<ExpenseLineAdd>();
                //Add Line Item
                foreach (BillLineItem_selectResult billLineItemSelectResult in billLineItemResults)
                {
                    ExpenseLineAdd billExpense = new ExpenseLineAdd();
                    billExpense.AccountRef = new DataProcessor.Ref.AccountRef
                    {
                        ListID = billLineItemSelectResult.ExpenseLineAccountRefListID,
                        FullName = billLineItemSelectResult.AccountName,

                    };
                    billExpense.Amount = billLineItemSelectResult.ExpenseLineAmount.ToString();
                    billExpense.Memo = Regex.Replace(billLineItemSelectResult.ExpenseLineMemo, "[^a-zA-Z0-9_.-]+", " ", RegexOptions.Compiled);

                    if (!string.IsNullOrEmpty(billLineItemSelectResult.ExpenseLineCustomerListID))
                    {
                        billExpense.CustomerRef = new DataProcessor.Sync.ModelAdd.CustomerRef
                        {
                            ListID = billLineItemSelectResult.ExpenseLineCustomerListID,
                            FullName = billLineItemSelectResult.FundName,
                        };
                        // billExpense.BillableStatus = "NotBillable";
                    }

                    if (!string.IsNullOrEmpty(billLineItemSelectResult.ExpenseLineClassListID))
                    {
                        billExpense.ClassRef = new DataProcessor.Ref.ClassRef
                        {
                            ListID = billLineItemSelectResult.ExpenseLineClassListID,
                            FullName = billLineItemSelectResult.CostCenterName,

                        };
                    }
                    //billExpense.BillableStatus = "Billable";
                    expenses.Add(billExpense);
                }


                ccChargeAdd.ExpenseLineAdd = expenses.ToArray();

                List<GlobalValue.Reponse> saveBill = qbxCC.SaveCreditCardCharge(ccChargeAdd);


                if (saveBill.Count > 0 && !saveBill[0].IsError)
                {
                    List<CreditCardChargeRet> serializeXml = SyncConstant.SerializeXml<CreditCardChargeRet>(saveBill[0].EntityObject.ToString(),
                        QueryXML.Vendor, false);
                    if (serializeXml.Count > 0)
                    {
                        responses.IsError = false;
                        responses.Message = serializeXml[0].TxnID;
                        return responses;
                    }

                }
                if (saveBill[0].IsError)
                {
                    string errMsg = saveBill[0].Message.ToString();
                    string id = null;
                    responses.IsError = true;
                    string resMsg = "Bill not  added with Ref Number " + ccChargeAdd.RefNumber + ",\n for Vendor " +
                                    ccSelectAddQuickBooksResult.VendorName + ", with amount $" +
                                    ccSelectAddQuickBooksResult.AmountDue + ", was not added to QuickBooks because: ";
                    if (errMsg.Contains("invalid reference to QuickBooks AP Account"))
                    {
                        if (errMsg.IndexOf('\"') > -1)
                        {
                            id = errMsg.Substring(errMsg.IndexOf('\"') + 1,
                                errMsg.LastIndexOf('\"') - errMsg.IndexOf('\"') - 1);
                        }
                        if (ccChargeAdd.AccountRef != null && ccChargeAdd.AccountRef.ListID == id)
                            responses.Message += resMsg + "The default Account Payables " + ccChargeAdd.AccountRef.FullName +
                                                 " is incorrect, Please change the default Account Payable.\n";
                    }
                    else
                    {
                        if (errMsg.IndexOf('\"') > -1)
                        {
                            id = errMsg.Substring(errMsg.IndexOf('\"') + 1,
                                errMsg.LastIndexOf('\"') - errMsg.IndexOf('\"') - 1);
                        }
                        if (ccChargeAdd.AccountRef != null && ccChargeAdd.AccountRef.ListID == id)
                        {
                            responses.Message += resMsg + "the Account " + ccChargeAdd.AccountRef.FullName +
                                       ", does not exist in QuickBooks.\n";
                            GlobalValue.Eventlogupdate("Bill not  added with ref " + ccChargeAdd.RefNumber + "\n for billID " +
                          responses.Message, null, "Error");
                            //return responses;
                        }
                        if (ccChargeAdd.PayeeEntityRef != null && ccChargeAdd.PayeeEntityRef.ListID == id)
                        {
                            responses.Message += resMsg + "the Vendor " + ccChargeAdd.PayeeEntityRef.FullName +
                                       ", does not exist in QuickBooks.\n";
                            GlobalValue.Eventlogupdate("Bill not  added with ref " + ccChargeAdd.RefNumber + "\n for billID " +
                          responses.Message, null, "Error");
                            //return responses;
                        }
                        foreach (ExpenseLineAdd exp in ccChargeAdd.ExpenseLineAdd)
                        {
                            if (exp.AccountRef != null && exp.AccountRef.ListID == id)
                            {
                                responses.Message += resMsg + "the Account " + exp.AccountRef.FullName +
                                           " , does not exist in QuickBooks.\n";
                                GlobalValue.Eventlogupdate("Bill not  added with ref " + ccChargeAdd.RefNumber + "\n for billID " +
                            responses.Message, null, "Error");
                                //return responses;
                            }
                            if (exp.ClassRef != null && exp.ClassRef.ListID == id)
                            {
                                responses.Message += resMsg + "the Class " + exp.ClassRef.FullName +
                                           ", does not exist in QuickBooks.\n";
                                GlobalValue.Eventlogupdate("Bill not  added with ref " + ccChargeAdd.RefNumber + "\n for billID " +
                            responses.Message, null, "Error");
                               // return responses;
                            }
                            if (exp.CustomerRef != null && exp.CustomerRef.ListID == id)
                            {
                                responses.Message += resMsg + "The Fund " + exp.CustomerRef.FullName +
                                           ", does not exist in QuickBooks.\n";
                                GlobalValue.Eventlogupdate("Bill not  added with ref " + ccChargeAdd.RefNumber + "\n for billID " +
                            responses.Message, null, "Error");
                               // return responses;
                            }
                        }
                        if (errMsg != "")
                        {
                            responses.Message += resMsg + errMsg;
                            GlobalValue.Eventlogupdate(responses.Message, null, "Error");
                           // return responses;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                GlobalValue.Eventlogupdate(ex, null, "Error");
                responses.IsError = true;
                responses.Message = ex.Message;
            }
            return responses;
        }


        //public static GlobalValue.Reponse BillsPull(Action<ProgressEventArgs> onProgressChanged)
        //{
        //    GlobalValue.Reponse responses = new GlobalValue.Reponse();
        //    int insertBillCount = 0;
        //    try
        //    {
        //        DateTime fromdate = Convert.ToDateTime("2021-06-01");//Credentials.Get.SyncStartDate;
        //        List<BillRet> billRets = (new QBXBill()).GetAllBills(fromdate);
        //        foreach (BillRet billRet in billRets)
        //        {
        //            insertBillCount++;
        //            GlobalValue.Eventlogupdate("Setting inactive Bill record having name " + billRet.RefNumber, null);
        //            DataContext.QB_NewBillSave(billRet.TxnID, billRet.TimeCreated, billRet.TimeModified, billRet.EditSequence,
        //            billRet.TxnNumber, billRet.VendorRef?.ListID, billRet.VendorRef?.FullName, billRet.APAccountRef?.ListID, billRet.TxnDate,
        //            billRet.DueDate,  billRet.RefNumber, billRet.Memo,  billRet.AmountDue.ToString()
        //            );                  
                    
        //            onProgressChanged(new ProgressEventArgs
        //            {
        //                Total = billRets.Count,
        //                Current = insertBillCount
        //            });
        //        }
        //        onProgressChanged(new ProgressEventArgs
        //        {
        //            Completed = true
        //        });
        //        if (insertBillCount > 0)
        //        {
        //            responses.Message += Environment.NewLine + insertBillCount + " Bill(s) inserted in database.";
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        GlobalValue.Eventlogupdate(ex, null, "Error");
        //        responses.IsError = true;
        //        responses.Message = ex.Message;
        //    }
        //    return responses;
        //}
    }
}
