﻿using DataProcessor;
using DataProcessor.QB.XMLProcessor;
using SBTSync.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SBTSync.DataBase.DAL
{
   public class Company:DBBase
    {
        protected static readonly QBAUTOUPDATEDataContext DataContext = new QBAUTOUPDATEDataContext(Credentials.Get.ConnectionString);
        public static  int ValidateCompany()
        {

           
            QBXCompany Company = new QBXCompany();
            try
            {
                DateTime? qbClassLastMod = null;

                CompanyRet cmpnyRets = Company.GetCompanyInformation(qbClassLastMod);

                if (Credentials.CompanyEIN.Length == 0)
                {
                    if (cmpnyRets.EIN != null)
                        DataContext.UpdateCompanyEIN(Credentials.Get.CompanyId, cmpnyRets.EIN);
                }
                else if (Credentials.CompanyEIN != cmpnyRets.EIN)
                {
                    MailSender.Send(Settings.Default.MAILTO, Credentials.Get.CompanyId + ": Error in EIN check " + Credentials.SyncDateTime.ToString("MM-dd-yyyy HH:mm"), " DB EIN : "+Credentials.CompanyEIN+" -- QB EIN: "+cmpnyRets.EIN);
                    return 0;
                }

                return 1;

            }
          catch(Exception ex)
            {
                MailSender.Send(Settings.Default.MAILTO, Credentials.Get.CompanyId + ": Error in EIN check " + Credentials.SyncDateTime.ToString("MM-dd-yyyy HH:mm"), ex.Message);
                return 1;
            }
            
        }

    }
}
