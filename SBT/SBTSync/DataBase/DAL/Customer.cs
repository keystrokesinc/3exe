﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Linq;
using DataProcessor;
using DataProcessor.QB.XMLProcessor;
using DataProcessor.Sync.ModelRet;
using SBTSync.Properties;
using DataProcessor.Sync.ModelAdd;
using DataProcessor.Sync;
using System.Data;


namespace SBTSync.DataBase.DAL
{
    public class Customer : DBBase
    {
        private static string errMsg = string.Empty;
        static DataCommand dc = new DataCommand();
        public static GlobalValue.Reponse ProcessCustomer(Action<ProgressEventArgs> onProgressChanged)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            responses.Message = "";
            QBXCustomer customer = new QBXCustomer();
            errMsg = string.Empty;
            DateTime? qbAccountLastMod = GetLastModified("Customer");
            //IList<CustomerRet> customerRets1 = customer.SearchCustomer("Chisamore, Joan");


            IList<CustomerRet> customerRets = customer.GetAllSyncCustomers(null).GroupBy(cust => new { cust.ListID, cust.FullName }).Select(group => group.First()).ToList();             
            IList<TemplateRet> templateRets = customer.GetAllSyncTemplates(null);
            List<CustomersList> customerList = new List<CustomersList>();  
            int insertCustomerCount = 0, insertFundCount = 0, updateCustomerCount = 0, updateFundCount = 0, insertTemplateCount = 0, updateTemplateCount = 0;
                            
            foreach (CustomerRet customerRet in customerRets)
            {
                try
                {
                    if (customerRet.Sublevel > 0)
                    {
                        IList<Funds_SelectResult> selectResults = DataContext.Funds_Select(customerRet.ListID, Credentials.Get.CompanyId).ToList();
                        if (selectResults.Count == 0)
                        {
                            DataContext.Funds_Insert(Credentials.Get.CompanyId, customerRet.ListID, customerRet.TimeCreated, customerRet.TimeModified,
                                                     customerRet.FullName, customerRet.Sublevel == 1, customerRet.IsActive);
                            GlobalValue.Eventlogupdate("Funds having Name " + customerRet.FullName + " Inserted", null);
                            insertFundCount++;
                        }
                        else
                        {
                            DataContext.Funds_Update(Credentials.Get.CompanyId, customerRet.ListID, customerRet.TimeCreated, customerRet.TimeModified,
                                                     customerRet.FullName, customerRet.Sublevel == 1, customerRet.IsActive);
                            updateFundCount++;
                            GlobalValue.Eventlogupdate("Fund having Name " + customerRet.FullName + " Updated", null);
                        }
                    }
                    else
                    {
                        //Commented by parvathy for bulk insert
                        IList<Customer_SelectResult> selectResults = DataContext.Customer_Select(customerRet.ListID, Credentials.Get.CompanyId).ToList();
                        if (selectResults.Count == 0)
                        {


                            DataContext.Customer_Insert(Credentials.Get.CompanyId, customerRet.ListID, customerRet.TimeCreated, customerRet.TimeModified,
                                                                customerRet.FullName, customerRet.Sublevel == 0, customerRet.IsActive,
                                                                customerRet.LastName, customerRet.CompanyName, customerRet.MiddleName,
                                                                customerRet.Balance, customerRet.Phone, customerRet.Email, customerRet.Fax,
                                                                customerRet.BillAddress != null ? customerRet.BillAddress.Addr1 : "",
                                                                customerRet.BillAddress != null ? customerRet.BillAddress.Addr2 : "",
                                                                customerRet.BillAddress != null ? customerRet.BillAddress.Addr3 : "",
                                                                customerRet.BillAddress != null ? customerRet.BillAddress.Addr4 : "",
                                                                customerRet.BillAddress != null ? customerRet.BillAddress.Addr5 : "",
                                                                customerRet.BillAddress != null ? customerRet.BillAddress.City : "",
                                                                customerRet.BillAddress != null ? customerRet.BillAddress.Country : "",
                                                                customerRet.FirstName,
                                                                customerRet.BillAddress != null ? customerRet.BillAddress.State : "",
                                                                customerRet.BillAddress != null ? customerRet.BillAddress.PostalCode : "", customerRet.Salutation);
                            insertCustomerCount++;
                            GlobalValue.Eventlogupdate("Customer having Name " + customerRet.FullName + " Inserted", null);
                        }



                        else
                        {
                            DataContext.Customer_Update(Credentials.Get.CompanyId, customerRet.ListID, customerRet.TimeCreated, customerRet.TimeModified,
                                                        customerRet.FullName, customerRet.Sublevel == 0, customerRet.IsActive,
                                                        customerRet.LastName, customerRet.CompanyName, customerRet.MiddleName,
                                                            customerRet.Balance, customerRet.Phone, customerRet.Email, customerRet.Fax,
                                                            customerRet.BillAddress != null ? customerRet.BillAddress.Addr1 : "",
                                                            customerRet.BillAddress != null ? customerRet.BillAddress.Addr2 : "",
                                                            customerRet.BillAddress != null ? customerRet.BillAddress.Addr3 : "",
                                                            customerRet.BillAddress != null ? customerRet.BillAddress.Addr4 : "",
                                                            customerRet.BillAddress != null ? customerRet.BillAddress.Addr5 : "",
                                                            customerRet.BillAddress != null ? customerRet.BillAddress.City : "",
                                                            customerRet.BillAddress != null ? customerRet.BillAddress.Country : "",
                                                            customerRet.FirstName,
                                                            customerRet.BillAddress != null ? customerRet.BillAddress.State : "",
                                                            customerRet.BillAddress != null ? customerRet.BillAddress.PostalCode : "",
                                                            customerRet.Salutation);
                            updateCustomerCount++;
                            GlobalValue.Eventlogupdate("Customer having Name " + customerRet.FullName + " Updated", null);
                        }
                        //CustomersList cust = new CustomersList(Credentials.Get.CompanyId,
                        //customerRet.ListID,
                        // Convert.ToDateTime(customerRet.TimeCreated),
                        // Convert.ToDateTime(customerRet.TimeModified),
                        // customerRet.FullName,
                        // Convert.ToBoolean(customerRet.IsActive),                            
                        // customerRet.Salutation,                             
                        // customerRet.MiddleName,
                        // customerRet.LastName,
                        // customerRet.CompanyName,
                        // customerRet.Phone, 
                        // customerRet.Fax,
                        // customerRet.Email,
                        // customerRet.BillAddress != null ? customerRet.BillAddress.Addr1 : "",
                        // customerRet.BillAddress != null ? customerRet.BillAddress.Addr2 : "",
                        // customerRet.BillAddress != null ? customerRet.BillAddress.Addr3 : "",
                        // customerRet.BillAddress != null ? customerRet.BillAddress.Addr4 : "",
                        // customerRet.BillAddress != null ? customerRet.BillAddress.Addr5 : "",
                        // customerRet.BillAddress != null ? customerRet.BillAddress.City : "",
                        // customerRet.BillAddress != null ? customerRet.BillAddress.State : "",
                        // customerRet.BillAddress != null ? customerRet.BillAddress.Country : "",
                        // customerRet.BillAddress != null ? customerRet.BillAddress.PostalCode : "",
                        // customerRet.Balance);
                        // customerList.Add(cust);
                        //insertCustomerCount++;
                             //GlobalValue.Eventlogupdate("Customer having Name " + customerRet.FullName + " Inserted", null);
                    }
                    onProgressChanged(new ProgressEventArgs
                    {
                        Total = customerRets.Count,
                        Current = updateCustomerCount + insertCustomerCount + insertFundCount + updateFundCount + insertTemplateCount + updateTemplateCount
                    });
                }
                catch (Exception ex)
                {
                    GlobalValue.Eventlogupdate(ex, null, "Error");
                    errMsg += ex.Message + Environment.NewLine;
                }
            }

            //if (customerList.Count > 0)
            //{
            //    try
            //    {
            //        var custselect = new SqlParameter()
            //        {
            //            ParameterName = "CustomersList",
            //            SqlDbType = SqlDbType.Structured,
            //            Value = customerList.ToDataTable()
            //        };
            //        dc.ExecuteAsNonQuery("AllCustomers_InsertNew", custselect);                    
            //    }
            //    catch (Exception ex)
            //    {
            //        GlobalValue.Eventlogupdate(ex, null, "Error");
            //        errMsg += ex.Message + Environment.NewLine;
            //    }
            //}           
            
                //Templates Save to DB
            if (templateRets.Count > 0)
            {
                foreach (TemplateRet templateRet in templateRets)
                {
                    IList<Templates_SelectResult> templateSelectResults = DataContext.Template_Select(templateRet.ListID, Credentials.Get.CompanyId).ToList();
                    if (templateSelectResults.Count == 0)
                    {
                        DataContext.Template_Insert(Credentials.Get.CompanyId, templateRet.ListID, templateRet.TimeCreated, templateRet.TimeModified, templateRet.Name, templateRet.EditSequence, templateRet.TemplateType, templateRet.IsActive);
                        GlobalValue.Eventlogupdate("Template having Name " + templateRet.Name + " Inserted", null);
                        insertTemplateCount++;
                    }
                    else
                    {
                        DataContext.Template_Update(Credentials.Get.CompanyId, templateRet.ListID, templateRet.TimeCreated, templateRet.TimeModified, templateRet.Name, templateRet.EditSequence, templateRet.TemplateType, templateRet.IsActive);
                        GlobalValue.Eventlogupdate("Template having Name " + templateRet.Name + " updated", null);
                        updateTemplateCount++;
                    }
                }
            }
            onProgressChanged(new ProgressEventArgs
            {
                Completed = true
            });

            if(errMsg.Length>0)
             responses.Message += errMsg;

            responses.Message += $"{insertFundCount} Fund(s) added to database.\n";
            responses.Message += $"{insertCustomerCount} Customer(s) added to database.\n";
            responses.Message += $"{insertTemplateCount} Template(s) added to database.\n";
          

            if (updateCustomerCount > 0)
                responses.Message += $"{updateCustomerCount } Customer(s) updated in database.";
            if (updateFundCount > 0)
                responses.Message += updateFundCount + " Fund(s) updated in database.";
			if (updateTemplateCount > 0)
                responses.Message += updateTemplateCount + " Template(s) updated in database.\n";

            //responses.Message += DeleteCustomer(customerRets).Message;
            if (!string.IsNullOrEmpty(errMsg))
            {

                MailSender.Send(Settings.Default.MAILTO, Credentials.Get.CompanyId + ": Error in Customer Sync " + Credentials.SyncDateTime.ToString("MM-dd-yyyy HH:mm"), errMsg);
            }
            else
            {
                DataContext.Settings_InsertUpdate("Customer", DateTime.UtcNow, Credentials.Get.CompanyId);
            }

            return responses;
        }

        public static GlobalValue.Reponse DeleteCustomer(IList<CustomerRet> customerRets = null)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            try
            {
                customerRets = customerRets ?? (new QBXCustomer()).GetAllSyncCustomers(null);
                int qbFundRecordCount = customerRets.Count(c => c.Sublevel > 0);
                int qbCustomerRecordCount = customerRets.Count(c => c.Sublevel == 0);

                ISingleResult<Customer_SelectResult> customerSelectResults = DataContext.Customer_Select(null, Credentials.Get.CompanyId);
                ISingleResult<Funds_SelectResult> fundsSelectResults = DataContext.Funds_Select(null, Credentials.Get.CompanyId);

                List<Customer_SelectResult> dbCustomer = new List<Customer_SelectResult>(customerSelectResults);
                List<Funds_SelectResult> dbFund = new List<Funds_SelectResult>(fundsSelectResults);

                if (dbFund.Count > 0)
                {
                    foreach (Funds_SelectResult fundsSelectResult in dbFund)
                    {
                        CustomerRet classR = customerRets.FirstOrDefault(c => c.ListID == fundsSelectResult.ListID);
                        if (classR == null)
                        {
                            GlobalValue.Eventlogupdate(
                                "Setting inactive fund record having name " + fundsSelectResult.FullName, null);
                            DataContext.Funds_Update(Credentials.Get.CompanyId, fundsSelectResult.ListID,
                                fundsSelectResult.TimeCreated, fundsSelectResult.TimeModified,
                                fundsSelectResult.FullName, true, false);
                        }
                    }


                    foreach (Customer_SelectResult fundsSelectResult in dbCustomer)
                    {
                        CustomerRet classR = customerRets.FirstOrDefault(c => c.ListID == fundsSelectResult.ListID);
                        if (classR == null)
                        {
                            GlobalValue.Eventlogupdate(
                                "Setting inactive Funds record having name " + fundsSelectResult.FullName, null);
                            DataContext.Customer_Update(Credentials.Get.CompanyId, fundsSelectResult.ListID,
                                fundsSelectResult.TimeCreated, fundsSelectResult.TimeModified,
                                fundsSelectResult.FullName, false, true,
                                string.Empty, string.Empty, string.Empty,
                                                            string.Empty, string.Empty, string.Empty, string.Empty,
                                                           string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
                                                           responses.RecordCount++;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                errMsg = ex.Message + Environment.NewLine;
                GlobalValue.Eventlogupdate("Customer DELETE: " + ex.Message, null, "Error");
            }

            responses.Message = responses.RecordCount + " Fund(s) set to inactive status.";
            return responses;
        }
        public static string insertCustomerToQB(Logs_SelectResult custData)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            string CustomerID = "";
            IList<Customer_SelectResult> selectResults = DataContext.Customer_Select_ByName(custData.Customer, Credentials.Get.CompanyId).ToList();
            
            if (selectResults.Count > 0)
            {
                CustomerID = selectResults[0].ListID;
            }
            if (CustomerID.Length < 1)// If custoemr not found in DB
            {
                QBXCustomer qBXCustomer = new QBXCustomer();
                try
                {

                    CustomerAdd customer = new CustomerAdd
                    {
                        Name = custData.Customer,
                        IsActive = "true",
                        FirstName = custData.FirstName,
                        LastName = custData.LastName,
                        BillAddress = new DataProcessor.Ref.BillAddress
                        {
                            Addr2 = custData.BillAddressAddr1,
                            Addr1 = custData.FirstName + " " + custData.LastName,
                            State = custData.BillAddressState,
                            City = custData.BillAddressCity,
                            //Country  =   custData.BillAddressCountry,
                            PostalCode = custData.BillAddressPostalCode},
                        Email = custData.Email
                    };

                    List<GlobalValue.Reponse> saveCustomer = qBXCustomer.SaveCustomer(customer);
                    if (saveCustomer.Count > 0 && !saveCustomer[0].IsError)
                    {
                        var checkSerializeXml = SyncConstant.SerializeXml<CustomerRet>(saveCustomer[0].EntityObject.ToString(), QueryXML.CustomerAdd, false);
                        if (checkSerializeXml?.Count > 0)
                        {
                            DataContext.InvoiceCustomer_Update(Credentials.Get.CompanyId, checkSerializeXml[0].ListID, checkSerializeXml[0].FullName);
                            GlobalValue.Eventlogupdate("Customer having FullName " + checkSerializeXml[0].FullName + " Inserted", null);

                            CustomerID = checkSerializeXml[0].ListID;
                        }
                    }
                    else if (saveCustomer[0].IsError)
                    {
                        CustomerID = "Error!- Create customer " + saveCustomer[0].Message;
                    }
                }
                catch (Exception ex)
                {
                    GlobalValue.Eventlogupdate(ex, null, "Error");
                    responses.IsError = true;
                    responses.Message = ex.Message;
                }
            }
            return CustomerID;
        }


        public static string searchCustomer(Logs_SelectResult custData)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            string CustomerID = "";
            QBXCustomer qxCustomer = new QBXCustomer();
            errMsg = string.Empty;
            try
            {

                IList<CustomerRet> customerRets = qxCustomer.SearchCustomer(custData.Customer);
                foreach (CustomerRet customerRet in customerRets)
                {
                    if (customerRet.Email != custData.Email)
                    {
                        CustomerAdd customer = new CustomerAdd
                        {
                            ListID = customerRet.ListID,
                            EditSequence = customerRet.EditSequence,
                            IsActive = "true",
                            Email = custData.Email
                        };

                        List<GlobalValue.Reponse> saveCustomer = qxCustomer.UpdateCustomer(customer);
                        if (saveCustomer.Count > 0 && !saveCustomer[0].IsError)
                        {
                            CustomerID = customerRet.ListID;
                        }
                        else if (saveCustomer[0].IsError)
                        {
                            CustomerID = "Error!- Create customer " + saveCustomer[0].Message;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                GlobalValue.Eventlogupdate(ex, null, "Error");
                responses.IsError = true;
                responses.Message = ex.Message;
            }
            return CustomerID;
        }


        public static string insertMOWCustomerToQB(Logs_SelectResult custData)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            string CustomerID = "";            
            QBXCustomer qBXCustomer = new QBXCustomer();
            try
            {

                CustomerAdd customer = new CustomerAdd
                {
                    Name = custData.Customer,
                    IsActive = "true",
                    FirstName = custData.FirstName,
                    LastName = custData.LastName,
                    BillAddress = new DataProcessor.Ref.BillAddress
                    {
                        Addr2 = custData.BillAddressAddr1,
                        Addr1 = custData.FirstName + " " + custData.LastName,
                        State = custData.BillAddressState,
                        City = custData.BillAddressCity,
                        //Country  =   custData.BillAddressCountry,
                        PostalCode = custData.BillAddressPostalCode
                    },
                    Email = custData.Email
                };

                List<GlobalValue.Reponse> saveCustomer = qBXCustomer.SaveCustomer(customer);
                if (saveCustomer.Count > 0 && !saveCustomer[0].IsError)
                {
                    var checkSerializeXml = SyncConstant.SerializeXml<CustomerRet>(saveCustomer[0].EntityObject.ToString(), QueryXML.CustomerAdd, false);
                    if (checkSerializeXml?.Count > 0)
                    {
                        DataContext.InvoiceCustomer_Update(Credentials.Get.CompanyId, checkSerializeXml[0].ListID, checkSerializeXml[0].FullName);
                        GlobalValue.Eventlogupdate("Customer having FullName " + checkSerializeXml[0].FullName + " Inserted", null);

                        CustomerID = checkSerializeXml[0].ListID;
                    }
                }
                else if (saveCustomer[0].IsError)
                {
                    CustomerID = "Error!- Create customer " + saveCustomer[0].Message;
                }
            }
            catch (Exception ex)
            {
                GlobalValue.Eventlogupdate(ex, null, "Error");
                responses.IsError = true;
                responses.Message = ex.Message;
            }
           
            return CustomerID;
        }
    }
}
