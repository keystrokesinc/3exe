﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Linq;
using DataProcessor;
using DataProcessor.QB.XMLProcessor;
using DataProcessor.Sync;
using DataProcessor.Sync.ModelRet;
using SBTSync.Properties;
using System.Text.RegularExpressions;
using DataProcessor.Ref;
using DataProcessor.Sync.ModelAdd;

namespace SBTSync.DataBase.DAL
{
    public class BillPayments : DBBase
    {
        private static string errMsg;
        public static GlobalValue.Reponse BillPaymentsPull(Action<ProgressEventArgs> onProgressChanged)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            int insertBillCount = 0;
            int itemsave = 0;
            GlobalValue.Eventlogupdate("Getting BillPayments", null);
            DateTime? fromDate = Convert.ToDateTime("2017-01-01"); 

            while (fromDate <= DateTime.Now)
            {
                List<BillPaymentCheckRet> billPaymentRets = (new QBXBillPayment()).GetAllBillPaymentsCHK(fromDate);                
                foreach (BillPaymentCheckRet billPaymentRet in billPaymentRets)
                {
                    try
                    {
                        insertBillCount++;
                        GlobalValue.Eventlogupdate("Setting inactive Bill Payments record having name " + billPaymentRet.RefNumber, null);
                        DataContext.QB_BillPaymentSave(billPaymentRet.TxnID,
                            billPaymentRet.AppliedToTxnRet[0]?.TxnID,
                            billPaymentRet.TimeCreated,
                            billPaymentRet.TimeModified,
                            billPaymentRet.EditSequence,
                            billPaymentRet.TxnNumber, 
                            billPaymentRet.PayeeEntityRef?.ListID, 
                            billPaymentRet.PayeeEntityRef?.FullName,
                            billPaymentRet.APAccountRef?.ListID,
                            billPaymentRet.APAccountRef?.FullName,
                            billPaymentRet.TxnDate,
                            billPaymentRet.BankAccountRef?.ListID,
                            billPaymentRet.BankAccountRef?.FullName,
                            billPaymentRet.Amount.ToString(),
                            billPaymentRet.AppliedToTxnRet[0]?. RefNumber,
                            billPaymentRet.Memo
                        );
                    }
                    catch (Exception ex)
                    {
                        GlobalValue.Eventlogupdate(ex, null, "Error");
                        //   richTextBox1.AppendText(ex.Message + "\n");
                    }
                    onProgressChanged(new ProgressEventArgs
                    {
                        Total = billPaymentRets.Count,
                        Current = insertBillCount
                    });
                }
                fromDate = fromDate.Value.AddDays(2);

            }
            onProgressChanged(new ProgressEventArgs
            {
                Completed = true
            });           

            return responses;
        }
    }
}
