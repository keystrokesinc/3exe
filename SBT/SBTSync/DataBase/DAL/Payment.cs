﻿using System;
using System.Collections.Generic;
using DataProcessor;
using DataProcessor.QB.XMLProcessor;
using DataProcessor.Sync.ModelRet;
using System.Data.Linq;
using System.Data.SqlClient;
using DataProcessor.Sync.ModelAdd;
using DataProcessor.Sync;
using SBTSync.Properties;

namespace SBTSync.DataBase.DAL
{
    public class Payment : DBBase
    {
      
        public static int insertPaymentCount = 0;
        public static GlobalValue.Reponse ProcessPayment(Action<ProgressEventArgs> onProgressChanged)
        {

            GlobalValue.Reponse responses = new GlobalValue.Reponse();
            QBXPaymentMethod payment = new QBXPaymentMethod();

            DateTime? qbInvoiceLastMod = GetLastModified("Payment");

            GlobalValue.Eventlogupdate("Getting Payments", null);


            ISingleResult<Logs_SelectResult> paymentSelectResults = DataContext.Payment_Select_AddQuickBooks(Credentials.Get.CompanyId);
            List<Logs_SelectResult> selectResults = new List<Logs_SelectResult>(paymentSelectResults);
            insertPaymentCount = 0;
            if (selectResults.Count > 0)
            {
                foreach (Logs_SelectResult invoiceSelectResult in selectResults)
                {

                    GlobalValue.Reponse res = new GlobalValue.Reponse();
                    try
                    {
                        if (invoiceSelectResult.CustomerID == null)
                        {
                            invoiceSelectResult.CustomerID = Customer.insertCustomerToQB(invoiceSelectResult); //error need to handle
                        }
                         res = SavePayment(invoiceSelectResult, payment);
                    }
                    catch (Exception ex)
                    {
                        GlobalValue.Eventlogupdate(ex, null, "Error");
                    }
                }
            }
            onProgressChanged(new ProgressEventArgs
            {
                Completed = true
            });
            if (insertPaymentCount > 0)
                responses.Message =  $"{insertPaymentCount} Payment(s) added to quickbooks\n";
            return responses;
        }
       public static GlobalValue.Reponse SavePayment(Logs_SelectResult paySelectAddQuickBooksResult, QBXPaymentMethod payment)
        {
            GlobalValue.Reponse responses = new GlobalValue.Reponse();            
            try
            {
                ReceivePaymentAdd paymentaddRq = new ReceivePaymentAdd
                {
                    CustomerRef = new DataProcessor.Sync.ModelAdd.ReceivePaymentRetCustomerRef
                    {
                        ListID = paySelectAddQuickBooksResult.CustomerID,
                        FullName = paySelectAddQuickBooksResult.Customer
                    },
                    TotalAmount = string.Format("{0:0.00}", paySelectAddQuickBooksResult.Amount),
                    IsAutoApply = false,                   


            };
                if (!string.IsNullOrEmpty(paySelectAddQuickBooksResult.Memo))
                    paymentaddRq.Memo = paySelectAddQuickBooksResult.Memo + " " + paySelectAddQuickBooksResult.PaymentID;
                if (!string.IsNullOrEmpty(paySelectAddQuickBooksResult.FormNumber))
                    paymentaddRq.RefNumber = paySelectAddQuickBooksResult.FormNumber;
                if (!string.IsNullOrEmpty(paySelectAddQuickBooksResult.MoneyVoucherDate))
                    paymentaddRq.TxnDate = paySelectAddQuickBooksResult.MoneyVoucherDate;               
                List<GlobalValue.Reponse> savePayment = payment.SavePayment(paymentaddRq);
                insertPaymentCount++;
                if (savePayment.Count > 0 && !savePayment[0].IsError)
                {                   
                    var checkSerializeXml = SyncConstant.SerializeXml<ReceivePaymentRet>(savePayment[0].EntityObject.ToString(), QueryXML.ReceivePayment, false);
                    if (checkSerializeXml.Count > 0)
                    {
                        DataContext.UpdateLogPayment_afterSync(paySelectAddQuickBooksResult.ID, Convert.ToInt32(checkSerializeXml[0].TxnNumber));
                        GlobalValue.Eventlogupdate("Payment having TxnNumber " + checkSerializeXml[0].TxnNumber + " inserted to database", null);
                        responses.IsError = false;
                        responses.Message =  checkSerializeXml[0].TxnID;                       
                    }                   
                }


                if (savePayment[0].IsError)
                {
                    responses.IsError = true;
                    string resMsg = "Error!- Payment for customer " + paySelectAddQuickBooksResult.Customer + ",\n PaymentID " +
                              paySelectAddQuickBooksResult.PaymentID + ", with amount $" +
                              paySelectAddQuickBooksResult.Amount + ", was not added to QuickBooks because: ";
                    responses.Message += resMsg + savePayment[0].Message + "\n";
                    GlobalValue.Eventlogupdate(responses.Message, null, "Error");
                  //  MailSender.Send(Settings.Default.MAILTO, Credentials.Get.CompanyId + ": Error in Payment Sync " + Credentials.SyncDateTime.ToString("MM-dd-yyyy HH:mm"), responses.Message);
                    return responses;

                }
            }
            catch (Exception ex)
            {
                responses.IsError = true;
                string resMsg = "Error!- Payment for customer " + paySelectAddQuickBooksResult.Customer + ",\n PaymentID " +
                              paySelectAddQuickBooksResult.PaymentID + ", with amount $" +
                              paySelectAddQuickBooksResult.Amount + ", was not added to QuickBooks because: ";
                responses.Message += resMsg + ex.Message + "\n";
                GlobalValue.Eventlogupdate(responses.Message, null, "Error");
                
            }
            return responses;
        }

    }
}
