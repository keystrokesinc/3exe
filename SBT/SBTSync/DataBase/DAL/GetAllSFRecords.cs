﻿using Newtonsoft.Json;
using SBTSync.DataBase;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using SBTSync.DataBase.DAL;

namespace SBTSync.DataBase.DAL
{
    class GetAllSFRecords
    {

        public class Item
        {
            public string OpportunityStage { get; set; }
            public string Opportunity { get; set; }
            public string Name { get; set; }
            public string Id { get; set; }
            public string GeneralAccountingUnit { get; set; }
            public string DateOfDonation { get; set; }
            public string ClassName { get; set; }
            public double Amount { get; set; }
        }

        public class Invoice
        {
            public object TxnID { get; set; }
            public string TxnDate { get; set; }
            public string SFReferance { get; set; }
            public string RefNumber { get; set; }
            public string PaidInstallments { get; set; }
            public string PaidAmount { get; set; }
            public List<Item> Items { get; set; }
            public string ItemRefFullName { get; set; }
            public string IitemLIstID { get; set; }
            public string DueDate { get; set; }
            public object CustomerRefListID { get; set; }
            public string CustomerRefFullName { get; set; }
            public string ClassRefListID { get; set; }
            public string ClassRefFullName { get; set; }
            public string Amount { get; set; }
        }

        public class RootObject
        {
            public string Message { get; set; }
            public bool isSuccess { get; set; }
            public List<Invoice> invoices { get; set; }
        }

        public class Customer
        {
            public string SFReferance { get; set; }
            public string Salutation { get; set; }
            public string Name { get; set; }
            public string LastName { get; set; }
            public string FirstName { get; set; }
            public string BillAddressState { get; set; }
            public string BillAddressPostalCode { get; set; }
            public string BillAddressCountry { get; set; }
            public string BillAddressCity { get; set; }
            public string BillAddressAddr1 { get; set; }
        }

        public class SalesReceipt
        {
            public string TxnID { get; set; }
            public string TxnDate { get; set; }
            public string TotalAmount { get; set; }
            public string SFReferance { get; set; }
            public string RefNumber { get; set; }
            public string RecordtypeName { get; set; }
            public string OpportunityStage { get; set; }
            public string DepositToAccountRefListID { get; set; }
            public string DepositToAccountRefFullName { get; set; }
            public string DateOfDonation { get; set; }
            public string CustomerRefListID { get; set; }
            public string CustomerRefFullName { get; set; }
            public string closeDate { get; set; }
            public string DonationDescription { get; set; }
            public string donationImportbatch { get; set; }
            public string paymentMethod { get; set; }
            public Customer customer { get; set; }
            public string ClassRefListID { get; set; }
            public string ClassRefFullName { get; set; }
            public string CheckNumber { get; set; }
        }
        

        public class InvoiceLinkedTxn
        {
            public string TxnID { get; set; }
            public string TxnDate { get; set; }
            public string TotalAmount { get; set; }
            public string SFReferance { get; set; }
            public object RefNumber { get; set; }
            public string RecordtypeName { get; set; }
            public string OpportunityStage { get; set; }
            public string LinktoTxnID1 { get; set; }
            public string DateOfDonation { get; set; }
            public string CustomerRefListID { get; set; }
            public string CustomerRefFullName { get; set; }
            public Customer customer { get; set; }
            public string ClassRefListID { get; set; }
            public string ClassRefFullName { get; set; }
            public string ARAccountRefListID { get; set; }
            public string ARAccountRefFullName { get; set; }
            public string InvoiceId { get; set; }
        }

        public class RootObject1
        {
            public List<SalesReceipt> salesReceipts { get; set; }
            public string Message { get; set; }
            public bool isSuccess { get; set; }
            public List<InvoiceLinkedTxn> invoiceLinkedTxns { get; set; }
        }

        class ApiSendData
        {
            public string grant_type { get; set; }
            public string client_id { get; set; }
            public string client_secret { get; set; }
            public string username { get; set; }
            public string password { get; set; }
        }
        public class LoginResponse
        {
            public string access_token { get; set; }
            public string instance_url { get; set; }
            public string id { get; set; }
            public string token_type { get; set; }
            public string issued_at { get; set; }
            public string signature { get; set; }
        }
        public static string GetRecords()
        {
            string resMessage = "";
            string connetionString = Credentials.Get.ConnectionString;
            SqlConnection cnn = new SqlConnection(connetionString);
            try
            {
                resMessage += "Salesforce Sync started....\n";
                LoginResponse res = null;
                //***DB CONNECTION ***//
                //string connetionString = @"Data Source = 172.16.0.43; Initial Catalog = SBT; Persist Security Info = True; User ID = SBTuser; Password = @!~SBT123";
                                
                cnn.Open();
                //Get Last Modified Date
                string qbInvoiceLastMod;
                string currentDate = DateTime.Now.ToString("dd-MM-yyyy");
                using (SqlCommand cmd = new SqlCommand("Settings_Select", cnn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("EntityName", SqlDbType.VarChar).Value = "Log";
                    cmd.Parameters.Add("CompanyID", SqlDbType.Int).Value = Credentials.Get.CompanyId;
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        var tb = new DataTable();
                        tb.Load(dr);
                        qbInvoiceLastMod = Convert.ToDateTime(tb.Rows[0]["LastModifyDate"]).ToString("dd-MM-yyyy");
                    }

                }
                

                //string loginURL = "https://pvp--qbtesting.my.salesforce.com/services/oauth2/token?";
                //string loginFullURL = "https://pvp--qbtesting.my.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9Nc1qcZ7BbZ0a4Q2fhHoqlj9I2wvpEGk0two3SD9maCoL.iFSnSEUBEwTrPvsRaRdZYNz0FGeFLudcZWL&client_secret=42BC9E36CDD596425B7177AEFEDD0668BCCCDE01C91A2319B1D57E97FCDC92A9&username=dharris@parkinsonvoiceproject.org.qbtesting&password=@!~Park123";
                //string instanceURL = "https://pvp--qbtesting.my.salesforce.com/services/apexrest/";

                //**LIVE URL**//
                string loginURL = "https://login.salesforce.com/services/oauth2/token?";
                string loginFullURL = "https://login.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9oNqAtcJCF.EGnjKYNbXn57OdNsZyw8kuhVd0x__LMZvRMaHMIDxWGSqS.FzMFNs2_tVu6N74OZvwB3j4&client_secret=F106A47159B0AD459C7378BB43D21D2AA2BE92155B3C5B1DD1C0A571DD0AE1C3&username=dharris@parkinsonvoiceproject.org&password=Park@@321rT6YfmG6YlY3liA6P12EQtClJ";
                string instanceURL = "https://pvp.my.salesforce.com/services/apexrest/";
               
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(loginURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    // HTTP POST Sales Force Login
                    var datatobeSent = new ApiSendData()
                    {
                        //grant_type = "password",
                        //client_id = "3MVG9Nc1qcZ7BbZ0a4Q2fhHoqlj9I2wvpEGk0two3SD9maCoL.iFSnSEUBEwTrPvsRaRdZYNz0FGeFLudcZWL",
                        //client_secret = "42BC9E36CDD596425B7177AEFEDD0668BCCCDE01C91A2319B1D57E97FCDC92A9",
                        //username = "dharris@parkinsonvoiceproject.org.qbtesting",
                        //password = "@!~Park123"

                        //**FOR LIVE **//
                        grant_type = "password",
                        client_id = "3MVG9oNqAtcJCF.EGnjKYNbXn57OdNsZyw8kuhVd0x__LMZvRMaHMIDxWGSqS.FzMFNs2_tVu6N74OZvwB3j4",
                        client_secret = "F106A47159B0AD459C7378BB43D21D2AA2BE92155B3C5B1DD1C0A571DD0AE1C3",
                        username = "dharris@parkinsonvoiceproject.org",
                        password = "Park@@321rT6YfmG6YlY3liA6P12EQtClJ"
                    };

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    var response = client.PostAsJsonAsync(loginFullURL, datatobeSent).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        res = response.Content.ReadAsAsync<LoginResponse>().Result;
                    }
                }
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(instanceURL + "customerTransactions?SearchBy=all&filterBy=cd&fd=" + qbInvoiceLastMod +"&td=" + qbInvoiceLastMod);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(res.token_type, res.access_token);

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    var response1 = client.GetAsync(instanceURL + "customerTransactions?SearchBy=all&filterBy=cd&fd=" + qbInvoiceLastMod + "&td=" + qbInvoiceLastMod).Result;
                    if (response1.IsSuccessStatusCode)
                    {
                        var jsonString = response1.Content.ReadAsStringAsync();
                        var objData = JsonConvert.DeserializeObject<RootObject1>(jsonString.Result);
                        foreach (var row in objData.salesReceipts)
                        {

                            if (row.ClassRefFullName != null && row.TxnID == null && row.donationImportbatch== "2019_09_12 Checks")
                            {
                                if (row.ClassRefFullName.Contains("  "))
                                    row.ClassRefFullName = row.ClassRefFullName.Replace("  ", " ");
                               
                                    using (SqlCommand cmd = new SqlCommand("SFSalesReceipt_Insert", cnn))
                                    {
                                    try
                                    {
                                        cmd.CommandType = CommandType.StoredProcedure;
                                        cmd.Parameters.Add("CustomerRefListID", SqlDbType.VarChar).Value = row.CustomerRefListID != null ? row.CustomerRefListID : null;
                                        cmd.Parameters.Add("CustomerRefFullName", SqlDbType.VarChar).Value = row.CustomerRefFullName != null ? row.CustomerRefFullName : null;
                                        cmd.Parameters.Add("ClassRefListID", SqlDbType.VarChar).Value = row.ClassRefListID.Contains("\t") ? row.ClassRefListID.Replace("\t", " ") : row.ClassRefListID;
                                        cmd.Parameters.Add("ClassRefFullName", SqlDbType.VarChar).Value = row.ClassRefFullName.Contains("\t") ? row.ClassRefFullName.Replace("\t", " ") : row.ClassRefFullName;
                                        cmd.Parameters.Add("DepositToAccountRefListID", SqlDbType.VarChar).Value = row.DepositToAccountRefListID != null ? row.DepositToAccountRefListID : null;
                                        cmd.Parameters.Add("DepositToAccountRefFullName", SqlDbType.VarChar).Value = row.DepositToAccountRefFullName != null ? row.DepositToAccountRefFullName : null;
                                        cmd.Parameters.Add("TxnDate", SqlDbType.VarChar).Value = row.closeDate != null ? row.closeDate : null;
                                        cmd.Parameters.Add("RefNumber", SqlDbType.VarChar).Value = row.RefNumber != null ? row.RefNumber : null;
                                        cmd.Parameters.Add("SFReferance", SqlDbType.VarChar).Value = row.SFReferance != null ? row.SFReferance : null;
                                        cmd.Parameters.Add("TotalAmount", SqlDbType.VarChar).Value = row.TotalAmount != null ? row.TotalAmount : null;
                                        cmd.Parameters.Add("RecordtypeName", SqlDbType.VarChar).Value = row.RecordtypeName != null ? row.RecordtypeName : null;
                                        cmd.Parameters.Add("CheckNumber", SqlDbType.VarChar).Value = row.CheckNumber != null ? row.CheckNumber : null;
                                        cmd.Parameters.Add("TxnID", SqlDbType.VarChar).Value = row.TxnID != null ? row.TxnID : null;
                                        cmd.Parameters.Add("OpportunityStage", SqlDbType.VarChar).Value = row.OpportunityStage != null ? row.OpportunityStage : null;
                                        cmd.Parameters.Add("DateOfDonation", SqlDbType.VarChar).Value = row.DateOfDonation != null ? row.DateOfDonation : null;
                                        cmd.Parameters.Add("CustSFReferance", SqlDbType.VarChar).Value = row.customer.SFReferance != null ? row.customer.SFReferance : null;
                                        cmd.Parameters.Add("Salutation", SqlDbType.VarChar).Value = row.customer.Salutation != null ? row.customer.Salutation : null;
                                        cmd.Parameters.Add("Name", SqlDbType.VarChar).Value = row.customer.Name != null ? row.customer.Name : null;
                                        cmd.Parameters.Add("LastName", SqlDbType.VarChar).Value = row.customer.LastName != null ? row.customer.LastName : null;
                                        cmd.Parameters.Add("FirstName", SqlDbType.VarChar).Value = row.customer.FirstName != null ? row.customer.FirstName : null;
                                        cmd.Parameters.Add("BillAddressState", SqlDbType.VarChar).Value = row.customer.BillAddressState != null ? row.customer.BillAddressState : null;
                                        cmd.Parameters.Add("BillAddressPostalCode", SqlDbType.VarChar).Value = row.customer.BillAddressPostalCode != null ? row.customer.BillAddressPostalCode : null;
                                        cmd.Parameters.Add("BillAddressCountry", SqlDbType.VarChar).Value = row.customer.BillAddressCountry != null ? row.customer.BillAddressCountry : null;
                                        cmd.Parameters.Add("BillAddressCity", SqlDbType.VarChar).Value = row.customer.BillAddressCity != null ? row.customer.BillAddressCity : null;
                                        cmd.Parameters.Add("BillAddressAddr1", SqlDbType.VarChar).Value = row.customer.BillAddressAddr1 != null ? row.customer.BillAddressAddr1 : null;
                                        cmd.Parameters.Add("CompanyID", SqlDbType.Int).Value = Credentials.Get.CompanyId;
                                        cmd.Parameters.Add("ItemDesc", SqlDbType.VarChar).Value = row.DonationDescription != null ? row.DonationDescription : null;
                                        cmd.Parameters.Add("Memo", SqlDbType.VarChar).Value = row.donationImportbatch != null ? row.donationImportbatch : null;
                                        cmd.Parameters.Add("PaymentMethod", SqlDbType.VarChar).Value = row.paymentMethod != null ? row.paymentMethod : null;
                                     
                                        cmd.ExecuteNonQuery();
                                     
                                    }
                                    catch(Exception ex)
                                    {

                                    }
                                    }
                                    resMessage += "PVP Sales Receipt : " + row.SFReferance + " saved to DB.\n";
                               // }
                            }
                        }
                        //if (objData.invoiceLinkedTxns?.Count > 0)
                        //{
                        //    foreach (var transaction in objData.invoiceLinkedTxns)
                        //    {
                        //        //InvoiceLinkedTxns Insert
                        //        using (SqlCommand cmd = new SqlCommand("InvoiceLinkedTxn_Insert", cnn))
                        //        {
                        //            cmd.CommandType = CommandType.StoredProcedure;
                        //            cmd.Parameters.Add("CustomerRefListID", SqlDbType.VarChar).Value = transaction.CustomerRefListID;
                        //            cmd.Parameters.Add("CustomerRefFullName", SqlDbType.VarChar).Value = transaction.CustomerRefFullName;
                        //            cmd.Parameters.Add("ClassRefListID", SqlDbType.VarChar).Value = transaction.ClassRefListID;
                        //            cmd.Parameters.Add("ClassRefFullName", SqlDbType.VarChar).Value = transaction.ClassRefFullName;
                        //            cmd.Parameters.Add("ARAccountRefListID", SqlDbType.VarChar).Value = transaction.ARAccountRefListID;
                        //            cmd.Parameters.Add("ARAccountRefFullName", SqlDbType.VarChar).Value = transaction.ARAccountRefFullName;
                        //            cmd.Parameters.Add("TxnDate", SqlDbType.DateTime).Value = transaction.TxnDate;
                        //            cmd.Parameters.Add("TotalAmount", SqlDbType.VarChar).Value = transaction.TotalAmount;
                        //            cmd.Parameters.Add("SFReferance", SqlDbType.VarChar).Value = transaction.SFReferance;
                        //            cmd.Parameters.Add("RefNumber", SqlDbType.VarChar).Value = transaction.RefNumber;
                        //            cmd.Parameters.Add("RecordtypeName", SqlDbType.VarChar).Value = transaction.RecordtypeName;
                        //            cmd.Parameters.Add("LinktoTxnID1", SqlDbType.VarChar).Value = transaction.LinktoTxnID1;
                        //            cmd.Parameters.Add("TxnNumber", SqlDbType.VarChar).Value = transaction.TxnID;
                        //            cmd.Parameters.Add("TxnID", SqlDbType.VarChar).Value = transaction.TxnID;
                        //            cmd.Parameters.Add("OpportunityStage", SqlDbType.VarChar).Value = transaction.OpportunityStage;
                        //            cmd.Parameters.Add("DateOfDonation", SqlDbType.VarChar).Value = transaction.DateOfDonation;
                        //            cmd.Parameters.Add("CustSFReferance", SqlDbType.VarChar).Value = transaction.customer.SFReferance;
                        //            cmd.Parameters.Add("Salutation", SqlDbType.VarChar).Value = transaction.customer.Salutation;
                        //            cmd.Parameters.Add("Name", SqlDbType.VarChar).Value = transaction.customer.Name;
                        //            cmd.Parameters.Add("LastName", SqlDbType.VarChar).Value = transaction.customer.LastName;
                        //            cmd.Parameters.Add("FirstName", SqlDbType.VarChar).Value = transaction.customer.FirstName;
                        //            cmd.Parameters.Add("BillAddressState", SqlDbType.VarChar).Value = transaction.customer.BillAddressState;
                        //            cmd.Parameters.Add("BillAddressPostalCode", SqlDbType.VarChar).Value = transaction.customer.BillAddressPostalCode;
                        //            cmd.Parameters.Add("BillAddressCountry", SqlDbType.VarChar).Value = transaction.customer.BillAddressCountry;
                        //            cmd.Parameters.Add("BillAddressCity", SqlDbType.VarChar).Value = transaction.customer.BillAddressCity;
                        //            cmd.Parameters.Add("BillAddressAddr1", SqlDbType.VarChar).Value = transaction.customer.BillAddressAddr1;
                        //            cmd.Parameters.Add("InvSFReference", SqlDbType.VarChar).Value = transaction.InvoiceId; 
                        //            cmd.ExecuteNonQuery();
                        //        }
                        //        resMessage += "PVP Linked Transaction : " + transaction.SFReferance + " saved to DB.\n";
                        //    }


                        //}
                    }
                }
                //***INVOICE***// 
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(instanceURL + "invoices?SearchBy=all&filterBy=cd&fd" + qbInvoiceLastMod + "&td=" + qbInvoiceLastMod);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(res.token_type, res.access_token);

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    var response = client.GetAsync(instanceURL + "invoices?SearchBy=all&filterBy=cd&fd" + qbInvoiceLastMod + "&td=" + qbInvoiceLastMod).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var jsonString = response.Content.ReadAsStringAsync();
                        var objData = JsonConvert.DeserializeObject<RootObject>(jsonString.Result);
                        if (objData.invoices?.Count > 0)
                        {
                            foreach (var transaction in objData.invoices)
                            {
                                if (transaction.ClassRefFullName != null)
                                {
                                    if (transaction.ClassRefFullName.Contains("  "))
                                        transaction.ClassRefFullName = transaction.ClassRefFullName.Replace("  ", " ");
                                    int InvoiceID = 0;
                                    using (SqlCommand cmd = new SqlCommand("SFInvoice_Insert", cnn))
                                    {
                                        cmd.CommandType = CommandType.StoredProcedure;
                                        cmd.Parameters.Add("CustomerRefListID", SqlDbType.VarChar).Value = transaction.CustomerRefListID;
                                        cmd.Parameters.Add("CustomerRefFullName", SqlDbType.VarChar).Value = transaction.CustomerRefFullName;
                                        cmd.Parameters.Add("ClassRefListID", SqlDbType.VarChar).Value = transaction.ClassRefListID.Contains("\t") ? transaction.ClassRefListID.Replace("\t", " ") : transaction.ClassRefListID;
                                        cmd.Parameters.Add("ClassRefFullName", SqlDbType.VarChar).Value = transaction.ClassRefFullName.Contains("\t") ? transaction.ClassRefFullName.Replace("\t", " ") : transaction.ClassRefFullName;
                                        cmd.Parameters.Add("IitemLIstID", SqlDbType.VarChar).Value = transaction.IitemLIstID;
                                        cmd.Parameters.Add("ItemRefFullName", SqlDbType.VarChar).Value = transaction.ItemRefFullName;
                                        cmd.Parameters.Add("TxnDate", SqlDbType.VarChar).Value = transaction.TxnDate;
                                        cmd.Parameters.Add("RefNumber", SqlDbType.VarChar).Value = transaction.RefNumber;
                                        cmd.Parameters.Add("DueDate", SqlDbType.VarChar).Value = transaction.DueDate;
                                        cmd.Parameters.Add("Amount", SqlDbType.VarChar).Value = transaction.Amount;
                                        cmd.Parameters.Add("PaidInstallments", SqlDbType.VarChar).Value = transaction.PaidInstallments;
                                        cmd.Parameters.Add("PaidAmount", SqlDbType.VarChar).Value = transaction.PaidAmount;
                                        cmd.Parameters.Add("TxnID", SqlDbType.VarChar).Value = transaction.TxnID;
                                        cmd.Parameters.Add("SFReferance", SqlDbType.VarChar).Value = transaction.SFReferance;
                                        cmd.Parameters.Add("CompanyID", SqlDbType.Int).Value = Credentials.Get.CompanyId;
                                        InvoiceID = (int)cmd.ExecuteScalar();
                                    }
                                    resMessage += "PVP Invoice : " + transaction.SFReferance + " saved to DB.\n";
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            resMessage += "Salesforce Sync Completed.\n";
            //Update Modified Date
            using (SqlCommand cmd = new SqlCommand("Settings_InsertUpdate", cnn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("EntityName", SqlDbType.VarChar).Value = "Log";
                cmd.Parameters.Add("Date", SqlDbType.DateTime).Value = DateTime.UtcNow;
                cmd.Parameters.Add("CompanyID", SqlDbType.VarChar).Value = Credentials.Get.CompanyId;
                //cmd.ExecuteNonQuery();
            }

            return resMessage;
        }
    }
}
