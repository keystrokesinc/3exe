﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBTSync.DataBase.DAL
{
    class CustomerListClass
    {
        public class CustomersList
        {
            public int CompanyID { get; set; }
            public string ListID { get; set; }
            public DateTime TimeCreated { get; set; }
            public DateTime TimeModified { get; set; }
            public string FullName { get; set; }
            public bool IsActive { get; set; }
            public string Sal { get; set; }
            public string MiddleName { get; set; }
            public string LastName { get; set; }
            public string CompanyName { get; set; }
            public string PrimaryPhone { get; set; }
            public string Fax { get; set; }
            public string PrimaryEmailAddr { get; set; }
            public string BillAddrLine1 { get; set; }
            public string BillAddrLine2 { get; set; }
            public string BillAddrLine3 { get; set; }
            public string BillAddrLine4 { get; set; }
            public string BillAddrLine5 { get; set; }
            public string BillAddrCity { get; set; }
            public string BillAddrState { get; set; }
            public string BillAddrCountry { get; set; }
            public string BillAddrPostalCode { get; set; }
            public string Balance { get; set; }

            public CustomersList(int CompanyID_, string ListID_, DateTime TimeCreated_, DateTime TimeModified_, string FullName_, bool IsActive_, string Sal_, string MiddleName_, string LastName_, string CompanyName_, string PrimaryPhone_, string Fax_, string PrimaryEmailAddr_, string BillAddrLine1_, string BillAddrLine2_, string BillAddrLine3_, string BillAddrLine4_, string BillAddrLine5_, string BillAddrCity_, string BillAddrState_, string BillAddrCountry_, string BillAddrPostalCode_, string Balance_)
            {
                this.CompanyID = CompanyID_;
                this.ListID = ListID_;
                this.TimeCreated = TimeCreated_;
                this.TimeModified = TimeModified_;
                this.FullName = FullName_;
                this.IsActive = IsActive_;
                this.Sal = Sal_;
                this.MiddleName = MiddleName_;
                this.LastName = LastName_;
                this.CompanyName = CompanyName_;
                this.PrimaryPhone = PrimaryPhone_;
                this.Fax = Fax_;
                this.PrimaryEmailAddr = PrimaryEmailAddr_;
                this.BillAddrLine1 = BillAddrLine1_;
                this.BillAddrLine2 = BillAddrLine2_;
                this.BillAddrLine3 = BillAddrLine3_;
                this.BillAddrLine4 = BillAddrLine4_;
                this.BillAddrLine5 = BillAddrLine5_;
                this.BillAddrCity = BillAddrCity_;
                this.BillAddrState = BillAddrState_;
                this.BillAddrCountry = BillAddrCountry_;
                this.BillAddrPostalCode = BillAddrPostalCode_;
                this.Balance = Balance_;
            }
        }
    }
}
