namespace SBTSync.DataBase
{
    public class ProgressEventArgs
    {
        public int Total { get; set; }
        public int Current { get; set; }
        public bool Completed { get; set; }
    }
}