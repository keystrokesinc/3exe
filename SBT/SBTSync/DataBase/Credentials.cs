﻿using System;
using System.Net;
using System.Text;
using System.Windows.Forms;
using DataProcessor;
using SBTSync.Properties;
using DataProcessor.QB.XMLProcessor;
using DataProcessor.Sync;
using DataProcessor.Sync.ModelRet;
using SBTSync.DataBase.DAL;
using System.Configuration;

namespace SBTSync.DataBase
{
    public class Credentials
    {
        public int CompanyId { get; private set; }

        public string ConnectionString { get; private set; }

        private static int companyId;

        private static string connectionString;
        public  string companyName { get; set; }

        public static string CompanyEIN;

        public static string CompanyName { get; set; }
        public static DateTime SyncDateTime { get; set; }

        public static Credentials Get
        {
            get
            {
                return new Credentials
                {
                    CompanyId = companyId,
                    ConnectionString = connectionString,
                    companyName = "SBT - " + CompanyName
                };
            }
        }

        public static bool Update()
        {
            try
            {
                SyncDateTime = DateTime.Now;
                using (var client = new WebClient())
                {
                    client.Encoding = Encoding.UTF8;
                    client.Headers[HttpRequestHeader.ContentType] = "application/json";
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    string response = client.UploadString(Settings.Default.APIURI, "{LicenseKey:'" + config.AppSettings.Settings["LICENSEKEY"].Value.ToString() + "'}");
                    var splitted = response.Split(new[] { "||||" }, StringSplitOptions.None);
                    connectionString = splitted[0];
                    companyId = Convert.ToInt32(splitted[1]);

                    //connectionString = "Data Source=67.210.238.62,12381;Initial Catalog=IVSR;uid=QBSync1;password=@!~QBS123";//"Data Source=172.16.0.21;Initial Catalog=SBTLive;uid=SBTLive;password=@!~sbtlive123"; //"Data Source=172.16.0.43;Initial Catalog=SBT;uid=SBTuser;password=@!~SBT123 "; //"Data Source=172.16.0.21;Initial Catalog=SBTLive;uid=SBTLive;password=@!~sbtlive123";//splitted[0]; //
                    //companyId = 6;// 23;//parkinson 46;// 2826;// 6;// 2826;//52;
                    Company_SelectResult cInfo = DBBase.GetCompanyInfo(companyId);

                    if (cInfo != null)
                    {
                        CompanyName = cInfo.CompanyName;
                        CompanyEIN = cInfo.EIN;
                    }
                    QBXCompany objCompanyConn = new QBXCompany();
                    DateTime? qbCompanyLastMod = null;
                    CompanyRet objCompany = objCompanyConn.GetCompanyInformation(qbCompanyLastMod);
                    CompanyEIN = objCompany.EIN;
                    CompanyName = objCompany.CompanyName;
                    //GlobalValue.CompanyEIN = CompanyEIN;

                }
            }
            catch (WebException exception)
            {
                LogWriter.WriteToLog("Inside Update Call" + DataProcessor.GlobalValue.GetAllFootprints(exception));
                MessageBox.Show(exception.Message.Contains("400")
                    ? "Invalid license key."
                    : "Cannot connect to API. Please check API URI or contact system administrator.");

                return false;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                GlobalValue.Eventlogupdate("Could't update database credentials. Error: " + exception.Message, null, "Error");
                return false;
            }

            return true;
        }
    }
}
