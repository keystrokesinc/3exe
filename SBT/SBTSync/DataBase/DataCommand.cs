﻿using SBTSync.DataBase;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace DataProcessor
{

    public class DataCommand
    {
        private readonly string _connectionString;

        public DataCommand(string name = "DBEntities")
        {
            // _connectionString = "Data Source = 172.16.0.15; Initial Catalog = Lazarus; Persist Security Info = True; User ID = Sunny_Login; Password = Sunny@123"; 
            //   _connectionString = "Data Source = 172.16.0.43; Initial Catalog = EtzHayim; Persist Security Info = True; User ID = sa; Password = @!~Admin123";

            _connectionString = Credentials.Get.ConnectionString;//SBTSync.Properties.Settings.Default.SBTQAConnectionString;// "Data Source = 44.233.102.202; Initial Catalog = Raw; Persist Security Info = True; User ID = KeyUser; Password = @!~Keyuser123; connection timeout=40000";

        }
        public DataTable GetTablewithQuery(string sql, params SqlParameter[] parameters)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var command = connection.CreateCommand();

                try
                {
                    command.CommandType = CommandType.Text;
                    command.Parameters.AddRange(parameters);
                    command.CommandText = sql;

                    connection.Open();

                    using (var adapter = new SqlDataAdapter(command))
                    {
                        var dataTable = new DataTable();
                        adapter.Fill(dataTable);
                        return dataTable;
                    }
                }
                catch (SqlException exception)
                {
                    throw new WrappedSqlException(exception, command);
                }
                finally
                {
                    command.Dispose();
                }
            }
        }
        public DataTable GetTable(string procedureName, params SqlParameter[] parameters)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var command = connection.CreateCommand();

                try
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddRange(parameters);
                    command.CommandText = procedureName;

                    connection.Open();

                    using (var adapter = new SqlDataAdapter(command))
                    {
                        var dataTable = new DataTable();
                        adapter.Fill(dataTable);
                        return dataTable;
                    }
                }
                catch (SqlException exception)
                {
                    throw new WrappedSqlException(exception, command);
                }
                finally
                {
                    command.Dispose();
                }
            }
        }

        public DataSet GetSet(string procedureName, params SqlParameter[] parameters)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var command = connection.CreateCommand();

                try
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddRange(parameters);
                    command.CommandText = procedureName;

                    connection.Open();

                    using (var adapter = new SqlDataAdapter(command))
                    {
                        var dataSet = new DataSet();
                        adapter.Fill(dataSet);
                        return dataSet;
                    }
                }
                catch (SqlException exception)
                {
                    throw new WrappedSqlException(exception, command);
                }
                finally
                {
                    command.Dispose();
                }
            }
        }

        public T ExecuteScalar<T>(string procedureName, params SqlParameter[] parameters)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var command = connection.CreateCommand();

                try
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddRange(parameters);
                    command.CommandText = procedureName;

                    connection.Open();

                    var execResult = command.ExecuteScalar();

                    var result = execResult != null
                        ? (T)execResult
                        : default(T);

                    return result;
                }
                catch (SqlException exception)
                {
                    throw new WrappedSqlException(exception, command);
                }
                finally
                {
                    command.Dispose();
                }
            }
        }

        public int ExecuteAsNonQuery(string procedureName, params SqlParameter[] parameters)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var command = connection.CreateCommand();

                try
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddRange(parameters);
                    command.CommandText = procedureName;

                    connection.Open();

                    return command.ExecuteNonQuery();
                }
                catch (SqlException exception)
                {
                    throw new WrappedSqlException(exception, command);
                }
                finally
                {
                    command.Dispose();
                }
            }
        }

        public int ExecuteAsNonQueryCustomTimeout(string procedureName, int timeout, params SqlParameter[] parameters)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var command = connection.CreateCommand();

                try
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddRange(parameters);
                    command.CommandText = procedureName;
                    command.CommandTimeout = timeout;

                    connection.Open();

                    return command.ExecuteNonQuery();
                }
                catch (SqlException exception)
                {
                    throw new WrappedSqlException(exception, command);
                }
                finally
                {
                    command.Dispose();
                }
            }
        }
        public int Execute(string cmdText, params object[] paramList)
        {
            SqlConnection sqlConnection1 = new SqlConnection(this._connectionString);
            try
            {
                sqlConnection1.Open();
                SqlCommand paramCommand = this.GetParamCommand(cmdText, paramList);
                int num = 1200;
                paramCommand.CommandTimeout = num;
                SqlConnection sqlConnection2 = sqlConnection1;
                paramCommand.Connection = sqlConnection2;
                return paramCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                sqlConnection1.Close();
            }
        }


        private SqlCommand GetParamCommand(string cmdText, object[] paramList)
        {
            SqlCommand sqlCommand = new SqlCommand();
            if (paramList.Length != 0)
            {
                if (cmdText.ToLower().IndexOf("@") > -1)
                {
                    sqlCommand.CommandText = string.Format(cmdText, paramList);
                }
                else
                {
                    object[] objArray = new object[paramList.Length];
                    for (int index = 0; index < paramList.Length; ++index)
                        objArray[index] = (object)("@Param" + index.ToString());
                    sqlCommand.CommandText = string.Format(cmdText, objArray);
                    for (int index = 0; index < paramList.Length; ++index)
                        sqlCommand.Parameters.AddWithValue("@Param" + index.ToString(), paramList[index]);
                }
            }
            else
                sqlCommand.CommandText = cmdText;
            return sqlCommand;
        }

       
    }

    public class WrappedSqlException : Exception
    {
        private readonly string _query;

        public WrappedSqlException(Exception innerException, SqlCommand command)
            : base(innerException.Message, innerException)
        {
            _query = GetQuery(command);
        }

        private static string GetQuery(SqlCommand command)
        {
            if (command.Parameters == null || command.Parameters.Count <= 0)
            {
                return command.CommandText;
            }

            var @params = command.Parameters
                                 .Cast<SqlParameter>()
                                 .Select(parameter =>
                                 {
                                     var name = "@" + parameter.ParameterName;
                                     var value = parameter.SqlDbType == SqlDbType.Int
                                                                      ? parameter.Value != null
                                                                                         ? parameter.Value.ToString()
                                                                                         : ""
                                                                      : "'" + (parameter.Value != null
                                                                                                ? parameter.Value.ToString()
                                                                                                : "") + "'";

                                     return name + " = " + value;

                                 });

            return "exec " + command.CommandText + " " + string.Join(", ", @params);
        }

        public override string ToString()
        {
            return "\r\nSql Query: " + _query + "\r\n" + base.ToString();
        }


    }
   
}