﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DataProcessor.Ref
{
    [XmlRoot(ElementName = "ClassRef")]
    public class ClassRef
    {
        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }
        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }

    [XmlRoot(ElementName = "SalesRepRef")]
    public class SalesRepRef
    {
        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }
        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }

    [XmlRoot(ElementName = "CustomerRef")]
    public class CustomerRef
    {
        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }
        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }

    [XmlRoot(ElementName = "AccountRef")]
    public class AccountRef
    {
        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }
        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }

    /// <remarks/>
    [XmlRoot(ElementName = "ParentRef")]
    public partial class ParentRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlRoot(ElementName = "BillAddress")]
    public partial class BillAddress
    {
        public string Addr1
        {
            get; set;
        }

        /// <remarks/>
        public string Addr2
        {
            get; set;
        }

        /// <remarks/>
        public string Addr3
        {
            get; set;
        }

        /// <remarks/>
        public string Addr4
        {
            get; set;
        }

        /// <remarks/>
        public string Addr5
        {
            get; set;
        }

        /// <remarks/>
        public string City
        {
            get; set;
        }

        /// <remarks/>
        public string State
        {
            get; set;
        }

        /// <remarks/>
        public string PostalCode
        {
            get; set;
        }

        /// <remarks/>
        public string Country
        {
            get; set;
        }

        public string Note
        {
            get; set;
        }
    }

    /// <remarks/>
    [XmlRoot(ElementName = "BillAddressBlock")]
    public partial class BillAddressBlock
    {
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string Addr3 { get; set; }
        public string Addr4 { get; set; }
        public string Addr5 { get; set; }
    }

    /// <remarks/>
    [XmlRoot(ElementName = "ShipAddress")]
    public partial class ShipAddress
    {
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string Addr3 { get; set; }
        public string Addr4 { get; set; }
        public string Addr5 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Note { get; set; }
    }

    /// <remarks/>
    [XmlRoot(ElementName = "ShipAddressBlock")]
    public partial class ShipAddressBlock
    {
        private string addr1Field;

        private string addr2Field;

        private string addr3Field;

        private string addr4Field;

        private string addr5Field;

        /// <remarks/>
        public string Addr1
        {
            get
            {
                return addr1Field;
            }
            set
            {
                addr1Field = value;
            }
        }

        /// <remarks/>
        public string Addr2
        {
            get
            {
                return addr2Field;
            }
            set
            {
                addr2Field = value;
            }
        }

        /// <remarks/>
        public string Addr3
        {
            get
            {
                return addr3Field;
            }
            set
            {
                addr3Field = value;
            }
        }

        /// <remarks/>
        public string Addr4
        {
            get
            {
                return addr4Field;
            }
            set
            {
                addr4Field = value;
            }
        }

        /// <remarks/>
        public string Addr5
        {
            get
            {
                return addr5Field;
            }
            set
            {
                addr5Field = value;
            }
        }
    }

    [XmlRoot(ElementName = "TermsRef")]
    public partial class TermsRef
    {
        public string ListID { get; set; }

        /// <remarks/>
        public string FullName { get; set; }
    }

    [XmlRoot(ElementName = "ShipToAddress")]
    public partial class ShipToAddress
    {
        public string Name { get; set; }
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string Addr3 { get; set; }
        public string Addr4 { get; set; }
        public string Addr5 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Note { get; set; }

        public string DefaultShipTo { get; set; }
    }
}
