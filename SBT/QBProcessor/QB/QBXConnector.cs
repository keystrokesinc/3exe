﻿using System;
using DataProcessor.Properties;
using Interop.QBXMLRP2;

namespace DataProcessor.QB
{
    /// <summary>
    ///     Responsible to create connection with QuickBooks (Singelton Class)
    /// </summary>
    public class QBXConnector
    {
        public RequestProcessor2 RequestProcessor;
        private static QBXConnector _qbConnector;
        public string Ticket;
        private string _errorMessage;
        bool ConnectionOpen = false;
        bool SessionBegun = false;
        private QBXConnector()
        {
            if (!CreateConnection())
            {
            }
        }

        public static QBXConnector GetQBConnector()
        {
            return _qbConnector ?? (_qbConnector = new QBXConnector());
        }

        private  bool CreateConnection()
        {
            string filePath = Settings.Default.QBPATH;//"C:\\Projects\\SBTLive_Knock_QbFile\\knock knoc children's museum.qbw";

            string APPID = "7007";
            string cAppName = "SBT QuickBooks Sync";
          
            try
            {
                //Start Session and connection---------------------------------------------------------------------------------------------
                if (!SessionBegun)
                {
                    RequestProcessor = new RequestProcessor2();
                    RequestProcessor.OpenConnection(APPID , cAppName);
                    ConnectionOpen = true;

                  

                    Ticket = RequestProcessor.BeginSession(filePath, QBFileMode.qbFileOpenDoNotCare);

                   
                    SessionBegun = true;

                 
                }
                return SessionBegun;
            }
            catch (Exception e)
            {
                GlobalValue.Eventlogupdate("CreateConnection "+e.Message, null, "Error");
                ErrorLogger.LoggError(e);
                _errorMessage = e.Message;
                if (SessionBegun)
                    RequestProcessor.EndSession(Ticket);
                if (ConnectionOpen)
                    RequestProcessor.CloseConnection();

                return false;
            }
        }

        public void CloseConnection()
        {
            if (RequestProcessor != null)
            {
                if (SessionBegun)
                RequestProcessor.EndSession(Ticket);
                SessionBegun = false;
                if (ConnectionOpen)
                RequestProcessor.CloseConnection();
                ConnectionOpen = false;

            }
          _qbConnector = null;

        }
    }
}