﻿using DataProcessor.Sync;
using DataProcessor.Sync.ModelAdd;
using DataProcessor.Sync.ModelRet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataProcessor.QB.XMLProcessor
{
    public class QBXCreditCard : QBXBase
    {
        private static List<CheckRet> _fg;
        //public List<CheckRet> GetAllChecks(DateTime? qbBillLastMod, bool isFreshData = true)
        //{

        //    if (isFreshData)
        //        _fg = GetQBData<CheckRet>(QueryXML.Check, qbBillLastMod);
        //    return _fg;
        //}

        public List<GlobalValue.Reponse> SaveCreditCardCharge(CreditCardChargeAdd cc)
        {
            List<CreditCardChargeAdd> lstCreditcards = new List<CreditCardChargeAdd>();
            lstCreditcards.Add(cc);
            List<GlobalValue.Reponse> reponses = Save(QueryXML.CreditCardCharge, lstCreditcards);
            return reponses;
        }
    }
}
