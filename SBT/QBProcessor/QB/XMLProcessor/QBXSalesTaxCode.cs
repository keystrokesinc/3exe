﻿using System.Collections.Generic;
using System.Xml;
using DataProcessor.Sync;
using DataProcessor.Sync.ModelRet;

namespace DataProcessor.QB.XMLProcessor
{
    public class QBXSalesTaxCode : QBXBase
    {
        private string GetSalesTaxCodeXML =
            "<?xml version=\"1.0\" ?>\r\n<?qbxml version=\"8.0\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"stopOnError\">\r\n<SalesTaxCodeQueryRq>\r\n<ActiveStatus >ActiveOnly</ActiveStatus>\r\n</SalesTaxCodeQueryRq>\r\n</QBXMLMsgsRq>\r\n</QBXML>";

       

        private static List<SalesTaxCodeRet> _salesTaxCodeRets;
        public List<SalesTaxCodeRet> GetAllSyncSalesTaxCode(bool isFreshData = true)
        {
            if (isFreshData)
                _salesTaxCodeRets = GetQBData<SalesTaxCodeRet>(QueryXML.SalesTaxCode);
            return _salesTaxCodeRets;
        }

    }
}