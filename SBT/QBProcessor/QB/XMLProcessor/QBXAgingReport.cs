﻿using System;
using System.Collections.Generic;
using DataProcessor.Sync;
using DataProcessor.Sync.ModelRet;
using DataProcessor.Sync.ModelAdd;

namespace DataProcessor.QB.XMLProcessor
{
    public class QBXAgingReport : QBXBase
    {
      

       
        private static List<ReportRet> _fg1;        
        public IList<ReportRet> GetAgingReport()
        {
            _fg1 = GetQBData<ReportRet>(QueryXML.AgingReportQuery, null, null);
            return _fg1;
        }


    }
}