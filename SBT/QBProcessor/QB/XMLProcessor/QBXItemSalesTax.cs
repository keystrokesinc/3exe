﻿using System.Collections.Generic;
using System.Xml;
using DataProcessor.Sync;
using DataProcessor.Sync.ModelRet;

namespace DataProcessor.QB.XMLProcessor
{
    public class QBXItemSalesTax : QBXBase
    {
        

        private static List<ItemSalesTaxRet> itemSalesTaxRets;
        public List<ItemSalesTaxRet> GetAllSyncItemSalesTax(bool isFreshData = true)
        {
            if (isFreshData)
           itemSalesTaxRets = GetQBData<ItemSalesTaxRet>(QueryXML.ItemSalesTax);
            return itemSalesTaxRets;
        }
    }
}