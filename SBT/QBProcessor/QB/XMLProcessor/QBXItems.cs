﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using DataProcessor.Sync;
using DataProcessor.Sync.ModelRet;

namespace DataProcessor.QB.XMLProcessor
{
    public class QBXItems : QBXBase
    {
        private const string GetAllInventoryXML = "<?xml version=\"1.0\" ?>\r\n<?qbxml version=\"8.0\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"stopOnError\">\r\n<ItemInventoryQueryRq>\r\n</ItemInventoryQueryRq>\r\n</QBXMLMsgsRq>\r\n</QBXML>";

        private const string GetAllNonInventoryXML = "<?xml version=\"1.0\" ?>\r\n<?qbxml version=\"8.0\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"stopOnError\">\r\n<ItemNonInventoryQueryRq>\r\n</ItemNonInventoryQueryRq>\r\n</QBXMLMsgsRq>\r\n</QBXML>";

        private const string GetAllServiceXML = "<?xml version=\"1.0\" ?>\r\n<?qbxml version=\"8.0\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"stopOnError\">\r\n<ItemServiceQueryRq>\r\n</ItemServiceQueryRq>\r\n</QBXMLMsgsRq>\r\n</QBXML>";

        private const string GetAllOtherChargesXML = "<?xml version=\"1.0\" ?>\r\n<?qbxml version=\"8.0\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"stopOnError\">\r\n<ItemOtherChargeQueryRq>\r\n</ItemOtherChargeQueryRq>\r\n</QBXMLMsgsRq>\r\n</QBXML>";

        private const string GetAllDiscountXML = "<?xml version=\"1.0\" ?>\r\n<?qbxml version=\"8.0\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"stopOnError\">\r\n<ItemDiscountQueryRq>\r\n</ItemDiscountQueryRq>\r\n</QBXMLMsgsRq>\r\n</QBXML>";

      
        static List<ItemDiscountRet> _itemDiscountRets;
        static List<ItemFixedAssetRet> _itemFixedAssetRets;
        static List<ItemInventoryRet> _itemInventoryRets;
        static List<ItemInventoryAssemblyRet> _itemInventoryAssemblyRets;
        static List<ItemNonInventoryRet> _itemNonInventoryRets;
        static List<ItemOtherChargeRet> _itemOtherChargeRets;
        static List<ItemPaymentRet> _itemPaymentRets;
        static List<ItemServiceRet> _itemServiceRets;

        public Dictionary<QueryXML , List<Object>> GetQBSyncItems(bool isFreshData = true)
        {
            if (isFreshData)
            {
                _itemDiscountRets = GetQBData<ItemDiscountRet>(QueryXML.ItemDiscount);
                _itemFixedAssetRets = GetQBData<ItemFixedAssetRet>(QueryXML.ItemFixedAsset);
                _itemInventoryRets = GetQBData<ItemInventoryRet>(QueryXML.ItemInventory);
                _itemInventoryAssemblyRets = GetQBData<ItemInventoryAssemblyRet>(QueryXML.ItemInventoryAssembly);
                _itemNonInventoryRets = GetQBData<ItemNonInventoryRet>(QueryXML.ItemNonInventory);
                _itemOtherChargeRets = GetQBData<ItemOtherChargeRet>(QueryXML.ItemOtherCharge);
                _itemPaymentRets = GetQBData<ItemPaymentRet>(QueryXML.ItemPayment);
                _itemServiceRets = GetQBData<ItemServiceRet>(QueryXML.ItemService);
            }

            Dictionary<QueryXML , List<object>> itemSyncList = new Dictionary<QueryXML , List<object>>
            {
                {QueryXML.ItemDiscount, _itemDiscountRets.Cast<object>().ToList()},
                {QueryXML.ItemFixedAsset, _itemFixedAssetRets.Cast<object>().ToList()},
                {QueryXML.ItemInventory, _itemInventoryRets.Cast<object>().ToList()},
                {QueryXML.ItemInventoryAssembly, _itemInventoryAssemblyRets.Cast<object>().ToList()},
                {QueryXML.ItemNonInventory, _itemNonInventoryRets.Cast<object>().ToList()},
                {QueryXML.ItemOtherCharge, _itemOtherChargeRets.Cast<object>().ToList()},
                {QueryXML.ItemPayment, _itemPaymentRets.Cast<object>().ToList()},
                {QueryXML.ItemService, _itemServiceRets.Cast<object>().ToList()}
            };

            return itemSyncList;
        }


    }
}