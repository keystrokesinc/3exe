﻿using System;
using System.Collections.Generic;
using DataProcessor.Sync;
using DataProcessor.Sync.ModelAdd;
using DataProcessor.Sync.ModelRet;

namespace DataProcessor.QB.XMLProcessor
{
    public class QBXBill : QBXBase
    {
        private static List<BillRet> _fg;
        public List<BillRet> GetAllBillsByVendor(string Vendor, bool isFreshData = true)
        {

            if (isFreshData)
                _fg = GetQBData<BillRet>(QueryXML.Bill, null, Vendor, null);
            return _fg;
        }
        public List<BillRet> GetAllBills(DateTime? qbBillLastMod, bool isFreshData = true)
        {

            if (isFreshData)
                _fg = GetQBData<BillRet>(QueryXML.Bill, qbBillLastMod,null, qbBillLastMod.Value.AddDays(1));
            return _fg;
        }

        public List<BillRet> GetBillByRefNumber(string refNumber)
        {

            _fg = GetQBDataByRefNumber<BillRet>(QueryXML.Bill, refNumber);
            return _fg;
        }

        public List<GlobalValue.Reponse> SaveBill(BillAdd bill)
        {
            List<BillAdd> bills = new List<BillAdd>();
            bills.Add(bill);
            List<GlobalValue.Reponse> reponses = Save<BillAdd>(QueryXML.Bill, bills);
            return reponses;
        }

        public bool Delete(BillAdd bill)
        {
            List<BillAdd> bills = new List<BillAdd>();
            bills.Add(bill);
            List<GlobalValue.Reponse> reponses = Save<BillAdd>(QueryXML.Bill, bills);
            return false;
        }
    }
}