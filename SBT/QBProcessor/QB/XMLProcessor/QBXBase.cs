﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using DataProcessor.Sync;
using Interop.QBXMLRP2;
using System.Linq;


namespace DataProcessor.QB.XMLProcessor
{
    public class QBXBase
    {
        protected readonly RequestProcessor2 Rp2;
        protected string Response;
        protected readonly string Ticket;
        protected readonly XmlDocument Doc = new XmlDocument();

        protected QBXBase()
        {
            QBXConnector objQBConnector = QBXConnector.GetQBConnector();
            Rp2 = objQBConnector.RequestProcessor;
            Ticket = objQBConnector.Ticket;
        }

        protected List<T> GetQBData<T>(QueryXML query, DateTime? fromDate = null, string searchName = null, DateTime? toDate = null)
        {
            string queryXml = query.EnumDescription();

            //Retrive records accroding to dateModified------------------
            string fromDateParameter = "";
            if (toDate == null)
                toDate = Convert.ToDateTime("2021-06-10");// DateTime.Now;
            if (fromDate.HasValue)
            {                
                if (DateTime.Now.IsDaylightSavingTime()) fromDate = fromDate.Value.AddHours(-1);
                fromDateParameter = "<FromModifiedDate>" + fromDate.Value.ToString("yyyy-MM-ddTHH:mm:ss") + "</FromModifiedDate>\r\n" +
                                "<ToModifiedDate>" + "2021-02-01" + "</ToModifiedDate>\r\n"; 
                    //"<ToModifiedDate>" + DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss") + "</ToModifiedDate>\r\n";
                if (query == QueryXML.Invoice  || query == QueryXML.ReceivePayment)
                    fromDateParameter = "<ModifiedDateRangeFilter>" + fromDateParameter + "</ModifiedDateRangeFilter>";
                if ( query == QueryXML.BillPaymentCheckQuery || query == QueryXML.Bill)
                {                    
                        fromDateParameter = "<FromTxnDate>" + fromDate.Value.ToString("yyyy-MM-dd") + "</FromTxnDate>\r\n" +
                                  "<ToTxnDate >" + fromDate.Value.ToString("yyyy-MM-dd") + "</ToTxnDate >\r\n";
                        fromDateParameter = "<TxnDateRangeFilter>" + fromDateParameter + "</TxnDateRangeFilter>";
                    
                }

            }
            if(query == QueryXML.Bill)
            {
                if (searchName != null)
                {
                    if(searchName.IndexOf('&') >= 0)
                        searchName = searchName.Replace("&", "&amp;");   
                    
                    fromDateParameter = "<EntityFilter>\r\n" + "<FullName>" + searchName + "</FullName>\r\n" +
                    "</EntityFilter>";
                }
            }
            //------------------------------------------------------------

            //if (query == QueryXML.AgingReportQuery)
            //{
            //    fromDateParameter = "<AgingReportType>APAgingSummary</AgingReportType>\r\n" +

            //        "<ReportAccountFilter>\r\n <AccountTypeFilter>Accounts Payable</AccountTypeFilter>\r\n </ReportAccountFilter>";

            //}
            if (query == QueryXML.AgingReportQuery)
            {
                fromDateParameter = "<AgingReportType>APAgingDetail</AgingReportType>\r\n" +
                    "<ReportAgingAsOf>ReportEndDate</ReportAgingAsOf>\r\n";


                DateTime? fDate = Convert.ToDateTime("2021-01-01");
                DateTime? tDate = Convert.ToDateTime("2021-06-10");
                fromDateParameter =
                   // "<FromReportDate>" + fDate.Value.ToString("yyyy-MM-dd") + "</FromReportDate>\r\n" +
                     "<ToReportDate >" + tDate.Value.ToString("yyyy-MM-dd") + "</ToReportDate>\r\n";
                fromDateParameter = "<AgingReportType>APAgingDetail</AgingReportType>\r\n" +
                    "<ReportPeriod>" + fromDateParameter + "</ReportPeriod>\r\n" +
                    "<ReportAccountFilter>\r\n<AccountTypeFilter>AccountsPayable</AccountTypeFilter>\r\n"+
                    //"<FullName>Accounts Payable</FullName>\r\n"+
                    "</ReportAccountFilter>";

            }
            ////-----------------------------------------------------------
            if (query == QueryXML.CustomerSearch || query == QueryXML.Account)
                {
                    if (searchName != null && searchName.IndexOf('&') >= 0)
                    {
                        searchName = searchName.Replace("&", "&amp;");
                    }
                    fromDateParameter =  "<FullName>" + searchName + "</FullName>\r\n"  ;
                    //fromDateParameter = "<MatchCriterion>" + "StartsWith" + " </MatchCriterion>\r\n" +
                    //    "<Name>" + searchName + "</Name>\r\n";
                    //fromDateParameter = "<NameFilter>" + fromDateParameter + "</NameFilter>\r\n";
                }
            queryXml = queryXml.Replace(SyncConstant.PARAMETER_CAPTION, fromDateParameter);

            GlobalValue.Eventlogupdate("Getting " + query.EnumName() + " from QuickBooks", null);
            Response = Rp2.ProcessRequest(Ticket, queryXml);

            string xml = SyncConstant.GenerateXML(Response);
            List<T> lstcust = SyncConstant.SerializeXml<T>(xml, query);

            GlobalValue.Eventlogupdate(lstcust.Count + " " + query.EnumName() + " found in quickBooks" + (fromDate.HasValue ? (" having modified date greater then " + fromDate.Value) : "."), null);

            return lstcust;
        }

        protected List<T> GetQBDataQuery<T>(QueryXML query, string listID)
        {
            string queryXml = query.EnumDescription();

            //Retrive records accroding to dateModified------------------
            string _listID = "";
            _listID = "<ListID>" + listID + "</ListID>\r\n";

            ////-----------------------------------------------------------

            queryXml = queryXml.Replace(SyncConstant.PARAMETER_CAPTION, _listID);

            GlobalValue.Eventlogupdate("Getting " + query.EnumName() + " from QuickBooks", null);
            Response = Rp2.ProcessRequest(Ticket, queryXml);

            string xml = SyncConstant.GenerateXML(Response);
            List<T> lstcust = SyncConstant.SerializeXml<T>(xml, query);
            if (lstcust.Count == 0)
            {
                var serializer = new XmlSerializer(typeof(ErrorResponse));
                //   ErrorResponse r = (ErrorResponse)serializer.Deserialize(new StringReader(xml));

                //   string statusMessage = XmlElement.Parse(xml).Attribute("statusMessage").Value;
            }

            //  GlobalValue.Eventlogupdate(lstcust.Count + " " + query.EnumName() + " found in quickBooks" + (fromDate.HasValue ? (" having modified date greater then " + fromDate.Value) : "."), null);

            return lstcust;
        }

        protected List<T> GetQBDataByRefNumber<T>(QueryXML query, string refNumber)
        {
            string queryXml = query.EnumDescription();

            //Retrive records accroding to dateModified------------------
            string _refNumber = "";
            _refNumber = "<RefNumber>" + refNumber + "</RefNumber>\r\n";

            ////-----------------------------------------------------------

            queryXml = queryXml.Replace(SyncConstant.PARAMETER_CAPTION, _refNumber);

            GlobalValue.Eventlogupdate("Getting " + query.EnumName() + " from QuickBooks", null);
            Response = Rp2.ProcessRequest(Ticket, queryXml);

            string xml = SyncConstant.GenerateXML(Response);
            List<T> lstcust = SyncConstant.SerializeXml<T>(xml, query);
            if (lstcust.Count == 0)
            {
                var serializer = new XmlSerializer(typeof(ErrorResponse));
                //   ErrorResponse r = (ErrorResponse)serializer.Deserialize(new StringReader(xml));

                //   string statusMessage = XmlElement.Parse(xml).Attribute("statusMessage").Value;
            }

            //  GlobalValue.Eventlogupdate(lstcust.Count + " " + query.EnumName() + " found in quickBooks" + (fromDate.HasValue ? (" having modified date greater then " + fromDate.Value) : "."), null);

            return lstcust;
        }

        public class ErrorResponse
        {
            public string StatusMessage { get; set; }
        }

        public bool DeleteByTxnId(string query, string txnId)
        {
            if (!string.IsNullOrEmpty(query) && !string.IsNullOrEmpty(txnId))
            {
                string objectToXml;
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("<TxnDelRq>");
                sb.AppendLine(string.Format("<TxnDelType>{0}</TxnDelType>", query));
                sb.AppendLine(string.Format("<TxnID>{0}</TxnID>", txnId));
                sb.AppendLine("</TxnDelRq>");
                objectToXml = string.Format(
                        "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<?qbxml version=\"9.0\"?>\n<QBXML>\n<QBXMLMsgsRq onError=\"continueOnError\">\n{0}\n</QBXMLMsgsRq>\n</QBXML>",
                        sb);
                List<GlobalValue.Reponse> responses = SaveQBData(objectToXml);
                if (responses.Count == 1 && !responses[0].IsError)
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }

        #region
        private List<GlobalValue.Reponse> SaveQBData(string query)
        {
            List<GlobalValue.Reponse> entityResponce = new List<GlobalValue.Reponse>();
            try
            {
                GlobalValue.Eventlogupdate("Sending  xml to save in quickBooks", null);
                Response = Rp2.ProcessRequest(Ticket, query);
                XmlDocument xDocument = new XmlDocument();
                xDocument.LoadXml(Response);
                XmlNode selectSingleNode = xDocument.SelectSingleNode("QBXML");
                if (selectSingleNode != null)
                {
                    XmlNode singleNode = selectSingleNode.SelectSingleNode("QBXMLMsgsRs");
                    if (singleNode != null)
                        foreach (XmlNode node in singleNode.ChildNodes)
                        {
                            if (node.Attributes != null)
                            {
                                GlobalValue.Reponse er = new GlobalValue.Reponse();
                                if (node.Attributes["statusSeverity"].Value == "Error")
                                {
                                    er.IsError = true;
                                    er.EntityObject = node.Name.Replace("AddRs", "");
                                    GlobalValue.Eventlogupdate("ERROR ON INSERT to QB: " + node.Attributes["statusMessage"].Value, null, "Error");
                                }
                                else
                                {
                                    er.EntityObject = node.InnerXml;
                                }
                                er.Message = node.Attributes["statusMessage"].Value;
                                entityResponce.Add(er);
                            }
                            else
                            {
                                entityResponce.Add(new GlobalValue.Reponse
                                {
                                    Message = "Invalid request.",
                                    IsError = true
                                });
                            }
                        }
                }
            }
            catch (COMException ex)
            {
                GlobalValue.Eventlogupdate("Error: " + ex.Message, null, "Error");
                entityResponce.Add(new GlobalValue.Reponse
                {
                    Message = ex.Message,
                    IsError = true,
                    EntityObject = query

                });
            }
            catch (Exception ex)
            {
                GlobalValue.Eventlogupdate("Error: " + ex.Message, null, "Error");
                entityResponce.Add(new GlobalValue.Reponse
                {
                    Message = ex.Message,
                    IsError = true
                });
            }

            return entityResponce;
        }
        private readonly List<SerializerHoler> _serializerHoler = new List<SerializerHoler>();
        protected List<GlobalValue.Reponse> Save<T>(QueryXML syncEntity, IList<T> syncObjectList)
        {
            //   if(syncObjectList.Count > 0)
            //{
            GlobalValue.Eventlogupdate("Preparing " + syncEntity.EnumName() + " xml to save in quickBooks", null);

            StringBuilder sb = new StringBuilder();
            Type type = null;
            if (syncObjectList.Count > 0)
            {
                type = syncObjectList[0].GetType();
            }
            string objectToXml;
            string EntityName = syncEntity.EnumName();
            foreach (T syncObject in syncObjectList)
            {
                if (syncEntity.EnumName() == "InvoiceAdd")
                    EntityName = "Invoice";
                else if(syncEntity.EnumName() == "SalesReceiptAdd")
                    EntityName = "SalesReceipt";
                else if (syncEntity.EnumName() == "ReceivePaymentAdd")
                    EntityName = "ReceivePayment";
                else if (syncEntity.EnumName() == "CustomerAdd")
                    EntityName = "Customer";
                else if (syncEntity.EnumName() == "CustomerMod")
                    EntityName = "CustomerMod";
                else if (syncEntity.EnumName() == "VendorCreditAdd")
                    EntityName = "VendorCredit";
                else if (syncEntity.EnumName() == "BillPaymentCheckAdd")
                    EntityName = "BillPaymentCheck";


                string xmlNodeName = EntityName;
                XmlSerializerNamespaces emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                XmlRootAttribute xmlRootAttribute = new XmlRootAttribute
                {
                    ElementName = xmlNodeName + "Add"
                };
                XmlSerializer xs = GetSerializer(type, xmlRootAttribute);
                XmlWriterSettings settings = new XmlWriterSettings
                {
                    Indent = true,
                    OmitXmlDeclaration = true
                };
                using (var stream = new StringWriter())
                using (var writer = XmlWriter.Create(stream, settings))
                {
                    xs.Serialize(writer, syncObject, emptyNamepsaces);
                    objectToXml = stream.ToString();
                }

                objectToXml =
                    string.Format(
                        "<{0}{2}Rq>\n{1}\n</{0}{2}Rq>",
                        xmlNodeName, objectToXml, "Add");



                sb.AppendLine(objectToXml);
            }
            objectToXml = string.Format(
                    "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<?qbxml version=\"13.0\"?>\n<QBXML>\n<QBXMLMsgsRq onError=\"continueOnError\">\n{0}</QBXMLMsgsRq>\n</QBXML>",
                sb);
            
            List <GlobalValue.Reponse> saveQBData = SaveQBData(objectToXml);



            GlobalValue.Eventlogupdate(saveQBData.Count(s => !s.IsError) + " " + syncEntity.EnumName() + " records are inserted in Quickbooks.", null);
            return saveQBData;
            // }
            //else
            //{
            //    return new List<GlobalValue.Reponse>
            //    {
            //        new GlobalValue.Reponse
            //        {
            //             IsError=false,
            //              Message=syncEntity.EnumName() + " not found to insert."
            //        }
            //    };
            //}
        }
        protected List<GlobalValue.Reponse> UpdateCustomer<T>(QueryXML syncEntity, IList<T> syncObjectList)
        {
            //   if(syncObjectList.Count > 0)
            //{
            GlobalValue.Eventlogupdate("Preparing " + syncEntity.EnumName() + " xml to save in quickBooks", null);

            StringBuilder sb = new StringBuilder();
            Type type = null;
            if (syncObjectList.Count > 0)
            {
                type = syncObjectList[0].GetType();
            }
            string objectToXml;
            string EntityName = syncEntity.EnumName();
            foreach (T syncObject in syncObjectList)
            {                
                 if (syncEntity.EnumName() == "CustomerMod")
                    EntityName = "Customer";


                string xmlNodeName = EntityName;
                XmlSerializerNamespaces emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                XmlRootAttribute xmlRootAttribute = new XmlRootAttribute
                {
                    ElementName = xmlNodeName + "Mod"
                };
                XmlSerializer xs = GetSerializer(type, xmlRootAttribute);
                XmlWriterSettings settings = new XmlWriterSettings
                {
                    Indent = true,
                    OmitXmlDeclaration = true
                };
                using (var stream = new StringWriter())
                using (var writer = XmlWriter.Create(stream, settings))
                {
                    xs.Serialize(writer, syncObject, emptyNamepsaces);
                    objectToXml = stream.ToString();
                }

                objectToXml =
                    string.Format(
                        "<{0}{2}Rq>\n{1}\n</{0}{2}Rq>",
                        xmlNodeName, objectToXml, "Mod");



                sb.AppendLine(objectToXml);
            }
            objectToXml = string.Format(
                    "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<?qbxml version=\"9.0\"?>\n<QBXML>\n<QBXMLMsgsRq onError=\"continueOnError\">\n{0}\n</QBXMLMsgsRq>\n</QBXML>",
                sb);

            List<GlobalValue.Reponse> saveQBData = SaveQBData(objectToXml);



            GlobalValue.Eventlogupdate(saveQBData.Count(s => !s.IsError) + " " + syncEntity.EnumName() + " records are inserted in Quickbooks.", null);
            return saveQBData;           
        }

        private XmlSerializer GetSerializer(Type type, XmlRootAttribute root)
        {
            foreach (SerializerHoler sh in _serializerHoler)
            {
                if (sh.Type == type && sh.XmlRootAttribute.ElementName == root.ElementName)
                {
                    return sh.XmlSerializer;
                }
            }
            XmlSerializer xs = new XmlSerializer(type, root);
            _serializerHoler.Add(new SerializerHoler
            {
                XmlSerializer = xs,
                XmlRootAttribute = root,
                Type = type
            });
            return xs;
        }


        private class SerializerHoler
        {
            public XmlSerializer XmlSerializer { get; set; }
            public Type Type { get; set; }
            public XmlRootAttribute XmlRootAttribute { get; set; }
        }

        #endregion
    }


}