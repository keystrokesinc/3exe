﻿using DataProcessor.Sync;
using DataProcessor.Sync.ModelAdd;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataProcessor.QB.XMLProcessor
{
    public class QBXBillPayment: QBXBase
    {
        private static List<BillRet> _fg;
        private static List<BillPaymentCheckRet> _cg;
        public List<BillRet> GetAllBillPayments(DateTime? qbBillLastMod, bool isFreshData = true)
        {

            if (isFreshData)
                _fg = GetQBData<BillRet>(QueryXML.Bill, qbBillLastMod);
            return _fg;
        }
        public List<BillPaymentCheckRet> GetAllBillPaymentsCHK(DateTime? qbBillLastMod, bool isFreshData = true)
        {

            if (isFreshData)
                _cg = GetQBData<BillPaymentCheckRet>(QueryXML.BillPaymentCheckQuery, qbBillLastMod,null, qbBillLastMod.Value.AddDays(1));
            return _cg;
        }

        public List<GlobalValue.Reponse> SaveBillPayment(BillPaymentCheckAdd billpayment)
        {
            List<BillPaymentCheckAdd> bills = new List<BillPaymentCheckAdd>();
            bills.Add(billpayment);
            List<GlobalValue.Reponse> reponses = Save<BillPaymentCheckAdd>(QueryXML.BillPaymentCheck, bills);
            return reponses;
        }
    }
}
