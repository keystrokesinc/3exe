﻿using System;
using System.Collections.Generic;
using DataProcessor.Sync;
using DataProcessor.Sync.ModelAdd;
using DataProcessor.Sync.ModelRet;

namespace DataProcessor.QB.XMLProcessor
{
   public class QBXVendor:QBXBase
    {
        public List<VendorRet> GetAllVendor(DateTime? fromDate=null)
        {


            List<VendorRet> vendorAdds = GetQBData<VendorRet>(QueryXML.Vendor, fromDate);
            return vendorAdds;
        }


        public List<VendorRet> GetVendorQuery(string listID)
        {


            List<VendorRet> vendorAdds = GetQBDataQuery<VendorRet>(QueryXML.VendorQuery, listID);
            return vendorAdds;
        }
        public List<GlobalValue.Reponse> SaveVendor(List<VendorAdd> vendors)
        {
            List<GlobalValue.Reponse> reponses = Save<VendorAdd>(QueryXML.Vendor, vendors);
            return reponses;
        }
    }
}
