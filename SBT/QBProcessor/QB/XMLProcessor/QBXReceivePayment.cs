﻿using System;
using System.Collections.Generic;
using DataProcessor.Sync;
using DataProcessor.Sync.ModelRet;
using DataProcessor.Sync.ModelAdd;

namespace DataProcessor.QB.XMLProcessor
{
    public class QBXReceivePayment : QBXBase
    {
        private static List<ReceivePaymentRet> _receivePaymentRets;
        public List<ReceivePaymentRet> GetReceivePayment(DateTime? qbReceivePaymentLastMod, bool isFreshData = true)
        {
            if (isFreshData)
                _receivePaymentRets = GetQBData<ReceivePaymentRet>(QueryXML.ReceivePayment,qbReceivePaymentLastMod);
            return _receivePaymentRets;
        }

        public List<GlobalValue.Reponse> SaveReceivePayment(List<ReceivePaymentAdd> receivePaymentAdd)
        {
            List<GlobalValue.Reponse> reponses = Save<ReceivePaymentAdd>(QueryXML.ReceivePayment, receivePaymentAdd);
            return reponses;
        }
    }
}
