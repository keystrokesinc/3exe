﻿using System;
using System.Collections.Generic;
using DataProcessor.Sync;
using DataProcessor.Sync.ModelRet;
using DataProcessor.Sync.ModelAdd;

namespace DataProcessor.QB.XMLProcessor
{
    public class QBXCustomer : QBXBase
    {
      

        private static List<CustomerRet> _fg;
        public IList<CustomerRet> GetAllSyncCustomers(DateTime? qbAccountLastMod=null)
        {
                _fg = GetQBData<CustomerRet>(QueryXML.Customer,qbAccountLastMod);
            return _fg;
        }
        private static List<TemplateRet> _tm;
        public IList<TemplateRet> GetAllSyncTemplates(DateTime? qbAccountLastMod = null)
        {
            _tm = GetQBData<TemplateRet>(QueryXML.Template, qbAccountLastMod);
            return _tm;
        }
        public List<GlobalValue.Reponse> SaveCustomer(CustomerAdd customer)
        {
            List<CustomerAdd> customers = new List<CustomerAdd>();
            customers.Add(customer);
            List<GlobalValue.Reponse> reponses = Save<CustomerAdd>(QueryXML.CustomerAdd, customers);
            return reponses;
        }
        public IList<CustomerRet> SearchCustomer(String name)
        {
            _fg = GetQBData<CustomerRet>(QueryXML.CustomerSearch, null, name);
            return _fg;
        }
        public List<GlobalValue.Reponse> UpdateCustomer(CustomerAdd customer)
        {
            List<CustomerAdd> customers = new List<CustomerAdd>();
            customers.Add(customer);
            List<GlobalValue.Reponse> reponses = UpdateCustomer<CustomerAdd>(QueryXML.CustomerMod, customers);
            return reponses;
        }

    }
}