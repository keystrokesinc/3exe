﻿using System.Collections.Generic;
using DataProcessor.Sync;
using DataProcessor.Sync.ModelRet;
using DataProcessor.Sync.ModelAdd;

namespace DataProcessor.QB.XMLProcessor
{
    public class QBXPaymentMethod : QBXBase
    {
      
        public List<PaymentMethodRet> GetSyncPaymentMethod()
        {
            List<PaymentMethodRet> paymentMethodRets = GetQBData<PaymentMethodRet>(QueryXML.PaymentMethod);
            return paymentMethodRets;
        }
        public List<GlobalValue.Reponse> SavePayment(ReceivePaymentAdd Payment)
        {
            List<ReceivePaymentAdd> Payments = new List<ReceivePaymentAdd>();
            Payments.Add(Payment);
            List<GlobalValue.Reponse> reponses = Save<ReceivePaymentAdd>(QueryXML.ReceivePayment, Payments);
            return reponses;
        }

    }
}