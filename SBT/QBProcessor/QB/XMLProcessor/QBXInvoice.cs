﻿using System;
using System.Collections.Generic;
using DataProcessor.Sync;
using DataProcessor.Sync.ModelRet;
using DataProcessor.Sync.ModelAdd;

namespace DataProcessor.QB.XMLProcessor
{
    public class QBXInvoice : QBXBase
    {
        private static List<InvoiceRet> _unPaidInvoiceRets;
        private static List<InvoiceRet> _paidInvoiceRets;

        /// <summary>
        /// It return all unpaid invoices and paid invoices which has modified date yesterday
        /// </summary>
        /// <param name="qbInvoiceLastMod"></param>
        /// <param name="isFreshData"></param>
        /// <returns></returns>
        public List<InvoiceRet> GetAllInvoice(DateTime? qbInvoiceLastMod, bool isFreshData = true)
        {


            List<InvoiceRet> invoices = GetQBData<InvoiceRet>(QueryXML.Invoice, qbInvoiceLastMod);
            return invoices;
        }
        public List<GlobalValue.Reponse> SaveInvoice(InvoiceAdd invoice)
        {
            List<InvoiceAdd> invoices = new List<InvoiceAdd>();
            invoices.Add(invoice);
            List<GlobalValue.Reponse> reponses = Save<InvoiceAdd>(QueryXML.InvoiceAdd, invoices);
            return reponses;
        }


    }
}