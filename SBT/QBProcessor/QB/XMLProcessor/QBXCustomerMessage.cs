﻿using System.Collections.Generic;
using DataProcessor.Sync;
using DataProcessor.Sync.ModelRet;

namespace DataProcessor.QB.XMLProcessor
{
    public class QBXCustomerMessage:QBXBase
    {
        public IEnumerable<CustomerMsgRet> GetAllSyncCustomerMessage()
        {
            List<CustomerMsgRet> customerMsgRets = GetQBData<CustomerMsgRet>(QueryXML.CustomerMsg);
            return customerMsgRets;
        }

      
    }
}
