﻿using System.Collections.Generic;
using DataProcessor.Sync;
using DataProcessor.Sync.ModelAdd;

namespace DataProcessor.QB.XMLProcessor
{
   public class QBXVendorCredit: QBXBase
   {
        public List<GlobalValue.Reponse> SaveVC(VendorCreditAdd vc)
        {
            List<VendorCreditAdd> vcs = new List<VendorCreditAdd>();
            vcs.Add(vc);
            List<GlobalValue.Reponse> reponses = Save<VendorCreditAdd>(QueryXML.VendorCredit, vcs);
            return reponses;
        }
        public List<GlobalValue.Reponse> ApplyCredit(BillPaymentCheckAdd vc)
        {
            List<BillPaymentCheckAdd> vcs = new List<BillPaymentCheckAdd>();
            vcs.Add(vc);
            List<GlobalValue.Reponse> reponses = Save<BillPaymentCheckAdd>(QueryXML.BillPaymentCheck, vcs);
            return reponses;
        }
    }
}
