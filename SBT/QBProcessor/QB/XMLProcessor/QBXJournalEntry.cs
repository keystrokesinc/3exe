﻿using System;
using System.Collections.Generic;
using DataProcessor.Sync;
using DataProcessor.Sync.ModelRet;
using DataProcessor.Sync.ModelAdd;

namespace DataProcessor.QB.XMLProcessor
{
    public class QBXJournalEntry : QBXBase
    {           
        public List<GlobalValue.Reponse> SaveJE(JournalEntryAdd po)
        {
            List<JournalEntryAdd> pos = new List<JournalEntryAdd>();
            pos.Add(po);          
            List<GlobalValue.Reponse> reponses = Save<JournalEntryAdd>(QueryXML.JournalEntry, pos);
            return reponses;
        }
       


    }
}