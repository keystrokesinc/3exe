﻿using System;
using System.Collections.Generic;
using DataProcessor.Sync;
using DataProcessor.Sync.ModelRet;
//using QBEngine;

namespace DataProcessor.QB.XMLProcessor
{
    public class QBXAccount : QBXBase
    {
      
        public List<AccountRet> GetAllAccounts(DateTime? qbAccountLastMod)
        {
            List<AccountRet> accountRets = GetQBData<AccountRet>(QueryXML.Account, qbAccountLastMod);
            return accountRets;
          
        }

    }
}