﻿using System;
using System.Collections.Generic;
using DataProcessor.Sync;
using DataProcessor.Sync.ModelAdd;
using DataProcessor.Sync.ModelRet;

namespace DataProcessor.QB.XMLProcessor
{
    public class QBXCheque : QBXBase
    {
        private static List<CheckRet> _fg;
        public List<CheckRet> GetAllChecks(DateTime? qbBillLastMod, bool isFreshData = true)
        {

            if (isFreshData)
                _fg = GetQBData<CheckRet>(QueryXML.Check, qbBillLastMod);
            return _fg;
        }

        public List<GlobalValue.Reponse> SaveCheck(CheckAdd cheque)
        {
            List<CheckAdd> cheques = new List<CheckAdd>();
            cheques.Add(cheque);
            List<GlobalValue.Reponse> reponses = Save<CheckAdd>(QueryXML.Check, cheques);
            return reponses;
        }
    }
}
