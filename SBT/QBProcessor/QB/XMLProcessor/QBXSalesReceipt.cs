﻿using System.Collections.Generic;
using DataProcessor.Sync;
using DataProcessor.Sync.ModelRet;
using DataProcessor.Sync.ModelAdd;

namespace DataProcessor.QB.XMLProcessor
{
    public class QBXSalesReceipt : QBXBase
    {
        private static List<SalesReceiptRet> salesReceiptRets;
        public List<SalesReceiptRet> GetAllSyncSalesReceipt(bool isFreshData = true)
        {
            if(isFreshData)
             salesReceiptRets = GetQBData<SalesReceiptRet>(QueryXML.SalesReceipt);
            return salesReceiptRets;
        }
        public List<GlobalValue.Reponse> SaveSalesReceipt(SalesReceiptAdd SalesReceipt)
        {
            List<SalesReceiptAdd> SalesReceipts = new List<SalesReceiptAdd>();
            SalesReceipts.Add(SalesReceipt);
            List<GlobalValue.Reponse> reponses = Save<SalesReceiptAdd>(QueryXML.SalesReceiptAdd, SalesReceipts);
            return reponses;
        }
    }
}
