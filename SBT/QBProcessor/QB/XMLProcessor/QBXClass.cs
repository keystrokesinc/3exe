﻿using System;
using System.Collections.Generic;
using DataProcessor.Sync;
using DataProcessor.Sync.ModelRet;
//using QBEngine;

namespace DataProcessor.QB.XMLProcessor
{
    public class QBXClass : QBXBase
    {

        public List<ClassRet> GetAllClass(DateTime? qbAccountLastMod)
        {
            List<ClassRet> classRets = GetQBData<ClassRet>(QueryXML.Class, qbAccountLastMod);
            return classRets;
          
        }

    }
}