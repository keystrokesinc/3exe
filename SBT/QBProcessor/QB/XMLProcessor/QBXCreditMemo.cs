﻿using System.Collections.Generic;
using DataProcessor.Sync;

namespace DataProcessor.QB.XMLProcessor
{
   public class QBXCreditMemo:QBXBase
   {
       private static List<CreditMemoRet> _accountRets;
       public IEnumerable<CreditMemoRet> GetAllCreditMemo(bool isFreshData = true)
       {
           if(isFreshData)
                _accountRets = GetQBData<CreditMemoRet>(QueryXML.CreditMemo);
           return _accountRets;

       }
    }
}
