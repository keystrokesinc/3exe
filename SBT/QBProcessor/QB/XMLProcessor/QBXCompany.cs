﻿
using System;
using System.Collections.Generic;
using DataProcessor.Sync;
using DataProcessor.Sync.ModelAdd;
using DataProcessor.Sync.ModelRet;

namespace DataProcessor.QB.XMLProcessor
{
    public class QBXCompany : QBXBase
    {
        public CompanyRet GetCompanyInformation(DateTime? fromDate = null)
        {
            List<CompanyRet> lstcompanyInfo = GetQBData<CompanyRet>(QueryXML.Company, fromDate);
            return lstcompanyInfo[0];
        }
    }
}
