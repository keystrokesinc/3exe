﻿using System;
using System.Xml.Serialization;

namespace DataProcessor.Sync.ModelRet
{




    [XmlType(AnonymousType = true)]
    public class VendorRef
    {

        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }

        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }

    [XmlType(AnonymousType = true)]
    public class APAccountRef
    {

        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }

        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }

    //[XmlType(AnonymousType = true)]
    //public class CurrencyRef
    //{

    //    [XmlElement(ElementName = "ListID")]
    //    public string ListID { get; set; }

    //    [XmlElement(ElementName = "FullName")]
    //    public string FullName { get; set; }
    //}

    //[XmlType(AnonymousType = true)]
    //public class LinkedTxn
    //{

    //    [XmlElement(ElementName = "TxnID")]
    //    public string TxnID { get; set; }

    //    [XmlElement(ElementName = "TxnType")]
    //    public string TxnType { get; set; }

    //    [XmlElement(ElementName = "TxnDate")]
    //    public string TxnDate { get; set; }

    //    [XmlElement(ElementName = "RefNumber")]
    //    public string RefNumber { get; set; }

    //    [XmlElement(ElementName = "LinkType")]
    //    public string LinkType { get; set; }

    //    [XmlElement(ElementName = "Amount")]
    //    public string Amount { get; set; }
    //}

    [XmlType(AnonymousType = true)]
    public class AccountRef
    {

        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }

        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }

    [XmlType(AnonymousType = true)]
    public class CustomerRef
    {

        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }

        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }

    [XmlType(AnonymousType = true)]
    public class ClassRef
    {

        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }

        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }

    //[XmlType(AnonymousType = true)]
    //public class SalesRepRef
    //{

    //    [XmlElement(ElementName = "ListID")]
    //    public string ListID { get; set; }

    //    [XmlElement(ElementName = "FullName")]
    //    public string FullName { get; set; }
    //}

    //[XmlType(AnonymousType = true)]
    //public class DataExtRet
    //{

    //    [XmlElement(ElementName = "OwnerID")]
    //    public string OwnerID { get; set; }

    //    [XmlElement(ElementName = "DataExtName")]
    //    public string DataExtName { get; set; }

    //    [XmlElement(ElementName = "DataExtType")]
    //    public string DataExtType { get; set; }

    //    [XmlElement(ElementName = "DataExtValue")]
    //    public string DataExtValue { get; set; }
    //}

    //[XmlType(AnonymousType = true)]
    //public class ExpenseLineRet
    //{

    //    [XmlElement(ElementName = "TxnLineID")]
    //    public string TxnLineID { get; set; }

    //    [XmlElement(ElementName = "AccountRef")]
    //    public AccountRef AccountRef { get; set; }

    //    [XmlElement(ElementName = "Amount")]
    //    public string Amount { get; set; }

    //    [XmlElement(ElementName = "Memo")]
    //    public string Memo { get; set; }

    //    [XmlElement(ElementName = "CustomerRef")]
    //    public CustomerRef CustomerRef { get; set; }

    //    [XmlElement(ElementName = "ClassRef")]
    //    public ClassRef ClassRef { get; set; }

    //    [XmlElement(ElementName = "BillableStatus")]
    //    public string BillableStatus { get; set; }

    //    [XmlElement(ElementName = "SalesRepRef")]
    //    public SalesRepRef SalesRepRef { get; set; }

    //    [XmlElement(ElementName = "DataExtRet")]
    //    public DataExtRet DataExtRet { get; set; }
    //}

    //[XmlType(AnonymousType = true)]
    //public class ItemRef
    //{

    //    [XmlElement(ElementName = "ListID")]
    //    public string ListID { get; set; }

    //    [XmlElement(ElementName = "FullName")]
    //    public string FullName { get; set; }
    //}

    //[XmlType(AnonymousType = true)]
    //public class InventorySiteRef
    //{

    //    [XmlElement(ElementName = "ListID")]
    //    public string ListID { get; set; }

    //    [XmlElement(ElementName = "FullName")]
    //    public string FullName { get; set; }
    //}

    //[XmlType(AnonymousType = true)]
    //public class InventorySiteLocationRef
    //{

    //    [XmlElement(ElementName = "ListID")]
    //    public string ListID { get; set; }

    //    [XmlElement(ElementName = "FullName")]
    //    public string FullName { get; set; }
    //}

    //[XmlType(AnonymousType = true)]
    //public class OverrideUOMSetRef
    //{

    //    [XmlElement(ElementName = "ListID")]
    //    public string ListID { get; set; }

    //    [XmlElement(ElementName = "FullName")]
    //    public string FullName { get; set; }
    //}

    //[XmlType(AnonymousType = true)]
    //public class ItemLineRet
    //{

    //    [XmlElement(ElementName = "TxnLineID")]
    //    public string TxnLineID { get; set; }

    //    [XmlElement(ElementName = "ItemRef")]
    //    public ItemRef ItemRef { get; set; }

    //    [XmlElement(ElementName = "InventorySiteRef")]
    //    public InventorySiteRef InventorySiteRef { get; set; }

    //    [XmlElement(ElementName = "InventorySiteLocationRef")]
    //    public InventorySiteLocationRef InventorySiteLocationRef { get; set; }

    //    [XmlElement(ElementName = "SerialNumber")]
    //    public string SerialNumber { get; set; }

    //    [XmlElement(ElementName = "LotNumber")]
    //    public string LotNumber { get; set; }

    //    [XmlElement(ElementName = "Desc")]
    //    public string Desc { get; set; }

    //    [XmlElement(ElementName = "Quantity")]
    //    public string Quantity { get; set; }

    //    [XmlElement(ElementName = "UnitOfMeasure")]
    //    public string UnitOfMeasure { get; set; }

    //    [XmlElement(ElementName = "OverrideUOMSetRef")]
    //    public OverrideUOMSetRef OverrideUOMSetRef { get; set; }

    //    [XmlElement(ElementName = "Cost")]
    //    public string Cost { get; set; }

    //    [XmlElement(ElementName = "Amount")]
    //    public string Amount { get; set; }

    //    [XmlElement(ElementName = "CustomerRef")]
    //    public CustomerRef CustomerRef { get; set; }

    //    [XmlElement(ElementName = "ClassRef")]
    //    public ClassRef ClassRef { get; set; }

    //    [XmlElement(ElementName = "BillableStatus")]
    //    public string BillableStatus { get; set; }

    //    [XmlElement(ElementName = "SalesRepRef")]
    //    public SalesRepRef SalesRepRef { get; set; }

    //    [XmlElement(ElementName = "DataExtRet")]
    //    public DataExtRet DataExtRet { get; set; }
    //}

    //[XmlType(AnonymousType = true)]
    //public class ItemGroupRef
    //{

    //    [XmlElement(ElementName = "ListID")]
    //    public string ListID { get; set; }

    //    [XmlElement(ElementName = "FullName")]
    //    public string FullName { get; set; }
    //}

    //[XmlType(AnonymousType = true)]
    //public class DataExt
    //{

    //    [XmlElement(ElementName = "OwnerID")]
    //    public string OwnerID { get; set; }

    //    [XmlElement(ElementName = "DataExtName")]
    //    public string DataExtName { get; set; }

    //    [XmlElement(ElementName = "DataExtValue")]
    //    public string DataExtValue { get; set; }
    //}

    //[XmlType(AnonymousType = true)]
    //public class ItemGroupLineRet
    //{

    //    [XmlElement(ElementName = "TxnLineID")]
    //    public string TxnLineID { get; set; }

    //    [XmlElement(ElementName = "ItemGroupRef")]
    //    public ItemGroupRef ItemGroupRef { get; set; }

    //    [XmlElement(ElementName = "Desc")]
    //    public string Desc { get; set; }

    //    [XmlElement(ElementName = "Quantity")]
    //    public string Quantity { get; set; }

    //    [XmlElement(ElementName = "UnitOfMeasure")]
    //    public string UnitOfMeasure { get; set; }

    //    [XmlElement(ElementName = "OverrideUOMSetRef")]
    //    public OverrideUOMSetRef OverrideUOMSetRef { get; set; }

    //    [XmlElement(ElementName = "TotalAmount")]
    //    public string TotalAmount { get; set; }

    //    [XmlElement(ElementName = "ItemLineRet")]
    //    public ItemLineRet ItemLineRet { get; set; }

    //    [XmlElement(ElementName = "DataExt")]
    //    public DataExt DataExt { get; set; }
    //}

    [XmlRoot(ElementName = "VendorCreditRet")]
    public partial class VendorCreditRet
    {

        [XmlElement(ElementName = "TxnID")]
        public string TxnID { get; set; }

        [XmlElement(ElementName = "TimeCreated")]
        public string TimeCreated { get; set; }

        [XmlElement(ElementName = "TimeModified")]
        public string TimeModified { get; set; }

        [XmlElement(ElementName = "EditSequence")]
        public string EditSequence { get; set; }

        [XmlElement(ElementName = "TxnNumber")]
        public string TxnNumber { get; set; }

        [XmlElement(ElementName = "VendorRef")]
        public VendorRef VendorRef { get; set; }

        [XmlElement(ElementName = "APAccountRef")]
        public APAccountRef APAccountRef { get; set; }

        [XmlElement(ElementName = "TxnDate")]
        public string TxnDate { get; set; }

        [XmlElement(ElementName = "CreditAmount")]
        public string CreditAmount { get; set; }

        [XmlElement(ElementName = "CurrencyRef")]
        public CurrencyRef CurrencyRef { get; set; }

        [XmlElement(ElementName = "ExchangeRate")]
        public string ExchangeRate { get; set; }

        [XmlElement(ElementName = "CreditAmountInHomeCurrency")]
        public string CreditAmountInHomeCurrency { get; set; }

        [XmlElement(ElementName = "RefNumber")]
        public string RefNumber { get; set; }

        [XmlElement(ElementName = "Memo")]
        public string Memo { get; set; }

        [XmlElement(ElementName = "ExternalGUID")]
        public string ExternalGUID { get; set; }

        [XmlElement(ElementName = "LinkedTxn")]
        public LinkedTxn LinkedTxn { get; set; }

        [XmlElement(ElementName = "ExpenseLineRet")]
        public ExpenseLineRet ExpenseLineRet { get; set; }

        [XmlElement(ElementName = "ItemLineRet")]
        public ItemLineRet ItemLineRet { get; set; }

        [XmlElement(ElementName = "ItemGroupLineRet")]
        public ItemGroupLineRet ItemGroupLineRet { get; set; }

        [XmlElement(ElementName = "OpenAmount")]
        public string OpenAmount { get; set; }

        [XmlElement(ElementName = "DataExtRet")]
        public DataExtRet DataExtRet { get; set; }
    }





}
