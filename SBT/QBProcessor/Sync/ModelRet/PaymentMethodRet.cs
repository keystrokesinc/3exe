﻿using System.Xml.Serialization;

namespace DataProcessor.Sync.ModelRet
{

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "" , IsNullable = false)]
    public partial class PaymentMethodRet
    {

        private string listIDField;

        private string timeCreatedField;

        private string timeModifiedField;

        private string editSequenceField;

        private string nameField;

        private string isActiveField;

        private string paymentMethodTypeField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string TimeCreated
        {
            get
            {
                return timeCreatedField;
            }
            set
            {
                timeCreatedField = value;
            }
        }

        /// <remarks/>
        public string TimeModified
        {
            get
            {
                return timeModifiedField;
            }
            set
            {
                timeModifiedField = value;
            }
        }

        /// <remarks/>
        public string EditSequence
        {
            get
            {
                return editSequenceField;
            }
            set
            {
                editSequenceField = value;
            }
        }

        /// <remarks/>
        public string Name
        {
            get
            {
                return nameField;
            }
            set
            {
                nameField = value;
            }
        }

        /// <remarks/>
        public string IsActive
        {
            get
            {
                return isActiveField;
            }
            set
            {
                isActiveField = value;
            }
        }

        /// <summary>
        /// PaymentMethodType may have one of the following values: 
        ///  AmericanExpress,
        ///  Cash,
        ///  Check,
        ///  DebitCard,
        ///  Discover,
        ///  ECheck,
        ///  GiftCard,
        ///  MasterCard,
        ///  Other,
        ///  OtherCreditCard,
        ///  Visa
        /// </summary>
        public string PaymentMethodType
        {
            get
            {
                return paymentMethodTypeField;
            }
            set
            {
                paymentMethodTypeField = value;
            }
        }
    }


}
