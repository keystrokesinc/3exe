﻿using System;
using System.Xml.Serialization;

namespace DataProcessor.Sync.ModelRet
{


    /// <remarks/>
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "" , IsNullable = false)]
    public partial class VendorRet
    {

        private string listIDField;

        private DateTime? timeCreatedField;

        private DateTime? timeModifiedField;

        private string editSequenceField;

        private string nameField;

        private bool? isActiveField;

        private VendorRetClassRef classRefField;

        private string companyNameField;

        private string salutationField;

        private string firstNameField;

        private string middleNameField;

        private string lastNameField;

        private string jobTitleField;

        private VendorRetVendorAddress vendorAddressField;

        private VendorRetVendorAddressBlock vendorAddressBlockField;

        private VendorRetShipAddress shipAddressField;

        private string phoneField;

        private string altPhoneField;

        private string faxField;

        private string emailField;

        private string ccField;

        private string contactField;

        private string altContactField;

        private VendorRetAdditionalContactRef additionalContactRefField;

        private VendorRetContactsRet contactsRetField;

        private string nameOnCheckField;

        private string accountNumberField;

        private string notesField;

        private VendorRetAdditionalNotesRet additionalNotesRetField;

        private VendorRetVendorTypeRef vendorTypeRefField;

        private VendorRetTermsRef termsRefField;

        private string creditLimitField;

        private string vendorTaxIdentField;

        private string isVendorEligibleFor1099Field;

        private string balanceField;

        private VendorRetBillingRateRef billingRateRefField;

        private string externalGUIDField;

        private VendorRetPrefillAccountRef prefillAccountRefField;

        private VendorRetCurrencyRef currencyRefField;

        private VendorRetDataExtRet dataExtRetField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public DateTime? TimeCreated
        {
            get
            {
                return timeCreatedField;
            }
            set
            {
                timeCreatedField = value;
            }
        }

        /// <remarks/>
        public DateTime? TimeModified
        {
            get
            {
                return timeModifiedField;
            }
            set
            {
                timeModifiedField = value;
            }
        }

        /// <remarks/>
        public string EditSequence
        {
            get
            {
                return editSequenceField;
            }
            set
            {
                editSequenceField = value;
            }
        }

        /// <remarks/>
        public string Name
        {
            get
            {
                return nameField;
            }
            set
            {
                nameField = value;
            }
        }

        /// <remarks/>
        public bool? IsActive
        {
            get
            {
                return isActiveField;
            }
            set
            {
                isActiveField = value;
            }
        }

        /// <remarks/>
        public VendorRetClassRef ClassRef
        {
            get
            {
                return classRefField;
            }
            set
            {
                classRefField = value;
            }
        }

        /// <remarks/>
        public string CompanyName
        {
            get
            {
                return companyNameField;
            }
            set
            {
                companyNameField = value;
            }
        }

        /// <remarks/>
        public string Salutation
        {
            get
            {
                return salutationField;
            }
            set
            {
                salutationField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return firstNameField;
            }
            set
            {
                firstNameField = value;
            }
        }

        /// <remarks/>
        public string MiddleName
        {
            get
            {
                return middleNameField;
            }
            set
            {
                middleNameField = value;
            }
        }

        /// <remarks/>
        public string LastName
        {
            get
            {
                return lastNameField;
            }
            set
            {
                lastNameField = value;
            }
        }

        /// <remarks/>
        public string JobTitle
        {
            get
            {
                return jobTitleField;
            }
            set
            {
                jobTitleField = value;
            }
        }

        /// <remarks/>
        public VendorRetVendorAddress VendorAddress
        {
            get
            {
                return vendorAddressField;
            }
            set
            {
                vendorAddressField = value;
            }
        }

        /// <remarks/>
        public VendorRetVendorAddressBlock VendorAddressBlock
        {
            get
            {
                return vendorAddressBlockField;
            }
            set
            {
                vendorAddressBlockField = value;
            }
        }

        /// <remarks/>
        public VendorRetShipAddress ShipAddress
        {
            get
            {
                return shipAddressField;
            }
            set
            {
                shipAddressField = value;
            }
        }

        /// <remarks/>
        public string Phone
        {
            get
            {
                return phoneField;
            }
            set
            {
                phoneField = value;
            }
        }

        /// <remarks/>
        public string AltPhone
        {
            get
            {
                return altPhoneField;
            }
            set
            {
                altPhoneField = value;
            }
        }

        /// <remarks/>
        public string Fax
        {
            get
            {
                return faxField;
            }
            set
            {
                faxField = value;
            }
        }

        /// <remarks/>
        public string Email
        {
            get
            {
                return emailField;
            }
            set
            {
                emailField = value;
            }
        }

        /// <remarks/>
        public string Cc
        {
            get
            {
                return ccField;
            }
            set
            {
                ccField = value;
            }
        }

        /// <remarks/>
        public string Contact
        {
            get
            {
                return contactField;
            }
            set
            {
                contactField = value;
            }
        }

        /// <remarks/>
        public string AltContact
        {
            get
            {
                return altContactField;
            }
            set
            {
                altContactField = value;
            }
        }

        /// <remarks/>
        public VendorRetAdditionalContactRef AdditionalContactRef
        {
            get
            {
                return additionalContactRefField;
            }
            set
            {
                additionalContactRefField = value;
            }
        }

        /// <remarks/>
        public VendorRetContactsRet ContactsRet
        {
            get
            {
                return contactsRetField;
            }
            set
            {
                contactsRetField = value;
            }
        }

        /// <remarks/>
        public string NameOnCheck
        {
            get
            {
                return nameOnCheckField;
            }
            set
            {
                nameOnCheckField = value;
            }
        }

        /// <remarks/>
        public string AccountNumber
        {
            get
            {
                return accountNumberField;
            }
            set
            {
                accountNumberField = value;
            }
        }

        /// <remarks/>
        public string Notes
        {
            get
            {
                return notesField;
            }
            set
            {
                notesField = value;
            }
        }

        /// <remarks/>
        public VendorRetAdditionalNotesRet AdditionalNotesRet
        {
            get
            {
                return additionalNotesRetField;
            }
            set
            {
                additionalNotesRetField = value;
            }
        }

        /// <remarks/>
        public VendorRetVendorTypeRef VendorTypeRef
        {
            get
            {
                return vendorTypeRefField;
            }
            set
            {
                vendorTypeRefField = value;
            }
        }

        /// <remarks/>
        public VendorRetTermsRef TermsRef
        {
            get
            {
                return termsRefField;
            }
            set
            {
                termsRefField = value;
            }
        }

        /// <remarks/>
        public string CreditLimit
        {
            get
            {
                return creditLimitField;
            }
            set
            {
                creditLimitField = value;
            }
        }

        /// <remarks/>
        public string VendorTaxIdent
        {
            get
            {
                return vendorTaxIdentField;
            }
            set
            {
                vendorTaxIdentField = value;
            }
        }

        /// <remarks/>
        public string IsVendorEligibleFor1099
        {
            get
            {
                return isVendorEligibleFor1099Field;
            }
            set
            {
                isVendorEligibleFor1099Field = value;
            }
        }

        /// <remarks/>
        public string Balance
        {
            get
            {
                return balanceField;
            }
            set
            {
                balanceField = value;
            }
        }

        /// <remarks/>
        public VendorRetBillingRateRef BillingRateRef
        {
            get
            {
                return billingRateRefField;
            }
            set
            {
                billingRateRefField = value;
            }
        }

        /// <remarks/>
        public string ExternalGUID
        {
            get
            {
                return externalGUIDField;
            }
            set
            {
                externalGUIDField = value;
            }
        }

        /// <remarks/>
        public VendorRetPrefillAccountRef PrefillAccountRef
        {
            get
            {
                return prefillAccountRefField;
            }
            set
            {
                prefillAccountRefField = value;
            }
        }

        /// <remarks/>
        public VendorRetCurrencyRef CurrencyRef
        {
            get
            {
                return currencyRefField;
            }
            set
            {
                currencyRefField = value;
            }
        }

        /// <remarks/>
        public VendorRetDataExtRet DataExtRet
        {
            get
            {
                return dataExtRetField;
            }
            set
            {
                dataExtRetField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class VendorRetClassRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class VendorRetVendorAddress
    {

        private string addr1Field;

        private string addr2Field;

        private string addr3Field;

        private string addr4Field;

        private string addr5Field;

        private string cityField;

        private string stateField;

        private string postalCodeField;

        private string countryField;

        private string noteField;

        /// <remarks/>
        public string Addr1
        {
            get
            {
                return addr1Field;
            }
            set
            {
                addr1Field = value;
            }
        }

        /// <remarks/>
        public string Addr2
        {
            get
            {
                return addr2Field;
            }
            set
            {
                addr2Field = value;
            }
        }

        /// <remarks/>
        public string Addr3
        {
            get
            {
                return addr3Field;
            }
            set
            {
                addr3Field = value;
            }
        }

        /// <remarks/>
        public string Addr4
        {
            get
            {
                return addr4Field;
            }
            set
            {
                addr4Field = value;
            }
        }

        /// <remarks/>
        public string Addr5
        {
            get
            {
                return addr5Field;
            }
            set
            {
                addr5Field = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return cityField;
            }
            set
            {
                cityField = value;
            }
        }

        /// <remarks/>
        public string State
        {
            get
            {
                return stateField;
            }
            set
            {
                stateField = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return postalCodeField;
            }
            set
            {
                postalCodeField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return countryField;
            }
            set
            {
                countryField = value;
            }
        }

        /// <remarks/>
        public string Note
        {
            get
            {
                return noteField;
            }
            set
            {
                noteField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class VendorRetVendorAddressBlock
    {

        private string addr1Field;

        private string addr2Field;

        private string addr3Field;

        private string addr4Field;

        private string addr5Field;

        /// <remarks/>
        public string Addr1
        {
            get
            {
                return addr1Field;
            }
            set
            {
                addr1Field = value;
            }
        }

        /// <remarks/>
        public string Addr2
        {
            get
            {
                return addr2Field;
            }
            set
            {
                addr2Field = value;
            }
        }

        /// <remarks/>
        public string Addr3
        {
            get
            {
                return addr3Field;
            }
            set
            {
                addr3Field = value;
            }
        }

        /// <remarks/>
        public string Addr4
        {
            get
            {
                return addr4Field;
            }
            set
            {
                addr4Field = value;
            }
        }

        /// <remarks/>
        public string Addr5
        {
            get
            {
                return addr5Field;
            }
            set
            {
                addr5Field = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class VendorRetShipAddress
    {

        private string addr1Field;

        private string addr2Field;

        private string addr3Field;

        private string addr4Field;

        private string addr5Field;

        private string cityField;

        private string stateField;

        private string postalCodeField;

        private string countryField;

        private string noteField;

        /// <remarks/>
        public string Addr1
        {
            get
            {
                return addr1Field;
            }
            set
            {
                addr1Field = value;
            }
        }

        /// <remarks/>
        public string Addr2
        {
            get
            {
                return addr2Field;
            }
            set
            {
                addr2Field = value;
            }
        }

        /// <remarks/>
        public string Addr3
        {
            get
            {
                return addr3Field;
            }
            set
            {
                addr3Field = value;
            }
        }

        /// <remarks/>
        public string Addr4
        {
            get
            {
                return addr4Field;
            }
            set
            {
                addr4Field = value;
            }
        }

        /// <remarks/>
        public string Addr5
        {
            get
            {
                return addr5Field;
            }
            set
            {
                addr5Field = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return cityField;
            }
            set
            {
                cityField = value;
            }
        }

        /// <remarks/>
        public string State
        {
            get
            {
                return stateField;
            }
            set
            {
                stateField = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return postalCodeField;
            }
            set
            {
                postalCodeField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return countryField;
            }
            set
            {
                countryField = value;
            }
        }

        /// <remarks/>
        public string Note
        {
            get
            {
                return noteField;
            }
            set
            {
                noteField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class VendorRetAdditionalContactRef
    {

        private string contactNameField;

        private string contactValueField;

        /// <remarks/>
        public string ContactName
        {
            get
            {
                return contactNameField;
            }
            set
            {
                contactNameField = value;
            }
        }

        /// <remarks/>
        public string ContactValue
        {
            get
            {
                return contactValueField;
            }
            set
            {
                contactValueField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class VendorRetContactsRet
    {

        private string listIDField;

        private string timeCreatedField;

        private string timeModifiedField;

        private string editSequenceField;

        private string contactField;

        private string salutationField;

        private string firstNameField;

        private string middleNameField;

        private string lastNameField;

        private string jobTitleField;

        private VendorRetContactsRetAdditionalContactRef additionalContactRefField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string TimeCreated
        {
            get
            {
                return timeCreatedField;
            }
            set
            {
                timeCreatedField = value;
            }
        }

        /// <remarks/>
        public string TimeModified
        {
            get
            {
                return timeModifiedField;
            }
            set
            {
                timeModifiedField = value;
            }
        }

        /// <remarks/>
        public string EditSequence
        {
            get
            {
                return editSequenceField;
            }
            set
            {
                editSequenceField = value;
            }
        }

        /// <remarks/>
        public string Contact
        {
            get
            {
                return contactField;
            }
            set
            {
                contactField = value;
            }
        }

        /// <remarks/>
        public string Salutation
        {
            get
            {
                return salutationField;
            }
            set
            {
                salutationField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return firstNameField;
            }
            set
            {
                firstNameField = value;
            }
        }

        /// <remarks/>
        public string MiddleName
        {
            get
            {
                return middleNameField;
            }
            set
            {
                middleNameField = value;
            }
        }

        /// <remarks/>
        public string LastName
        {
            get
            {
                return lastNameField;
            }
            set
            {
                lastNameField = value;
            }
        }

        /// <remarks/>
        public string JobTitle
        {
            get
            {
                return jobTitleField;
            }
            set
            {
                jobTitleField = value;
            }
        }

        /// <remarks/>
        public VendorRetContactsRetAdditionalContactRef AdditionalContactRef
        {
            get
            {
                return additionalContactRefField;
            }
            set
            {
                additionalContactRefField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class VendorRetContactsRetAdditionalContactRef
    {

        private string contactNameField;

        private string contactValueField;

        /// <remarks/>
        public string ContactName
        {
            get
            {
                return contactNameField;
            }
            set
            {
                contactNameField = value;
            }
        }

        /// <remarks/>
        public string ContactValue
        {
            get
            {
                return contactValueField;
            }
            set
            {
                contactValueField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class VendorRetAdditionalNotesRet
    {

        private string noteIDField;

        private string dateField;

        private string noteField;

        /// <remarks/>
        public string NoteID
        {
            get
            {
                return noteIDField;
            }
            set
            {
                noteIDField = value;
            }
        }

        /// <remarks/>
        public string Date
        {
            get
            {
                return dateField;
            }
            set
            {
                dateField = value;
            }
        }

        /// <remarks/>
        public string Note
        {
            get
            {
                return noteField;
            }
            set
            {
                noteField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class VendorRetVendorTypeRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class VendorRetTermsRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class VendorRetBillingRateRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class VendorRetPrefillAccountRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class VendorRetCurrencyRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class VendorRetDataExtRet
    {

        private string ownerIDField;

        private string dataExtNameField;

        private string dataExtTypeField;

        private string dataExtValueField;

        /// <remarks/>
        public string OwnerID
        {
            get
            {
                return ownerIDField;
            }
            set
            {
                ownerIDField = value;
            }
        }

        /// <remarks/>
        public string DataExtName
        {
            get
            {
                return dataExtNameField;
            }
            set
            {
                dataExtNameField = value;
            }
        }

        /// <remarks/>
        public string DataExtType
        {
            get
            {
                return dataExtTypeField;
            }
            set
            {
                dataExtTypeField = value;
            }
        }

        /// <remarks/>
        public string DataExtValue
        {
            get
            {
                return dataExtValueField;
            }
            set
            {
                dataExtValueField = value;
            }
        }
    }



}
