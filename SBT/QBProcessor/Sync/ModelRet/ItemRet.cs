﻿using System;
using System.Xml.Serialization;

namespace DataProcessor.Sync.ModelRet
{

    public class ItemRet
    {
        public ItemNonInventoryRet ItemNonInventoryRet { get; set; }

        public ItemInventoryRet ItemInventoryRet { get; set; }

        public ItemServiceRet ItemServiceRet { get; set; }

        public ItemDiscountRet ItemDiscountRet { get; set; }
        public ItemFixedAssetRet ItemFixedAssetRet { get; set; }
        public ItemInventoryAssemblyRet ItemInventoryAssemblyRet { get; set; }
        public ItemOtherChargeRet ItemOtherChargeRet { get; set; }
        public ItemPaymentRet ItemPaymentRet { get; set; }
    }

    #region ItemNonInventoryRet

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "" , IsNullable = false)]
    public partial class ItemNonInventoryRet
    {

        private string listIDField;

        private DateTime timeCreatedField;

        private DateTime timeModifiedField;

        private string editSequenceField;

        private string nameField;

        private string fullNameField;

        private string barCodeValueField;

        private bool isActiveField;

        private ItemNonInventoryRetClassRef classRefField;

        private ItemNonInventoryRetParentRef parentRefField;

        private int? sublevelField;

        private string manufacturerPartNumberField;

        private ItemNonInventoryRetUnitOfMeasureSetRef unitOfMeasureSetRefField;

        private ItemNonInventoryRetSalesTaxCodeRef salesTaxCodeRefField;

        private ItemNonInventoryRetSalesOrPurchase salesOrPurchaseField;

        private ItemNonInventoryRetSalesAndPurchase salesAndPurchaseField;

        private string externalGUIDField;

        private ItemNonInventoryRetDataExtRet dataExtRetField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public DateTime TimeCreated
        {
            get
            {
                return timeCreatedField;
            }
            set
            {
                timeCreatedField = value;
            }
        }

        /// <remarks/>
        public DateTime TimeModified
        {
            get
            {
                return timeModifiedField;
            }
            set
            {
                timeModifiedField = value;
            }
        }

        /// <remarks/>
        public string EditSequence
        {
            get
            {
                return editSequenceField;
            }
            set
            {
                editSequenceField = value;
            }
        }

        /// <remarks/>
        public string Name
        {
            get
            {
                return nameField;
            }
            set
            {
                nameField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }

        /// <remarks/>
        public string BarCodeValue
        {
            get
            {
                return barCodeValueField;
            }
            set
            {
                barCodeValueField = value;
            }
        }

        /// <remarks/>
        public bool IsActive
        {
            get
            {
                return isActiveField;
            }
            set
            {
                isActiveField = value;
            }
        }

        /// <remarks/>
        public ItemNonInventoryRetClassRef ClassRef
        {
            get
            {
                return classRefField;
            }
            set
            {
                classRefField = value;
            }
        }

        /// <remarks/>
        public ItemNonInventoryRetParentRef ParentRef
        {
            get
            {
                return parentRefField;
            }
            set
            {
                parentRefField = value;
            }
        }

        /// <remarks/>
        public int? Sublevel
        {
            get
            {
                return sublevelField;
            }
            set
            {
                sublevelField = value;
            }
        }

        /// <remarks/>
        public string ManufacturerPartNumber
        {
            get
            {
                return manufacturerPartNumberField;
            }
            set
            {
                manufacturerPartNumberField = value;
            }
        }

        /// <remarks/>
        public ItemNonInventoryRetUnitOfMeasureSetRef UnitOfMeasureSetRef
        {
            get
            {
                return unitOfMeasureSetRefField;
            }
            set
            {
                unitOfMeasureSetRefField = value;
            }
        }

        /// <remarks/>
        public ItemNonInventoryRetSalesTaxCodeRef SalesTaxCodeRef
        {
            get
            {
                return salesTaxCodeRefField;
            }
            set
            {
                salesTaxCodeRefField = value;
            }
        }

        /// <remarks/>
        public ItemNonInventoryRetSalesOrPurchase SalesOrPurchase
        {
            get
            {
                return salesOrPurchaseField;
            }
            set
            {
                salesOrPurchaseField = value;
            }
        }

        /// <remarks/>
        public ItemNonInventoryRetSalesAndPurchase SalesAndPurchase
        {
            get
            {
                return salesAndPurchaseField;
            }
            set
            {
                salesAndPurchaseField = value;
            }
        }

        /// <remarks/>
        public string ExternalGUID
        {
            get
            {
                return externalGUIDField;
            }
            set
            {
                externalGUIDField = value;
            }
        }

        /// <remarks/>
        public ItemNonInventoryRetDataExtRet DataExtRet
        {
            get
            {
                return dataExtRetField;
            }
            set
            {
                dataExtRetField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemNonInventoryRetClassRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemNonInventoryRetParentRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemNonInventoryRetUnitOfMeasureSetRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemNonInventoryRetSalesTaxCodeRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemNonInventoryRetSalesOrPurchase
    {

        private string descField;

        private decimal? priceField;

        private decimal? pricePercentField;

        private ItemNonInventoryRetSalesOrPurchaseAccountRef accountRefField;

        /// <remarks/>
        public string Desc
        {
            get
            {
                return descField;
            }
            set
            {
                descField = value;
            }
        }

        /// <remarks/>
        public decimal? Price
        {
            get
            {
                return priceField;
            }
            set
            {
                priceField = value;
            }
        }

        /// <remarks/>
        public decimal? PricePercent
        {
            get
            {
                return pricePercentField;
            }
            set
            {
                pricePercentField = value;
            }
        }

        /// <remarks/>
        public ItemNonInventoryRetSalesOrPurchaseAccountRef AccountRef
        {
            get
            {
                return accountRefField;
            }
            set
            {
                accountRefField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemNonInventoryRetSalesOrPurchaseAccountRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemNonInventoryRetSalesAndPurchase
    {

        private string salesDescField;

        private decimal? salesPriceField;

        private ItemNonInventoryRetSalesAndPurchaseIncomeAccountRef incomeAccountRefField;

        private string purchaseDescField;

        private decimal? purchaseCostField;

        private ItemNonInventoryRetSalesAndPurchaseExpenseAccountRef expenseAccountRefField;

        private ItemNonInventoryRetSalesAndPurchasePrefVendorRef prefVendorRefField;

        /// <remarks/>
        public string SalesDesc
        {
            get
            {
                return salesDescField;
            }
            set
            {
                salesDescField = value;
            }
        }

        /// <remarks/>
        public decimal? SalesPrice
        {
            get
            {
                return salesPriceField;
            }
            set
            {
                salesPriceField = value;
            }
        }

        /// <remarks/>
        public ItemNonInventoryRetSalesAndPurchaseIncomeAccountRef IncomeAccountRef
        {
            get
            {
                return incomeAccountRefField;
            }
            set
            {
                incomeAccountRefField = value;
            }
        }

        /// <remarks/>
        public string PurchaseDesc
        {
            get
            {
                return purchaseDescField;
            }
            set
            {
                purchaseDescField = value;
            }
        }

        /// <remarks/>
        public decimal? PurchaseCost
        {
            get
            {
                return purchaseCostField;
            }
            set
            {
                purchaseCostField = value;
            }
        }

        /// <remarks/>
        public ItemNonInventoryRetSalesAndPurchaseExpenseAccountRef ExpenseAccountRef
        {
            get
            {
                return expenseAccountRefField;
            }
            set
            {
                expenseAccountRefField = value;
            }
        }

        /// <remarks/>
        public ItemNonInventoryRetSalesAndPurchasePrefVendorRef PrefVendorRef
        {
            get
            {
                return prefVendorRefField;
            }
            set
            {
                prefVendorRefField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemNonInventoryRetSalesAndPurchaseIncomeAccountRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemNonInventoryRetSalesAndPurchaseExpenseAccountRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemNonInventoryRetSalesAndPurchasePrefVendorRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemNonInventoryRetDataExtRet
    {

        private string ownerIDField;

        private string dataExtNameField;

        private string dataExtTypeField;

        private string dataExtValueField;

        /// <remarks/>
        public string OwnerID
        {
            get
            {
                return ownerIDField;
            }
            set
            {
                ownerIDField = value;
            }
        }

        /// <remarks/>
        public string DataExtName
        {
            get
            {
                return dataExtNameField;
            }
            set
            {
                dataExtNameField = value;
            }
        }

        /// <remarks/>
        public string DataExtType
        {
            get
            {
                return dataExtTypeField;
            }
            set
            {
                dataExtTypeField = value;
            }
        }

        /// <remarks/>
        public string DataExtValue
        {
            get
            {
                return dataExtValueField;
            }
            set
            {
                dataExtValueField = value;
            }
        }
    }

    #endregion

    #region "ItemInventoryRet"
    /// <remarks/>
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "" , IsNullable = false)]
    public partial class ItemInventoryRet
    {

        private string listIDField;

        private DateTime timeCreatedField;

        private DateTime timeModifiedField;

        private string editSequenceField;

        private string nameField;

        private string fullNameField;

        private string barCodeValueField;

        private bool isActiveField;

        private ItemInventoryRetClassRef classRefField;

        private ItemInventoryRetParentRef parentRefField;

        private int? sublevelField;

        private string manufacturerPartNumberField;

        private ItemInventoryRetUnitOfMeasureSetRef unitOfMeasureSetRefField;

        private ItemInventoryRetSalesTaxCodeRef salesTaxCodeRefField;

        private string salesDescField;

        private decimal? salesPriceField;

        private ItemInventoryRetIncomeAccountRef incomeAccountRefField;

        private string purchaseDescField;

        private decimal? purchaseCostField;

        private ItemInventoryRetCOGSAccountRef cOGSAccountRefField;

        private ItemInventoryRetPrefVendorRef prefVendorRefField;

        private ItemInventoryRetAssetAccountRef assetAccountRefField;

        private string reorderPointField;

        private string maxField;

        private string quantityOnHandField;

        private decimal? averageCostField;

        private string quantityOnOrderField;

        private string quantityOnSalesOrderField;

        private string externalGUIDField;

        private ItemInventoryRetDataExtRet dataExtRetField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public DateTime TimeCreated
        {
            get
            {
                return timeCreatedField;
            }
            set
            {
                timeCreatedField = value;
            }
        }

        /// <remarks/>
        public DateTime TimeModified
        {
            get
            {
                return timeModifiedField;
            }
            set
            {
                timeModifiedField = value;
            }
        }

        /// <remarks/>
        public string EditSequence
        {
            get
            {
                return editSequenceField;
            }
            set
            {
                editSequenceField = value;
            }
        }

        /// <remarks/>
        public string Name
        {
            get
            {
                return nameField;
            }
            set
            {
                nameField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }

        /// <remarks/>
        public string BarCodeValue
        {
            get
            {
                return barCodeValueField;
            }
            set
            {
                barCodeValueField = value;
            }
        }

        /// <remarks/>
        public bool IsActive
        {
            get
            {
                return isActiveField;
            }
            set
            {
                isActiveField = value;
            }
        }

        /// <remarks/>
        public ItemInventoryRetClassRef ClassRef
        {
            get
            {
                return classRefField;
            }
            set
            {
                classRefField = value;
            }
        }

        /// <remarks/>
        public ItemInventoryRetParentRef ParentRef
        {
            get
            {
                return parentRefField;
            }
            set
            {
                parentRefField = value;
            }
        }

        /// <remarks/>
        public int? Sublevel
        {
            get
            {
                return sublevelField;
            }
            set
            {
                sublevelField = value;
            }
        }

        /// <remarks/>
        public string ManufacturerPartNumber
        {
            get
            {
                return manufacturerPartNumberField;
            }
            set
            {
                manufacturerPartNumberField = value;
            }
        }

        /// <remarks/>
        public ItemInventoryRetUnitOfMeasureSetRef UnitOfMeasureSetRef
        {
            get
            {
                return unitOfMeasureSetRefField;
            }
            set
            {
                unitOfMeasureSetRefField = value;
            }
        }

        /// <remarks/>
        public ItemInventoryRetSalesTaxCodeRef SalesTaxCodeRef
        {
            get
            {
                return salesTaxCodeRefField;
            }
            set
            {
                salesTaxCodeRefField = value;
            }
        }

        /// <remarks/>
        public string SalesDesc
        {
            get
            {
                return salesDescField;
            }
            set
            {
                salesDescField = value;
            }
        }

        /// <remarks/>
        public decimal? SalesPrice
        {
            get
            {
                return salesPriceField;
            }
            set
            {
                salesPriceField = value;
            }
        }

        /// <remarks/>
        public ItemInventoryRetIncomeAccountRef IncomeAccountRef
        {
            get
            {
                return incomeAccountRefField;
            }
            set
            {
                incomeAccountRefField = value;
            }
        }

        /// <remarks/>
        public string PurchaseDesc
        {
            get
            {
                return purchaseDescField;
            }
            set
            {
                purchaseDescField = value;
            }
        }

        /// <remarks/>
        public decimal? PurchaseCost
        {
            get
            {
                return purchaseCostField;
            }
            set
            {
                purchaseCostField = value;
            }
        }

        /// <remarks/>
        public ItemInventoryRetCOGSAccountRef COGSAccountRef
        {
            get
            {
                return cOGSAccountRefField;
            }
            set
            {
                cOGSAccountRefField = value;
            }
        }

        /// <remarks/>
        public ItemInventoryRetPrefVendorRef PrefVendorRef
        {
            get
            {
                return prefVendorRefField;
            }
            set
            {
                prefVendorRefField = value;
            }
        }

        /// <remarks/>
        public ItemInventoryRetAssetAccountRef AssetAccountRef
        {
            get
            {
                return assetAccountRefField;
            }
            set
            {
                assetAccountRefField = value;
            }
        }

        /// <remarks/>
        public string ReorderPoint
        {
            get
            {
                return reorderPointField;
            }
            set
            {
                reorderPointField = value;
            }
        }

        /// <remarks/>
        public string Max
        {
            get
            {
                return maxField;
            }
            set
            {
                maxField = value;
            }
        }

        /// <remarks/>
        public string QuantityOnHand
        {
            get
            {
                return quantityOnHandField;
            }
            set
            {
                quantityOnHandField = value;
            }
        }

        /// <remarks/>
        public decimal? AverageCost
        {
            get
            {
                return averageCostField;
            }
            set
            {
                averageCostField = value;
            }
        }

        /// <remarks/>
        public string QuantityOnOrder
        {
            get
            {
                return quantityOnOrderField;
            }
            set
            {
                quantityOnOrderField = value;
            }
        }

        /// <remarks/>
        public string QuantityOnSalesOrder
        {
            get
            {
                return quantityOnSalesOrderField;
            }
            set
            {
                quantityOnSalesOrderField = value;
            }
        }

        /// <remarks/>
        public string ExternalGUID
        {
            get
            {
                return externalGUIDField;
            }
            set
            {
                externalGUIDField = value;
            }
        }

        /// <remarks/>
        public ItemInventoryRetDataExtRet DataExtRet
        {
            get
            {
                return dataExtRetField;
            }
            set
            {
                dataExtRetField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemInventoryRetClassRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemInventoryRetParentRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemInventoryRetUnitOfMeasureSetRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemInventoryRetSalesTaxCodeRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemInventoryRetIncomeAccountRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemInventoryRetCOGSAccountRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemInventoryRetPrefVendorRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemInventoryRetAssetAccountRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemInventoryRetDataExtRet
    {

        private string ownerIDField;

        private string dataExtNameField;

        private string dataExtTypeField;

        private string dataExtValueField;

        /// <remarks/>
        public string OwnerID
        {
            get
            {
                return ownerIDField;
            }
            set
            {
                ownerIDField = value;
            }
        }

        /// <remarks/>
        public string DataExtName
        {
            get
            {
                return dataExtNameField;
            }
            set
            {
                dataExtNameField = value;
            }
        }

        /// <remarks/>
        public string DataExtType
        {
            get
            {
                return dataExtTypeField;
            }
            set
            {
                dataExtTypeField = value;
            }
        }

        /// <remarks/>
        public string DataExtValue
        {
            get
            {
                return dataExtValueField;
            }
            set
            {
                dataExtValueField = value;
            }
        }
    }
    #endregion

    #region "ItemService"
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "" , IsNullable = false)]
    public partial class ItemServiceRet
    {

        private string listIDField;

        private DateTime timeCreatedField;

        private DateTime timeModifiedField;

        private string editSequenceField;

        private string nameField;

        private string fullNameField;

        private string barCodeValueField;

        private bool isActiveField;

        private ItemServiceRetClassRef classRefField;

        private ItemServiceRetParentRef parentRefField;

        private int? sublevelField;

        private ItemServiceRetUnitOfMeasureSetRef unitOfMeasureSetRefField;

        private ItemServiceRetSalesTaxCodeRef salesTaxCodeRefField;

        private ItemServiceRetSalesOrPurchase salesOrPurchaseField;

        private ItemServiceRetSalesAndPurchase salesAndPurchaseField;

        private string externalGUIDField;

        private ItemServiceRetDataExtRet dataExtRetField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public DateTime TimeCreated
        {
            get
            {
                return timeCreatedField;
            }
            set
            {
                timeCreatedField = value;
            }
        }

        /// <remarks/>
        public DateTime TimeModified
        {
            get
            {
                return timeModifiedField;
            }
            set
            {
                timeModifiedField = value;
            }
        }

        /// <remarks/>
        public string EditSequence
        {
            get
            {
                return editSequenceField;
            }
            set
            {
                editSequenceField = value;
            }
        }

        /// <remarks/>
        public string Name
        {
            get
            {
                return nameField;
            }
            set
            {
                nameField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }

        /// <remarks/>
        public string BarCodeValue
        {
            get
            {
                return barCodeValueField;
            }
            set
            {
                barCodeValueField = value;
            }
        }

        /// <remarks/>
        public bool IsActive
        {
            get
            {
                return isActiveField;
            }
            set
            {
                isActiveField = value;
            }
        }

        /// <remarks/>
        public ItemServiceRetClassRef ClassRef
        {
            get
            {
                return classRefField;
            }
            set
            {
                classRefField = value;
            }
        }

        /// <remarks/>
        public ItemServiceRetParentRef ParentRef
        {
            get
            {
                return parentRefField;
            }
            set
            {
                parentRefField = value;
            }
        }

        /// <remarks/>
        public int? Sublevel
        {
            get
            {
                return sublevelField;
            }
            set
            {
                sublevelField = value;
            }
        }

        /// <remarks/>
        public ItemServiceRetUnitOfMeasureSetRef UnitOfMeasureSetRef
        {
            get
            {
                return unitOfMeasureSetRefField;
            }
            set
            {
                unitOfMeasureSetRefField = value;
            }
        }

        /// <remarks/>
        public ItemServiceRetSalesTaxCodeRef SalesTaxCodeRef
        {
            get
            {
                return salesTaxCodeRefField;
            }
            set
            {
                salesTaxCodeRefField = value;
            }
        }

        /// <remarks/>
        public ItemServiceRetSalesOrPurchase SalesOrPurchase
        {
            get
            {
                return salesOrPurchaseField;
            }
            set
            {
                salesOrPurchaseField = value;
            }
        }

        /// <remarks/>
        public ItemServiceRetSalesAndPurchase SalesAndPurchase
        {
            get
            {
                return salesAndPurchaseField;
            }
            set
            {
                salesAndPurchaseField = value;
            }
        }

        /// <remarks/>
        public string ExternalGUID
        {
            get
            {
                return externalGUIDField;
            }
            set
            {
                externalGUIDField = value;
            }
        }

        /// <remarks/>
        public ItemServiceRetDataExtRet DataExtRet
        {
            get
            {
                return dataExtRetField;
            }
            set
            {
                dataExtRetField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemServiceRetClassRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemServiceRetParentRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemServiceRetUnitOfMeasureSetRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemServiceRetSalesTaxCodeRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemServiceRetSalesOrPurchase
    {

        private string descField;

        private decimal? priceField;

        private string pricePercentField;

        private ItemServiceRetSalesOrPurchaseAccountRef accountRefField;

        /// <remarks/>
        public string Desc
        {
            get
            {
                return descField;
            }
            set
            {
                descField = value;
            }
        }

        /// <remarks/>
        public decimal? Price
        {
            get
            {
                return priceField;
            }
            set
            {
                priceField = value;
            }
        }

        /// <remarks/>
        public string PricePercent
        {
            get
            {
                return pricePercentField;
            }
            set
            {
                pricePercentField = value;
            }
        }

        /// <remarks/>
        public ItemServiceRetSalesOrPurchaseAccountRef AccountRef
        {
            get
            {
                return accountRefField;
            }
            set
            {
                accountRefField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemServiceRetSalesOrPurchaseAccountRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemServiceRetSalesAndPurchase
    {

        private string salesDescField;

        private decimal? salesPriceField;

        private ItemServiceRetSalesAndPurchaseIncomeAccountRef incomeAccountRefField;

        private string purchaseDescField;

        private decimal? purchaseCostField;

        private ItemServiceRetSalesAndPurchaseExpenseAccountRef expenseAccountRefField;

        private ItemServiceRetSalesAndPurchasePrefVendorRef prefVendorRefField;

        /// <remarks/>
        public string SalesDesc
        {
            get
            {
                return salesDescField;
            }
            set
            {
                salesDescField = value;
            }
        }

        /// <remarks/>
        public decimal? SalesPrice
        {
            get
            {
                return salesPriceField;
            }
            set
            {
                salesPriceField = value;
            }
        }

        /// <remarks/>
        public ItemServiceRetSalesAndPurchaseIncomeAccountRef IncomeAccountRef
        {
            get
            {
                return incomeAccountRefField;
            }
            set
            {
                incomeAccountRefField = value;
            }
        }

        /// <remarks/>
        public string PurchaseDesc
        {
            get
            {
                return purchaseDescField;
            }
            set
            {
                purchaseDescField = value;
            }
        }

        /// <remarks/>
        public decimal? PurchaseCost
        {
            get
            {
                return purchaseCostField;
            }
            set
            {
                purchaseCostField = value;
            }
        }

        /// <remarks/>
        public ItemServiceRetSalesAndPurchaseExpenseAccountRef ExpenseAccountRef
        {
            get
            {
                return expenseAccountRefField;
            }
            set
            {
                expenseAccountRefField = value;
            }
        }

        /// <remarks/>
        public ItemServiceRetSalesAndPurchasePrefVendorRef PrefVendorRef
        {
            get
            {
                return prefVendorRefField;
            }
            set
            {
                prefVendorRefField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemServiceRetSalesAndPurchaseIncomeAccountRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemServiceRetSalesAndPurchaseExpenseAccountRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemServiceRetSalesAndPurchasePrefVendorRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemServiceRetDataExtRet
    {

        private string ownerIDField;

        private string dataExtNameField;

        private string dataExtTypeField;

        private string dataExtValueField;

        /// <remarks/>
        public string OwnerID
        {
            get
            {
                return ownerIDField;
            }
            set
            {
                ownerIDField = value;
            }
        }

        /// <remarks/>
        public string DataExtName
        {
            get
            {
                return dataExtNameField;
            }
            set
            {
                dataExtNameField = value;
            }
        }

        /// <remarks/>
        public string DataExtType
        {
            get
            {
                return dataExtTypeField;
            }
            set
            {
                dataExtTypeField = value;
            }
        }

        /// <remarks/>
        public string DataExtValue
        {
            get
            {
                return dataExtValueField;
            }
            set
            {
                dataExtValueField = value;
            }
        }
    }
    #endregion

    #region "ItemDiscountRq"

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "" , IsNullable = false)]
    public partial class ItemDiscountRet
    {

        private string listIDField;

        private DateTime timeCreatedField;

        private DateTime timeModifiedField;

        private string editSequenceField;

        private string nameField;

        private string fullNameField;

        private string barCodeValueField;

        private bool isActiveField;

        private ItemDiscountRetClassRef classRefField;

        private ItemDiscountRetParentRef parentRefField;

        private int? sublevelField;

        private string itemDescField;

        private ItemDiscountRetSalesTaxCodeRef salesTaxCodeRefField;

        private decimal? discountRateField;

        private decimal? discountRatePercentField;

        private ItemDiscountRetAccountRef accountRefField;

        private string externalGUIDField;

        private ItemDiscountRetDataExtRet dataExtRetField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public DateTime TimeCreated
        {
            get
            {
                return timeCreatedField;
            }
            set
            {
                timeCreatedField = value;
            }
        }

        /// <remarks/>
        public DateTime TimeModified
        {
            get
            {
                return timeModifiedField;
            }
            set
            {
                timeModifiedField = value;
            }
        }

        /// <remarks/>
        public string EditSequence
        {
            get
            {
                return editSequenceField;
            }
            set
            {
                editSequenceField = value;
            }
        }

        /// <remarks/>
        public string Name
        {
            get
            {
                return nameField;
            }
            set
            {
                nameField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }

        /// <remarks/>
        public string BarCodeValue
        {
            get
            {
                return barCodeValueField;
            }
            set
            {
                barCodeValueField = value;
            }
        }

        /// <remarks/>
        public bool IsActive
        {
            get
            {
                return isActiveField;
            }
            set
            {
                isActiveField = value;
            }
        }

        /// <remarks/>
        public ItemDiscountRetClassRef ClassRef
        {
            get
            {
                return classRefField;
            }
            set
            {
                classRefField = value;
            }
        }

        /// <remarks/>
        public ItemDiscountRetParentRef ParentRef
        {
            get
            {
                return parentRefField;
            }
            set
            {
                parentRefField = value;
            }
        }

        /// <remarks/>
        public int? Sublevel
        {
            get
            {
                return sublevelField;
            }
            set
            {
                sublevelField = value;
            }
        }

        /// <remarks/>
        public string ItemDesc
        {
            get
            {
                return itemDescField;
            }
            set
            {
                itemDescField = value;
            }
        }

        /// <remarks/>
        public ItemDiscountRetSalesTaxCodeRef SalesTaxCodeRef
        {
            get
            {
                return salesTaxCodeRefField;
            }
            set
            {
                salesTaxCodeRefField = value;
            }
        }

        /// <remarks/>
        public decimal? DiscountRate
        {
            get
            {
                return discountRateField;
            }
            set
            {
                discountRateField = value;
            }
        }

        /// <remarks/>
        public decimal? DiscountRatePercent
        {
            get
            {
                return discountRatePercentField;
            }
            set
            {
                discountRatePercentField = value;
            }
        }

        /// <remarks/>
        public ItemDiscountRetAccountRef AccountRef
        {
            get
            {
                return accountRefField;
            }
            set
            {
                accountRefField = value;
            }
        }

        /// <remarks/>
        public string ExternalGUID
        {
            get
            {
                return externalGUIDField;
            }
            set
            {
                externalGUIDField = value;
            }
        }

        /// <remarks/>
        public ItemDiscountRetDataExtRet DataExtRet
        {
            get
            {
                return dataExtRetField;
            }
            set
            {
                dataExtRetField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemDiscountRetClassRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemDiscountRetParentRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemDiscountRetSalesTaxCodeRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemDiscountRetAccountRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemDiscountRetDataExtRet
    {

        private string ownerIDField;

        private string dataExtNameField;

        private string dataExtTypeField;

        private string dataExtValueField;

        /// <remarks/>
        public string OwnerID
        {
            get
            {
                return ownerIDField;
            }
            set
            {
                ownerIDField = value;
            }
        }

        /// <remarks/>
        public string DataExtName
        {
            get
            {
                return dataExtNameField;
            }
            set
            {
                dataExtNameField = value;
            }
        }

        /// <remarks/>
        public string DataExtType
        {
            get
            {
                return dataExtTypeField;
            }
            set
            {
                dataExtTypeField = value;
            }
        }

        /// <remarks/>
        public string DataExtValue
        {
            get
            {
                return dataExtValueField;
            }
            set
            {
                dataExtValueField = value;
            }
        }
    }



#endregion

    #region "ItemFixedAssetRet"
    /// <remarks/>
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "" , IsNullable = false)]
    public partial class ItemFixedAssetRet
    {

        private string listIDField;

        private DateTime timeCreatedField;

        private DateTime timeModifiedField;

        private string editSequenceField;

        private string nameField;

        private string barCodeValueField;

        private bool isActiveField;

        private ItemFixedAssetRetClassRef classRefField;

        private string acquiredAsField;

        private string purchaseDescField;

        private string purchaseDateField;

        private float purchaseCostField;

        private string vendorOrPayeeNameField;

        private ItemFixedAssetRetAssetAccountRef assetAccountRefField;

        private ItemFixedAssetRetFixedAssetSalesInfo fixedAssetSalesInfoField;

        private string assetDescField;

        private string locationField;

        private string pONumberField;

        private string serialNumberField;

        private string warrantyExpDateField;

        private string notesField;

        private string assetNumberField;

        private string costBasisField;

        private string yearEndAccumulatedDepreciationField;

        private string yearEndBookValueField;

        private string externalGUIDField;

        private ItemFixedAssetRetDataExtRet dataExtRetField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public DateTime TimeCreated
        {
            get
            {
                return timeCreatedField;
            }
            set
            {
                timeCreatedField = value;
            }
        }

        /// <remarks/>
        public DateTime TimeModified
        {
            get
            {
                return timeModifiedField;
            }
            set
            {
                timeModifiedField = value;
            }
        }

        /// <remarks/>
        public string EditSequence
        {
            get
            {
                return editSequenceField;
            }
            set
            {
                editSequenceField = value;
            }
        }

        /// <remarks/>
        public string Name
        {
            get
            {
                return nameField;
            }
            set
            {
                nameField = value;
            }
        }

        /// <remarks/>
        public string BarCodeValue
        {
            get
            {
                return barCodeValueField;
            }
            set
            {
                barCodeValueField = value;
            }
        }

        /// <remarks/>
        public bool IsActive
        {
            get
            {
                return isActiveField;
            }
            set
            {
                isActiveField = value;
            }
        }

        /// <remarks/>
        public ItemFixedAssetRetClassRef ClassRef
        {
            get
            {
                return classRefField;
            }
            set
            {
                classRefField = value;
            }
        }

        /// <remarks/>
        public string AcquiredAs
        {
            get
            {
                return acquiredAsField;
            }
            set
            {
                acquiredAsField = value;
            }
        }

        /// <remarks/>
        public string PurchaseDesc
        {
            get
            {
                return purchaseDescField;
            }
            set
            {
                purchaseDescField = value;
            }
        }

        /// <remarks/>
        public string PurchaseDate
        {
            get
            {
                return purchaseDateField;
            }
            set
            {
                purchaseDateField = value;
            }
        }

        /// <remarks/>
        public float PurchaseCost
        {
            get
            {
                return purchaseCostField;
            }
            set
            {
                purchaseCostField = value;
            }
        }

        /// <remarks/>
        public string VendorOrPayeeName
        {
            get
            {
                return vendorOrPayeeNameField;
            }
            set
            {
                vendorOrPayeeNameField = value;
            }
        }

        /// <remarks/>
        public ItemFixedAssetRetAssetAccountRef AssetAccountRef
        {
            get
            {
                return assetAccountRefField;
            }
            set
            {
                assetAccountRefField = value;
            }
        }

        /// <remarks/>
        public ItemFixedAssetRetFixedAssetSalesInfo FixedAssetSalesInfo
        {
            get
            {
                return fixedAssetSalesInfoField;
            }
            set
            {
                fixedAssetSalesInfoField = value;
            }
        }

        /// <remarks/>
        public string AssetDesc
        {
            get
            {
                return assetDescField;
            }
            set
            {
                assetDescField = value;
            }
        }

        /// <remarks/>
        public string Location
        {
            get
            {
                return locationField;
            }
            set
            {
                locationField = value;
            }
        }

        /// <remarks/>
        public string PONumber
        {
            get
            {
                return pONumberField;
            }
            set
            {
                pONumberField = value;
            }
        }

        /// <remarks/>
        public string SerialNumber
        {
            get
            {
                return serialNumberField;
            }
            set
            {
                serialNumberField = value;
            }
        }

        /// <remarks/>
        public string WarrantyExpDate
        {
            get
            {
                return warrantyExpDateField;
            }
            set
            {
                warrantyExpDateField = value;
            }
        }

        /// <remarks/>
        public string Notes
        {
            get
            {
                return notesField;
            }
            set
            {
                notesField = value;
            }
        }

        /// <remarks/>
        public string AssetNumber
        {
            get
            {
                return assetNumberField;
            }
            set
            {
                assetNumberField = value;
            }
        }

        /// <remarks/>
        public string CostBasis
        {
            get
            {
                return costBasisField;
            }
            set
            {
                costBasisField = value;
            }
        }

        /// <remarks/>
        public string YearEndAccumulatedDepreciation
        {
            get
            {
                return yearEndAccumulatedDepreciationField;
            }
            set
            {
                yearEndAccumulatedDepreciationField = value;
            }
        }

        /// <remarks/>
        public string YearEndBookValue
        {
            get
            {
                return yearEndBookValueField;
            }
            set
            {
                yearEndBookValueField = value;
            }
        }

        /// <remarks/>
        public string ExternalGUID
        {
            get
            {
                return externalGUIDField;
            }
            set
            {
                externalGUIDField = value;
            }
        }

        /// <remarks/>
        public ItemFixedAssetRetDataExtRet DataExtRet
        {
            get
            {
                return dataExtRetField;
            }
            set
            {
                dataExtRetField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemFixedAssetRetClassRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemFixedAssetRetAssetAccountRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemFixedAssetRetFixedAssetSalesInfo
    {

        private string salesDescField;

        private string salesDateField;

        private string salesPriceField;

        private string salesExpenseField;

        /// <remarks/>
        public string SalesDesc
        {
            get
            {
                return salesDescField;
            }
            set
            {
                salesDescField = value;
            }
        }

        /// <remarks/>
        public string SalesDate
        {
            get
            {
                return salesDateField;
            }
            set
            {
                salesDateField = value;
            }
        }

        /// <remarks/>
        public string SalesPrice
        {
            get
            {
                return salesPriceField;
            }
            set
            {
                salesPriceField = value;
            }
        }

        /// <remarks/>
        public string SalesExpense
        {
            get
            {
                return salesExpenseField;
            }
            set
            {
                salesExpenseField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemFixedAssetRetDataExtRet
    {

        private string ownerIDField;

        private string dataExtNameField;

        private string dataExtTypeField;

        private string dataExtValueField;

        /// <remarks/>
        public string OwnerID
        {
            get
            {
                return ownerIDField;
            }
            set
            {
                ownerIDField = value;
            }
        }

        /// <remarks/>
        public string DataExtName
        {
            get
            {
                return dataExtNameField;
            }
            set
            {
                dataExtNameField = value;
            }
        }

        /// <remarks/>
        public string DataExtType
        {
            get
            {
                return dataExtTypeField;
            }
            set
            {
                dataExtTypeField = value;
            }
        }

        /// <remarks/>
        public string DataExtValue
        {
            get
            {
                return dataExtValueField;
            }
            set
            {
                dataExtValueField = value;
            }
        }
    }

#endregion

    

    #region "ItemInventoryAssemblyRet"
    /// <remarks/>
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "" , IsNullable = false)]
    public partial class ItemInventoryAssemblyRet
    {

        private string listIDField;

        private DateTime timeCreatedField;

        private DateTime timeModifiedField;

        private string editSequenceField;

        private string nameField;

        private string fullNameField;

        private string barCodeValueField;

        private bool isActiveField;

        private ItemInventoryAssemblyRetClassRef classRefField;

        private ItemInventoryAssemblyRetParentRef parentRefField;

        private int? sublevelField;

        private string manufacturerPartNumberField;

        private ItemInventoryAssemblyRetUnitOfMeasureSetRef unitOfMeasureSetRefField;

        private ItemInventoryAssemblyRetSalesTaxCodeRef salesTaxCodeRefField;

        private string salesDescField;

        private float salesPriceField;

        private ItemInventoryAssemblyRetIncomeAccountRef incomeAccountRefField;

        private string purchaseDescField;

        private string purchaseCostField;

        private ItemInventoryAssemblyRetCOGSAccountRef cOGSAccountRefField;

        private ItemInventoryAssemblyRetPrefVendorRef prefVendorRefField;

        private ItemInventoryAssemblyRetAssetAccountRef assetAccountRefField;

        private string buildPointField;

        private string maxField;

        private string quantityOnHandField;

        private decimal? averageCostField;

        private string quantityOnOrderField;

        private string quantityOnSalesOrderField;

        private string externalGUIDField;

        private ItemInventoryAssemblyRetItemInventoryAssemblyLine itemInventoryAssemblyLineField;

        private ItemInventoryAssemblyRetDataExtRet dataExtRetField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public DateTime TimeCreated
        {
            get
            {
                return timeCreatedField;
            }
            set
            {
                timeCreatedField = value;
            }
        }

        /// <remarks/>
        public DateTime TimeModified
        {
            get
            {
                return timeModifiedField;
            }
            set
            {
                timeModifiedField = value;
            }
        }

        /// <remarks/>
        public string EditSequence
        {
            get
            {
                return editSequenceField;
            }
            set
            {
                editSequenceField = value;
            }
        }

        /// <remarks/>
        public string Name
        {
            get
            {
                return nameField;
            }
            set
            {
                nameField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }

        /// <remarks/>
        public string BarCodeValue
        {
            get
            {
                return barCodeValueField;
            }
            set
            {
                barCodeValueField = value;
            }
        }

        /// <remarks/>
        public bool IsActive
        {
            get
            {
                return isActiveField;
            }
            set
            {
                isActiveField = value;
            }
        }

        /// <remarks/>
        public ItemInventoryAssemblyRetClassRef ClassRef
        {
            get
            {
                return classRefField;
            }
            set
            {
                classRefField = value;
            }
        }

        /// <remarks/>
        public ItemInventoryAssemblyRetParentRef ParentRef
        {
            get
            {
                return parentRefField;
            }
            set
            {
                parentRefField = value;
            }
        }

        /// <remarks/>
        public int? Sublevel
        {
            get
            {
                return sublevelField;
            }
            set
            {
                sublevelField = value;
            }
        }

        /// <remarks/>
        public string ManufacturerPartNumber
        {
            get
            {
                return manufacturerPartNumberField;
            }
            set
            {
                manufacturerPartNumberField = value;
            }
        }

        /// <remarks/>
        public ItemInventoryAssemblyRetUnitOfMeasureSetRef UnitOfMeasureSetRef
        {
            get
            {
                return unitOfMeasureSetRefField;
            }
            set
            {
                unitOfMeasureSetRefField = value;
            }
        }

        /// <remarks/>
        public ItemInventoryAssemblyRetSalesTaxCodeRef SalesTaxCodeRef
        {
            get
            {
                return salesTaxCodeRefField;
            }
            set
            {
                salesTaxCodeRefField = value;
            }
        }

        /// <remarks/>
        public string SalesDesc
        {
            get
            {
                return salesDescField;
            }
            set
            {
                salesDescField = value;
            }
        }

        /// <remarks/>
        public float SalesPrice
        {
            get
            {
                return salesPriceField;
            }
            set
            {
                salesPriceField = value;
            }
        }

        /// <remarks/>
        public ItemInventoryAssemblyRetIncomeAccountRef IncomeAccountRef
        {
            get
            {
                return incomeAccountRefField;
            }
            set
            {
                incomeAccountRefField = value;
            }
        }

        /// <remarks/>
        public string PurchaseDesc
        {
            get
            {
                return purchaseDescField;
            }
            set
            {
                purchaseDescField = value;
            }
        }

        /// <remarks/>
        public string PurchaseCost
        {
            get
            {
                return purchaseCostField;
            }
            set
            {
                purchaseCostField = value;
            }
        }

        /// <remarks/>
        public ItemInventoryAssemblyRetCOGSAccountRef COGSAccountRef
        {
            get
            {
                return cOGSAccountRefField;
            }
            set
            {
                cOGSAccountRefField = value;
            }
        }

        /// <remarks/>
        public ItemInventoryAssemblyRetPrefVendorRef PrefVendorRef
        {
            get
            {
                return prefVendorRefField;
            }
            set
            {
                prefVendorRefField = value;
            }
        }

        /// <remarks/>
        public ItemInventoryAssemblyRetAssetAccountRef AssetAccountRef
        {
            get
            {
                return assetAccountRefField;
            }
            set
            {
                assetAccountRefField = value;
            }
        }

        /// <remarks/>
        public string BuildPoint
        {
            get
            {
                return buildPointField;
            }
            set
            {
                buildPointField = value;
            }
        }

        /// <remarks/>
        public string Max
        {
            get
            {
                return maxField;
            }
            set
            {
                maxField = value;
            }
        }

        /// <remarks/>
        public string QuantityOnHand
        {
            get
            {
                return quantityOnHandField;
            }
            set
            {
                quantityOnHandField = value;
            }
        }

        /// <remarks/>
        public decimal? AverageCost
        {
            get
            {
                return averageCostField;
            }
            set
            {
                averageCostField = value;
            }
        }

        /// <remarks/>
        public string QuantityOnOrder
        {
            get
            {
                return quantityOnOrderField;
            }
            set
            {
                quantityOnOrderField = value;
            }
        }

        /// <remarks/>
        public string QuantityOnSalesOrder
        {
            get
            {
                return quantityOnSalesOrderField;
            }
            set
            {
                quantityOnSalesOrderField = value;
            }
        }

        /// <remarks/>
        public string ExternalGUID
        {
            get
            {
                return externalGUIDField;
            }
            set
            {
                externalGUIDField = value;
            }
        }

        /// <remarks/>
        public ItemInventoryAssemblyRetItemInventoryAssemblyLine ItemInventoryAssemblyLine
        {
            get
            {
                return itemInventoryAssemblyLineField;
            }
            set
            {
                itemInventoryAssemblyLineField = value;
            }
        }

        /// <remarks/>
        public ItemInventoryAssemblyRetDataExtRet DataExtRet
        {
            get
            {
                return dataExtRetField;
            }
            set
            {
                dataExtRetField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemInventoryAssemblyRetClassRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemInventoryAssemblyRetParentRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemInventoryAssemblyRetUnitOfMeasureSetRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemInventoryAssemblyRetSalesTaxCodeRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemInventoryAssemblyRetIncomeAccountRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemInventoryAssemblyRetCOGSAccountRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemInventoryAssemblyRetPrefVendorRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemInventoryAssemblyRetAssetAccountRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemInventoryAssemblyRetItemInventoryAssemblyLine
    {

        private ItemInventoryAssemblyRetItemInventoryAssemblyLineItemInventoryRef itemInventoryRefField;

        private string quantityField;

        /// <remarks/>
        public ItemInventoryAssemblyRetItemInventoryAssemblyLineItemInventoryRef ItemInventoryRef
        {
            get
            {
                return itemInventoryRefField;
            }
            set
            {
                itemInventoryRefField = value;
            }
        }

        /// <remarks/>
        public string Quantity
        {
            get
            {
                return quantityField;
            }
            set
            {
                quantityField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemInventoryAssemblyRetItemInventoryAssemblyLineItemInventoryRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemInventoryAssemblyRetDataExtRet
    {

        private string ownerIDField;

        private string dataExtNameField;

        private string dataExtTypeField;

        private string dataExtValueField;

        /// <remarks/>
        public string OwnerID
        {
            get
            {
                return ownerIDField;
            }
            set
            {
                ownerIDField = value;
            }
        }

        /// <remarks/>
        public string DataExtName
        {
            get
            {
                return dataExtNameField;
            }
            set
            {
                dataExtNameField = value;
            }
        }

        /// <remarks/>
        public string DataExtType
        {
            get
            {
                return dataExtTypeField;
            }
            set
            {
                dataExtTypeField = value;
            }
        }

        /// <remarks/>
        public string DataExtValue
        {
            get
            {
                return dataExtValueField;
            }
            set
            {
                dataExtValueField = value;
            }
        }
    }

#endregion

    #region "ItemOtherChargeRet"
    /// <remarks/>
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "" , IsNullable = false)]
    public partial class ItemOtherChargeRet
    {

        private string listIDField;

        private DateTime timeCreatedField;

        private DateTime timeModifiedField;

        private string editSequenceField;

        private string nameField;

        private string fullNameField;

        private string barCodeValueField;

        private bool isActiveField;

        private ItemOtherChargeRetClassRef classRefField;

        private ItemOtherChargeRetParentRef parentRefField;

        private int? sublevelField;

        private ItemOtherChargeRetSalesTaxCodeRef salesTaxCodeRefField;

        private ItemOtherChargeRetSalesOrPurchase salesOrPurchaseField;

        private ItemOtherChargeRetSalesAndPurchase salesAndPurchaseField;

        private string specialItemTypeField;

        private string externalGUIDField;

        private ItemOtherChargeRetDataExtRet dataExtRetField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public DateTime TimeCreated
        {
            get
            {
                return timeCreatedField;
            }
            set
            {
                timeCreatedField = value;
            }
        }

        /// <remarks/>
        public DateTime TimeModified
        {
            get
            {
                return timeModifiedField;
            }
            set
            {
                timeModifiedField = value;
            }
        }

        /// <remarks/>
        public string EditSequence
        {
            get
            {
                return editSequenceField;
            }
            set
            {
                editSequenceField = value;
            }
        }

        /// <remarks/>
        public string Name
        {
            get
            {
                return nameField;
            }
            set
            {
                nameField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }

        /// <remarks/>
        public string BarCodeValue
        {
            get
            {
                return barCodeValueField;
            }
            set
            {
                barCodeValueField = value;
            }
        }

        /// <remarks/>
        public bool IsActive
        {
            get
            {
                return isActiveField;
            }
            set
            {
                isActiveField = value;
            }
        }

        /// <remarks/>
        public ItemOtherChargeRetClassRef ClassRef
        {
            get
            {
                return classRefField;
            }
            set
            {
                classRefField = value;
            }
        }

        /// <remarks/>
        public ItemOtherChargeRetParentRef ParentRef
        {
            get
            {
                return parentRefField;
            }
            set
            {
                parentRefField = value;
            }
        }

        /// <remarks/>
        public int? Sublevel
        {
            get
            {
                return sublevelField;
            }
            set
            {
                sublevelField = value;
            }
        }

        /// <remarks/>
        public ItemOtherChargeRetSalesTaxCodeRef SalesTaxCodeRef
        {
            get
            {
                return salesTaxCodeRefField;
            }
            set
            {
                salesTaxCodeRefField = value;
            }
        }

        /// <remarks/>
        public ItemOtherChargeRetSalesOrPurchase SalesOrPurchase
        {
            get
            {
                return salesOrPurchaseField;
            }
            set
            {
                salesOrPurchaseField = value;
            }
        }

        /// <remarks/>
        public ItemOtherChargeRetSalesAndPurchase SalesAndPurchase
        {
            get
            {
                return salesAndPurchaseField;
            }
            set
            {
                salesAndPurchaseField = value;
            }
        }

        /// <remarks/>
        public string SpecialItemType
        {
            get
            {
                return specialItemTypeField;
            }
            set
            {
                specialItemTypeField = value;
            }
        }

        /// <remarks/>
        public string ExternalGUID
        {
            get
            {
                return externalGUIDField;
            }
            set
            {
                externalGUIDField = value;
            }
        }

        /// <remarks/>
        public ItemOtherChargeRetDataExtRet DataExtRet
        {
            get
            {
                return dataExtRetField;
            }
            set
            {
                dataExtRetField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemOtherChargeRetClassRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemOtherChargeRetParentRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemOtherChargeRetSalesTaxCodeRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemOtherChargeRetSalesOrPurchase
    {

        private string descField;

        private float priceField;

        private string pricePercentField;

        private ItemOtherChargeRetSalesOrPurchaseAccountRef accountRefField;

        /// <remarks/>
        public string Desc
        {
            get
            {
                return descField;
            }
            set
            {
                descField = value;
            }
        }

        /// <remarks/>
        public float Price
        {
            get
            {
                return priceField;
            }
            set
            {
                priceField = value;
            }
        }

        /// <remarks/>
        public string PricePercent
        {
            get
            {
                return pricePercentField;
            }
            set
            {
                pricePercentField = value;
            }
        }

        /// <remarks/>
        public ItemOtherChargeRetSalesOrPurchaseAccountRef AccountRef
        {
            get
            {
                return accountRefField;
            }
            set
            {
                accountRefField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemOtherChargeRetSalesOrPurchaseAccountRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemOtherChargeRetSalesAndPurchase
    {

        private string salesDescField;

        private float salesPriceField;

        private ItemOtherChargeRetSalesAndPurchaseIncomeAccountRef incomeAccountRefField;

        private string purchaseDescField;

        private string purchaseCostField;

        private ItemOtherChargeRetSalesAndPurchaseExpenseAccountRef expenseAccountRefField;

        private ItemOtherChargeRetSalesAndPurchasePrefVendorRef prefVendorRefField;

        /// <remarks/>
        public string SalesDesc
        {
            get
            {
                return salesDescField;
            }
            set
            {
                salesDescField = value;
            }
        }

        /// <remarks/>
        public float SalesPrice
        {
            get
            {
                return salesPriceField;
            }
            set
            {
                salesPriceField = value;
            }
        }

        /// <remarks/>
        public ItemOtherChargeRetSalesAndPurchaseIncomeAccountRef IncomeAccountRef
        {
            get
            {
                return incomeAccountRefField;
            }
            set
            {
                incomeAccountRefField = value;
            }
        }

        /// <remarks/>
        public string PurchaseDesc
        {
            get
            {
                return purchaseDescField;
            }
            set
            {
                purchaseDescField = value;
            }
        }

        /// <remarks/>
        public string PurchaseCost
        {
            get
            {
                return purchaseCostField;
            }
            set
            {
                purchaseCostField = value;
            }
        }

        /// <remarks/>
        public ItemOtherChargeRetSalesAndPurchaseExpenseAccountRef ExpenseAccountRef
        {
            get
            {
                return expenseAccountRefField;
            }
            set
            {
                expenseAccountRefField = value;
            }
        }

        /// <remarks/>
        public ItemOtherChargeRetSalesAndPurchasePrefVendorRef PrefVendorRef
        {
            get
            {
                return prefVendorRefField;
            }
            set
            {
                prefVendorRefField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemOtherChargeRetSalesAndPurchaseIncomeAccountRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemOtherChargeRetSalesAndPurchaseExpenseAccountRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemOtherChargeRetSalesAndPurchasePrefVendorRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemOtherChargeRetDataExtRet
    {

        private string ownerIDField;

        private string dataExtNameField;

        private string dataExtTypeField;

        private string dataExtValueField;

        /// <remarks/>
        public string OwnerID
        {
            get
            {
                return ownerIDField;
            }
            set
            {
                ownerIDField = value;
            }
        }

        /// <remarks/>
        public string DataExtName
        {
            get
            {
                return dataExtNameField;
            }
            set
            {
                dataExtNameField = value;
            }
        }

        /// <remarks/>
        public string DataExtType
        {
            get
            {
                return dataExtTypeField;
            }
            set
            {
                dataExtTypeField = value;
            }
        }

        /// <remarks/>
        public string DataExtValue
        {
            get
            {
                return dataExtValueField;
            }
            set
            {
                dataExtValueField = value;
            }
        }
    }

#endregion

    #region "ItemPaymentRet"
    /// <remarks/>
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "" , IsNullable = false)]
    public partial class ItemPaymentRet
    {

        private string listIDField;

        private DateTime timeCreatedField;

        private DateTime timeModifiedField;

        private string editSequenceField;

        private string nameField;

        private string barCodeValueField;

        private bool isActiveField;

        private ItemPaymentRetClassRef classRefField;

        private string itemDescField;

        private ItemPaymentRetDepositToAccountRef depositToAccountRefField;

        private ItemPaymentRetPaymentMethodRef paymentMethodRefField;

        private string externalGUIDField;

        private ItemPaymentRetDataExtRet dataExtRetField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public DateTime TimeCreated
        {
            get
            {
                return timeCreatedField;
            }
            set
            {
                timeCreatedField = value;
            }
        }

        /// <remarks/>
        public DateTime TimeModified
        {
            get
            {
                return timeModifiedField;
            }
            set
            {
                timeModifiedField = value;
            }
        }

        /// <remarks/>
        public string EditSequence
        {
            get
            {
                return editSequenceField;
            }
            set
            {
                editSequenceField = value;
            }
        }

        /// <remarks/>
        public string Name
        {
            get
            {
                return nameField;
            }
            set
            {
                nameField = value;
            }
        }

        /// <remarks/>
        public string BarCodeValue
        {
            get
            {
                return barCodeValueField;
            }
            set
            {
                barCodeValueField = value;
            }
        }

        /// <remarks/>
        public bool IsActive
        {
            get
            {
                return isActiveField;
            }
            set
            {
                isActiveField = value;
            }
        }

        /// <remarks/>
        public ItemPaymentRetClassRef ClassRef
        {
            get
            {
                return classRefField;
            }
            set
            {
                classRefField = value;
            }
        }

        /// <remarks/>
        public string ItemDesc
        {
            get
            {
                return itemDescField;
            }
            set
            {
                itemDescField = value;
            }
        }

        /// <remarks/>
        public ItemPaymentRetDepositToAccountRef DepositToAccountRef
        {
            get
            {
                return depositToAccountRefField;
            }
            set
            {
                depositToAccountRefField = value;
            }
        }

        /// <remarks/>
        public ItemPaymentRetPaymentMethodRef PaymentMethodRef
        {
            get
            {
                return paymentMethodRefField;
            }
            set
            {
                paymentMethodRefField = value;
            }
        }

        /// <remarks/>
        public string ExternalGUID
        {
            get
            {
                return externalGUIDField;
            }
            set
            {
                externalGUIDField = value;
            }
        }

        /// <remarks/>
        public ItemPaymentRetDataExtRet DataExtRet
        {
            get
            {
                return dataExtRetField;
            }
            set
            {
                dataExtRetField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemPaymentRetClassRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemPaymentRetDepositToAccountRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemPaymentRetPaymentMethodRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ItemPaymentRetDataExtRet
    {

        private string ownerIDField;

        private string dataExtNameField;

        private string dataExtTypeField;

        private string dataExtValueField;

        /// <remarks/>
        public string OwnerID
        {
            get
            {
                return ownerIDField;
            }
            set
            {
                ownerIDField = value;
            }
        }

        /// <remarks/>
        public string DataExtName
        {
            get
            {
                return dataExtNameField;
            }
            set
            {
                dataExtNameField = value;
            }
        }

        /// <remarks/>
        public string DataExtType
        {
            get
            {
                return dataExtTypeField;
            }
            set
            {
                dataExtTypeField = value;
            }
        }

        /// <remarks/>
        public string DataExtValue
        {
            get
            {
                return dataExtValueField;
            }
            set
            {
                dataExtValueField = value;
            }
        }
    }

#endregion
}
