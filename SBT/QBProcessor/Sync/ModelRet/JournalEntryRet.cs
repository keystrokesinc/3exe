﻿using DataProcessor.Ref;
using DataProcessor.Sync.ModelRet;
using System;
using System.Xml.Serialization;

namespace DataProcessor.Sync.ModelAdd
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class JournalEntryRet
    {
        public string TxnDate { get; set; }
        public string TxnID { get; set; }
        public string TxnNumber { get; set; }
        public string RefNumber { get; set; }
        public string IsAdjustment { get; set; }
        public string IsHomeCurrencyAdjustment { get; set; }
        public string IsAmountsEnteredInHomeCurrency { get; set; }
        public CurrencyRef CurrencyRef { get; set; }
        public string ExchangeRate { get; set; }
        public string ExternalGUID { get; set; }
        public DateTime? TimeCreated { get; set; }
        public DateTime? TimeModified { get; set; }
        public string EditSequence { get; set; }
        public JournalDebitLine JournalDebitLine { get; set; }
        public  JournalCreditLine JournalCreditLine { get; set; }
    }
    
    

   

}