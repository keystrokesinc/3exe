﻿using DataProcessor.Sync.ModelAdd;
using DataProcessor.Sync.ModelRet;
using System;
using System.Xml.Serialization;

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
public class BillPaymentCheckRet
{
    private string txnIDField;
    public string TxnID
    {
        get
        {
            return this.txnIDField;
        }
        set
        {
            this.txnIDField = value;
        }
    }

    [XmlElement(ElementName = "TimeCreated")]
    public DateTime TimeCreated { get; set; }
    [XmlElement(ElementName = "TimeModified")]
    public DateTime TimeModified { get; set; }
    [XmlElement(ElementName = "EditSequence")]
    public string EditSequence { get; set; }
    [XmlElement(ElementName = "TxnNumber")]
    public string TxnNumber { get; set; }
    [XmlElement(ElementName = "PayeeEntityRef")]
    public PayeeEntityRef PayeeEntityRef { get; set; }
    [XmlElement(ElementName = "APAccountRef")]
    public DataProcessor.Sync.ModelAdd.APAccountRef APAccountRef { get; set; }
    [XmlElement(ElementName = "TxnDate")]
    public DateTime TxnDate { get; set; }
    [XmlElement(ElementName = "BankAccountRef")]
    public BankAccountRef BankAccountRef { get; set; }
    [XmlElement(ElementName = "Amount")]
    public string Amount { get; set; }
    [XmlElement(ElementName = "CurrencyRef")]
    public CurrencyRef CurrencyRef { get; set; }
    [XmlElement(ElementName = "ExchangeRate")]
    public string ExchangeRate { get; set; }
    [XmlElement(ElementName = "AmountInHomeCurrency")]
    public string AmountInHomeCurrency { get; set; }
    [XmlElement(ElementName = "RefNumber")]
    public string RefNumber { get; set; }
    [XmlElement(ElementName = "Memo")]
    public string Memo { get; set; }
    [XmlElement(ElementName = "Address")]
    public DataProcessor.Sync.ModelAdd.Address Address { get; set; }
    [XmlElement(ElementName = "AddressBlock")]
    public AddressBlock AddressBlock { get; set; }
    [XmlElement(ElementName = "IsToBePrinted")]
    public string IsToBePrinted { get; set; }
    [XmlElement(ElementName = "ExternalGUID")]
    public string ExternalGUID { get; set; }
    [XmlElement(ElementName = "AppliedToTxnRet")]
    public AppliedToTxnRet[] AppliedToTxnRet { get; set; }
    //   [XmlElement(ElementName = "DataExtRet")]
    //  public DataExtRet DataExtRet { get; set; }


}
public class AppliedToTxnRet
{
    [XmlElement(ElementName = "TxnID")]
    public string TxnID { get; set; }
    [XmlElement(ElementName = "TxnType")]
    public string TxnType { get; set; }
    [XmlElement(ElementName = "TxnDate")]
    public DateTime TxnDate { get; set; }
    [XmlElement(ElementName = "RefNumber")]
    public string RefNumber { get; set; }
    [XmlElement(ElementName = "BalanceRemaining")]
    public string BalanceRemaining { get; set; }
    [XmlElement(ElementName = "Amount")]
    public string Amount { get; set; }
    [XmlElement(ElementName = "DiscountAmount")]
    public string DiscountAmount { get; set; }
    //   [XmlElement(ElementName = "DiscountAccountRef")]
    //  public DiscountAccountRef DiscountAccountRef { get; set; }
    //  [XmlElement(ElementName = "DiscountClassRef")]
    //  public DiscountClassRef DiscountClassRef { get; set; }
    [XmlElement(ElementName = "LinkedTxn")]
    public LinkedTxn LinkedTxn { get; set; }
}

public class PayeeEntityRef
{
    [XmlElement(ElementName = "ListID")]
    public string ListID { get; set; }
    [XmlElement(ElementName = "FullName")]
    public string FullName { get; set; }
}

public class CurrencyRef
{
    [XmlElement(ElementName = "ListID")]
    public string ListID { get; set; }
    [XmlElement(ElementName = "FullName")]
    public string FullName { get; set; }
}