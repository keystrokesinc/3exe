﻿using DataProcessor.Ref;
using System;
using System.Xml.Serialization;

namespace DataProcessor.Sync.ModelRet
{

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "" , IsNullable = false)]
    public partial class ReportRet
    {
        public string ReportTitle { get; set; }
        public string ReportSubtitle { get; set; }
        public string ReportBasis { get; set; }
        public int NumRows { get; set; }
        public int NumColumns { get; set; }
        public int NumColTitleRows { get; set; }

       // public  ColDesc[] ColDesc { get; set; }
        private ReportData[] reportData { get; set; }

        [XmlElement("ReportData")]
        public ReportData[] ReportData
        {
            get
            {
                return reportData;
            }
            set
            {
                reportData = value;
            }
        }
       


    }
    [XmlRoot(ElementName = "ReportData")]
    public partial class ReportData
    {
        private DataRow[] dataRow { get; set; }

        public string TextRow { get; set; }

        public SubtotalRow[] subtotalRow { get; set; }
        public TotalRow[] totalRow { get; set; }

        [XmlElement("DataRow")]
        public DataRow[] DataRow
        {
            get
            {
                return dataRow;
            }
            set
            {
                dataRow = value;
            }
        }
        [XmlElement("SubtotalRow")]
        public SubtotalRow[] SubtotalRow
        {
            get
            {
                return subtotalRow;
            }
            set
            {
                subtotalRow = value;
            }
        }
        [XmlElement("TotalRow")]
        public TotalRow[] TotalRow
        {
            get
            {
                return totalRow;
            }
            set
            {
                totalRow = value;
            }
        }


    }
    [XmlRoot(ElementName = "DataRow")]
    public partial class DataRow
    {
        private RowData rowData { get; set; }

        private ColData[] colData { get; set; }

        [XmlElement("RowData")]
        public RowData RowData
        {
            get
            {
                return rowData;
            }
            set
            {
                rowData = value;
            }
        }
        [XmlElement("ColData")]
        public ColData[] ColData
        {
            get
            {
                return colData;
            }
            set
            {
                colData = value;
            }
        }

    }

    [XmlRoot(ElementName = "SubtotalRow")]
    public partial class SubtotalRow
    {
        private RowData rowData { get; set; }

        private ColData[] colData { get; set; }

        [XmlElement("RowData")]
        public RowData RowData
        {
            get
            {
                return rowData;
            }
            set
            {
                rowData = value;
            }
        }
        [XmlElement("ColData")]
        public ColData[] ColData
        {
            get
            {
                return colData;
            }
            set
            {
                colData = value;
            }
        }

    }
    [XmlRoot(ElementName = "TotalRow")]
    public partial class TotalRow
    {
        private RowData rowData { get; set; }

        private ColData[] colData { get; set; }

        [XmlElement("RowData")]
        public RowData RowData
        {
            get
            {
                return rowData;
            }
            set
            {
                rowData = value;
            }
        }
        [XmlElement("ColData")]
        public ColData[] ColData
        {
            get
            {
                return colData;
            }
            set
            {
                colData = value;
            }
        }

    }
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class RowData
    {
        private string rowTypeField { get; set; }

        private string valueField { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string rowType
        {
            get
            {
                return this.rowTypeField;
            }
            set
            {
                this.rowTypeField = value;
            }
        }
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }

    }
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ColData
    {
        private int colIDField { get; set; }
        private string valueField { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int colID
        {
            get
            {
                return this.colIDField;
            }
            set
            {
                this.colIDField = value;
            }
        }
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }


    }


}
