﻿
using System;
using System.Xml.Serialization;

/// <remarks/>
[XmlType(AnonymousType = true)]
[XmlRoot(Namespace = "" , IsNullable = false)]
public partial class CreditMemoRet
{

    private string txnIDField;

    private string timeCreatedField;

    private string timeModifiedField;

    private string editSequenceField;

    private string txnNumberField;

    private CreditMemoRetCustomerRef customerRefField;

    private CreditMemoRetClassRef classRefField;

    private CreditMemoRetARAccountRef aRAccountRefField;

    private CreditMemoRetTemplateRef templateRefField;

    private DateTime txnDateField;

    private string refNumberField;

    private CreditMemoRetBillAddress billAddressField;

    private CreditMemoRetBillAddressBlock billAddressBlockField;

    private CreditMemoRetShipAddress shipAddressField;

    private CreditMemoRetShipAddressBlock shipAddressBlockField;

    private string isPendingField;

    private string pONumberField;

    private CreditMemoRetTermsRef termsRefField;

    private string dueDateField;

    private CreditMemoRetSalesRepRef salesRepRefField;

    private string fOBField;

    private string shipDateField;

    private CreditMemoRetShipMethodRef shipMethodRefField;

    private string subtotalField;

    private CreditMemoRetItemSalesTaxRef itemSalesTaxRefField;

    private string salesTaxPercentageField;

    private string salesTaxTotalField;

    private string totalAmountField;

    private string creditRemainingField;

    private CreditMemoRetCurrencyRef currencyRefField;

    private string exchangeRateField;

    private string creditRemainingInHomeCurrencyField;

    private string memoField;

    private CreditMemoRetCustomerMsgRef customerMsgRefField;

    private string isToBePrintedField;

    private string isToBeEmailedField;

    private CreditMemoRetCustomerSalesTaxCodeRef customerSalesTaxCodeRefField;

    private string otherField;

    private string externalGUIDField;

    private CreditMemoRetLinkedTxn linkedTxnField;

    private CreditMemoRetCreditMemoLineRet[] creditMemoLineRetField;

    private CreditMemoRetCreditMemoLineGroupRet creditMemoLineGroupRetField;

    private CreditMemoRetDataExtRet dataExtRetField;

    /// <remarks/>
    public string TxnID
    {
        get
        {
            return this.txnIDField;
        }
        set
        {
            this.txnIDField = value;
        }
    }

    /// <remarks/>
    public string TimeCreated
    {
        get
        {
            return this.timeCreatedField;
        }
        set
        {
            this.timeCreatedField = value;
        }
    }

    /// <remarks/>
    public string TimeModified
    {
        get
        {
            return this.timeModifiedField;
        }
        set
        {
            this.timeModifiedField = value;
        }
    }

    /// <remarks/>
    public string EditSequence
    {
        get
        {
            return this.editSequenceField;
        }
        set
        {
            this.editSequenceField = value;
        }
    }

    /// <remarks/>
    public string TxnNumber
    {
        get
        {
            return this.txnNumberField;
        }
        set
        {
            this.txnNumberField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetCustomerRef CustomerRef
    {
        get
        {
            return this.customerRefField;
        }
        set
        {
            this.customerRefField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetClassRef ClassRef
    {
        get
        {
            return this.classRefField;
        }
        set
        {
            this.classRefField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetARAccountRef ARAccountRef
    {
        get
        {
            return this.aRAccountRefField;
        }
        set
        {
            this.aRAccountRefField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetTemplateRef TemplateRef
    {
        get
        {
            return this.templateRefField;
        }
        set
        {
            this.templateRefField = value;
        }
    }

    /// <remarks/>
    public DateTime TxnDate
    {
        get
        {
            return this.txnDateField;
        }
        set
        {
            this.txnDateField = value;
        }
    }

    /// <remarks/>
    public string RefNumber
    {
        get
        {
            return this.refNumberField;
        }
        set
        {
            this.refNumberField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetBillAddress BillAddress
    {
        get
        {
            return this.billAddressField;
        }
        set
        {
            this.billAddressField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetBillAddressBlock BillAddressBlock
    {
        get
        {
            return this.billAddressBlockField;
        }
        set
        {
            this.billAddressBlockField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetShipAddress ShipAddress
    {
        get
        {
            return this.shipAddressField;
        }
        set
        {
            this.shipAddressField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetShipAddressBlock ShipAddressBlock
    {
        get
        {
            return this.shipAddressBlockField;
        }
        set
        {
            this.shipAddressBlockField = value;
        }
    }

    /// <remarks/>
    public string IsPending
    {
        get
        {
            return this.isPendingField;
        }
        set
        {
            this.isPendingField = value;
        }
    }

    /// <remarks/>
    public string PONumber
    {
        get
        {
            return this.pONumberField;
        }
        set
        {
            this.pONumberField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetTermsRef TermsRef
    {
        get
        {
            return this.termsRefField;
        }
        set
        {
            this.termsRefField = value;
        }
    }

    /// <remarks/>
    public string DueDate
    {
        get
        {
            return this.dueDateField;
        }
        set
        {
            this.dueDateField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetSalesRepRef SalesRepRef
    {
        get
        {
            return this.salesRepRefField;
        }
        set
        {
            this.salesRepRefField = value;
        }
    }

    /// <remarks/>
    public string FOB
    {
        get
        {
            return this.fOBField;
        }
        set
        {
            this.fOBField = value;
        }
    }

    /// <remarks/>
    public string ShipDate
    {
        get
        {
            return this.shipDateField;
        }
        set
        {
            this.shipDateField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetShipMethodRef ShipMethodRef
    {
        get
        {
            return this.shipMethodRefField;
        }
        set
        {
            this.shipMethodRefField = value;
        }
    }

    /// <remarks/>
    public string Subtotal
    {
        get
        {
            return this.subtotalField;
        }
        set
        {
            this.subtotalField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetItemSalesTaxRef ItemSalesTaxRef
    {
        get
        {
            return this.itemSalesTaxRefField;
        }
        set
        {
            this.itemSalesTaxRefField = value;
        }
    }

    /// <remarks/>
    public string SalesTaxPercentage
    {
        get
        {
            return this.salesTaxPercentageField;
        }
        set
        {
            this.salesTaxPercentageField = value;
        }
    }

    /// <remarks/>
    public string SalesTaxTotal
    {
        get
        {
            return this.salesTaxTotalField;
        }
        set
        {
            this.salesTaxTotalField = value;
        }
    }

    /// <remarks/>
    public string TotalAmount
    {
        get
        {
            return this.totalAmountField;
        }
        set
        {
            this.totalAmountField = value;
        }
    }

    /// <remarks/>
    public string CreditRemaining
    {
        get
        {
            return this.creditRemainingField;
        }
        set
        {
            this.creditRemainingField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetCurrencyRef CurrencyRef
    {
        get
        {
            return this.currencyRefField;
        }
        set
        {
            this.currencyRefField = value;
        }
    }

    /// <remarks/>
    public string ExchangeRate
    {
        get
        {
            return this.exchangeRateField;
        }
        set
        {
            this.exchangeRateField = value;
        }
    }

    /// <remarks/>
    public string CreditRemainingInHomeCurrency
    {
        get
        {
            return this.creditRemainingInHomeCurrencyField;
        }
        set
        {
            this.creditRemainingInHomeCurrencyField = value;
        }
    }

    /// <remarks/>
    public string Memo
    {
        get
        {
            return this.memoField;
        }
        set
        {
            this.memoField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetCustomerMsgRef CustomerMsgRef
    {
        get
        {
            return this.customerMsgRefField;
        }
        set
        {
            this.customerMsgRefField = value;
        }
    }

    /// <remarks/>
    public string IsToBePrinted
    {
        get
        {
            return this.isToBePrintedField;
        }
        set
        {
            this.isToBePrintedField = value;
        }
    }

    /// <remarks/>
    public string IsToBeEmailed
    {
        get
        {
            return this.isToBeEmailedField;
        }
        set
        {
            this.isToBeEmailedField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetCustomerSalesTaxCodeRef CustomerSalesTaxCodeRef
    {
        get
        {
            return this.customerSalesTaxCodeRefField;
        }
        set
        {
            this.customerSalesTaxCodeRefField = value;
        }
    }

    /// <remarks/>
    public string Other
    {
        get
        {
            return this.otherField;
        }
        set
        {
            this.otherField = value;
        }
    }

    /// <remarks/>
    public string ExternalGUID
    {
        get
        {
            return this.externalGUIDField;
        }
        set
        {
            this.externalGUIDField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetLinkedTxn LinkedTxn
    {
        get
        {
            return this.linkedTxnField;
        }
        set
        {
            this.linkedTxnField = value;
        }
    }

     [XmlElement("CreditMemoLineRet")]
    public CreditMemoRetCreditMemoLineRet[] CreditMemoLineRet
    {
        get
        {
            return this.creditMemoLineRetField;
        }
        set
        {
            this.creditMemoLineRetField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetCreditMemoLineGroupRet CreditMemoLineGroupRet
    {
        get
        {
            return this.creditMemoLineGroupRetField;
        }
        set
        {
            this.creditMemoLineGroupRetField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetDataExtRet DataExtRet
    {
        get
        {
            return this.dataExtRetField;
        }
        set
        {
            this.dataExtRetField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCustomerRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetClassRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetARAccountRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetTemplateRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetBillAddress
{

    private string addr1Field;

    private string addr2Field;

    private string addr3Field;

    private string addr4Field;

    private string addr5Field;

    private string cityField;

    private string stateField;

    private string postalCodeField;

    private string countryField;

    private string noteField;

    /// <remarks/>
    public string Addr1
    {
        get
        {
            return this.addr1Field;
        }
        set
        {
            this.addr1Field = value;
        }
    }

    /// <remarks/>
    public string Addr2
    {
        get
        {
            return this.addr2Field;
        }
        set
        {
            this.addr2Field = value;
        }
    }

    /// <remarks/>
    public string Addr3
    {
        get
        {
            return this.addr3Field;
        }
        set
        {
            this.addr3Field = value;
        }
    }

    /// <remarks/>
    public string Addr4
    {
        get
        {
            return this.addr4Field;
        }
        set
        {
            this.addr4Field = value;
        }
    }

    /// <remarks/>
    public string Addr5
    {
        get
        {
            return this.addr5Field;
        }
        set
        {
            this.addr5Field = value;
        }
    }

    /// <remarks/>
    public string City
    {
        get
        {
            return this.cityField;
        }
        set
        {
            this.cityField = value;
        }
    }

    /// <remarks/>
    public string State
    {
        get
        {
            return this.stateField;
        }
        set
        {
            this.stateField = value;
        }
    }

    /// <remarks/>
    public string PostalCode
    {
        get
        {
            return this.postalCodeField;
        }
        set
        {
            this.postalCodeField = value;
        }
    }

    /// <remarks/>
    public string Country
    {
        get
        {
            return this.countryField;
        }
        set
        {
            this.countryField = value;
        }
    }

    /// <remarks/>
    public string Note
    {
        get
        {
            return this.noteField;
        }
        set
        {
            this.noteField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetBillAddressBlock
{

    private string addr1Field;

    private string addr2Field;

    private string addr3Field;

    private string addr4Field;

    private string addr5Field;

    /// <remarks/>
    public string Addr1
    {
        get
        {
            return this.addr1Field;
        }
        set
        {
            this.addr1Field = value;
        }
    }

    /// <remarks/>
    public string Addr2
    {
        get
        {
            return this.addr2Field;
        }
        set
        {
            this.addr2Field = value;
        }
    }

    /// <remarks/>
    public string Addr3
    {
        get
        {
            return this.addr3Field;
        }
        set
        {
            this.addr3Field = value;
        }
    }

    /// <remarks/>
    public string Addr4
    {
        get
        {
            return this.addr4Field;
        }
        set
        {
            this.addr4Field = value;
        }
    }

    /// <remarks/>
    public string Addr5
    {
        get
        {
            return this.addr5Field;
        }
        set
        {
            this.addr5Field = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetShipAddress
{

    private string addr1Field;

    private string addr2Field;

    private string addr3Field;

    private string addr4Field;

    private string addr5Field;

    private string cityField;

    private string stateField;

    private string postalCodeField;

    private string countryField;

    private string noteField;

    /// <remarks/>
    public string Addr1
    {
        get
        {
            return this.addr1Field;
        }
        set
        {
            this.addr1Field = value;
        }
    }

    /// <remarks/>
    public string Addr2
    {
        get
        {
            return this.addr2Field;
        }
        set
        {
            this.addr2Field = value;
        }
    }

    /// <remarks/>
    public string Addr3
    {
        get
        {
            return this.addr3Field;
        }
        set
        {
            this.addr3Field = value;
        }
    }

    /// <remarks/>
    public string Addr4
    {
        get
        {
            return this.addr4Field;
        }
        set
        {
            this.addr4Field = value;
        }
    }

    /// <remarks/>
    public string Addr5
    {
        get
        {
            return this.addr5Field;
        }
        set
        {
            this.addr5Field = value;
        }
    }

    /// <remarks/>
    public string City
    {
        get
        {
            return this.cityField;
        }
        set
        {
            this.cityField = value;
        }
    }

    /// <remarks/>
    public string State
    {
        get
        {
            return this.stateField;
        }
        set
        {
            this.stateField = value;
        }
    }

    /// <remarks/>
    public string PostalCode
    {
        get
        {
            return this.postalCodeField;
        }
        set
        {
            this.postalCodeField = value;
        }
    }

    /// <remarks/>
    public string Country
    {
        get
        {
            return this.countryField;
        }
        set
        {
            this.countryField = value;
        }
    }

    /// <remarks/>
    public string Note
    {
        get
        {
            return this.noteField;
        }
        set
        {
            this.noteField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetShipAddressBlock
{

    private string addr1Field;

    private string addr2Field;

    private string addr3Field;

    private string addr4Field;

    private string addr5Field;

    /// <remarks/>
    public string Addr1
    {
        get
        {
            return this.addr1Field;
        }
        set
        {
            this.addr1Field = value;
        }
    }

    /// <remarks/>
    public string Addr2
    {
        get
        {
            return this.addr2Field;
        }
        set
        {
            this.addr2Field = value;
        }
    }

    /// <remarks/>
    public string Addr3
    {
        get
        {
            return this.addr3Field;
        }
        set
        {
            this.addr3Field = value;
        }
    }

    /// <remarks/>
    public string Addr4
    {
        get
        {
            return this.addr4Field;
        }
        set
        {
            this.addr4Field = value;
        }
    }

    /// <remarks/>
    public string Addr5
    {
        get
        {
            return this.addr5Field;
        }
        set
        {
            this.addr5Field = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetTermsRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetSalesRepRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetShipMethodRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetItemSalesTaxRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCurrencyRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCustomerMsgRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCustomerSalesTaxCodeRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetLinkedTxn
{

    private string txnIDField;

    private string txnTypeField;

    private string txnDateField;

    private string refNumberField;

    private string linkTypeField;

    private string amountField;

    /// <remarks/>
    public string TxnID
    {
        get
        {
            return this.txnIDField;
        }
        set
        {
            this.txnIDField = value;
        }
    }

    /// <remarks/>
    public string TxnType
    {
        get
        {
            return this.txnTypeField;
        }
        set
        {
            this.txnTypeField = value;
        }
    }

    /// <remarks/>
    public string TxnDate
    {
        get
        {
            return this.txnDateField;
        }
        set
        {
            this.txnDateField = value;
        }
    }

    /// <remarks/>
    public string RefNumber
    {
        get
        {
            return this.refNumberField;
        }
        set
        {
            this.refNumberField = value;
        }
    }

    /// <remarks/>
    public string LinkType
    {
        get
        {
            return this.linkTypeField;
        }
        set
        {
            this.linkTypeField = value;
        }
    }

    /// <remarks/>
    public string Amount
    {
        get
        {
            return this.amountField;
        }
        set
        {
            this.amountField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCreditMemoLineRet
{

    private string txnLineIDField;

    private CreditMemoRetCreditMemoLineRetItemRef itemRefField;

    private string descField;

    private string quantityField;

    private string unitOfMeasureField;

    private CreditMemoRetCreditMemoLineRetOverrideUOMSetRef overrideUOMSetRefField;

    private string rateField;

    private string ratePercentField;

    private CreditMemoRetCreditMemoLineRetClassRef classRefField;

    private string amountField;

    private CreditMemoRetCreditMemoLineRetInventorySiteRef inventorySiteRefField;

    private CreditMemoRetCreditMemoLineRetInventorySiteLocationRef inventorySiteLocationRefField;

    private string serialNumberField;

    private string lotNumberField;

    private string serviceDateField;

    private CreditMemoRetCreditMemoLineRetSalesTaxCodeRef salesTaxCodeRefField;

    private string other1Field;

    private string other2Field;

    private CreditMemoRetCreditMemoLineRetCreditCardTxnInfo creditCardTxnInfoField;

    private CreditMemoRetCreditMemoLineRetDataExtRet dataExtRetField;

    /// <remarks/>
    public string TxnLineID
    {
        get
        {
            return this.txnLineIDField;
        }
        set
        {
            this.txnLineIDField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetCreditMemoLineRetItemRef ItemRef
    {
        get
        {
            return this.itemRefField;
        }
        set
        {
            this.itemRefField = value;
        }
    }

    /// <remarks/>
    public string Desc
    {
        get
        {
            return this.descField;
        }
        set
        {
            this.descField = value;
        }
    }

    /// <remarks/>
    public string Quantity
    {
        get
        {
            return this.quantityField;
        }
        set
        {
            this.quantityField = value;
        }
    }

    /// <remarks/>
    public string UnitOfMeasure
    {
        get
        {
            return this.unitOfMeasureField;
        }
        set
        {
            this.unitOfMeasureField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetCreditMemoLineRetOverrideUOMSetRef OverrideUOMSetRef
    {
        get
        {
            return this.overrideUOMSetRefField;
        }
        set
        {
            this.overrideUOMSetRefField = value;
        }
    }

    /// <remarks/>
    public string Rate
    {
        get
        {
            return this.rateField;
        }
        set
        {
            this.rateField = value;
        }
    }

    /// <remarks/>
    public string RatePercent
    {
        get
        {
            return this.ratePercentField;
        }
        set
        {
            this.ratePercentField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetCreditMemoLineRetClassRef ClassRef
    {
        get
        {
            return this.classRefField;
        }
        set
        {
            this.classRefField = value;
        }
    }

    /// <remarks/>
    public string Amount
    {
        get
        {
            return this.amountField;
        }
        set
        {
            this.amountField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetCreditMemoLineRetInventorySiteRef InventorySiteRef
    {
        get
        {
            return this.inventorySiteRefField;
        }
        set
        {
            this.inventorySiteRefField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetCreditMemoLineRetInventorySiteLocationRef InventorySiteLocationRef
    {
        get
        {
            return this.inventorySiteLocationRefField;
        }
        set
        {
            this.inventorySiteLocationRefField = value;
        }
    }

    /// <remarks/>
    public string SerialNumber
    {
        get
        {
            return this.serialNumberField;
        }
        set
        {
            this.serialNumberField = value;
        }
    }

    /// <remarks/>
    public string LotNumber
    {
        get
        {
            return this.lotNumberField;
        }
        set
        {
            this.lotNumberField = value;
        }
    }

    /// <remarks/>
    public string ServiceDate
    {
        get
        {
            return this.serviceDateField;
        }
        set
        {
            this.serviceDateField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetCreditMemoLineRetSalesTaxCodeRef SalesTaxCodeRef
    {
        get
        {
            return this.salesTaxCodeRefField;
        }
        set
        {
            this.salesTaxCodeRefField = value;
        }
    }

    /// <remarks/>
    public string Other1
    {
        get
        {
            return this.other1Field;
        }
        set
        {
            this.other1Field = value;
        }
    }

    /// <remarks/>
    public string Other2
    {
        get
        {
            return this.other2Field;
        }
        set
        {
            this.other2Field = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetCreditMemoLineRetCreditCardTxnInfo CreditCardTxnInfo
    {
        get
        {
            return this.creditCardTxnInfoField;
        }
        set
        {
            this.creditCardTxnInfoField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetCreditMemoLineRetDataExtRet DataExtRet
    {
        get
        {
            return this.dataExtRetField;
        }
        set
        {
            this.dataExtRetField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCreditMemoLineRetItemRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCreditMemoLineRetOverrideUOMSetRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCreditMemoLineRetClassRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCreditMemoLineRetInventorySiteRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCreditMemoLineRetInventorySiteLocationRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCreditMemoLineRetSalesTaxCodeRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCreditMemoLineRetCreditCardTxnInfo
{

    private CreditMemoRetCreditMemoLineRetCreditCardTxnInfoCreditCardTxnInputInfo creditCardTxnInputInfoField;

    private CreditMemoRetCreditMemoLineRetCreditCardTxnInfoCreditCardTxnResultInfo creditCardTxnResultInfoField;

    /// <remarks/>
    public CreditMemoRetCreditMemoLineRetCreditCardTxnInfoCreditCardTxnInputInfo CreditCardTxnInputInfo
    {
        get
        {
            return this.creditCardTxnInputInfoField;
        }
        set
        {
            this.creditCardTxnInputInfoField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetCreditMemoLineRetCreditCardTxnInfoCreditCardTxnResultInfo CreditCardTxnResultInfo
    {
        get
        {
            return this.creditCardTxnResultInfoField;
        }
        set
        {
            this.creditCardTxnResultInfoField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCreditMemoLineRetCreditCardTxnInfoCreditCardTxnInputInfo
{

    private string creditCardNumberField;

    private string expirationMonthField;

    private string expirationYearField;

    private string nameOnCardField;

    private string creditCardAddressField;

    private string creditCardPostalCodeField;

    private string commercialCardCodeField;

    private string transactionModeField;

    private string creditCardTxnTypeField;

    /// <remarks/>
    public string CreditCardNumber
    {
        get
        {
            return this.creditCardNumberField;
        }
        set
        {
            this.creditCardNumberField = value;
        }
    }

    /// <remarks/>
    public string ExpirationMonth
    {
        get
        {
            return this.expirationMonthField;
        }
        set
        {
            this.expirationMonthField = value;
        }
    }

    /// <remarks/>
    public string ExpirationYear
    {
        get
        {
            return this.expirationYearField;
        }
        set
        {
            this.expirationYearField = value;
        }
    }

    /// <remarks/>
    public string NameOnCard
    {
        get
        {
            return this.nameOnCardField;
        }
        set
        {
            this.nameOnCardField = value;
        }
    }

    /// <remarks/>
    public string CreditCardAddress
    {
        get
        {
            return this.creditCardAddressField;
        }
        set
        {
            this.creditCardAddressField = value;
        }
    }

    /// <remarks/>
    public string CreditCardPostalCode
    {
        get
        {
            return this.creditCardPostalCodeField;
        }
        set
        {
            this.creditCardPostalCodeField = value;
        }
    }

    /// <remarks/>
    public string CommercialCardCode
    {
        get
        {
            return this.commercialCardCodeField;
        }
        set
        {
            this.commercialCardCodeField = value;
        }
    }

    /// <remarks/>
    public string TransactionMode
    {
        get
        {
            return this.transactionModeField;
        }
        set
        {
            this.transactionModeField = value;
        }
    }

    /// <remarks/>
    public string CreditCardTxnType
    {
        get
        {
            return this.creditCardTxnTypeField;
        }
        set
        {
            this.creditCardTxnTypeField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCreditMemoLineRetCreditCardTxnInfoCreditCardTxnResultInfo
{

    private string resultCodeField;

    private string resultMessageField;

    private string creditCardTransIDField;

    private string merchantAccountNumberField;

    private string authorizationCodeField;

    private string aVSStreetField;

    private string aVSZipField;

    private string cardSecurityCodeMatchField;

    private string reconBatchIDField;

    private string paymentGroupingCodeField;

    private string paymentStatusField;

    private string txnAuthorizationTimeField;

    private string txnAuthorizationStampField;

    private string clientTransIDField;

    /// <remarks/>
    public string ResultCode
    {
        get
        {
            return this.resultCodeField;
        }
        set
        {
            this.resultCodeField = value;
        }
    }

    /// <remarks/>
    public string ResultMessage
    {
        get
        {
            return this.resultMessageField;
        }
        set
        {
            this.resultMessageField = value;
        }
    }

    /// <remarks/>
    public string CreditCardTransID
    {
        get
        {
            return this.creditCardTransIDField;
        }
        set
        {
            this.creditCardTransIDField = value;
        }
    }

    /// <remarks/>
    public string MerchantAccountNumber
    {
        get
        {
            return this.merchantAccountNumberField;
        }
        set
        {
            this.merchantAccountNumberField = value;
        }
    }

    /// <remarks/>
    public string AuthorizationCode
    {
        get
        {
            return this.authorizationCodeField;
        }
        set
        {
            this.authorizationCodeField = value;
        }
    }

    /// <remarks/>
    public string AVSStreet
    {
        get
        {
            return this.aVSStreetField;
        }
        set
        {
            this.aVSStreetField = value;
        }
    }

    /// <remarks/>
    public string AVSZip
    {
        get
        {
            return this.aVSZipField;
        }
        set
        {
            this.aVSZipField = value;
        }
    }

    /// <remarks/>
    public string CardSecurityCodeMatch
    {
        get
        {
            return this.cardSecurityCodeMatchField;
        }
        set
        {
            this.cardSecurityCodeMatchField = value;
        }
    }

    /// <remarks/>
    public string ReconBatchID
    {
        get
        {
            return this.reconBatchIDField;
        }
        set
        {
            this.reconBatchIDField = value;
        }
    }

    /// <remarks/>
    public string PaymentGroupingCode
    {
        get
        {
            return this.paymentGroupingCodeField;
        }
        set
        {
            this.paymentGroupingCodeField = value;
        }
    }

    /// <remarks/>
    public string PaymentStatus
    {
        get
        {
            return this.paymentStatusField;
        }
        set
        {
            this.paymentStatusField = value;
        }
    }

    /// <remarks/>
    public string TxnAuthorizationTime
    {
        get
        {
            return this.txnAuthorizationTimeField;
        }
        set
        {
            this.txnAuthorizationTimeField = value;
        }
    }

    /// <remarks/>
    public string TxnAuthorizationStamp
    {
        get
        {
            return this.txnAuthorizationStampField;
        }
        set
        {
            this.txnAuthorizationStampField = value;
        }
    }

    /// <remarks/>
    public string ClientTransID
    {
        get
        {
            return this.clientTransIDField;
        }
        set
        {
            this.clientTransIDField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCreditMemoLineRetDataExtRet
{

    private string ownerIDField;

    private string dataExtNameField;

    private string dataExtTypeField;

    private string dataExtValueField;

    /// <remarks/>
    public string OwnerID
    {
        get
        {
            return this.ownerIDField;
        }
        set
        {
            this.ownerIDField = value;
        }
    }

    /// <remarks/>
    public string DataExtName
    {
        get
        {
            return this.dataExtNameField;
        }
        set
        {
            this.dataExtNameField = value;
        }
    }

    /// <remarks/>
    public string DataExtType
    {
        get
        {
            return this.dataExtTypeField;
        }
        set
        {
            this.dataExtTypeField = value;
        }
    }

    /// <remarks/>
    public string DataExtValue
    {
        get
        {
            return this.dataExtValueField;
        }
        set
        {
            this.dataExtValueField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCreditMemoLineGroupRet
{

    private string txnLineIDField;

    private CreditMemoRetCreditMemoLineGroupRetItemGroupRef itemGroupRefField;

    private string descField;

    private string quantityField;

    private string unitOfMeasureField;

    private CreditMemoRetCreditMemoLineGroupRetOverrideUOMSetRef overrideUOMSetRefField;

    private string isPrintItemsInGroupField;

    private string totalAmountField;

    private CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRet creditMemoLineRetField;

    private CreditMemoRetCreditMemoLineGroupRetDataExtRet dataExtRetField;

    /// <remarks/>
    public string TxnLineID
    {
        get
        {
            return this.txnLineIDField;
        }
        set
        {
            this.txnLineIDField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetCreditMemoLineGroupRetItemGroupRef ItemGroupRef
    {
        get
        {
            return this.itemGroupRefField;
        }
        set
        {
            this.itemGroupRefField = value;
        }
    }

    /// <remarks/>
    public string Desc
    {
        get
        {
            return this.descField;
        }
        set
        {
            this.descField = value;
        }
    }

    /// <remarks/>
    public string Quantity
    {
        get
        {
            return this.quantityField;
        }
        set
        {
            this.quantityField = value;
        }
    }

    /// <remarks/>
    public string UnitOfMeasure
    {
        get
        {
            return this.unitOfMeasureField;
        }
        set
        {
            this.unitOfMeasureField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetCreditMemoLineGroupRetOverrideUOMSetRef OverrideUOMSetRef
    {
        get
        {
            return this.overrideUOMSetRefField;
        }
        set
        {
            this.overrideUOMSetRefField = value;
        }
    }

    /// <remarks/>
    public string IsPrintItemsInGroup
    {
        get
        {
            return this.isPrintItemsInGroupField;
        }
        set
        {
            this.isPrintItemsInGroupField = value;
        }
    }

    /// <remarks/>
    public string TotalAmount
    {
        get
        {
            return this.totalAmountField;
        }
        set
        {
            this.totalAmountField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRet CreditMemoLineRet
    {
        get
        {
            return this.creditMemoLineRetField;
        }
        set
        {
            this.creditMemoLineRetField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetCreditMemoLineGroupRetDataExtRet DataExtRet
    {
        get
        {
            return this.dataExtRetField;
        }
        set
        {
            this.dataExtRetField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCreditMemoLineGroupRetItemGroupRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCreditMemoLineGroupRetOverrideUOMSetRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRet
{

    private string txnLineIDField;

    private CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetItemRef itemRefField;

    private string descField;

    private string quantityField;

    private string unitOfMeasureField;

    private CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetOverrideUOMSetRef overrideUOMSetRefField;

    private string rateField;

    private string ratePercentField;

    private CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetClassRef classRefField;

    private string amountField;

    private CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetInventorySiteRef inventorySiteRefField;

    private CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetInventorySiteLocationRef inventorySiteLocationRefField;

    private string serialNumberField;

    private string lotNumberField;

    private string serviceDateField;

    private CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetSalesTaxCodeRef salesTaxCodeRefField;

    private string other1Field;

    private string other2Field;

    private CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetCreditCardTxnInfo creditCardTxnInfoField;

    private CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetDataExtRet dataExtRetField;

    /// <remarks/>
    public string TxnLineID
    {
        get
        {
            return this.txnLineIDField;
        }
        set
        {
            this.txnLineIDField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetItemRef ItemRef
    {
        get
        {
            return this.itemRefField;
        }
        set
        {
            this.itemRefField = value;
        }
    }

    /// <remarks/>
    public string Desc
    {
        get
        {
            return this.descField;
        }
        set
        {
            this.descField = value;
        }
    }

    /// <remarks/>
    public string Quantity
    {
        get
        {
            return this.quantityField;
        }
        set
        {
            this.quantityField = value;
        }
    }

    /// <remarks/>
    public string UnitOfMeasure
    {
        get
        {
            return this.unitOfMeasureField;
        }
        set
        {
            this.unitOfMeasureField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetOverrideUOMSetRef OverrideUOMSetRef
    {
        get
        {
            return this.overrideUOMSetRefField;
        }
        set
        {
            this.overrideUOMSetRefField = value;
        }
    }

    /// <remarks/>
    public string Rate
    {
        get
        {
            return this.rateField;
        }
        set
        {
            this.rateField = value;
        }
    }

    /// <remarks/>
    public string RatePercent
    {
        get
        {
            return this.ratePercentField;
        }
        set
        {
            this.ratePercentField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetClassRef ClassRef
    {
        get
        {
            return this.classRefField;
        }
        set
        {
            this.classRefField = value;
        }
    }

    /// <remarks/>
    public string Amount
    {
        get
        {
            return this.amountField;
        }
        set
        {
            this.amountField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetInventorySiteRef InventorySiteRef
    {
        get
        {
            return this.inventorySiteRefField;
        }
        set
        {
            this.inventorySiteRefField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetInventorySiteLocationRef InventorySiteLocationRef
    {
        get
        {
            return this.inventorySiteLocationRefField;
        }
        set
        {
            this.inventorySiteLocationRefField = value;
        }
    }

    /// <remarks/>
    public string SerialNumber
    {
        get
        {
            return this.serialNumberField;
        }
        set
        {
            this.serialNumberField = value;
        }
    }

    /// <remarks/>
    public string LotNumber
    {
        get
        {
            return this.lotNumberField;
        }
        set
        {
            this.lotNumberField = value;
        }
    }

    /// <remarks/>
    public string ServiceDate
    {
        get
        {
            return this.serviceDateField;
        }
        set
        {
            this.serviceDateField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetSalesTaxCodeRef SalesTaxCodeRef
    {
        get
        {
            return this.salesTaxCodeRefField;
        }
        set
        {
            this.salesTaxCodeRefField = value;
        }
    }

    /// <remarks/>
    public string Other1
    {
        get
        {
            return this.other1Field;
        }
        set
        {
            this.other1Field = value;
        }
    }

    /// <remarks/>
    public string Other2
    {
        get
        {
            return this.other2Field;
        }
        set
        {
            this.other2Field = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetCreditCardTxnInfo CreditCardTxnInfo
    {
        get
        {
            return this.creditCardTxnInfoField;
        }
        set
        {
            this.creditCardTxnInfoField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetDataExtRet DataExtRet
    {
        get
        {
            return this.dataExtRetField;
        }
        set
        {
            this.dataExtRetField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetItemRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetOverrideUOMSetRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetClassRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetInventorySiteRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetInventorySiteLocationRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetSalesTaxCodeRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetCreditCardTxnInfo
{

    private CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetCreditCardTxnInfoCreditCardTxnInputInfo creditCardTxnInputInfoField;

    private CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetCreditCardTxnInfoCreditCardTxnResultInfo creditCardTxnResultInfoField;

    /// <remarks/>
    public CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetCreditCardTxnInfoCreditCardTxnInputInfo CreditCardTxnInputInfo
    {
        get
        {
            return this.creditCardTxnInputInfoField;
        }
        set
        {
            this.creditCardTxnInputInfoField = value;
        }
    }

    /// <remarks/>
    public CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetCreditCardTxnInfoCreditCardTxnResultInfo CreditCardTxnResultInfo
    {
        get
        {
            return this.creditCardTxnResultInfoField;
        }
        set
        {
            this.creditCardTxnResultInfoField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetCreditCardTxnInfoCreditCardTxnInputInfo
{

    private string creditCardNumberField;

    private string expirationMonthField;

    private string expirationYearField;

    private string nameOnCardField;

    private string creditCardAddressField;

    private string creditCardPostalCodeField;

    private string commercialCardCodeField;

    private string transactionModeField;

    private string creditCardTxnTypeField;

    /// <remarks/>
    public string CreditCardNumber
    {
        get
        {
            return this.creditCardNumberField;
        }
        set
        {
            this.creditCardNumberField = value;
        }
    }

    /// <remarks/>
    public string ExpirationMonth
    {
        get
        {
            return this.expirationMonthField;
        }
        set
        {
            this.expirationMonthField = value;
        }
    }

    /// <remarks/>
    public string ExpirationYear
    {
        get
        {
            return this.expirationYearField;
        }
        set
        {
            this.expirationYearField = value;
        }
    }

    /// <remarks/>
    public string NameOnCard
    {
        get
        {
            return this.nameOnCardField;
        }
        set
        {
            this.nameOnCardField = value;
        }
    }

    /// <remarks/>
    public string CreditCardAddress
    {
        get
        {
            return this.creditCardAddressField;
        }
        set
        {
            this.creditCardAddressField = value;
        }
    }

    /// <remarks/>
    public string CreditCardPostalCode
    {
        get
        {
            return this.creditCardPostalCodeField;
        }
        set
        {
            this.creditCardPostalCodeField = value;
        }
    }

    /// <remarks/>
    public string CommercialCardCode
    {
        get
        {
            return this.commercialCardCodeField;
        }
        set
        {
            this.commercialCardCodeField = value;
        }
    }

    /// <remarks/>
    public string TransactionMode
    {
        get
        {
            return this.transactionModeField;
        }
        set
        {
            this.transactionModeField = value;
        }
    }

    /// <remarks/>
    public string CreditCardTxnType
    {
        get
        {
            return this.creditCardTxnTypeField;
        }
        set
        {
            this.creditCardTxnTypeField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetCreditCardTxnInfoCreditCardTxnResultInfo
{

    private string resultCodeField;

    private string resultMessageField;

    private string creditCardTransIDField;

    private string merchantAccountNumberField;

    private string authorizationCodeField;

    private string aVSStreetField;

    private string aVSZipField;

    private string cardSecurityCodeMatchField;

    private string reconBatchIDField;

    private string paymentGroupingCodeField;

    private string paymentStatusField;

    private string txnAuthorizationTimeField;

    private string txnAuthorizationStampField;

    private string clientTransIDField;

    /// <remarks/>
    public string ResultCode
    {
        get
        {
            return this.resultCodeField;
        }
        set
        {
            this.resultCodeField = value;
        }
    }

    /// <remarks/>
    public string ResultMessage
    {
        get
        {
            return this.resultMessageField;
        }
        set
        {
            this.resultMessageField = value;
        }
    }

    /// <remarks/>
    public string CreditCardTransID
    {
        get
        {
            return this.creditCardTransIDField;
        }
        set
        {
            this.creditCardTransIDField = value;
        }
    }

    /// <remarks/>
    public string MerchantAccountNumber
    {
        get
        {
            return this.merchantAccountNumberField;
        }
        set
        {
            this.merchantAccountNumberField = value;
        }
    }

    /// <remarks/>
    public string AuthorizationCode
    {
        get
        {
            return this.authorizationCodeField;
        }
        set
        {
            this.authorizationCodeField = value;
        }
    }

    /// <remarks/>
    public string AVSStreet
    {
        get
        {
            return this.aVSStreetField;
        }
        set
        {
            this.aVSStreetField = value;
        }
    }

    /// <remarks/>
    public string AVSZip
    {
        get
        {
            return this.aVSZipField;
        }
        set
        {
            this.aVSZipField = value;
        }
    }

    /// <remarks/>
    public string CardSecurityCodeMatch
    {
        get
        {
            return this.cardSecurityCodeMatchField;
        }
        set
        {
            this.cardSecurityCodeMatchField = value;
        }
    }

    /// <remarks/>
    public string ReconBatchID
    {
        get
        {
            return this.reconBatchIDField;
        }
        set
        {
            this.reconBatchIDField = value;
        }
    }

    /// <remarks/>
    public string PaymentGroupingCode
    {
        get
        {
            return this.paymentGroupingCodeField;
        }
        set
        {
            this.paymentGroupingCodeField = value;
        }
    }

    /// <remarks/>
    public string PaymentStatus
    {
        get
        {
            return this.paymentStatusField;
        }
        set
        {
            this.paymentStatusField = value;
        }
    }

    /// <remarks/>
    public string TxnAuthorizationTime
    {
        get
        {
            return this.txnAuthorizationTimeField;
        }
        set
        {
            this.txnAuthorizationTimeField = value;
        }
    }

    /// <remarks/>
    public string TxnAuthorizationStamp
    {
        get
        {
            return this.txnAuthorizationStampField;
        }
        set
        {
            this.txnAuthorizationStampField = value;
        }
    }

    /// <remarks/>
    public string ClientTransID
    {
        get
        {
            return this.clientTransIDField;
        }
        set
        {
            this.clientTransIDField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCreditMemoLineGroupRetCreditMemoLineRetDataExtRet
{

    private string ownerIDField;

    private string dataExtNameField;

    private string dataExtTypeField;

    private string dataExtValueField;

    /// <remarks/>
    public string OwnerID
    {
        get
        {
            return this.ownerIDField;
        }
        set
        {
            this.ownerIDField = value;
        }
    }

    /// <remarks/>
    public string DataExtName
    {
        get
        {
            return this.dataExtNameField;
        }
        set
        {
            this.dataExtNameField = value;
        }
    }

    /// <remarks/>
    public string DataExtType
    {
        get
        {
            return this.dataExtTypeField;
        }
        set
        {
            this.dataExtTypeField = value;
        }
    }

    /// <remarks/>
    public string DataExtValue
    {
        get
        {
            return this.dataExtValueField;
        }
        set
        {
            this.dataExtValueField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetCreditMemoLineGroupRetDataExtRet
{

    private string ownerIDField;

    private string dataExtNameField;

    private string dataExtTypeField;

    private string dataExtValueField;

    /// <remarks/>
    public string OwnerID
    {
        get
        {
            return this.ownerIDField;
        }
        set
        {
            this.ownerIDField = value;
        }
    }

    /// <remarks/>
    public string DataExtName
    {
        get
        {
            return this.dataExtNameField;
        }
        set
        {
            this.dataExtNameField = value;
        }
    }

    /// <remarks/>
    public string DataExtType
    {
        get
        {
            return this.dataExtTypeField;
        }
        set
        {
            this.dataExtTypeField = value;
        }
    }

    /// <remarks/>
    public string DataExtValue
    {
        get
        {
            return this.dataExtValueField;
        }
        set
        {
            this.dataExtValueField = value;
        }
    }
}

/// <remarks/>
[XmlType(AnonymousType = true)]
public partial class CreditMemoRetDataExtRet
{

    private string ownerIDField;

    private string dataExtNameField;

    private string dataExtTypeField;

    private string dataExtValueField;

    /// <remarks/>
    public string OwnerID
    {
        get
        {
            return this.ownerIDField;
        }
        set
        {
            this.ownerIDField = value;
        }
    }

    /// <remarks/>
    public string DataExtName
    {
        get
        {
            return this.dataExtNameField;
        }
        set
        {
            this.dataExtNameField = value;
        }
    }

    /// <remarks/>
    public string DataExtType
    {
        get
        {
            return this.dataExtTypeField;
        }
        set
        {
            this.dataExtTypeField = value;
        }
    }

    /// <remarks/>
    public string DataExtValue
    {
        get
        {
            return this.dataExtValueField;
        }
        set
        {
            this.dataExtValueField = value;
        }
    }
}

