﻿using System;
using System.Xml.Serialization;

namespace DataProcessor.Sync.ModelRet
{

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public partial class InvoiceRet
    {

        private string txnIDField;

        private DateTime timeCreatedField;

        private DateTime timeModifiedField;

        private string editSequenceField;

        private string txnNumberField;

        private InvoiceRetCustomerRef customerRefField;

        private InvoiceRetClassRef classRefField;

        private InvoiceRetARAccountRef aRAccountRefField;

        private InvoiceRetTemplateRef templateRefField;

        private DateTime txnDateField;

        private string refNumberField;

        private InvoiceRetBillAddress billAddressField;

        private InvoiceRetBillAddressBlock billAddressBlockField;

        private InvoiceRetShipAddress shipAddressField;

        private InvoiceRetShipAddressBlock shipAddressBlockField;

        private string isPendingField;

        private string isFinanceChargeField;

        private string pONumberField;

        private InvoiceRetTermsRef termsRefField;

        private DateTime dueDateField;

        private InvoiceRetSalesRepRef salesRepRefField;

        private string fOBField;

        private string shipDateField;

        private InvoiceRetShipMethodRef shipMethodRefField;

        private string subtotalField;

        private InvoiceRetItemSalesTaxRef itemSalesTaxRefField;

        private string salesTaxPercentageField;

        private string salesTaxTotalField;

        private string appliedAmountField;

        private string balanceRemainingField;

        private InvoiceRetCurrencyRef currencyRefField;

        private string exchangeRateField;

        private string balanceRemainingInHomeCurrencyField;

        private string memoField;

        private bool isPaidField;

        private InvoiceRetCustomerMsgRef customerMsgRefField;

        private string isToBePrintedField;

        private string isToBeEmailedField;

        private InvoiceRetCustomerSalesTaxCodeRef customerSalesTaxCodeRefField;

        private string suggestedDiscountAmountField;

        private string suggestedDiscountDateField;

        private string otherField;

        private string externalGUIDField;

        private InvoiceRetLinkedTxn linkedTxnField;

        private InvoiceRetInvoiceLineRet[] invoiceLineRetField;

        private InvoiceRetInvoiceLineGroupRet invoiceLineGroupRetField;

        private InvoiceRetDataExtRet dataExtRetField;

        /// <remarks/>
        public string TxnID
        {
            get
            {
                return txnIDField;
            }
            set
            {
                txnIDField = value;
            }
        }

        /// <remarks/>
        public DateTime TimeCreated
        {
            get
            {
                return timeCreatedField;
            }
            set
            {
                timeCreatedField = value;
            }
        }

        /// <remarks/>
        public DateTime TimeModified
        {
            get
            {
                return timeModifiedField;
            }
            set
            {
                timeModifiedField = value;
            }
        }

        /// <remarks/>
        public string EditSequence
        {
            get
            {
                return editSequenceField;
            }
            set
            {
                editSequenceField = value;
            }
        }

        /// <remarks/>
        public string TxnNumber
        {
            get
            {
                return txnNumberField;
            }
            set
            {
                txnNumberField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetCustomerRef CustomerRef
        {
            get
            {
                return customerRefField;
            }
            set
            {
                customerRefField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetClassRef ClassRef
        {
            get
            {
                return classRefField;
            }
            set
            {
                classRefField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetARAccountRef ARAccountRef
        {
            get
            {
                return aRAccountRefField;
            }
            set
            {
                aRAccountRefField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetTemplateRef TemplateRef
        {
            get
            {
                return templateRefField;
            }
            set
            {
                templateRefField = value;
            }
        }

        /// <remarks/>
        public DateTime TxnDate
        {
            get
            {
                return txnDateField;
            }
            set
            {
                txnDateField = value;
            }
        }

        /// <remarks/>
        public string RefNumber
        {
            get
            {
                return refNumberField;
            }
            set
            {
                refNumberField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetBillAddress BillAddress
        {
            get
            {
                return billAddressField;
            }
            set
            {
                billAddressField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetBillAddressBlock BillAddressBlock
        {
            get
            {
                return billAddressBlockField;
            }
            set
            {
                billAddressBlockField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetShipAddress ShipAddress
        {
            get
            {
                return shipAddressField;
            }
            set
            {
                shipAddressField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetShipAddressBlock ShipAddressBlock
        {
            get
            {
                return shipAddressBlockField;
            }
            set
            {
                shipAddressBlockField = value;
            }
        }

        /// <remarks/>
        public string IsPending
        {
            get
            {
                return isPendingField;
            }
            set
            {
                isPendingField = value;
            }
        }

        /// <remarks/>
        public string IsFinanceCharge
        {
            get
            {
                return isFinanceChargeField;
            }
            set
            {
                isFinanceChargeField = value;
            }
        }

        /// <remarks/>
        public string PONumber
        {
            get
            {
                return pONumberField;
            }
            set
            {
                pONumberField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetTermsRef TermsRef
        {
            get
            {
                return termsRefField;
            }
            set
            {
                termsRefField = value;
            }
        }

        /// <remarks/>
        public DateTime DueDate
        {
            get
            {
                return dueDateField;
            }
            set
            {
                dueDateField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetSalesRepRef SalesRepRef
        {
            get
            {
                return salesRepRefField;
            }
            set
            {
                salesRepRefField = value;
            }
        }

        /// <remarks/>
        public string FOB
        {
            get
            {
                return fOBField;
            }
            set
            {
                fOBField = value;
            }
        }

        /// <remarks/>
        public string ShipDate
        {
            get
            {
                return shipDateField;
            }
            set
            {
                shipDateField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetShipMethodRef ShipMethodRef
        {
            get
            {
                return shipMethodRefField;
            }
            set
            {
                shipMethodRefField = value;
            }
        }

        /// <remarks/>
        public string Subtotal
        {
            get
            {
                return subtotalField;
            }
            set
            {
                subtotalField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetItemSalesTaxRef ItemSalesTaxRef
        {
            get
            {
                return itemSalesTaxRefField;
            }
            set
            {
                itemSalesTaxRefField = value;
            }
        }

        /// <remarks/>
        public string SalesTaxPercentage
        {
            get
            {
                return salesTaxPercentageField;
            }
            set
            {
                salesTaxPercentageField = value;
            }
        }

        /// <remarks/>
        public string SalesTaxTotal
        {
            get
            {
                return salesTaxTotalField;
            }
            set
            {
                salesTaxTotalField = value;
            }
        }

        /// <remarks/>
        public string AppliedAmount
        {
            get
            {
                return appliedAmountField;
            }
            set
            {
                appliedAmountField = value;
            }
        }

        /// <remarks/>
        public string BalanceRemaining
        {
            get
            {
                return balanceRemainingField;
            }
            set
            {
                balanceRemainingField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetCurrencyRef CurrencyRef
        {
            get
            {
                return currencyRefField;
            }
            set
            {
                currencyRefField = value;
            }
        }

        /// <remarks/>
        public string ExchangeRate
        {
            get
            {
                return exchangeRateField;
            }
            set
            {
                exchangeRateField = value;
            }
        }

        /// <remarks/>
        public string BalanceRemainingInHomeCurrency
        {
            get
            {
                return balanceRemainingInHomeCurrencyField;
            }
            set
            {
                balanceRemainingInHomeCurrencyField = value;
            }
        }

        /// <remarks/>
        public string Memo
        {
            get
            {
                return memoField;
            }
            set
            {
                memoField = value;
            }
        }

        /// <remarks/>
        public bool IsPaid
        {
            get
            {
                return isPaidField;
            }
            set
            {
                isPaidField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetCustomerMsgRef CustomerMsgRef
        {
            get
            {
                return customerMsgRefField;
            }
            set
            {
                customerMsgRefField = value;
            }
        }

        /// <remarks/>
        public string IsToBePrinted
        {
            get
            {
                return isToBePrintedField;
            }
            set
            {
                isToBePrintedField = value;
            }
        }

        /// <remarks/>
        public string IsToBeEmailed
        {
            get
            {
                return isToBeEmailedField;
            }
            set
            {
                isToBeEmailedField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetCustomerSalesTaxCodeRef CustomerSalesTaxCodeRef
        {
            get
            {
                return customerSalesTaxCodeRefField;
            }
            set
            {
                customerSalesTaxCodeRefField = value;
            }
        }

        /// <remarks/>
        public string SuggestedDiscountAmount
        {
            get
            {
                return suggestedDiscountAmountField;
            }
            set
            {
                suggestedDiscountAmountField = value;
            }
        }

        /// <remarks/>
        public string SuggestedDiscountDate
        {
            get
            {
                return suggestedDiscountDateField;
            }
            set
            {
                suggestedDiscountDateField = value;
            }
        }

        /// <remarks/>
        public string Other
        {
            get
            {
                return otherField;
            }
            set
            {
                otherField = value;
            }
        }

        /// <remarks/>
        public string ExternalGUID
        {
            get
            {
                return externalGUIDField;
            }
            set
            {
                externalGUIDField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetLinkedTxn LinkedTxn
        {
            get
            {
                return linkedTxnField;
            }
            set
            {
                linkedTxnField = value;
            }
        }

       [XmlElement("InvoiceLineRet")]
        public InvoiceRetInvoiceLineRet[] InvoiceLineRet
        {
            get
            {
                return invoiceLineRetField;
            }
            set
            {
                invoiceLineRetField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetInvoiceLineGroupRet InvoiceLineGroupRet
        {
            get
            {
                return invoiceLineGroupRetField;
            }
            set
            {
                invoiceLineGroupRetField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetDataExtRet DataExtRet
        {
            get
            {
                return dataExtRetField;
            }
            set
            {
                dataExtRetField = value;
            }
        }

     
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetCustomerRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetClassRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetARAccountRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetTemplateRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetBillAddress
    {

        private string addr1Field;

        private string addr2Field;

        private string addr3Field;

        private string addr4Field;

        private string addr5Field;

        private string cityField;

        private string stateField;

        private string postalCodeField;

        private string countryField;

        private string noteField;

        /// <remarks/>
        public string Addr1
        {
            get
            {
                return addr1Field;
            }
            set
            {
                addr1Field = value;
            }
        }

        /// <remarks/>
        public string Addr2
        {
            get
            {
                return addr2Field;
            }
            set
            {
                addr2Field = value;
            }
        }

        /// <remarks/>
        public string Addr3
        {
            get
            {
                return addr3Field;
            }
            set
            {
                addr3Field = value;
            }
        }

        /// <remarks/>
        public string Addr4
        {
            get
            {
                return addr4Field;
            }
            set
            {
                addr4Field = value;
            }
        }

        /// <remarks/>
        public string Addr5
        {
            get
            {
                return addr5Field;
            }
            set
            {
                addr5Field = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return cityField;
            }
            set
            {
                cityField = value;
            }
        }

        /// <remarks/>
        public string State
        {
            get
            {
                return stateField;
            }
            set
            {
                stateField = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return postalCodeField;
            }
            set
            {
                postalCodeField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return countryField;
            }
            set
            {
                countryField = value;
            }
        }

        /// <remarks/>
        public string Note
        {
            get
            {
                return noteField;
            }
            set
            {
                noteField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetBillAddressBlock
    {

        private string addr1Field;

        private string addr2Field;

        private string addr3Field;

        private string addr4Field;

        private string addr5Field;

        /// <remarks/>
        public string Addr1
        {
            get
            {
                return addr1Field;
            }
            set
            {
                addr1Field = value;
            }
        }

        /// <remarks/>
        public string Addr2
        {
            get
            {
                return addr2Field;
            }
            set
            {
                addr2Field = value;
            }
        }

        /// <remarks/>
        public string Addr3
        {
            get
            {
                return addr3Field;
            }
            set
            {
                addr3Field = value;
            }
        }

        /// <remarks/>
        public string Addr4
        {
            get
            {
                return addr4Field;
            }
            set
            {
                addr4Field = value;
            }
        }

        /// <remarks/>
        public string Addr5
        {
            get
            {
                return addr5Field;
            }
            set
            {
                addr5Field = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetShipAddress
    {

        private string addr1Field;

        private string addr2Field;

        private string addr3Field;

        private string addr4Field;

        private string addr5Field;

        private string cityField;

        private string stateField;

        private string postalCodeField;

        private string countryField;

        private string noteField;

        /// <remarks/>
        public string Addr1
        {
            get
            {
                return addr1Field;
            }
            set
            {
                addr1Field = value;
            }
        }

        /// <remarks/>
        public string Addr2
        {
            get
            {
                return addr2Field;
            }
            set
            {
                addr2Field = value;
            }
        }

        /// <remarks/>
        public string Addr3
        {
            get
            {
                return addr3Field;
            }
            set
            {
                addr3Field = value;
            }
        }

        /// <remarks/>
        public string Addr4
        {
            get
            {
                return addr4Field;
            }
            set
            {
                addr4Field = value;
            }
        }

        /// <remarks/>
        public string Addr5
        {
            get
            {
                return addr5Field;
            }
            set
            {
                addr5Field = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return cityField;
            }
            set
            {
                cityField = value;
            }
        }

        /// <remarks/>
        public string State
        {
            get
            {
                return stateField;
            }
            set
            {
                stateField = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return postalCodeField;
            }
            set
            {
                postalCodeField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return countryField;
            }
            set
            {
                countryField = value;
            }
        }

        /// <remarks/>
        public string Note
        {
            get
            {
                return noteField;
            }
            set
            {
                noteField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetShipAddressBlock
    {

        private string addr1Field;

        private string addr2Field;

        private string addr3Field;

        private string addr4Field;

        private string addr5Field;

        /// <remarks/>
        public string Addr1
        {
            get
            {
                return addr1Field;
            }
            set
            {
                addr1Field = value;
            }
        }

        /// <remarks/>
        public string Addr2
        {
            get
            {
                return addr2Field;
            }
            set
            {
                addr2Field = value;
            }
        }

        /// <remarks/>
        public string Addr3
        {
            get
            {
                return addr3Field;
            }
            set
            {
                addr3Field = value;
            }
        }

        /// <remarks/>
        public string Addr4
        {
            get
            {
                return addr4Field;
            }
            set
            {
                addr4Field = value;
            }
        }

        /// <remarks/>
        public string Addr5
        {
            get
            {
                return addr5Field;
            }
            set
            {
                addr5Field = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetTermsRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetSalesRepRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetShipMethodRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetItemSalesTaxRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetCurrencyRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetCustomerMsgRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetCustomerSalesTaxCodeRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetLinkedTxn
    {

        private string txnIDField;

        private string txnTypeField;

        private string txnDateField;

        private string refNumberField;

        private string linkTypeField;

        private string amountField;

        /// <remarks/>
        public string TxnID
        {
            get
            {
                return txnIDField;
            }
            set
            {
                txnIDField = value;
            }
        }

        /// <remarks/>
        public string TxnType
        {
            get
            {
                return txnTypeField;
            }
            set
            {
                txnTypeField = value;
            }
        }

        /// <remarks/>
        public string TxnDate
        {
            get
            {
                return txnDateField;
            }
            set
            {
                txnDateField = value;
            }
        }

        /// <remarks/>
        public string RefNumber
        {
            get
            {
                return refNumberField;
            }
            set
            {
                refNumberField = value;
            }
        }

        /// <remarks/>
        public string LinkType
        {
            get
            {
                return linkTypeField;
            }
            set
            {
                linkTypeField = value;
            }
        }

        /// <remarks/>
        public string Amount
        {
            get
            {
                return amountField;
            }
            set
            {
                amountField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetInvoiceLineRet
    {

        private string txnLineIDField;

        private InvoiceRetInvoiceLineRetItemRef itemRefField;

        private string descField;

        private int quantityField;

        private string unitOfMeasureField;

        private InvoiceRetInvoiceLineRetOverrideUOMSetRef overrideUOMSetRefField;

        private float rateField;

        private string ratePercentField;

        private InvoiceRetInvoiceLineRetClassRef classRefField;

        private float amountField;

        private InvoiceRetInvoiceLineRetInventorySiteRef inventorySiteRefField;

        private InvoiceRetInvoiceLineRetInventorySiteLocationRef inventorySiteLocationRefField;

        private string serialNumberField;

        private string lotNumberField;

        private string serviceDateField;

        private InvoiceRetInvoiceLineRetSalesTaxCodeRef salesTaxCodeRefField;

        private string other1Field;

        private string other2Field;

        private InvoiceRetInvoiceLineRetDataExtRet dataExtRetField;

        /// <remarks/>
        public string TxnLineID
        {
            get
            {
                return txnLineIDField;
            }
            set
            {
                txnLineIDField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetInvoiceLineRetItemRef ItemRef
        {
            get
            {
                return itemRefField;
            }
            set
            {
                itemRefField = value;
            }
        }

        /// <remarks/>
        public string Desc
        {
            get
            {
                return descField;
            }
            set
            {
                descField = value;
            }
        }

        /// <remarks/>
        public int Quantity
        {
            get
            {
                return quantityField;
            }
            set
            {
                quantityField = value;
            }
        }

        /// <remarks/>
        public string UnitOfMeasure
        {
            get
            {
                return unitOfMeasureField;
            }
            set
            {
                unitOfMeasureField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetInvoiceLineRetOverrideUOMSetRef OverrideUOMSetRef
        {
            get
            {
                return overrideUOMSetRefField;
            }
            set
            {
                overrideUOMSetRefField = value;
            }
        }

        /// <remarks/>
        public float Rate
        {
            get
            {
                return rateField;
            }
            set
            {
                rateField = value;
            }
        }

        /// <remarks/>
        public string RatePercent
        {
            get
            {
                return ratePercentField;
            }
            set
            {
                ratePercentField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetInvoiceLineRetClassRef ClassRef
        {
            get
            {
                return classRefField;
            }
            set
            {
                classRefField = value;
            }
        }

        /// <remarks/>
        public float Amount
        {
            get
            {
                return amountField;
            }
            set
            {
                amountField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetInvoiceLineRetInventorySiteRef InventorySiteRef
        {
            get
            {
                return inventorySiteRefField;
            }
            set
            {
                inventorySiteRefField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetInvoiceLineRetInventorySiteLocationRef InventorySiteLocationRef
        {
            get
            {
                return inventorySiteLocationRefField;
            }
            set
            {
                inventorySiteLocationRefField = value;
            }
        }

        /// <remarks/>
        public string SerialNumber
        {
            get
            {
                return serialNumberField;
            }
            set
            {
                serialNumberField = value;
            }
        }

        /// <remarks/>
        public string LotNumber
        {
            get
            {
                return lotNumberField;
            }
            set
            {
                lotNumberField = value;
            }
        }

        /// <remarks/>
        public string ServiceDate
        {
            get
            {
                return serviceDateField;
            }
            set
            {
                serviceDateField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetInvoiceLineRetSalesTaxCodeRef SalesTaxCodeRef
        {
            get
            {
                return salesTaxCodeRefField;
            }
            set
            {
                salesTaxCodeRefField = value;
            }
        }

        /// <remarks/>
        public string Other1
        {
            get
            {
                return other1Field;
            }
            set
            {
                other1Field = value;
            }
        }

        /// <remarks/>
        public string Other2
        {
            get
            {
                return other2Field;
            }
            set
            {
                other2Field = value;
            }
        }

        /// <remarks/>
        public InvoiceRetInvoiceLineRetDataExtRet DataExtRet
        {
            get
            {
                return dataExtRetField;
            }
            set
            {
                dataExtRetField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetInvoiceLineRetItemRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetInvoiceLineRetOverrideUOMSetRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetInvoiceLineRetClassRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetInvoiceLineRetInventorySiteRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetInvoiceLineRetInventorySiteLocationRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetInvoiceLineRetSalesTaxCodeRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetInvoiceLineRetDataExtRet
    {

        private string ownerIDField;

        private string dataExtNameField;

        private string dataExtTypeField;

        private string dataExtValueField;

        /// <remarks/>
        public string OwnerID
        {
            get
            {
                return ownerIDField;
            }
            set
            {
                ownerIDField = value;
            }
        }

        /// <remarks/>
        public string DataExtName
        {
            get
            {
                return dataExtNameField;
            }
            set
            {
                dataExtNameField = value;
            }
        }

        /// <remarks/>
        public string DataExtType
        {
            get
            {
                return dataExtTypeField;
            }
            set
            {
                dataExtTypeField = value;
            }
        }

        /// <remarks/>
        public string DataExtValue
        {
            get
            {
                return dataExtValueField;
            }
            set
            {
                dataExtValueField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetInvoiceLineGroupRet
    {

        private string txnLineIDField;

        private InvoiceRetInvoiceLineGroupRetItemGroupRef itemGroupRefField;

        private string descField;

        private string quantityField;

        private string unitOfMeasureField;

        private InvoiceRetInvoiceLineGroupRetOverrideUOMSetRef overrideUOMSetRefField;

        private string isPrintItemsInGroupField;

        private string totalAmountField;

        private InvoiceRetInvoiceLineGroupRetInvoiceLineRet invoiceLineRetField;

        private InvoiceRetInvoiceLineGroupRetDataExtRet dataExtRetField;

        /// <remarks/>
        public string TxnLineID
        {
            get
            {
                return txnLineIDField;
            }
            set
            {
                txnLineIDField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetInvoiceLineGroupRetItemGroupRef ItemGroupRef
        {
            get
            {
                return itemGroupRefField;
            }
            set
            {
                itemGroupRefField = value;
            }
        }

        /// <remarks/>
        public string Desc
        {
            get
            {
                return descField;
            }
            set
            {
                descField = value;
            }
        }

        /// <remarks/>
        public string Quantity
        {
            get
            {
                return quantityField;
            }
            set
            {
                quantityField = value;
            }
        }

        /// <remarks/>
        public string UnitOfMeasure
        {
            get
            {
                return unitOfMeasureField;
            }
            set
            {
                unitOfMeasureField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetInvoiceLineGroupRetOverrideUOMSetRef OverrideUOMSetRef
        {
            get
            {
                return overrideUOMSetRefField;
            }
            set
            {
                overrideUOMSetRefField = value;
            }
        }

        /// <remarks/>
        public string IsPrintItemsInGroup
        {
            get
            {
                return isPrintItemsInGroupField;
            }
            set
            {
                isPrintItemsInGroupField = value;
            }
        }

        /// <remarks/>
        public string TotalAmount
        {
            get
            {
                return totalAmountField;
            }
            set
            {
                totalAmountField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetInvoiceLineGroupRetInvoiceLineRet InvoiceLineRet
        {
            get
            {
                return invoiceLineRetField;
            }
            set
            {
                invoiceLineRetField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetInvoiceLineGroupRetDataExtRet DataExtRet
        {
            get
            {
                return dataExtRetField;
            }
            set
            {
                dataExtRetField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetInvoiceLineGroupRetItemGroupRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetInvoiceLineGroupRetOverrideUOMSetRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetInvoiceLineGroupRetInvoiceLineRet
    {

        private string txnLineIDField;

        private InvoiceRetInvoiceLineGroupRetInvoiceLineRetItemRef itemRefField;

        private string descField;

        private string quantityField;

        private string unitOfMeasureField;

        private InvoiceRetInvoiceLineGroupRetInvoiceLineRetOverrideUOMSetRef overrideUOMSetRefField;

        private string rateField;

        private string ratePercentField;

        private InvoiceRetInvoiceLineGroupRetInvoiceLineRetClassRef classRefField;

        private string amountField;

        private InvoiceRetInvoiceLineGroupRetInvoiceLineRetInventorySiteRef inventorySiteRefField;

        private InvoiceRetInvoiceLineGroupRetInvoiceLineRetInventorySiteLocationRef inventorySiteLocationRefField;

        private string serialNumberField;

        private string lotNumberField;

        private string serviceDateField;

        private InvoiceRetInvoiceLineGroupRetInvoiceLineRetSalesTaxCodeRef salesTaxCodeRefField;

        private string other1Field;

        private string other2Field;

        private InvoiceRetInvoiceLineGroupRetInvoiceLineRetDataExtRet dataExtRetField;

        /// <remarks/>
        public string TxnLineID
        {
            get
            {
                return txnLineIDField;
            }
            set
            {
                txnLineIDField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetInvoiceLineGroupRetInvoiceLineRetItemRef ItemRef
        {
            get
            {
                return itemRefField;
            }
            set
            {
                itemRefField = value;
            }
        }

        /// <remarks/>
        public string Desc
        {
            get
            {
                return descField;
            }
            set
            {
                descField = value;
            }
        }

        /// <remarks/>
        public string Quantity
        {
            get
            {
                return quantityField;
            }
            set
            {
                quantityField = value;
            }
        }

        /// <remarks/>
        public string UnitOfMeasure
        {
            get
            {
                return unitOfMeasureField;
            }
            set
            {
                unitOfMeasureField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetInvoiceLineGroupRetInvoiceLineRetOverrideUOMSetRef OverrideUOMSetRef
        {
            get
            {
                return overrideUOMSetRefField;
            }
            set
            {
                overrideUOMSetRefField = value;
            }
        }

        /// <remarks/>
        public string Rate
        {
            get
            {
                return rateField;
            }
            set
            {
                rateField = value;
            }
        }

        /// <remarks/>
        public string RatePercent
        {
            get
            {
                return ratePercentField;
            }
            set
            {
                ratePercentField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetInvoiceLineGroupRetInvoiceLineRetClassRef ClassRef
        {
            get
            {
                return classRefField;
            }
            set
            {
                classRefField = value;
            }
        }

        /// <remarks/>
        public string Amount
        {
            get
            {
                return amountField;
            }
            set
            {
                amountField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetInvoiceLineGroupRetInvoiceLineRetInventorySiteRef InventorySiteRef
        {
            get
            {
                return inventorySiteRefField;
            }
            set
            {
                inventorySiteRefField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetInvoiceLineGroupRetInvoiceLineRetInventorySiteLocationRef InventorySiteLocationRef
        {
            get
            {
                return inventorySiteLocationRefField;
            }
            set
            {
                inventorySiteLocationRefField = value;
            }
        }

        /// <remarks/>
        public string SerialNumber
        {
            get
            {
                return serialNumberField;
            }
            set
            {
                serialNumberField = value;
            }
        }

        /// <remarks/>
        public string LotNumber
        {
            get
            {
                return lotNumberField;
            }
            set
            {
                lotNumberField = value;
            }
        }

        /// <remarks/>
        public string ServiceDate
        {
            get
            {
                return serviceDateField;
            }
            set
            {
                serviceDateField = value;
            }
        }

        /// <remarks/>
        public InvoiceRetInvoiceLineGroupRetInvoiceLineRetSalesTaxCodeRef SalesTaxCodeRef
        {
            get
            {
                return salesTaxCodeRefField;
            }
            set
            {
                salesTaxCodeRefField = value;
            }
        }

        /// <remarks/>
        public string Other1
        {
            get
            {
                return other1Field;
            }
            set
            {
                other1Field = value;
            }
        }

        /// <remarks/>
        public string Other2
        {
            get
            {
                return other2Field;
            }
            set
            {
                other2Field = value;
            }
        }

        /// <remarks/>
        public InvoiceRetInvoiceLineGroupRetInvoiceLineRetDataExtRet DataExtRet
        {
            get
            {
                return dataExtRetField;
            }
            set
            {
                dataExtRetField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetInvoiceLineGroupRetInvoiceLineRetItemRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetInvoiceLineGroupRetInvoiceLineRetOverrideUOMSetRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetInvoiceLineGroupRetInvoiceLineRetClassRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetInvoiceLineGroupRetInvoiceLineRetInventorySiteRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetInvoiceLineGroupRetInvoiceLineRetInventorySiteLocationRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetInvoiceLineGroupRetInvoiceLineRetSalesTaxCodeRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetInvoiceLineGroupRetInvoiceLineRetDataExtRet
    {

        private string ownerIDField;

        private string dataExtNameField;

        private string dataExtTypeField;

        private string dataExtValueField;

        /// <remarks/>
        public string OwnerID
        {
            get
            {
                return ownerIDField;
            }
            set
            {
                ownerIDField = value;
            }
        }

        /// <remarks/>
        public string DataExtName
        {
            get
            {
                return dataExtNameField;
            }
            set
            {
                dataExtNameField = value;
            }
        }

        /// <remarks/>
        public string DataExtType
        {
            get
            {
                return dataExtTypeField;
            }
            set
            {
                dataExtTypeField = value;
            }
        }

        /// <remarks/>
        public string DataExtValue
        {
            get
            {
                return dataExtValueField;
            }
            set
            {
                dataExtValueField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetInvoiceLineGroupRetDataExtRet
    {

        private string ownerIDField;

        private string dataExtNameField;

        private string dataExtTypeField;

        private string dataExtValueField;

        /// <remarks/>
        public string OwnerID
        {
            get
            {
                return ownerIDField;
            }
            set
            {
                ownerIDField = value;
            }
        }

        /// <remarks/>
        public string DataExtName
        {
            get
            {
                return dataExtNameField;
            }
            set
            {
                dataExtNameField = value;
            }
        }

        /// <remarks/>
        public string DataExtType
        {
            get
            {
                return dataExtTypeField;
            }
            set
            {
                dataExtTypeField = value;
            }
        }

        /// <remarks/>
        public string DataExtValue
        {
            get
            {
                return dataExtValueField;
            }
            set
            {
                dataExtValueField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class InvoiceRetDataExtRet
    {

        private string ownerIDField;

        private string dataExtNameField;

        private string dataExtTypeField;

        private string dataExtValueField;

        /// <remarks/>
        public string OwnerID
        {
            get
            {
                return ownerIDField;
            }
            set
            {
                ownerIDField = value;
            }
        }

        /// <remarks/>
        public string DataExtName
        {
            get
            {
                return dataExtNameField;
            }
            set
            {
                dataExtNameField = value;
            }
        }

        /// <remarks/>
        public string DataExtType
        {
            get
            {
                return dataExtTypeField;
            }
            set
            {
                dataExtTypeField = value;
            }
        }

        /// <remarks/>
        public string DataExtValue
        {
            get
            {
                return dataExtValueField;
            }
            set
            {
                dataExtValueField = value;
            }
        }
    }


}
