﻿
using System;

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
public partial class ClassRet
{

    private string listIDField;

    private DateTime? timeCreatedField;

    private DateTime? timeModifiedField;

    private string editSequenceField;

    private string nameField;

    private string fullNameField;

    private bool? isActiveField;

    private ClassRetParentRef parentRefField;

    private string sublevelField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public DateTime? TimeCreated
    {
        get
        {
            return this.timeCreatedField;
        }
        set
        {
            this.timeCreatedField = value;
        }
    }

    /// <remarks/>
    public DateTime? TimeModified
    {
        get
        {
            return this.timeModifiedField;
        }
        set
        {
            this.timeModifiedField = value;
        }
    }

    /// <remarks/>
    public string EditSequence
    {
        get
        {
            return this.editSequenceField;
        }
        set
        {
            this.editSequenceField = value;
        }
    }

    /// <remarks/>
    public string Name
    {
        get
        {
            return this.nameField;
        }
        set
        {
            this.nameField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }

    /// <remarks/>
    public bool? IsActive
    {
        get
        {
            return this.isActiveField;
        }
        set
        {
            this.isActiveField = value;
        }
    }

    /// <remarks/>
    public ClassRetParentRef ParentRef
    {
        get
        {
            return this.parentRefField;
        }
        set
        {
            this.parentRefField = value;
        }
    }

    /// <remarks/>
    public string Sublevel
    {
        get
        {
            return this.sublevelField;
        }
        set
        {
            this.sublevelField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ClassRetParentRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

