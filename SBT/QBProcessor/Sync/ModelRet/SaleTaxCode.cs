﻿using System.Xml.Serialization;

namespace DataProcessor.Sync.ModelRet
{

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public partial class SalesTaxCodeRet
    {

        private string listIDField;

        private string timeCreatedField;

        private string timeModifiedField;

        private string editSequenceField;

        private string nameField;

        private bool isActiveField;

        private bool isTaxableField;

        private string descField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string TimeCreated
        {
            get
            {
                return timeCreatedField;
            }
            set
            {
                timeCreatedField = value;
            }
        }

        /// <remarks/>
        public string TimeModified
        {
            get
            {
                return timeModifiedField;
            }
            set
            {
                timeModifiedField = value;
            }
        }

        /// <remarks/>
        public string EditSequence
        {
            get
            {
                return editSequenceField;
            }
            set
            {
                editSequenceField = value;
            }
        }

        /// <remarks/>
        public string Name
        {
            get
            {
                return nameField;
            }
            set
            {
                nameField = value;
            }
        }

        /// <remarks/>
        public bool IsActive
        {
            get
            {
                return isActiveField;
            }
            set
            {
                isActiveField = value;
            }
        }

        /// <remarks/>
        public bool IsTaxable
        {
            get
            {
                return isTaxableField;
            }
            set
            {
                isTaxableField = value;
            }
        }

        /// <remarks/>
        public string Desc
        {
            get
            {
                return descField;
            }
            set
            {
                descField = value;
            }
        }
    }


}
