﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;


/// <remarks/>
[XmlType(AnonymousType = true)]
[XmlRoot(Namespace = "", IsNullable = false)]
public partial class CompanyRet
{
    public string EIN { get; set; }
    public string CompanyName { get; set; }
}

