﻿using System;
using System.Xml.Serialization;

namespace DataProcessor.Sync.ModelRet
{

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "" , IsNullable = false)]
    public partial class SalesReceiptRet
    {

        private string txnIDField;

        private string timeCreatedField;

        private DateTime timeModifiedField;

        private string editSequenceField;

        private string txnNumberField;

        private SalesReceiptRetCustomerRef customerRefField;

        private SalesReceiptRetClassRef classRefField;

        private SalesReceiptRetTemplateRef templateRefField;

        private DateTime txnDateField;

        private string refNumberField;

        private SalesReceiptRetBillAddress billAddressField;

        private SalesReceiptRetBillAddressBlock billAddressBlockField;

        private SalesReceiptRetShipAddress shipAddressField;

        private SalesReceiptRetShipAddressBlock shipAddressBlockField;

        private string isPendingField;

        private string checkNumberField;

        private SalesReceiptRetPaymentMethodRef paymentMethodRefField;

        private string dueDateField;

        private SalesReceiptRetSalesRepRef salesRepRefField;

        private string shipDateField;

        private SalesReceiptRetShipMethodRef shipMethodRefField;

        private string fOBField;

        private string subtotalField;

        private SalesReceiptRetItemSalesTaxRef itemSalesTaxRefField;

        private string salesTaxPercentageField;

        private string salesTaxTotalField;

        private float totalAmountField;

        private SalesReceiptRetCurrencyRef currencyRefField;

        private string exchangeRateField;

        private string totalAmountInHomeCurrencyField;

        private string memoField;

        private SalesReceiptRetCustomerMsgRef customerMsgRefField;

        private string isToBePrintedField;

        private string isToBeEmailedField;

        private SalesReceiptRetCustomerSalesTaxCodeRef customerSalesTaxCodeRefField;

        private SalesReceiptRetDepositToAccountRef depositToAccountRefField;

        private SalesReceiptRetCreditCardTxnInfo creditCardTxnInfoField;

        private string otherField;

        private string externalGUIDField;

        private SalesReceiptRetSalesReceiptLineRet[] salesReceiptLineRetField;

        private SalesReceiptRetSalesReceiptLineGroupRet salesReceiptLineGroupRetField;

        private SalesReceiptRetDataExtRet dataExtRetField;

        /// <remarks/>
        public string TxnID
        {
            get
            {
                return txnIDField;
            }
            set
            {
                txnIDField = value;
            }
        }

        /// <remarks/>
        public string TimeCreated
        {
            get
            {
                return timeCreatedField;
            }
            set
            {
                timeCreatedField = value;
            }
        }

        /// <remarks/>
        public DateTime TimeModified
        {
            get
            {
                return timeModifiedField;
            }
            set
            {
                timeModifiedField = value;
            }
        }

        /// <remarks/>
        public string EditSequence
        {
            get
            {
                return editSequenceField;
            }
            set
            {
                editSequenceField = value;
            }
        }

        /// <remarks/>
        public string TxnNumber
        {
            get
            {
                return txnNumberField;
            }
            set
            {
                txnNumberField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetCustomerRef CustomerRef
        {
            get
            {
                return customerRefField;
            }
            set
            {
                customerRefField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetClassRef ClassRef
        {
            get
            {
                return classRefField;
            }
            set
            {
                classRefField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetTemplateRef TemplateRef
        {
            get
            {
                return templateRefField;
            }
            set
            {
                templateRefField = value;
            }
        }

        /// <remarks/>
        public DateTime TxnDate
        {
            get
            {
                return txnDateField;
            }
            set
            {
                txnDateField = value;
            }
        }

        /// <remarks/>
        public string RefNumber
        {
            get
            {
                return refNumberField;
            }
            set
            {
                refNumberField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetBillAddress BillAddress
        {
            get
            {
                return billAddressField;
            }
            set
            {
                billAddressField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetBillAddressBlock BillAddressBlock
        {
            get
            {
                return billAddressBlockField;
            }
            set
            {
                billAddressBlockField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetShipAddress ShipAddress
        {
            get
            {
                return shipAddressField;
            }
            set
            {
                shipAddressField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetShipAddressBlock ShipAddressBlock
        {
            get
            {
                return shipAddressBlockField;
            }
            set
            {
                shipAddressBlockField = value;
            }
        }

        /// <remarks/>
        public string IsPending
        {
            get
            {
                return isPendingField;
            }
            set
            {
                isPendingField = value;
            }
        }

        /// <remarks/>
        public string CheckNumber
        {
            get
            {
                return checkNumberField;
            }
            set
            {
                checkNumberField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetPaymentMethodRef PaymentMethodRef
        {
            get
            {
                return paymentMethodRefField;
            }
            set
            {
                paymentMethodRefField = value;
            }
        }

        /// <remarks/>
        public string DueDate
        {
            get
            {
                return dueDateField;
            }
            set
            {
                dueDateField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetSalesRepRef SalesRepRef
        {
            get
            {
                return salesRepRefField;
            }
            set
            {
                salesRepRefField = value;
            }
        }

        /// <remarks/>
        public string ShipDate
        {
            get
            {
                return shipDateField;
            }
            set
            {
                shipDateField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetShipMethodRef ShipMethodRef
        {
            get
            {
                return shipMethodRefField;
            }
            set
            {
                shipMethodRefField = value;
            }
        }

        /// <remarks/>
        public string FOB
        {
            get
            {
                return fOBField;
            }
            set
            {
                fOBField = value;
            }
        }

        /// <remarks/>
        public string Subtotal
        {
            get
            {
                return subtotalField;
            }
            set
            {
                subtotalField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetItemSalesTaxRef ItemSalesTaxRef
        {
            get
            {
                return itemSalesTaxRefField;
            }
            set
            {
                itemSalesTaxRefField = value;
            }
        }

        /// <remarks/>
        public string SalesTaxPercentage
        {
            get
            {
                return salesTaxPercentageField;
            }
            set
            {
                salesTaxPercentageField = value;
            }
        }

        /// <remarks/>
        public string SalesTaxTotal
        {
            get
            {
                return salesTaxTotalField;
            }
            set
            {
                salesTaxTotalField = value;
            }
        }

        /// <remarks/>
        public float TotalAmount
        {
            get
            {
                return totalAmountField;
            }
            set
            {
                totalAmountField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetCurrencyRef CurrencyRef
        {
            get
            {
                return currencyRefField;
            }
            set
            {
                currencyRefField = value;
            }
        }

        /// <remarks/>
        public string ExchangeRate
        {
            get
            {
                return exchangeRateField;
            }
            set
            {
                exchangeRateField = value;
            }
        }

        /// <remarks/>
        public string TotalAmountInHomeCurrency
        {
            get
            {
                return totalAmountInHomeCurrencyField;
            }
            set
            {
                totalAmountInHomeCurrencyField = value;
            }
        }

        /// <remarks/>
        public string Memo
        {
            get
            {
                return memoField;
            }
            set
            {
                memoField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetCustomerMsgRef CustomerMsgRef
        {
            get
            {
                return customerMsgRefField;
            }
            set
            {
                customerMsgRefField = value;
            }
        }

        /// <remarks/>
        public string IsToBePrinted
        {
            get
            {
                return isToBePrintedField;
            }
            set
            {
                isToBePrintedField = value;
            }
        }

        /// <remarks/>
        public string IsToBeEmailed
        {
            get
            {
                return isToBeEmailedField;
            }
            set
            {
                isToBeEmailedField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetCustomerSalesTaxCodeRef CustomerSalesTaxCodeRef
        {
            get
            {
                return customerSalesTaxCodeRefField;
            }
            set
            {
                customerSalesTaxCodeRefField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetDepositToAccountRef DepositToAccountRef
        {
            get
            {
                return depositToAccountRefField;
            }
            set
            {
                depositToAccountRefField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetCreditCardTxnInfo CreditCardTxnInfo
        {
            get
            {
                return creditCardTxnInfoField;
            }
            set
            {
                creditCardTxnInfoField = value;
            }
        }

        /// <remarks/>
        public string Other
        {
            get
            {
                return otherField;
            }
            set
            {
                otherField = value;
            }
        }

        /// <remarks/>
        public string ExternalGUID
        {
            get
            {
                return externalGUIDField;
            }
            set
            {
                externalGUIDField = value;
            }
        }

        [XmlElement("SalesReceiptLineRet")]
        public SalesReceiptRetSalesReceiptLineRet[] SalesReceiptLineRet
        {
            get
            {
                return salesReceiptLineRetField;
            }
            set
            {
                salesReceiptLineRetField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetSalesReceiptLineGroupRet SalesReceiptLineGroupRet
        {
            get
            {
                return salesReceiptLineGroupRetField;
            }
            set
            {
                salesReceiptLineGroupRetField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetDataExtRet DataExtRet
        {
            get
            {
                return dataExtRetField;
            }
            set
            {
                dataExtRetField = value;
            }
        }

    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetCustomerRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetClassRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetTemplateRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetBillAddress
    {

        private string addr1Field;

        private string addr2Field;

        private string addr3Field;

        private string addr4Field;

        private string addr5Field;

        private string cityField;

        private string stateField;

        private string postalCodeField;

        private string countryField;

        private string noteField;

        /// <remarks/>
        public string Addr1
        {
            get
            {
                return addr1Field;
            }
            set
            {
                addr1Field = value;
            }
        }

        /// <remarks/>
        public string Addr2
        {
            get
            {
                return addr2Field;
            }
            set
            {
                addr2Field = value;
            }
        }

        /// <remarks/>
        public string Addr3
        {
            get
            {
                return addr3Field;
            }
            set
            {
                addr3Field = value;
            }
        }

        /// <remarks/>
        public string Addr4
        {
            get
            {
                return addr4Field;
            }
            set
            {
                addr4Field = value;
            }
        }

        /// <remarks/>
        public string Addr5
        {
            get
            {
                return addr5Field;
            }
            set
            {
                addr5Field = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return cityField;
            }
            set
            {
                cityField = value;
            }
        }

        /// <remarks/>
        public string State
        {
            get
            {
                return stateField;
            }
            set
            {
                stateField = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return postalCodeField;
            }
            set
            {
                postalCodeField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return countryField;
            }
            set
            {
                countryField = value;
            }
        }

        /// <remarks/>
        public string Note
        {
            get
            {
                return noteField;
            }
            set
            {
                noteField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetBillAddressBlock
    {

        private string addr1Field;

        private string addr2Field;

        private string addr3Field;

        private string addr4Field;

        private string addr5Field;

        /// <remarks/>
        public string Addr1
        {
            get
            {
                return addr1Field;
            }
            set
            {
                addr1Field = value;
            }
        }

        /// <remarks/>
        public string Addr2
        {
            get
            {
                return addr2Field;
            }
            set
            {
                addr2Field = value;
            }
        }

        /// <remarks/>
        public string Addr3
        {
            get
            {
                return addr3Field;
            }
            set
            {
                addr3Field = value;
            }
        }

        /// <remarks/>
        public string Addr4
        {
            get
            {
                return addr4Field;
            }
            set
            {
                addr4Field = value;
            }
        }

        /// <remarks/>
        public string Addr5
        {
            get
            {
                return addr5Field;
            }
            set
            {
                addr5Field = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetShipAddress
    {

        private string addr1Field;

        private string addr2Field;

        private string addr3Field;

        private string addr4Field;

        private string addr5Field;

        private string cityField;

        private string stateField;

        private string postalCodeField;

        private string countryField;

        private string noteField;

        /// <remarks/>
        public string Addr1
        {
            get
            {
                return addr1Field;
            }
            set
            {
                addr1Field = value;
            }
        }

        /// <remarks/>
        public string Addr2
        {
            get
            {
                return addr2Field;
            }
            set
            {
                addr2Field = value;
            }
        }

        /// <remarks/>
        public string Addr3
        {
            get
            {
                return addr3Field;
            }
            set
            {
                addr3Field = value;
            }
        }

        /// <remarks/>
        public string Addr4
        {
            get
            {
                return addr4Field;
            }
            set
            {
                addr4Field = value;
            }
        }

        /// <remarks/>
        public string Addr5
        {
            get
            {
                return addr5Field;
            }
            set
            {
                addr5Field = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return cityField;
            }
            set
            {
                cityField = value;
            }
        }

        /// <remarks/>
        public string State
        {
            get
            {
                return stateField;
            }
            set
            {
                stateField = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return postalCodeField;
            }
            set
            {
                postalCodeField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return countryField;
            }
            set
            {
                countryField = value;
            }
        }

        /// <remarks/>
        public string Note
        {
            get
            {
                return noteField;
            }
            set
            {
                noteField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetShipAddressBlock
    {

        private string addr1Field;

        private string addr2Field;

        private string addr3Field;

        private string addr4Field;

        private string addr5Field;

        /// <remarks/>
        public string Addr1
        {
            get
            {
                return addr1Field;
            }
            set
            {
                addr1Field = value;
            }
        }

        /// <remarks/>
        public string Addr2
        {
            get
            {
                return addr2Field;
            }
            set
            {
                addr2Field = value;
            }
        }

        /// <remarks/>
        public string Addr3
        {
            get
            {
                return addr3Field;
            }
            set
            {
                addr3Field = value;
            }
        }

        /// <remarks/>
        public string Addr4
        {
            get
            {
                return addr4Field;
            }
            set
            {
                addr4Field = value;
            }
        }

        /// <remarks/>
        public string Addr5
        {
            get
            {
                return addr5Field;
            }
            set
            {
                addr5Field = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetPaymentMethodRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetSalesRepRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetShipMethodRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetItemSalesTaxRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetCurrencyRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetCustomerMsgRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetCustomerSalesTaxCodeRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetDepositToAccountRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetCreditCardTxnInfo
    {

        private SalesReceiptRetCreditCardTxnInfoCreditCardTxnInputInfo creditCardTxnInputInfoField;

        private SalesReceiptRetCreditCardTxnInfoCreditCardTxnResultInfo creditCardTxnResultInfoField;

        /// <remarks/>
        public SalesReceiptRetCreditCardTxnInfoCreditCardTxnInputInfo CreditCardTxnInputInfo
        {
            get
            {
                return creditCardTxnInputInfoField;
            }
            set
            {
                creditCardTxnInputInfoField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetCreditCardTxnInfoCreditCardTxnResultInfo CreditCardTxnResultInfo
        {
            get
            {
                return creditCardTxnResultInfoField;
            }
            set
            {
                creditCardTxnResultInfoField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetCreditCardTxnInfoCreditCardTxnInputInfo
    {

        private string creditCardNumberField;

        private string expirationMonthField;

        private string expirationYearField;

        private string nameOnCardField;

        private string creditCardAddressField;

        private string creditCardPostalCodeField;

        private string commercialCardCodeField;

        private string transactionModeField;

        private string creditCardTxnTypeField;

        /// <remarks/>
        public string CreditCardNumber
        {
            get
            {
                return creditCardNumberField;
            }
            set
            {
                creditCardNumberField = value;
            }
        }

        /// <remarks/>
        public string ExpirationMonth
        {
            get
            {
                return expirationMonthField;
            }
            set
            {
                expirationMonthField = value;
            }
        }

        /// <remarks/>
        public string ExpirationYear
        {
            get
            {
                return expirationYearField;
            }
            set
            {
                expirationYearField = value;
            }
        }

        /// <remarks/>
        public string NameOnCard
        {
            get
            {
                return nameOnCardField;
            }
            set
            {
                nameOnCardField = value;
            }
        }

        /// <remarks/>
        public string CreditCardAddress
        {
            get
            {
                return creditCardAddressField;
            }
            set
            {
                creditCardAddressField = value;
            }
        }

        /// <remarks/>
        public string CreditCardPostalCode
        {
            get
            {
                return creditCardPostalCodeField;
            }
            set
            {
                creditCardPostalCodeField = value;
            }
        }

        /// <remarks/>
        public string CommercialCardCode
        {
            get
            {
                return commercialCardCodeField;
            }
            set
            {
                commercialCardCodeField = value;
            }
        }

        /// <remarks/>
        public string TransactionMode
        {
            get
            {
                return transactionModeField;
            }
            set
            {
                transactionModeField = value;
            }
        }

        /// <remarks/>
        public string CreditCardTxnType
        {
            get
            {
                return creditCardTxnTypeField;
            }
            set
            {
                creditCardTxnTypeField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetCreditCardTxnInfoCreditCardTxnResultInfo
    {

        private string resultCodeField;

        private string resultMessageField;

        private string creditCardTransIDField;

        private string merchantAccountNumberField;

        private string authorizationCodeField;

        private string aVSStreetField;

        private string aVSZipField;

        private string cardSecurityCodeMatchField;

        private string reconBatchIDField;

        private string paymentGroupingCodeField;

        private string paymentStatusField;

        private string txnAuthorizationTimeField;

        private string txnAuthorizationStampField;

        private string clientTransIDField;

        /// <remarks/>
        public string ResultCode
        {
            get
            {
                return resultCodeField;
            }
            set
            {
                resultCodeField = value;
            }
        }

        /// <remarks/>
        public string ResultMessage
        {
            get
            {
                return resultMessageField;
            }
            set
            {
                resultMessageField = value;
            }
        }

        /// <remarks/>
        public string CreditCardTransID
        {
            get
            {
                return creditCardTransIDField;
            }
            set
            {
                creditCardTransIDField = value;
            }
        }

        /// <remarks/>
        public string MerchantAccountNumber
        {
            get
            {
                return merchantAccountNumberField;
            }
            set
            {
                merchantAccountNumberField = value;
            }
        }

        /// <remarks/>
        public string AuthorizationCode
        {
            get
            {
                return authorizationCodeField;
            }
            set
            {
                authorizationCodeField = value;
            }
        }

        /// <remarks/>
        public string AVSStreet
        {
            get
            {
                return aVSStreetField;
            }
            set
            {
                aVSStreetField = value;
            }
        }

        /// <remarks/>
        public string AVSZip
        {
            get
            {
                return aVSZipField;
            }
            set
            {
                aVSZipField = value;
            }
        }

        /// <remarks/>
        public string CardSecurityCodeMatch
        {
            get
            {
                return cardSecurityCodeMatchField;
            }
            set
            {
                cardSecurityCodeMatchField = value;
            }
        }

        /// <remarks/>
        public string ReconBatchID
        {
            get
            {
                return reconBatchIDField;
            }
            set
            {
                reconBatchIDField = value;
            }
        }

        /// <remarks/>
        public string PaymentGroupingCode
        {
            get
            {
                return paymentGroupingCodeField;
            }
            set
            {
                paymentGroupingCodeField = value;
            }
        }

        /// <remarks/>
        public string PaymentStatus
        {
            get
            {
                return paymentStatusField;
            }
            set
            {
                paymentStatusField = value;
            }
        }

        /// <remarks/>
        public string TxnAuthorizationTime
        {
            get
            {
                return txnAuthorizationTimeField;
            }
            set
            {
                txnAuthorizationTimeField = value;
            }
        }

        /// <remarks/>
        public string TxnAuthorizationStamp
        {
            get
            {
                return txnAuthorizationStampField;
            }
            set
            {
                txnAuthorizationStampField = value;
            }
        }

        /// <remarks/>
        public string ClientTransID
        {
            get
            {
                return clientTransIDField;
            }
            set
            {
                clientTransIDField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetSalesReceiptLineRet
    {

        private string txnLineIDField;

        private SalesReceiptRetSalesReceiptLineRetItemRef itemRefField;

        private string descField;

        private double quantityField;

        private string unitOfMeasureField;

        private SalesReceiptRetSalesReceiptLineRetOverrideUOMSetRef overrideUOMSetRefField;

        private decimal? rateField;

        private string ratePercentField;

        private SalesReceiptRetSalesReceiptLineRetClassRef classRefField;

        private decimal? amountField;

        private SalesReceiptRetSalesReceiptLineRetInventorySiteRef inventorySiteRefField;

        private SalesReceiptRetSalesReceiptLineRetInventorySiteLocationRef inventorySiteLocationRefField;

        private string serialNumberField;

        private string lotNumberField;

        private string serviceDateField;

        private SalesReceiptRetSalesReceiptLineRetSalesTaxCodeRef salesTaxCodeRefField;

        private string other1Field;

        private string other2Field;

        private SalesReceiptRetSalesReceiptLineRetCreditCardTxnInfo creditCardTxnInfoField;

        private SalesReceiptRetSalesReceiptLineRetDataExtRet dataExtRetField;

        /// <remarks/>
        public string TxnLineID
        {
            get
            {
                return txnLineIDField;
            }
            set
            {
                txnLineIDField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetSalesReceiptLineRetItemRef ItemRef
        {
            get
            {
                return itemRefField;
            }
            set
            {
                itemRefField = value;
            }
        }

        /// <remarks/>
        public string Desc
        {
            get
            {
                return descField;
            }
            set
            {
                descField = value;
            }
        }

        /// <remarks/>
        public double Quantity
        {
            get
            {
                return quantityField;
            }
            set
            {
                quantityField = value;
            }
        }

        /// <remarks/>
        public string UnitOfMeasure
        {
            get
            {
                return unitOfMeasureField;
            }
            set
            {
                unitOfMeasureField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetSalesReceiptLineRetOverrideUOMSetRef OverrideUOMSetRef
        {
            get
            {
                return overrideUOMSetRefField;
            }
            set
            {
                overrideUOMSetRefField = value;
            }
        }

        /// <remarks/>
        public decimal? Rate
        {
            get
            {
                return rateField;
            }
            set
            {
                rateField = value;
            }
        }

        /// <remarks/>
        public string RatePercent
        {
            get
            {
                return ratePercentField;
            }
            set
            {
                ratePercentField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetSalesReceiptLineRetClassRef ClassRef
        {
            get
            {
                return classRefField;
            }
            set
            {
                classRefField = value;
            }
        }

        /// <remarks/>
        public decimal? Amount
        {
            get
            {
                return amountField;
            }
            set
            {
                amountField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetSalesReceiptLineRetInventorySiteRef InventorySiteRef
        {
            get
            {
                return inventorySiteRefField;
            }
            set
            {
                inventorySiteRefField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetSalesReceiptLineRetInventorySiteLocationRef InventorySiteLocationRef
        {
            get
            {
                return inventorySiteLocationRefField;
            }
            set
            {
                inventorySiteLocationRefField = value;
            }
        }

        /// <remarks/>
        public string SerialNumber
        {
            get
            {
                return serialNumberField;
            }
            set
            {
                serialNumberField = value;
            }
        }

        /// <remarks/>
        public string LotNumber
        {
            get
            {
                return lotNumberField;
            }
            set
            {
                lotNumberField = value;
            }
        }

        /// <remarks/>
        public string ServiceDate
        {
            get
            {
                return serviceDateField;
            }
            set
            {
                serviceDateField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetSalesReceiptLineRetSalesTaxCodeRef SalesTaxCodeRef
        {
            get
            {
                return salesTaxCodeRefField;
            }
            set
            {
                salesTaxCodeRefField = value;
            }
        }

        /// <remarks/>
        public string Other1
        {
            get
            {
                return other1Field;
            }
            set
            {
                other1Field = value;
            }
        }

        /// <remarks/>
        public string Other2
        {
            get
            {
                return other2Field;
            }
            set
            {
                other2Field = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetSalesReceiptLineRetCreditCardTxnInfo CreditCardTxnInfo
        {
            get
            {
                return creditCardTxnInfoField;
            }
            set
            {
                creditCardTxnInfoField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetSalesReceiptLineRetDataExtRet DataExtRet
        {
            get
            {
                return dataExtRetField;
            }
            set
            {
                dataExtRetField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetSalesReceiptLineRetItemRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetSalesReceiptLineRetOverrideUOMSetRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetSalesReceiptLineRetClassRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetSalesReceiptLineRetInventorySiteRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetSalesReceiptLineRetInventorySiteLocationRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetSalesReceiptLineRetSalesTaxCodeRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetSalesReceiptLineRetCreditCardTxnInfo
    {

        private SalesReceiptRetSalesReceiptLineRetCreditCardTxnInfoCreditCardTxnInputInfo creditCardTxnInputInfoField;

        private SalesReceiptRetSalesReceiptLineRetCreditCardTxnInfoCreditCardTxnResultInfo creditCardTxnResultInfoField;

        /// <remarks/>
        public SalesReceiptRetSalesReceiptLineRetCreditCardTxnInfoCreditCardTxnInputInfo CreditCardTxnInputInfo
        {
            get
            {
                return creditCardTxnInputInfoField;
            }
            set
            {
                creditCardTxnInputInfoField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetSalesReceiptLineRetCreditCardTxnInfoCreditCardTxnResultInfo CreditCardTxnResultInfo
        {
            get
            {
                return creditCardTxnResultInfoField;
            }
            set
            {
                creditCardTxnResultInfoField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetSalesReceiptLineRetCreditCardTxnInfoCreditCardTxnInputInfo
    {

        private string creditCardNumberField;

        private string expirationMonthField;

        private string expirationYearField;

        private string nameOnCardField;

        private string creditCardAddressField;

        private string creditCardPostalCodeField;

        private string commercialCardCodeField;

        private string transactionModeField;

        private string creditCardTxnTypeField;

        /// <remarks/>
        public string CreditCardNumber
        {
            get
            {
                return creditCardNumberField;
            }
            set
            {
                creditCardNumberField = value;
            }
        }

        /// <remarks/>
        public string ExpirationMonth
        {
            get
            {
                return expirationMonthField;
            }
            set
            {
                expirationMonthField = value;
            }
        }

        /// <remarks/>
        public string ExpirationYear
        {
            get
            {
                return expirationYearField;
            }
            set
            {
                expirationYearField = value;
            }
        }

        /// <remarks/>
        public string NameOnCard
        {
            get
            {
                return nameOnCardField;
            }
            set
            {
                nameOnCardField = value;
            }
        }

        /// <remarks/>
        public string CreditCardAddress
        {
            get
            {
                return creditCardAddressField;
            }
            set
            {
                creditCardAddressField = value;
            }
        }

        /// <remarks/>
        public string CreditCardPostalCode
        {
            get
            {
                return creditCardPostalCodeField;
            }
            set
            {
                creditCardPostalCodeField = value;
            }
        }

        /// <remarks/>
        public string CommercialCardCode
        {
            get
            {
                return commercialCardCodeField;
            }
            set
            {
                commercialCardCodeField = value;
            }
        }

        /// <remarks/>
        public string TransactionMode
        {
            get
            {
                return transactionModeField;
            }
            set
            {
                transactionModeField = value;
            }
        }

        /// <remarks/>
        public string CreditCardTxnType
        {
            get
            {
                return creditCardTxnTypeField;
            }
            set
            {
                creditCardTxnTypeField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetSalesReceiptLineRetCreditCardTxnInfoCreditCardTxnResultInfo
    {

        private string resultCodeField;

        private string resultMessageField;

        private string creditCardTransIDField;

        private string merchantAccountNumberField;

        private string authorizationCodeField;

        private string aVSStreetField;

        private string aVSZipField;

        private string cardSecurityCodeMatchField;

        private string reconBatchIDField;

        private string paymentGroupingCodeField;

        private string paymentStatusField;

        private string txnAuthorizationTimeField;

        private string txnAuthorizationStampField;

        private string clientTransIDField;

        /// <remarks/>
        public string ResultCode
        {
            get
            {
                return resultCodeField;
            }
            set
            {
                resultCodeField = value;
            }
        }

        /// <remarks/>
        public string ResultMessage
        {
            get
            {
                return resultMessageField;
            }
            set
            {
                resultMessageField = value;
            }
        }

        /// <remarks/>
        public string CreditCardTransID
        {
            get
            {
                return creditCardTransIDField;
            }
            set
            {
                creditCardTransIDField = value;
            }
        }

        /// <remarks/>
        public string MerchantAccountNumber
        {
            get
            {
                return merchantAccountNumberField;
            }
            set
            {
                merchantAccountNumberField = value;
            }
        }

        /// <remarks/>
        public string AuthorizationCode
        {
            get
            {
                return authorizationCodeField;
            }
            set
            {
                authorizationCodeField = value;
            }
        }

        /// <remarks/>
        public string AVSStreet
        {
            get
            {
                return aVSStreetField;
            }
            set
            {
                aVSStreetField = value;
            }
        }

        /// <remarks/>
        public string AVSZip
        {
            get
            {
                return aVSZipField;
            }
            set
            {
                aVSZipField = value;
            }
        }

        /// <remarks/>
        public string CardSecurityCodeMatch
        {
            get
            {
                return cardSecurityCodeMatchField;
            }
            set
            {
                cardSecurityCodeMatchField = value;
            }
        }

        /// <remarks/>
        public string ReconBatchID
        {
            get
            {
                return reconBatchIDField;
            }
            set
            {
                reconBatchIDField = value;
            }
        }

        /// <remarks/>
        public string PaymentGroupingCode
        {
            get
            {
                return paymentGroupingCodeField;
            }
            set
            {
                paymentGroupingCodeField = value;
            }
        }

        /// <remarks/>
        public string PaymentStatus
        {
            get
            {
                return paymentStatusField;
            }
            set
            {
                paymentStatusField = value;
            }
        }

        /// <remarks/>
        public string TxnAuthorizationTime
        {
            get
            {
                return txnAuthorizationTimeField;
            }
            set
            {
                txnAuthorizationTimeField = value;
            }
        }

        /// <remarks/>
        public string TxnAuthorizationStamp
        {
            get
            {
                return txnAuthorizationStampField;
            }
            set
            {
                txnAuthorizationStampField = value;
            }
        }

        /// <remarks/>
        public string ClientTransID
        {
            get
            {
                return clientTransIDField;
            }
            set
            {
                clientTransIDField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetSalesReceiptLineRetDataExtRet
    {

        private string ownerIDField;

        private string dataExtNameField;

        private string dataExtTypeField;

        private string dataExtValueField;

        /// <remarks/>
        public string OwnerID
        {
            get
            {
                return ownerIDField;
            }
            set
            {
                ownerIDField = value;
            }
        }

        /// <remarks/>
        public string DataExtName
        {
            get
            {
                return dataExtNameField;
            }
            set
            {
                dataExtNameField = value;
            }
        }

        /// <remarks/>
        public string DataExtType
        {
            get
            {
                return dataExtTypeField;
            }
            set
            {
                dataExtTypeField = value;
            }
        }

        /// <remarks/>
        public string DataExtValue
        {
            get
            {
                return dataExtValueField;
            }
            set
            {
                dataExtValueField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetSalesReceiptLineGroupRet
    {

        private string txnLineIDField;

        private SalesReceiptRetSalesReceiptLineGroupRetItemGroupRef itemGroupRefField;

        private string descField;

        private string quantityField;

        private string unitOfMeasureField;

        private SalesReceiptRetSalesReceiptLineGroupRetOverrideUOMSetRef overrideUOMSetRefField;

        private string isPrintItemsInGroupField;

        private string totalAmountField;

        private SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRet salesReceiptLineRetField;

        private SalesReceiptRetSalesReceiptLineGroupRetDataExtRet dataExtRetField;

        /// <remarks/>
        public string TxnLineID
        {
            get
            {
                return txnLineIDField;
            }
            set
            {
                txnLineIDField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetSalesReceiptLineGroupRetItemGroupRef ItemGroupRef
        {
            get
            {
                return itemGroupRefField;
            }
            set
            {
                itemGroupRefField = value;
            }
        }

        /// <remarks/>
        public string Desc
        {
            get
            {
                return descField;
            }
            set
            {
                descField = value;
            }
        }

        /// <remarks/>
        public string Quantity
        {
            get
            {
                return quantityField;
            }
            set
            {
                quantityField = value;
            }
        }

        /// <remarks/>
        public string UnitOfMeasure
        {
            get
            {
                return unitOfMeasureField;
            }
            set
            {
                unitOfMeasureField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetSalesReceiptLineGroupRetOverrideUOMSetRef OverrideUOMSetRef
        {
            get
            {
                return overrideUOMSetRefField;
            }
            set
            {
                overrideUOMSetRefField = value;
            }
        }

        /// <remarks/>
        public string IsPrintItemsInGroup
        {
            get
            {
                return isPrintItemsInGroupField;
            }
            set
            {
                isPrintItemsInGroupField = value;
            }
        }

        /// <remarks/>
        public string TotalAmount
        {
            get
            {
                return totalAmountField;
            }
            set
            {
                totalAmountField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRet SalesReceiptLineRet
        {
            get
            {
                return salesReceiptLineRetField;
            }
            set
            {
                salesReceiptLineRetField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetSalesReceiptLineGroupRetDataExtRet DataExtRet
        {
            get
            {
                return dataExtRetField;
            }
            set
            {
                dataExtRetField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetSalesReceiptLineGroupRetItemGroupRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetSalesReceiptLineGroupRetOverrideUOMSetRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRet
    {

        private string txnLineIDField;

        private SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetItemRef itemRefField;

        private string descField;

        private string quantityField;

        private string unitOfMeasureField;

        private SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetOverrideUOMSetRef overrideUOMSetRefField;

        private string rateField;

        private string ratePercentField;

        private SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetClassRef classRefField;

        private string amountField;

        private SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetInventorySiteRef inventorySiteRefField;

        private SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetInventorySiteLocationRef inventorySiteLocationRefField;

        private string serialNumberField;

        private string lotNumberField;

        private string serviceDateField;

        private SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetSalesTaxCodeRef salesTaxCodeRefField;

        private string other1Field;

        private string other2Field;

        private SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetCreditCardTxnInfo creditCardTxnInfoField;

        private SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetDataExtRet dataExtRetField;

        /// <remarks/>
        public string TxnLineID
        {
            get
            {
                return txnLineIDField;
            }
            set
            {
                txnLineIDField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetItemRef ItemRef
        {
            get
            {
                return itemRefField;
            }
            set
            {
                itemRefField = value;
            }
        }

        /// <remarks/>
        public string Desc
        {
            get
            {
                return descField;
            }
            set
            {
                descField = value;
            }
        }

        /// <remarks/>
        public string Quantity
        {
            get
            {
                return quantityField;
            }
            set
            {
                quantityField = value;
            }
        }

        /// <remarks/>
        public string UnitOfMeasure
        {
            get
            {
                return unitOfMeasureField;
            }
            set
            {
                unitOfMeasureField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetOverrideUOMSetRef OverrideUOMSetRef
        {
            get
            {
                return overrideUOMSetRefField;
            }
            set
            {
                overrideUOMSetRefField = value;
            }
        }

        /// <remarks/>
        public string Rate
        {
            get
            {
                return rateField;
            }
            set
            {
                rateField = value;
            }
        }

        /// <remarks/>
        public string RatePercent
        {
            get
            {
                return ratePercentField;
            }
            set
            {
                ratePercentField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetClassRef ClassRef
        {
            get
            {
                return classRefField;
            }
            set
            {
                classRefField = value;
            }
        }

        /// <remarks/>
        public string Amount
        {
            get
            {
                return amountField;
            }
            set
            {
                amountField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetInventorySiteRef InventorySiteRef
        {
            get
            {
                return inventorySiteRefField;
            }
            set
            {
                inventorySiteRefField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetInventorySiteLocationRef InventorySiteLocationRef
        {
            get
            {
                return inventorySiteLocationRefField;
            }
            set
            {
                inventorySiteLocationRefField = value;
            }
        }

        /// <remarks/>
        public string SerialNumber
        {
            get
            {
                return serialNumberField;
            }
            set
            {
                serialNumberField = value;
            }
        }

        /// <remarks/>
        public string LotNumber
        {
            get
            {
                return lotNumberField;
            }
            set
            {
                lotNumberField = value;
            }
        }

        /// <remarks/>
        public string ServiceDate
        {
            get
            {
                return serviceDateField;
            }
            set
            {
                serviceDateField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetSalesTaxCodeRef SalesTaxCodeRef
        {
            get
            {
                return salesTaxCodeRefField;
            }
            set
            {
                salesTaxCodeRefField = value;
            }
        }

        /// <remarks/>
        public string Other1
        {
            get
            {
                return other1Field;
            }
            set
            {
                other1Field = value;
            }
        }

        /// <remarks/>
        public string Other2
        {
            get
            {
                return other2Field;
            }
            set
            {
                other2Field = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetCreditCardTxnInfo CreditCardTxnInfo
        {
            get
            {
                return creditCardTxnInfoField;
            }
            set
            {
                creditCardTxnInfoField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetDataExtRet DataExtRet
        {
            get
            {
                return dataExtRetField;
            }
            set
            {
                dataExtRetField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetItemRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetOverrideUOMSetRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetClassRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetInventorySiteRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetInventorySiteLocationRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetSalesTaxCodeRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetCreditCardTxnInfo
    {

        private SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetCreditCardTxnInfoCreditCardTxnInputInfo creditCardTxnInputInfoField;

        private SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetCreditCardTxnInfoCreditCardTxnResultInfo creditCardTxnResultInfoField;

        /// <remarks/>
        public SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetCreditCardTxnInfoCreditCardTxnInputInfo CreditCardTxnInputInfo
        {
            get
            {
                return creditCardTxnInputInfoField;
            }
            set
            {
                creditCardTxnInputInfoField = value;
            }
        }

        /// <remarks/>
        public SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetCreditCardTxnInfoCreditCardTxnResultInfo CreditCardTxnResultInfo
        {
            get
            {
                return creditCardTxnResultInfoField;
            }
            set
            {
                creditCardTxnResultInfoField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetCreditCardTxnInfoCreditCardTxnInputInfo
    {

        private string creditCardNumberField;

        private string expirationMonthField;

        private string expirationYearField;

        private string nameOnCardField;

        private string creditCardAddressField;

        private string creditCardPostalCodeField;

        private string commercialCardCodeField;

        private string transactionModeField;

        private string creditCardTxnTypeField;

        /// <remarks/>
        public string CreditCardNumber
        {
            get
            {
                return creditCardNumberField;
            }
            set
            {
                creditCardNumberField = value;
            }
        }

        /// <remarks/>
        public string ExpirationMonth
        {
            get
            {
                return expirationMonthField;
            }
            set
            {
                expirationMonthField = value;
            }
        }

        /// <remarks/>
        public string ExpirationYear
        {
            get
            {
                return expirationYearField;
            }
            set
            {
                expirationYearField = value;
            }
        }

        /// <remarks/>
        public string NameOnCard
        {
            get
            {
                return nameOnCardField;
            }
            set
            {
                nameOnCardField = value;
            }
        }

        /// <remarks/>
        public string CreditCardAddress
        {
            get
            {
                return creditCardAddressField;
            }
            set
            {
                creditCardAddressField = value;
            }
        }

        /// <remarks/>
        public string CreditCardPostalCode
        {
            get
            {
                return creditCardPostalCodeField;
            }
            set
            {
                creditCardPostalCodeField = value;
            }
        }

        /// <remarks/>
        public string CommercialCardCode
        {
            get
            {
                return commercialCardCodeField;
            }
            set
            {
                commercialCardCodeField = value;
            }
        }

        /// <remarks/>
        public string TransactionMode
        {
            get
            {
                return transactionModeField;
            }
            set
            {
                transactionModeField = value;
            }
        }

        /// <remarks/>
        public string CreditCardTxnType
        {
            get
            {
                return creditCardTxnTypeField;
            }
            set
            {
                creditCardTxnTypeField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetCreditCardTxnInfoCreditCardTxnResultInfo
    {

        private string resultCodeField;

        private string resultMessageField;

        private string creditCardTransIDField;

        private string merchantAccountNumberField;

        private string authorizationCodeField;

        private string aVSStreetField;

        private string aVSZipField;

        private string cardSecurityCodeMatchField;

        private string reconBatchIDField;

        private string paymentGroupingCodeField;

        private string paymentStatusField;

        private string txnAuthorizationTimeField;

        private string txnAuthorizationStampField;

        private string clientTransIDField;

        /// <remarks/>
        public string ResultCode
        {
            get
            {
                return resultCodeField;
            }
            set
            {
                resultCodeField = value;
            }
        }

        /// <remarks/>
        public string ResultMessage
        {
            get
            {
                return resultMessageField;
            }
            set
            {
                resultMessageField = value;
            }
        }

        /// <remarks/>
        public string CreditCardTransID
        {
            get
            {
                return creditCardTransIDField;
            }
            set
            {
                creditCardTransIDField = value;
            }
        }

        /// <remarks/>
        public string MerchantAccountNumber
        {
            get
            {
                return merchantAccountNumberField;
            }
            set
            {
                merchantAccountNumberField = value;
            }
        }

        /// <remarks/>
        public string AuthorizationCode
        {
            get
            {
                return authorizationCodeField;
            }
            set
            {
                authorizationCodeField = value;
            }
        }

        /// <remarks/>
        public string AVSStreet
        {
            get
            {
                return aVSStreetField;
            }
            set
            {
                aVSStreetField = value;
            }
        }

        /// <remarks/>
        public string AVSZip
        {
            get
            {
                return aVSZipField;
            }
            set
            {
                aVSZipField = value;
            }
        }

        /// <remarks/>
        public string CardSecurityCodeMatch
        {
            get
            {
                return cardSecurityCodeMatchField;
            }
            set
            {
                cardSecurityCodeMatchField = value;
            }
        }

        /// <remarks/>
        public string ReconBatchID
        {
            get
            {
                return reconBatchIDField;
            }
            set
            {
                reconBatchIDField = value;
            }
        }

        /// <remarks/>
        public string PaymentGroupingCode
        {
            get
            {
                return paymentGroupingCodeField;
            }
            set
            {
                paymentGroupingCodeField = value;
            }
        }

        /// <remarks/>
        public string PaymentStatus
        {
            get
            {
                return paymentStatusField;
            }
            set
            {
                paymentStatusField = value;
            }
        }

        /// <remarks/>
        public string TxnAuthorizationTime
        {
            get
            {
                return txnAuthorizationTimeField;
            }
            set
            {
                txnAuthorizationTimeField = value;
            }
        }

        /// <remarks/>
        public string TxnAuthorizationStamp
        {
            get
            {
                return txnAuthorizationStampField;
            }
            set
            {
                txnAuthorizationStampField = value;
            }
        }

        /// <remarks/>
        public string ClientTransID
        {
            get
            {
                return clientTransIDField;
            }
            set
            {
                clientTransIDField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetSalesReceiptLineGroupRetSalesReceiptLineRetDataExtRet
    {

        private string ownerIDField;

        private string dataExtNameField;

        private string dataExtTypeField;

        private string dataExtValueField;

        /// <remarks/>
        public string OwnerID
        {
            get
            {
                return ownerIDField;
            }
            set
            {
                ownerIDField = value;
            }
        }

        /// <remarks/>
        public string DataExtName
        {
            get
            {
                return dataExtNameField;
            }
            set
            {
                dataExtNameField = value;
            }
        }

        /// <remarks/>
        public string DataExtType
        {
            get
            {
                return dataExtTypeField;
            }
            set
            {
                dataExtTypeField = value;
            }
        }

        /// <remarks/>
        public string DataExtValue
        {
            get
            {
                return dataExtValueField;
            }
            set
            {
                dataExtValueField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetSalesReceiptLineGroupRetDataExtRet
    {

        private string ownerIDField;

        private string dataExtNameField;

        private string dataExtTypeField;

        private string dataExtValueField;

        /// <remarks/>
        public string OwnerID
        {
            get
            {
                return ownerIDField;
            }
            set
            {
                ownerIDField = value;
            }
        }

        /// <remarks/>
        public string DataExtName
        {
            get
            {
                return dataExtNameField;
            }
            set
            {
                dataExtNameField = value;
            }
        }

        /// <remarks/>
        public string DataExtType
        {
            get
            {
                return dataExtTypeField;
            }
            set
            {
                dataExtTypeField = value;
            }
        }

        /// <remarks/>
        public string DataExtValue
        {
            get
            {
                return dataExtValueField;
            }
            set
            {
                dataExtValueField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class SalesReceiptRetDataExtRet
    {

        private string ownerIDField;

        private string dataExtNameField;

        private string dataExtTypeField;

        private string dataExtValueField;

        /// <remarks/>
        public string OwnerID
        {
            get
            {
                return ownerIDField;
            }
            set
            {
                ownerIDField = value;
            }
        }

        /// <remarks/>
        public string DataExtName
        {
            get
            {
                return dataExtNameField;
            }
            set
            {
                dataExtNameField = value;
            }
        }

        /// <remarks/>
        public string DataExtType
        {
            get
            {
                return dataExtTypeField;
            }
            set
            {
                dataExtTypeField = value;
            }
        }

        /// <remarks/>
        public string DataExtValue
        {
            get
            {
                return dataExtValueField;
            }
            set
            {
                dataExtValueField = value;
            }
        }
    }



}
