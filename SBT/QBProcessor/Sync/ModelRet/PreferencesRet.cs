﻿using System;
using System.Xml.Serialization;

namespace DataProcessor.Sync.ModelRet
{

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public partial class PreferencesRet
    {


        public AccountingPreferences AccountingPreferences { get; set; }

        public FinanceChargePreferences FinanceChargePreferences { get; set; }

        public JobsAndEstimatesPreferences JobsAndEstimatesPreferences { get; set; }

        public MultiCurrencyPreferences MultiCurrencyPreferences { get; set; }

        public MultiLocationInventoryPreferences MultiLocationInventoryPreferences { get; set; }

        public PurchasesAndVendorsPreferences PurchasesAndVendorsPreferences { get; set; }

        public ReportsPreferences ReportsPreferences { get; set; }

        public SalesAndCustomersPreferences SalesAndCustomersPreferences { get; set; }

        public TimeTrackingPreferences TimeTrackingPreferences { get; set; }

        public CurrentAppAccessRights CurrentAppAccessRights { get; set; }

        public ItemsAndInventoryPreferences ItemsAndInventoryPreferences { get; set; }

        


    }
    public class AccountingPreferences
    {
        [XmlElement(ElementName = "IsUsingAccountNumbers")]
        public string IsUsingAccountNumbers { get; set; }
        [XmlElement(ElementName = "IsRequiringAccounts")]
        public string IsRequiringAccounts { get; set; }
        [XmlElement(ElementName = "IsUsingClassTracking")]
        public string IsUsingClassTracking { get; set; }
        [XmlElement(ElementName = "IsUsingAuditTrail")]
        public string IsUsingAuditTrail { get; set; }
        [XmlElement(ElementName = "IsAssigningJournalEntryNumbers")]
        public string IsAssigningJournalEntryNumbers { get; set; }
        [XmlElement(ElementName = "ClosingDate")]
        public string ClosingDate { get; set; }
    }

    [XmlRoot(ElementName = "FinanceChargePreferences")]
    public class FinanceChargePreferences
    {
        [XmlElement(ElementName = "AnnualInterestRate")]
        public string AnnualInterestRate { get; set; }
        [XmlElement(ElementName = "MinFinanceCharge")]
        public string MinFinanceCharge { get; set; }
        [XmlElement(ElementName = "GracePeriod")]
        public string GracePeriod { get; set; }
        [XmlElement(ElementName = "IsAssessingForOverdueCharges")]
        public string IsAssessingForOverdueCharges { get; set; }
        [XmlElement(ElementName = "CalculateChargesFrom")]
        public string CalculateChargesFrom { get; set; }
        [XmlElement(ElementName = "IsMarkedToBePrinted")]
        public string IsMarkedToBePrinted { get; set; }
    }

    [XmlRoot(ElementName = "JobsAndEstimatesPreferences")]
    public class JobsAndEstimatesPreferences
    {
        [XmlElement(ElementName = "IsUsingEstimates")]
        public string IsUsingEstimates { get; set; }
        [XmlElement(ElementName = "IsUsingProgressInvoicing")]
        public string IsUsingProgressInvoicing { get; set; }
        [XmlElement(ElementName = "IsPrintingItemsWithZeroAmounts")]
        public string IsPrintingItemsWithZeroAmounts { get; set; }
    }

    [XmlRoot(ElementName = "MultiCurrencyPreferences")]
    public class MultiCurrencyPreferences
    {
        [XmlElement(ElementName = "IsMultiCurrencyOn")]
        public string IsMultiCurrencyOn { get; set; }
    }

    [XmlRoot(ElementName = "MultiLocationInventoryPreferences")]
    public class MultiLocationInventoryPreferences
    {
        [XmlElement(ElementName = "IsMultiLocationInventoryAvailable")]
        public string IsMultiLocationInventoryAvailable { get; set; }
        [XmlElement(ElementName = "IsMultiLocationInventoryEnabled")]
        public string IsMultiLocationInventoryEnabled { get; set; }
    }

    [XmlRoot(ElementName = "PurchasesAndVendorsPreferences")]
    public class PurchasesAndVendorsPreferences
    {
        [XmlElement(ElementName = "IsUsingInventory")]
        public string IsUsingInventory { get; set; }
        [XmlElement(ElementName = "DaysBillsAreDue")]
        public string DaysBillsAreDue { get; set; }
        [XmlElement(ElementName = "IsAutomaticallyUsingDiscounts")]
        public string IsAutomaticallyUsingDiscounts { get; set; }
    }

    [XmlRoot(ElementName = "ReportsPreferences")]
    public class ReportsPreferences
    {
        [XmlElement(ElementName = "AgingReportBasis")]
        public string AgingReportBasis { get; set; }
        [XmlElement(ElementName = "SummaryReportBasis")]
        public string SummaryReportBasis { get; set; }
    }

    [XmlRoot(ElementName = "PriceLevels")]
    public class PriceLevels
    {
        [XmlElement(ElementName = "IsUsingPriceLevels")]
        public string IsUsingPriceLevels { get; set; }
        [XmlElement(ElementName = "IsRoundingSalesPriceUp")]
        public string IsRoundingSalesPriceUp { get; set; }
    }

    [XmlRoot(ElementName = "SalesAndCustomersPreferences")]
    public class SalesAndCustomersPreferences
    {
        [XmlElement(ElementName = "IsTrackingReimbursedExpensesAsIncome")]
        public string IsTrackingReimbursedExpensesAsIncome { get; set; }
        [XmlElement(ElementName = "IsAutoApplyingPayments")]
        public string IsAutoApplyingPayments { get; set; }
        [XmlElement(ElementName = "PriceLevels")]
        public PriceLevels PriceLevels { get; set; }
    }

    [XmlRoot(ElementName = "TimeTrackingPreferences")]
    public class TimeTrackingPreferences
    {
        [XmlElement(ElementName = "FirstDayOfWeek")]
        public string FirstDayOfWeek { get; set; }
    }

    [XmlRoot(ElementName = "CurrentAppAccessRights")]
    public class CurrentAppAccessRights
    {
        [XmlElement(ElementName = "IsAutomaticLoginAllowed")]
        public string IsAutomaticLoginAllowed { get; set; }
        [XmlElement(ElementName = "AutomaticLoginUserName")]
        public string AutomaticLoginUserName { get; set; }
        [XmlElement(ElementName = "IsPersonalDataAccessAllowed")]
        public string IsPersonalDataAccessAllowed { get; set; }
    }

    [XmlRoot(ElementName = "ItemsAndInventoryPreferences")]
    public class ItemsAndInventoryPreferences
    {
        [XmlElement(ElementName = "EnhancedInventoryReceivingEnabled")]
        public string EnhancedInventoryReceivingEnabled { get; set; }
        [XmlElement(ElementName = "IsTrackingSerialOrLotNumber")]
        public string IsTrackingSerialOrLotNumber { get; set; }
        [XmlElement(ElementName = "FIFOEnabled")]
        public string FIFOEnabled { get; set; }
        [XmlElement(ElementName = "IsRSBEnabled")]
        public string IsRSBEnabled { get; set; }
        [XmlElement(ElementName = "IsBarcodeEnabled")]
        public string IsBarcodeEnabled { get; set; }
    }


}
