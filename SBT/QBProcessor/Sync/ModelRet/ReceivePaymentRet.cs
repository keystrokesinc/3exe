﻿using System;
using System.Xml.Serialization;

namespace DataProcessor.Sync.ModelRet
{

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "" , IsNullable = false)]
    public partial class ReceivePaymentRet
    {

        private string txnIDField;

        private DateTime? timeCreatedField;

        private DateTime timeModifiedField;

        private string editSequenceField;

        private string txnNumberField;

        private ReceivePaymentRetCustomerRef customerRefField;

        private ReceivePaymentRetARAccountRef aRAccountRefField;

        private DateTime? txnDateField;

        private string refNumberField;

        private decimal? totalAmountField;

        private ReceivePaymentRetCurrencyRef currencyRefField;

        private string exchangeRateField;

        private string totalAmountInHomeCurrencyField;

        private ReceivePaymentRetPaymentMethodRef paymentMethodRefField;

        private string memoField;

        private ReceivePaymentRetDepositToAccountRef depositToAccountRefField;

        private ReceivePaymentRetCreditCardTxnInfo creditCardTxnInfoField;

        private decimal? unusedPaymentField;

        private decimal? unusedCreditsField;

        private string externalGUIDField;

        private ReceivePaymentRetAppliedToTxnRet[] appliedToTxnRetField;

        private ReceivePaymentRetDataExtRet dataExtRetField;

        /// <remarks/>
        public string TxnID
        {
            get
            {
                return txnIDField;
            }
            set
            {
                txnIDField = value;
            }
        }

        /// <remarks/>
        public DateTime? TimeCreated
        {
            get
            {
                return timeCreatedField;
            }
            set
            {
                timeCreatedField = value;
            }
        }

        /// <remarks/>
        public DateTime TimeModified
        {
            get
            {
                return timeModifiedField;
            }
            set
            {
                timeModifiedField = value;
            }
        }

        /// <remarks/>
        public string EditSequence
        {
            get
            {
                return editSequenceField;
            }
            set
            {
                editSequenceField = value;
            }
        }

        /// <remarks/>
        public string TxnNumber
        {
            get
            {
                return txnNumberField;
            }
            set
            {
                txnNumberField = value;
            }
        }

        /// <remarks/>
        public ReceivePaymentRetCustomerRef CustomerRef
        {
            get
            {
                return customerRefField;
            }
            set
            {
                customerRefField = value;
            }
        }

        /// <remarks/>
        public ReceivePaymentRetARAccountRef ARAccountRef
        {
            get
            {
                return aRAccountRefField;
            }
            set
            {
                aRAccountRefField = value;
            }
        }

        /// <remarks/>
        public DateTime? TxnDate
        {
            get
            {
                return txnDateField;
            }
            set
            {
                txnDateField = value;
            }
        }

        /// <remarks/>
        public string RefNumber
        {
            get
            {
                return refNumberField;
            }
            set
            {
                refNumberField = value;
            }
        }

        /// <remarks/>
        public decimal? TotalAmount
        {
            get
            {
                return totalAmountField;
            }
            set
            {
                totalAmountField = value;
            }
        }

        /// <remarks/>
        public ReceivePaymentRetCurrencyRef CurrencyRef
        {
            get
            {
                return currencyRefField;
            }
            set
            {
                currencyRefField = value;
            }
        }

        /// <remarks/>
        public string ExchangeRate
        {
            get
            {
                return exchangeRateField;
            }
            set
            {
                exchangeRateField = value;
            }
        }

        /// <remarks/>
        public string TotalAmountInHomeCurrency
        {
            get
            {
                return totalAmountInHomeCurrencyField;
            }
            set
            {
                totalAmountInHomeCurrencyField = value;
            }
        }

        /// <remarks/>
        public ReceivePaymentRetPaymentMethodRef PaymentMethodRef
        {
            get
            {
                return paymentMethodRefField;
            }
            set
            {
                paymentMethodRefField = value;
            }
        }

        /// <remarks/>
        public string Memo
        {
            get
            {
                return memoField;
            }
            set
            {
                memoField = value;
            }
        }

        /// <remarks/>
        public ReceivePaymentRetDepositToAccountRef DepositToAccountRef
        {
            get
            {
                return depositToAccountRefField;
            }
            set
            {
                depositToAccountRefField = value;
            }
        }

        /// <remarks/>
        public ReceivePaymentRetCreditCardTxnInfo CreditCardTxnInfo
        {
            get
            {
                return creditCardTxnInfoField;
            }
            set
            {
                creditCardTxnInfoField = value;
            }
        }

        /// <remarks/>
        public decimal? UnusedPayment
        {
            get
            {
                return unusedPaymentField;
            }
            set
            {
                unusedPaymentField = value;
            }
        }

        /// <remarks/>
        public decimal? UnusedCredits
        {
            get
            {
                return unusedCreditsField;
            }
            set
            {
                unusedCreditsField = value;
            }
        }

        /// <remarks/>
        public string ExternalGUID
        {
            get
            {
                return externalGUIDField;
            }
            set
            {
                externalGUIDField = value;
            }
        }

        [XmlElement("AppliedToTxnRet")]
        public ReceivePaymentRetAppliedToTxnRet[] AppliedToTxnRet
        {
            get
            {
                return appliedToTxnRetField;
            }
            set
            {
                appliedToTxnRetField = value;
            }
        }

        /// <remarks/>
        public ReceivePaymentRetDataExtRet DataExtRet
        {
            get
            {
                return dataExtRetField;
            }
            set
            {
                dataExtRetField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ReceivePaymentRetCustomerRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ReceivePaymentRetARAccountRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ReceivePaymentRetCurrencyRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ReceivePaymentRetPaymentMethodRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ReceivePaymentRetDepositToAccountRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ReceivePaymentRetCreditCardTxnInfo
    {

        private ReceivePaymentRetCreditCardTxnInfoCreditCardTxnInputInfo creditCardTxnInputInfoField;

        private ReceivePaymentRetCreditCardTxnInfoCreditCardTxnResultInfo creditCardTxnResultInfoField;

        /// <remarks/>
        public ReceivePaymentRetCreditCardTxnInfoCreditCardTxnInputInfo CreditCardTxnInputInfo
        {
            get
            {
                return creditCardTxnInputInfoField;
            }
            set
            {
                creditCardTxnInputInfoField = value;
            }
        }

        /// <remarks/>
        public ReceivePaymentRetCreditCardTxnInfoCreditCardTxnResultInfo CreditCardTxnResultInfo
        {
            get
            {
                return creditCardTxnResultInfoField;
            }
            set
            {
                creditCardTxnResultInfoField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ReceivePaymentRetCreditCardTxnInfoCreditCardTxnInputInfo
    {

        private string creditCardNumberField;

        private string expirationMonthField;

        private string expirationYearField;

        private string nameOnCardField;

        private string creditCardAddressField;

        private string creditCardPostalCodeField;

        private string commercialCardCodeField;

        private string transactionModeField;

        private string creditCardTxnTypeField;

        /// <remarks/>
        public string CreditCardNumber
        {
            get
            {
                return creditCardNumberField;
            }
            set
            {
                creditCardNumberField = value;
            }
        }

        /// <remarks/>
        public string ExpirationMonth
        {
            get
            {
                return expirationMonthField;
            }
            set
            {
                expirationMonthField = value;
            }
        }

        /// <remarks/>
        public string ExpirationYear
        {
            get
            {
                return expirationYearField;
            }
            set
            {
                expirationYearField = value;
            }
        }

        /// <remarks/>
        public string NameOnCard
        {
            get
            {
                return nameOnCardField;
            }
            set
            {
                nameOnCardField = value;
            }
        }

        /// <remarks/>
        public string CreditCardAddress
        {
            get
            {
                return creditCardAddressField;
            }
            set
            {
                creditCardAddressField = value;
            }
        }

        /// <remarks/>
        public string CreditCardPostalCode
        {
            get
            {
                return creditCardPostalCodeField;
            }
            set
            {
                creditCardPostalCodeField = value;
            }
        }

        /// <remarks/>
        public string CommercialCardCode
        {
            get
            {
                return commercialCardCodeField;
            }
            set
            {
                commercialCardCodeField = value;
            }
        }

        /// <remarks/>
        public string TransactionMode
        {
            get
            {
                return transactionModeField;
            }
            set
            {
                transactionModeField = value;
            }
        }

        /// <remarks/>
        public string CreditCardTxnType
        {
            get
            {
                return creditCardTxnTypeField;
            }
            set
            {
                creditCardTxnTypeField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ReceivePaymentRetCreditCardTxnInfoCreditCardTxnResultInfo
    {

        private string resultCodeField;

        private string resultMessageField;

        private string creditCardTransIDField;

        private string merchantAccountNumberField;

        private string authorizationCodeField;

        private string aVSStreetField;

        private string aVSZipField;

        private string cardSecurityCodeMatchField;

        private string reconBatchIDField;

        private string paymentGroupingCodeField;

        private string paymentStatusField;

        private string txnAuthorizationTimeField;

        private string txnAuthorizationStampField;

        private string clientTransIDField;

        /// <remarks/>
        public string ResultCode
        {
            get
            {
                return resultCodeField;
            }
            set
            {
                resultCodeField = value;
            }
        }

        /// <remarks/>
        public string ResultMessage
        {
            get
            {
                return resultMessageField;
            }
            set
            {
                resultMessageField = value;
            }
        }

        /// <remarks/>
        public string CreditCardTransID
        {
            get
            {
                return creditCardTransIDField;
            }
            set
            {
                creditCardTransIDField = value;
            }
        }

        /// <remarks/>
        public string MerchantAccountNumber
        {
            get
            {
                return merchantAccountNumberField;
            }
            set
            {
                merchantAccountNumberField = value;
            }
        }

        /// <remarks/>
        public string AuthorizationCode
        {
            get
            {
                return authorizationCodeField;
            }
            set
            {
                authorizationCodeField = value;
            }
        }

        /// <remarks/>
        public string AVSStreet
        {
            get
            {
                return aVSStreetField;
            }
            set
            {
                aVSStreetField = value;
            }
        }

        /// <remarks/>
        public string AVSZip
        {
            get
            {
                return aVSZipField;
            }
            set
            {
                aVSZipField = value;
            }
        }

        /// <remarks/>
        public string CardSecurityCodeMatch
        {
            get
            {
                return cardSecurityCodeMatchField;
            }
            set
            {
                cardSecurityCodeMatchField = value;
            }
        }

        /// <remarks/>
        public string ReconBatchID
        {
            get
            {
                return reconBatchIDField;
            }
            set
            {
                reconBatchIDField = value;
            }
        }

        /// <remarks/>
        public string PaymentGroupingCode
        {
            get
            {
                return paymentGroupingCodeField;
            }
            set
            {
                paymentGroupingCodeField = value;
            }
        }

        /// <remarks/>
        public string PaymentStatus
        {
            get
            {
                return paymentStatusField;
            }
            set
            {
                paymentStatusField = value;
            }
        }

        /// <remarks/>
        public string TxnAuthorizationTime
        {
            get
            {
                return txnAuthorizationTimeField;
            }
            set
            {
                txnAuthorizationTimeField = value;
            }
        }

        /// <remarks/>
        public string TxnAuthorizationStamp
        {
            get
            {
                return txnAuthorizationStampField;
            }
            set
            {
                txnAuthorizationStampField = value;
            }
        }

        /// <remarks/>
        public string ClientTransID
        {
            get
            {
                return clientTransIDField;
            }
            set
            {
                clientTransIDField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ReceivePaymentRetAppliedToTxnRet
    {

        private string txnIDField;

        private string txnTypeField;

        private DateTime? txnDateField;

        private string refNumberField;

        private decimal? balanceRemainingField;

        private decimal amountField;

        private string discountAmountField;

        private ReceivePaymentRetAppliedToTxnRetDiscountAccountRef discountAccountRefField;

        private ReceivePaymentRetAppliedToTxnRetDiscountClassRef discountClassRefField;

        private ReceivePaymentRetAppliedToTxnRetLinkedTxn linkedTxnField;

        /// <remarks/>
        public string TxnID
        {
            get
            {
                return txnIDField;
            }
            set
            {
                txnIDField = value;
            }
        }

        /// <remarks/>
        public string TxnType
        {
            get
            {
                return txnTypeField;
            }
            set
            {
                txnTypeField = value;
            }
        }

        /// <remarks/>
        public DateTime? TxnDate
        {
            get
            {
                return txnDateField;
            }
            set
            {
                txnDateField = value;
            }
        }

        /// <remarks/>
        public string RefNumber
        {
            get
            {
                return refNumberField;
            }
            set
            {
                refNumberField = value;
            }
        }

        /// <remarks/>
        public decimal? BalanceRemaining
        {
            get
            {
                return balanceRemainingField;
            }
            set
            {
                balanceRemainingField = value;
            }
        }

        /// <remarks/>
        public decimal Amount
        {
            get
            {
                return amountField;
            }
            set
            {
                amountField = value;
            }
        }

        /// <remarks/>
        public string DiscountAmount
        {
            get
            {
                return discountAmountField;
            }
            set
            {
                discountAmountField = value;
            }
        }

        /// <remarks/>
        public ReceivePaymentRetAppliedToTxnRetDiscountAccountRef DiscountAccountRef
        {
            get
            {
                return discountAccountRefField;
            }
            set
            {
                discountAccountRefField = value;
            }
        }

        /// <remarks/>
        public ReceivePaymentRetAppliedToTxnRetDiscountClassRef DiscountClassRef
        {
            get
            {
                return discountClassRefField;
            }
            set
            {
                discountClassRefField = value;
            }
        }

        /// <remarks/>
        public ReceivePaymentRetAppliedToTxnRetLinkedTxn LinkedTxn
        {
            get
            {
                return linkedTxnField;
            }
            set
            {
                linkedTxnField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ReceivePaymentRetAppliedToTxnRetDiscountAccountRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ReceivePaymentRetAppliedToTxnRetDiscountClassRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ReceivePaymentRetAppliedToTxnRetLinkedTxn
    {

        private string txnIDField;

        private string txnTypeField;

        private string txnDateField;

        private string refNumberField;

        private string linkTypeField;

        private string amountField;

        /// <remarks/>
        public string TxnID
        {
            get
            {
                return txnIDField;
            }
            set
            {
                txnIDField = value;
            }
        }

        /// <remarks/>
        public string TxnType
        {
            get
            {
                return txnTypeField;
            }
            set
            {
                txnTypeField = value;
            }
        }

        /// <remarks/>
        public string TxnDate
        {
            get
            {
                return txnDateField;
            }
            set
            {
                txnDateField = value;
            }
        }

        /// <remarks/>
        public string RefNumber
        {
            get
            {
                return refNumberField;
            }
            set
            {
                refNumberField = value;
            }
        }

        /// <remarks/>
        public string LinkType
        {
            get
            {
                return linkTypeField;
            }
            set
            {
                linkTypeField = value;
            }
        }

        /// <remarks/>
        public string Amount
        {
            get
            {
                return amountField;
            }
            set
            {
                amountField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class ReceivePaymentRetDataExtRet
    {

        private string ownerIDField;

        private string dataExtNameField;

        private string dataExtTypeField;

        private string dataExtValueField;

        /// <remarks/>
        public string OwnerID
        {
            get
            {
                return ownerIDField;
            }
            set
            {
                ownerIDField = value;
            }
        }

        /// <remarks/>
        public string DataExtName
        {
            get
            {
                return dataExtNameField;
            }
            set
            {
                dataExtNameField = value;
            }
        }

        /// <remarks/>
        public string DataExtType
        {
            get
            {
                return dataExtTypeField;
            }
            set
            {
                dataExtTypeField = value;
            }
        }

        /// <remarks/>
        public string DataExtValue
        {
            get
            {
                return dataExtValueField;
            }
            set
            {
                dataExtValueField = value;
            }
        }
    }


}
