﻿
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
public class CreditCardChargeRet
{

    private string txnIDField;
    public string TxnID
    {
        get
        {
            return this.txnIDField;
        }
        set
        {
            this.txnIDField = value;
        }
    }

}

