﻿using System;
using System.Xml.Serialization;

namespace DataProcessor.Sync.ModelRet
{

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public partial class CustomerRet
    {

        private string listIDField;

        private DateTime? timeCreatedField;

        private DateTime? timeModifiedField;

        private string editSequenceField;

        private string nameField;

        private string fullNameField;

        private bool? isActiveField;

        private CustomerRetClassRef classRefField;

        private CustomerRetParentRef parentRefField;

        private int sublevelField;

        private string companyNameField;

        private string salutationField;

        private string firstNameField;

        private string middleNameField;

        private string lastNameField;

        private string jobTitleField;

        private CustomerRetBillAddress billAddressField;

        private CustomerRetBillAddressBlock billAddressBlockField;

        private CustomerRetShipAddress shipAddressField;

        private CustomerRetShipAddressBlock shipAddressBlockField;

        private CustomerRetShipToAddress shipToAddressField;

        private string phoneField;

        private string altPhoneField;

        private string faxField;

        private string emailField;

        private string ccField;

        private string contactField;

        private string altContactField;

        private CustomerRetAdditionalContactRef additionalContactRefField;

        private CustomerRetContactsRet contactsRetField;

        private CustomerRetCustomerTypeRef customerTypeRefField;

        private CustomerRetTermsRef termsRefField;

        private CustomerRetSalesRepRef salesRepRefField;

        private string balanceField;

        private string totalBalanceField;

        private CustomerRetSalesTaxCodeRef salesTaxCodeRefField;

        private CustomerRetItemSalesTaxRef itemSalesTaxRefField;

        private string resaleNumberField;

        private string accountNumberField;

        private string creditLimitField;

        private CustomerRetPreferredPaymentMethodRef preferredPaymentMethodRefField;

        private CustomerRetCreditCardInfo creditCardInfoField;

        private string jobStatusField;

        private string jobStartDateField;

        private string jobProjectedEndDateField;

        private string jobEndDateField;

        private string jobDescField;

        private CustomerRetJobTypeRef jobTypeRefField;

        private string notesField;

        private CustomerRetAdditionalNotesRet additionalNotesRetField;

        private string preferredDeliveryMethodField;

        private CustomerRetPriceLevelRef priceLevelRefField;

        private string externalGUIDField;

        private CustomerRetCurrencyRef currencyRefField;

        private CustomerRetDataExtRet dataExtRetField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public DateTime? TimeCreated
        {
            get
            {
                return timeCreatedField;
            }
            set
            {
                timeCreatedField = value;
            }
        }

        /// <remarks/>
        public DateTime? TimeModified
        {
            get
            {
                return timeModifiedField;
            }
            set
            {
                timeModifiedField = value;
            }
        }

        /// <remarks/>
        public string EditSequence
        {
            get
            {
                return editSequenceField;
            }
            set
            {
                editSequenceField = value;
            }
        }

        /// <remarks/>
        public string Name
        {
            get
            {
                return nameField;
            }
            set
            {
                nameField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }

        /// <remarks/>
        public bool? IsActive
        {
            get
            {
                return isActiveField;
            }
            set
            {
                isActiveField = value;
            }
        }

        /// <remarks/>
        public CustomerRetClassRef ClassRef
        {
            get
            {
                return classRefField;
            }
            set
            {
                classRefField = value;
            }
        }

        /// <remarks/>
        public CustomerRetParentRef ParentRef
        {
            get
            {
                return parentRefField;
            }
            set
            {
                parentRefField = value;
            }
        }

        /// <remarks/>
        public int Sublevel
        {
            get
            {
                return sublevelField;
            }
            set
            {
                sublevelField = value;
            }
        }

        /// <remarks/>
        public string CompanyName
        {
            get
            {
                return companyNameField;
            }
            set
            {
                companyNameField = value;
            }
        }

        /// <remarks/>
        public string Salutation
        {
            get
            {
                return salutationField;
            }
            set
            {
                salutationField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return firstNameField;
            }
            set
            {
                firstNameField = value;
            }
        }

        /// <remarks/>
        public string MiddleName
        {
            get
            {
                return middleNameField;
            }
            set
            {
                middleNameField = value;
            }
        }

        /// <remarks/>
        public string LastName
        {
            get
            {
                return lastNameField;
            }
            set
            {
                lastNameField = value;
            }
        }

        /// <remarks/>
        public string JobTitle
        {
            get
            {
                return jobTitleField;
            }
            set
            {
                jobTitleField = value;
            }
        }

        /// <remarks/>
        public CustomerRetBillAddress BillAddress
        {
            get
            {
                return billAddressField;
            }
            set
            {
                billAddressField = value;
            }
        }

        /// <remarks/>
        public CustomerRetBillAddressBlock BillAddressBlock
        {
            get
            {
                return billAddressBlockField;
            }
            set
            {
                billAddressBlockField = value;
            }
        }

        /// <remarks/>
        public CustomerRetShipAddress ShipAddress
        {
            get
            {
                return shipAddressField;
            }
            set
            {
                shipAddressField = value;
            }
        }

        /// <remarks/>
        public CustomerRetShipAddressBlock ShipAddressBlock
        {
            get
            {
                return shipAddressBlockField;
            }
            set
            {
                shipAddressBlockField = value;
            }
        }

        /// <remarks/>
        public CustomerRetShipToAddress ShipToAddress
        {
            get
            {
                return shipToAddressField;
            }
            set
            {
                shipToAddressField = value;
            }
        }

        /// <remarks/>
        public string Phone
        {
            get
            {
                return phoneField;
            }
            set
            {
                phoneField = value;
            }
        }

        /// <remarks/>
        public string AltPhone
        {
            get
            {
                return altPhoneField;
            }
            set
            {
                altPhoneField = value;
            }
        }

        /// <remarks/>
        public string Fax
        {
            get
            {
                return faxField;
            }
            set
            {
                faxField = value;
            }
        }

        /// <remarks/>
        public string Email
        {
            get
            {
                return emailField;
            }
            set
            {
                emailField = value;
            }
        }

        /// <remarks/>
        public string Cc
        {
            get
            {
                return ccField;
            }
            set
            {
                ccField = value;
            }
        }

        /// <remarks/>
        public string Contact
        {
            get
            {
                return contactField;
            }
            set
            {
                contactField = value;
            }
        }

        /// <remarks/>
        public string AltContact
        {
            get
            {
                return altContactField;
            }
            set
            {
                altContactField = value;
            }
        }

        /// <remarks/>
        public CustomerRetAdditionalContactRef AdditionalContactRef
        {
            get
            {
                return additionalContactRefField;
            }
            set
            {
                additionalContactRefField = value;
            }
        }

        /// <remarks/>
        public CustomerRetContactsRet ContactsRet
        {
            get
            {
                return contactsRetField;
            }
            set
            {
                contactsRetField = value;
            }
        }

        /// <remarks/>
        public CustomerRetCustomerTypeRef CustomerTypeRef
        {
            get
            {
                return customerTypeRefField;
            }
            set
            {
                customerTypeRefField = value;
            }
        }

        /// <remarks/>
        public CustomerRetTermsRef TermsRef
        {
            get
            {
                return termsRefField;
            }
            set
            {
                termsRefField = value;
            }
        }

        /// <remarks/>
        public CustomerRetSalesRepRef SalesRepRef
        {
            get
            {
                return salesRepRefField;
            }
            set
            {
                salesRepRefField = value;
            }
        }

        /// <remarks/>
        public string Balance
        {
            get
            {
                return balanceField;
            }
            set
            {
                balanceField = value;
            }
        }

        /// <remarks/>
        public string TotalBalance
        {
            get
            {
                return totalBalanceField;
            }
            set
            {
                totalBalanceField = value;
            }
        }

        /// <remarks/>
        public CustomerRetSalesTaxCodeRef SalesTaxCodeRef
        {
            get
            {
                return salesTaxCodeRefField;
            }
            set
            {
                salesTaxCodeRefField = value;
            }
        }

        /// <remarks/>
        public CustomerRetItemSalesTaxRef ItemSalesTaxRef
        {
            get
            {
                return itemSalesTaxRefField;
            }
            set
            {
                itemSalesTaxRefField = value;
            }
        }

        /// <remarks/>
        public string ResaleNumber
        {
            get
            {
                return resaleNumberField;
            }
            set
            {
                resaleNumberField = value;
            }
        }

        /// <remarks/>
        public string AccountNumber
        {
            get
            {
                return accountNumberField;
            }
            set
            {
                accountNumberField = value;
            }
        }

        /// <remarks/>
        public string CreditLimit
        {
            get
            {
                return creditLimitField;
            }
            set
            {
                creditLimitField = value;
            }
        }

        /// <remarks/>
        public CustomerRetPreferredPaymentMethodRef PreferredPaymentMethodRef
        {
            get
            {
                return preferredPaymentMethodRefField;
            }
            set
            {
                preferredPaymentMethodRefField = value;
            }
        }

        /// <remarks/>
        public CustomerRetCreditCardInfo CreditCardInfo
        {
            get
            {
                return creditCardInfoField;
            }
            set
            {
                creditCardInfoField = value;
            }
        }

        /// <remarks/>
        public string JobStatus
        {
            get
            {
                return jobStatusField;
            }
            set
            {
                jobStatusField = value;
            }
        }

        /// <remarks/>
        public string JobStartDate
        {
            get
            {
                return jobStartDateField;
            }
            set
            {
                jobStartDateField = value;
            }
        }

        /// <remarks/>
        public string JobProjectedEndDate
        {
            get
            {
                return jobProjectedEndDateField;
            }
            set
            {
                jobProjectedEndDateField = value;
            }
        }

        /// <remarks/>
        public string JobEndDate
        {
            get
            {
                return jobEndDateField;
            }
            set
            {
                jobEndDateField = value;
            }
        }

        /// <remarks/>
        public string JobDesc
        {
            get
            {
                return jobDescField;
            }
            set
            {
                jobDescField = value;
            }
        }

        /// <remarks/>
        public CustomerRetJobTypeRef JobTypeRef
        {
            get
            {
                return jobTypeRefField;
            }
            set
            {
                jobTypeRefField = value;
            }
        }

        /// <remarks/>
        public string Notes
        {
            get
            {
                return notesField;
            }
            set
            {
                notesField = value;
            }
        }

        /// <remarks/>
        public CustomerRetAdditionalNotesRet AdditionalNotesRet
        {
            get
            {
                return additionalNotesRetField;
            }
            set
            {
                additionalNotesRetField = value;
            }
        }

        /// <remarks/>
        public string PreferredDeliveryMethod
        {
            get
            {
                return preferredDeliveryMethodField;
            }
            set
            {
                preferredDeliveryMethodField = value;
            }
        }

        /// <remarks/>
        public CustomerRetPriceLevelRef PriceLevelRef
        {
            get
            {
                return priceLevelRefField;
            }
            set
            {
                priceLevelRefField = value;
            }
        }

        /// <remarks/>
        public string ExternalGUID
        {
            get
            {
                return externalGUIDField;
            }
            set
            {
                externalGUIDField = value;
            }
        }

        /// <remarks/>
        public CustomerRetCurrencyRef CurrencyRef
        {
            get
            {
                return currencyRefField;
            }
            set
            {
                currencyRefField = value;
            }
        }

        /// <remarks/>
        public CustomerRetDataExtRet DataExtRet
        {
            get
            {
                return dataExtRetField;
            }
            set
            {
                dataExtRetField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class CustomerRetClassRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class CustomerRetParentRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class CustomerRetBillAddress
    {

        private string addr1Field;

        private string addr2Field;

        private string addr3Field;

        private string addr4Field;

        private string addr5Field;

        private string cityField;

        private string stateField;

        private string postalCodeField;

        private string countryField;

        private string noteField;

        /// <remarks/>
        public string Addr1
        {
            get
            {
                return addr1Field;
            }
            set
            {
                addr1Field = value;
            }
        }

        /// <remarks/>
        public string Addr2
        {
            get
            {
                return addr2Field;
            }
            set
            {
                addr2Field = value;
            }
        }

        /// <remarks/>
        public string Addr3
        {
            get
            {
                return addr3Field;
            }
            set
            {
                addr3Field = value;
            }
        }

        /// <remarks/>
        public string Addr4
        {
            get
            {
                return addr4Field;
            }
            set
            {
                addr4Field = value;
            }
        }

        /// <remarks/>
        public string Addr5
        {
            get
            {
                return addr5Field;
            }
            set
            {
                addr5Field = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return cityField;
            }
            set
            {
                cityField = value;
            }
        }

        /// <remarks/>
        public string State
        {
            get
            {
                return stateField;
            }
            set
            {
                stateField = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return postalCodeField;
            }
            set
            {
                postalCodeField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return countryField;
            }
            set
            {
                countryField = value;
            }
        }

        /// <remarks/>
        public string Note
        {
            get
            {
                return noteField;
            }
            set
            {
                noteField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class CustomerRetBillAddressBlock
    {

        private string addr1Field;

        private string addr2Field;

        private string addr3Field;

        private string addr4Field;

        private string addr5Field;

        /// <remarks/>
        public string Addr1
        {
            get
            {
                return addr1Field;
            }
            set
            {
                addr1Field = value;
            }
        }

        /// <remarks/>
        public string Addr2
        {
            get
            {
                return addr2Field;
            }
            set
            {
                addr2Field = value;
            }
        }

        /// <remarks/>
        public string Addr3
        {
            get
            {
                return addr3Field;
            }
            set
            {
                addr3Field = value;
            }
        }

        /// <remarks/>
        public string Addr4
        {
            get
            {
                return addr4Field;
            }
            set
            {
                addr4Field = value;
            }
        }

        /// <remarks/>
        public string Addr5
        {
            get
            {
                return addr5Field;
            }
            set
            {
                addr5Field = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class CustomerRetShipAddress
    {

        private string addr1Field;

        private string addr2Field;

        private string addr3Field;

        private string addr4Field;

        private string addr5Field;

        private string cityField;

        private string stateField;

        private string postalCodeField;

        private string countryField;

        private string noteField;

        /// <remarks/>
        public string Addr1
        {
            get
            {
                return addr1Field;
            }
            set
            {
                addr1Field = value;
            }
        }

        /// <remarks/>
        public string Addr2
        {
            get
            {
                return addr2Field;
            }
            set
            {
                addr2Field = value;
            }
        }

        /// <remarks/>
        public string Addr3
        {
            get
            {
                return addr3Field;
            }
            set
            {
                addr3Field = value;
            }
        }

        /// <remarks/>
        public string Addr4
        {
            get
            {
                return addr4Field;
            }
            set
            {
                addr4Field = value;
            }
        }

        /// <remarks/>
        public string Addr5
        {
            get
            {
                return addr5Field;
            }
            set
            {
                addr5Field = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return cityField;
            }
            set
            {
                cityField = value;
            }
        }

        /// <remarks/>
        public string State
        {
            get
            {
                return stateField;
            }
            set
            {
                stateField = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return postalCodeField;
            }
            set
            {
                postalCodeField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return countryField;
            }
            set
            {
                countryField = value;
            }
        }

        /// <remarks/>
        public string Note
        {
            get
            {
                return noteField;
            }
            set
            {
                noteField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class CustomerRetShipAddressBlock
    {

        private string addr1Field;

        private string addr2Field;

        private string addr3Field;

        private string addr4Field;

        private string addr5Field;

        /// <remarks/>
        public string Addr1
        {
            get
            {
                return addr1Field;
            }
            set
            {
                addr1Field = value;
            }
        }

        /// <remarks/>
        public string Addr2
        {
            get
            {
                return addr2Field;
            }
            set
            {
                addr2Field = value;
            }
        }

        /// <remarks/>
        public string Addr3
        {
            get
            {
                return addr3Field;
            }
            set
            {
                addr3Field = value;
            }
        }

        /// <remarks/>
        public string Addr4
        {
            get
            {
                return addr4Field;
            }
            set
            {
                addr4Field = value;
            }
        }

        /// <remarks/>
        public string Addr5
        {
            get
            {
                return addr5Field;
            }
            set
            {
                addr5Field = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class CustomerRetShipToAddress
    {

        private string nameField;

        private string addr1Field;

        private string addr2Field;

        private string addr3Field;

        private string addr4Field;

        private string addr5Field;

        private string cityField;

        private string stateField;

        private string postalCodeField;

        private string countryField;

        private string noteField;

        private string defaultShipToField;

        /// <remarks/>
        public string Name
        {
            get
            {
                return nameField;
            }
            set
            {
                nameField = value;
            }
        }

        /// <remarks/>
        public string Addr1
        {
            get
            {
                return addr1Field;
            }
            set
            {
                addr1Field = value;
            }
        }

        /// <remarks/>
        public string Addr2
        {
            get
            {
                return addr2Field;
            }
            set
            {
                addr2Field = value;
            }
        }

        /// <remarks/>
        public string Addr3
        {
            get
            {
                return addr3Field;
            }
            set
            {
                addr3Field = value;
            }
        }

        /// <remarks/>
        public string Addr4
        {
            get
            {
                return addr4Field;
            }
            set
            {
                addr4Field = value;
            }
        }

        /// <remarks/>
        public string Addr5
        {
            get
            {
                return addr5Field;
            }
            set
            {
                addr5Field = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return cityField;
            }
            set
            {
                cityField = value;
            }
        }

        /// <remarks/>
        public string State
        {
            get
            {
                return stateField;
            }
            set
            {
                stateField = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return postalCodeField;
            }
            set
            {
                postalCodeField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return countryField;
            }
            set
            {
                countryField = value;
            }
        }

        /// <remarks/>
        public string Note
        {
            get
            {
                return noteField;
            }
            set
            {
                noteField = value;
            }
        }

        /// <remarks/>
        public string DefaultShipTo
        {
            get
            {
                return defaultShipToField;
            }
            set
            {
                defaultShipToField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class CustomerRetAdditionalContactRef
    {

        private string contactNameField;

        private string contactValueField;

        /// <remarks/>
        public string ContactName
        {
            get
            {
                return contactNameField;
            }
            set
            {
                contactNameField = value;
            }
        }

        /// <remarks/>
        public string ContactValue
        {
            get
            {
                return contactValueField;
            }
            set
            {
                contactValueField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class CustomerRetContactsRet
    {

        private string listIDField;

        private string timeCreatedField;

        private string timeModifiedField;

        private string editSequenceField;

        private string contactField;

        private string salutationField;

        private string firstNameField;

        private string middleNameField;

        private string lastNameField;

        private string jobTitleField;

        private CustomerRetContactsRetAdditionalContactRef additionalContactRefField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string TimeCreated
        {
            get
            {
                return timeCreatedField;
            }
            set
            {
                timeCreatedField = value;
            }
        }

        /// <remarks/>
        public string TimeModified
        {
            get
            {
                return timeModifiedField;
            }
            set
            {
                timeModifiedField = value;
            }
        }

        /// <remarks/>
        public string EditSequence
        {
            get
            {
                return editSequenceField;
            }
            set
            {
                editSequenceField = value;
            }
        }

        /// <remarks/>
        public string Contact
        {
            get
            {
                return contactField;
            }
            set
            {
                contactField = value;
            }
        }

        /// <remarks/>
        public string Salutation
        {
            get
            {
                return salutationField;
            }
            set
            {
                salutationField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return firstNameField;
            }
            set
            {
                firstNameField = value;
            }
        }

        /// <remarks/>
        public string MiddleName
        {
            get
            {
                return middleNameField;
            }
            set
            {
                middleNameField = value;
            }
        }

        /// <remarks/>
        public string LastName
        {
            get
            {
                return lastNameField;
            }
            set
            {
                lastNameField = value;
            }
        }

        /// <remarks/>
        public string JobTitle
        {
            get
            {
                return jobTitleField;
            }
            set
            {
                jobTitleField = value;
            }
        }

        /// <remarks/>
        public CustomerRetContactsRetAdditionalContactRef AdditionalContactRef
        {
            get
            {
                return additionalContactRefField;
            }
            set
            {
                additionalContactRefField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class CustomerRetContactsRetAdditionalContactRef
    {

        private string contactNameField;

        private string contactValueField;

        /// <remarks/>
        public string ContactName
        {
            get
            {
                return contactNameField;
            }
            set
            {
                contactNameField = value;
            }
        }

        /// <remarks/>
        public string ContactValue
        {
            get
            {
                return contactValueField;
            }
            set
            {
                contactValueField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class CustomerRetCustomerTypeRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class CustomerRetTermsRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class CustomerRetSalesRepRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class CustomerRetSalesTaxCodeRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class CustomerRetItemSalesTaxRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class CustomerRetPreferredPaymentMethodRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class CustomerRetCreditCardInfo
    {

        private string creditCardNumberField;

        private string expirationMonthField;

        private string expirationYearField;

        private string nameOnCardField;

        private string creditCardAddressField;

        private string creditCardPostalCodeField;

        /// <remarks/>
        public string CreditCardNumber
        {
            get
            {
                return creditCardNumberField;
            }
            set
            {
                creditCardNumberField = value;
            }
        }

        /// <remarks/>
        public string ExpirationMonth
        {
            get
            {
                return expirationMonthField;
            }
            set
            {
                expirationMonthField = value;
            }
        }

        /// <remarks/>
        public string ExpirationYear
        {
            get
            {
                return expirationYearField;
            }
            set
            {
                expirationYearField = value;
            }
        }

        /// <remarks/>
        public string NameOnCard
        {
            get
            {
                return nameOnCardField;
            }
            set
            {
                nameOnCardField = value;
            }
        }

        /// <remarks/>
        public string CreditCardAddress
        {
            get
            {
                return creditCardAddressField;
            }
            set
            {
                creditCardAddressField = value;
            }
        }

        /// <remarks/>
        public string CreditCardPostalCode
        {
            get
            {
                return creditCardPostalCodeField;
            }
            set
            {
                creditCardPostalCodeField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class CustomerRetJobTypeRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class CustomerRetAdditionalNotesRet
    {

        private string noteIDField;

        private string dateField;

        private string noteField;

        /// <remarks/>
        public string NoteID
        {
            get
            {
                return noteIDField;
            }
            set
            {
                noteIDField = value;
            }
        }

        /// <remarks/>
        public string Date
        {
            get
            {
                return dateField;
            }
            set
            {
                dateField = value;
            }
        }

        /// <remarks/>
        public string Note
        {
            get
            {
                return noteField;
            }
            set
            {
                noteField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class CustomerRetPriceLevelRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class CustomerRetCurrencyRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class CustomerRetDataExtRet
    {

        private string ownerIDField;

        private string dataExtNameField;

        private string dataExtTypeField;

        private string dataExtValueField;

        /// <remarks/>
        public string OwnerID
        {
            get
            {
                return ownerIDField;
            }
            set
            {
                ownerIDField = value;
            }
        }

        /// <remarks/>
        public string DataExtName
        {
            get
            {
                return dataExtNameField;
            }
            set
            {
                dataExtNameField = value;
            }
        }

        /// <remarks/>
        public string DataExtType
        {
            get
            {
                return dataExtTypeField;
            }
            set
            {
                dataExtTypeField = value;
            }
        }

        /// <remarks/>
        public string DataExtValue
        {
            get
            {
                return dataExtValueField;
            }
            set
            {
                dataExtValueField = value;
            }
        }
    }


}
