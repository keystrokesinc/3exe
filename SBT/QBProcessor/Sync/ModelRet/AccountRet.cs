﻿using DataProcessor.Ref;
using System;
using System.Xml.Serialization;

namespace DataProcessor.Sync.ModelRet
{

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "" , IsNullable = false)]
    public partial class AccountRet
    {

        private string listIDField;

        private DateTime? timeCreatedField;

        private DateTime timeModifiedField;

        private string editSequenceField;

        private string nameField;

        private string fullNameField;

        private bool? isActiveField;

        private ParentRef parentRefField;

        private string sublevelField;

        private string accountTypeField;

        private string specialAccountTypeField;

        private string accountNumberField;

        private string bankNumberField;

        private string descField;

        private string balanceField;

        private string totalBalanceField;

        private AccountRetTaxLineInfoRet taxLineInfoRetField;

        private string cashFlowClassificationField;

        private AccountRetCurrencyRef currencyRefField;

        private AccountRetDataExtRet dataExtRetField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public DateTime? TimeCreated
        {
            get
            {
                return timeCreatedField;
            }
            set
            {
                timeCreatedField = value;
            }
        }

        /// <remarks/>
        public DateTime TimeModified
        {
            get
            {
                return timeModifiedField;
            }
            set
            {
                timeModifiedField = value;
            }
        }

        /// <remarks/>
        public string EditSequence
        {
            get
            {
                return editSequenceField;
            }
            set
            {
                editSequenceField = value;
            }
        }

        /// <remarks/>
        public string Name
        {
            get
            {
                return nameField;
            }
            set
            {
                nameField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }

        /// <remarks/>        
        public bool? IsActive
        {
            get
            {
                return isActiveField;
            }
            set
            {
                isActiveField = value;
            }
        }
        /// <remarks/>
        public ParentRef ParentRef
        {
            get
            {
                return parentRefField;
            }
            set
            {
                parentRefField = value;
            }
        }

        /// <remarks/>
        public string Sublevel
        {
            get
            {
                return sublevelField;
            }
            set
            {
                sublevelField = value;
            }
        }

        /// <remarks/>
        public string AccountType
        {
            get
            {
                return accountTypeField;
            }
            set
            {
                accountTypeField = value;
            }
        }

        /// <remarks/>
        public string SpecialAccountType
        {
            get
            {
                return specialAccountTypeField;
            }
            set
            {
                specialAccountTypeField = value;
            }
        }

        /// <remarks/>
        public string AccountNumber
        {
            get
            {
                return accountNumberField;
            }
            set
            {
                accountNumberField = value;
            }
        }

        /// <remarks/>
        public string BankNumber
        {
            get
            {
                return bankNumberField;
            }
            set
            {
                bankNumberField = value;
            }
        }

        /// <remarks/>
        public string Desc
        {
            get
            {
                return descField;
            }
            set
            {
                descField = value;
            }
        }

        /// <remarks/>
        public string Balance
        {
            get
            {
                return balanceField;
            }
            set
            {
                balanceField = value;
            }
        }

        /// <remarks/>
        public string TotalBalance
        {
            get
            {
                return totalBalanceField;
            }
            set
            {
                totalBalanceField = value;
            }
        }

        /// <remarks/>
        public AccountRetTaxLineInfoRet TaxLineInfoRet
        {
            get
            {
                return taxLineInfoRetField;
            }
            set
            {
                taxLineInfoRetField = value;
            }
        }

        /// <remarks/>
        public string CashFlowClassification
        {
            get
            {
                return cashFlowClassificationField;
            }
            set
            {
                cashFlowClassificationField = value;
            }
        }

        /// <remarks/>
        public AccountRetCurrencyRef CurrencyRef
        {
            get
            {
                return currencyRefField;
            }
            set
            {
                currencyRefField = value;
            }
        }

        /// <remarks/>
        public AccountRetDataExtRet DataExtRet
        {
            get
            {
                return dataExtRetField;
            }
            set
            {
                dataExtRetField = value;
            }
        }
    }

    

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class AccountRetTaxLineInfoRet
    {

        private string taxLineIDField;

        private string taxLineNameField;

        /// <remarks/>
        public string TaxLineID
        {
            get
            {
                return taxLineIDField;
            }
            set
            {
                taxLineIDField = value;
            }
        }

        /// <remarks/>
        public string TaxLineName
        {
            get
            {
                return taxLineNameField;
            }
            set
            {
                taxLineNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class AccountRetCurrencyRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class AccountRetDataExtRet
    {

        private string ownerIDField;

        private string dataExtNameField;

        private string dataExtTypeField;

        private string dataExtValueField;

        /// <remarks/>
        public string OwnerID
        {
            get
            {
                return ownerIDField;
            }
            set
            {
                ownerIDField = value;
            }
        }

        /// <remarks/>
        public string DataExtName
        {
            get
            {
                return dataExtNameField;
            }
            set
            {
                dataExtNameField = value;
            }
        }

        /// <remarks/>
        public string DataExtType
        {
            get
            {
                return dataExtTypeField;
            }
            set
            {
                dataExtTypeField = value;
            }
        }

        /// <remarks/>
        public string DataExtValue
        {
            get
            {
                return dataExtValueField;
            }
            set
            {
                dataExtValueField = value;
            }
        }
    }


}
