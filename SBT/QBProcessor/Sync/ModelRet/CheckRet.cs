﻿
using DataProcessor.Ref;
using System;
using System.Xml.Serialization;

namespace DataProcessor.Sync.ModelRet
{
    //[XmlRoot(ElementName = "AccountRef")]
    //public class AccountRef
    //{
    //    [XmlElement(ElementName = "ListID")]
    //    public string ListID { get; set; }
    //    [XmlElement(ElementName = "FullName")]
    //    public string FullName { get; set; }
    //}

    [XmlRoot(ElementName = "PayeeEntityRef")]
    public class PayeeEntityRef
    {
        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }
        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }

    [XmlRoot(ElementName = "CurrencyRef")]
    public class CurrencyRef
    {
        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }
        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }

    [XmlRoot(ElementName = "Address")]
    public class Address
    {
        [XmlElement(ElementName = "Addr1")]
        public string Addr1 { get; set; }
        [XmlElement(ElementName = "Addr2")]
        public string Addr2 { get; set; }
        [XmlElement(ElementName = "Addr3")]
        public string Addr3 { get; set; }
        [XmlElement(ElementName = "Addr4")]
        public string Addr4 { get; set; }
        [XmlElement(ElementName = "Addr5")]
        public string Addr5 { get; set; }
        [XmlElement(ElementName = "City")]
        public string City { get; set; }
        [XmlElement(ElementName = "State")]
        public string State { get; set; }
        [XmlElement(ElementName = "PostalCode")]
        public string PostalCode { get; set; }
        [XmlElement(ElementName = "Country")]
        public string Country { get; set; }
        [XmlElement(ElementName = "Note")]
        public string Note { get; set; }
    }

    [XmlRoot(ElementName = "AddressBlock")]
    public class AddressBlock
    {
        [XmlElement(ElementName = "Addr1")]
        public string Addr1 { get; set; }
        [XmlElement(ElementName = "Addr2")]
        public string Addr2 { get; set; }
        [XmlElement(ElementName = "Addr3")]
        public string Addr3 { get; set; }
        [XmlElement(ElementName = "Addr4")]
        public string Addr4 { get; set; }
        [XmlElement(ElementName = "Addr5")]
        public string Addr5 { get; set; }
    }

    [XmlRoot(ElementName = "LinkedTxn")]
    public class LinkedTxn
    {
        [XmlElement(ElementName = "TxnID")]
        public string TxnID { get; set; }
        [XmlElement(ElementName = "TxnType")]
        public string TxnType { get; set; }
        [XmlElement(ElementName = "TxnDate")]
        public string TxnDate { get; set; }
        [XmlElement(ElementName = "RefNumber")]
        public string RefNumber { get; set; }
        [XmlElement(ElementName = "LinkType")]
        public string LinkType { get; set; }
        [XmlElement(ElementName = "Amount")]
        public string Amount { get; set; }
    }

    [XmlRoot(ElementName = "SalesRepRef")]
    public class SalesRepRef
    {
        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }
        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }

    [XmlRoot(ElementName = "DataExtRet")]
    public class DataExtRet
    {
        [XmlElement(ElementName = "OwnerID")]
        public string OwnerID { get; set; }
        [XmlElement(ElementName = "DataExtName")]
        public string DataExtName { get; set; }
        [XmlElement(ElementName = "DataExtType")]
        public string DataExtType { get; set; }
        [XmlElement(ElementName = "DataExtValue")]
        public string DataExtValue { get; set; }
    }

    [XmlRoot(ElementName = "ExpenseLineRet")]
    public class ExpenseLineRet
    {
        [XmlElement(ElementName = "TxnLineID")]
        public string TxnLineID { get; set; }
        [XmlElement(ElementName = "AccountRef")]
        public AccountRef AccountRef { get; set; }
        [XmlElement(ElementName = "Amount")]
        public string Amount { get; set; }
        [XmlElement(ElementName = "Memo")]
        public string Memo { get; set; }
        [XmlElement(ElementName = "CustomerRef")]
        public CustomerRef CustomerRef { get; set; }
        [XmlElement(ElementName = "ClassRef")]
        public ClassRef ClassRef { get; set; }
        [XmlElement(ElementName = "BillableStatus")]
        public string BillableStatus { get; set; }
        [XmlElement(ElementName = "SalesRepRef")]
        public SalesRepRef SalesRepRef { get; set; }
        [XmlElement(ElementName = "DataExtRet")]
        public DataExtRet DataExtRet { get; set; }
    }

    [XmlRoot(ElementName = "ItemRef")]
    public class ItemRef
    {
        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }
        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }

    [XmlRoot(ElementName = "InventorySiteRef")]
    public class InventorySiteRef
    {
        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }
        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }

    [XmlRoot(ElementName = "InventorySiteLocationRef")]
    public class InventorySiteLocationRef
    {
        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }
        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }

    [XmlRoot(ElementName = "OverrideUOMSetRef")]
    public class OverrideUOMSetRef
    {
        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }
        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }

    [XmlRoot(ElementName = "ItemLineRet")]
    public class ItemLineRet
    {
        [XmlElement(ElementName = "TxnLineID")]
        public string TxnLineID { get; set; }
        [XmlElement(ElementName = "ItemRef")]
        public ItemRef ItemRef { get; set; }
        [XmlElement(ElementName = "InventorySiteRef")]
        public InventorySiteRef InventorySiteRef { get; set; }
        [XmlElement(ElementName = "InventorySiteLocationRef")]
        public InventorySiteLocationRef InventorySiteLocationRef { get; set; }
        [XmlElement(ElementName = "SerialNumber")]
        public string SerialNumber { get; set; }
        [XmlElement(ElementName = "LotNumber")]
        public string LotNumber { get; set; }
        [XmlElement(ElementName = "Desc")]
        public string Desc { get; set; }
        [XmlElement(ElementName = "Quantity")]
        public string Quantity { get; set; }
        [XmlElement(ElementName = "UnitOfMeasure")]
        public string UnitOfMeasure { get; set; }
        [XmlElement(ElementName = "OverrideUOMSetRef")]
        public OverrideUOMSetRef OverrideUOMSetRef { get; set; }
        [XmlElement(ElementName = "Cost")]
        public string Cost { get; set; }
        [XmlElement(ElementName = "Amount")]
        public string Amount { get; set; }
        [XmlElement(ElementName = "CustomerRef")]
        public CustomerRef CustomerRef { get; set; }
        [XmlElement(ElementName = "ClassRef")]
        public ClassRef ClassRef { get; set; }
        [XmlElement(ElementName = "BillableStatus")]
        public string BillableStatus { get; set; }
        [XmlElement(ElementName = "SalesRepRef")]
        public SalesRepRef SalesRepRef { get; set; }
        [XmlElement(ElementName = "DataExtRet")]
        public DataExtRet DataExtRet { get; set; }
    }

    [XmlRoot(ElementName = "ItemGroupRef")]
    public class ItemGroupRef
    {
        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }
        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }

    [XmlRoot(ElementName = "DataExt")]
    public class DataExt
    {
        [XmlElement(ElementName = "OwnerID")]
        public string OwnerID { get; set; }
        [XmlElement(ElementName = "DataExtName")]
        public string DataExtName { get; set; }
        [XmlElement(ElementName = "DataExtValue")]
        public string DataExtValue { get; set; }
    }

    [XmlRoot(ElementName = "ItemGroupLineRet")]
    public class ItemGroupLineRet
    {
        [XmlElement(ElementName = "TxnLineID")]
        public string TxnLineID { get; set; }
        [XmlElement(ElementName = "ItemGroupRef")]
        public ItemGroupRef ItemGroupRef { get; set; }
        [XmlElement(ElementName = "Desc")]
        public string Desc { get; set; }
        [XmlElement(ElementName = "Quantity")]
        public string Quantity { get; set; }
        [XmlElement(ElementName = "UnitOfMeasure")]
        public string UnitOfMeasure { get; set; }
        [XmlElement(ElementName = "OverrideUOMSetRef")]
        public OverrideUOMSetRef OverrideUOMSetRef { get; set; }
        [XmlElement(ElementName = "TotalAmount")]
        public string TotalAmount { get; set; }
        [XmlElement(ElementName = "ItemLineRet")]
        public ItemLineRet ItemLineRet { get; set; }
        [XmlElement(ElementName = "DataExt")]
        public DataExt DataExt { get; set; }
    }

    [XmlRoot(ElementName = "CheckRet")]
    public class CheckRet
    {
        [XmlElement(ElementName = "TxnID")]
        public string TxnID { get; set; }
        [XmlElement(ElementName = "TimeCreated")]
        public string TimeCreated { get; set; }
        [XmlElement(ElementName = "TimeModified")]
        public string TimeModified { get; set; }
        [XmlElement(ElementName = "EditSequence")]
        public string EditSequence { get; set; }
        [XmlElement(ElementName = "TxnNumber")]
        public string TxnNumber { get; set; }
        [XmlElement(ElementName = "AccountRef")]
        public AccountRef AccountRef { get; set; }
        [XmlElement(ElementName = "PayeeEntityRef")]
        public PayeeEntityRef PayeeEntityRef { get; set; }
        [XmlElement(ElementName = "RefNumber")]
        public string RefNumber { get; set; }
        [XmlElement(ElementName = "TxnDate")]
        public string TxnDate { get; set; }
        [XmlElement(ElementName = "Amount")]
        public string Amount { get; set; }
        [XmlElement(ElementName = "CurrencyRef")]
        public CurrencyRef CurrencyRef { get; set; }
        [XmlElement(ElementName = "ExchangeRate")]
        public string ExchangeRate { get; set; }
        [XmlElement(ElementName = "AmountInHomeCurrency")]
        public string AmountInHomeCurrency { get; set; }
        [XmlElement(ElementName = "Memo")]
        public string Memo { get; set; }
        [XmlElement(ElementName = "Address")]
        public Address Address { get; set; }
        [XmlElement(ElementName = "AddressBlock")]
        public AddressBlock AddressBlock { get; set; }
        [XmlElement(ElementName = "IsToBePrinted")]
        public string IsToBePrinted { get; set; }
        [XmlElement(ElementName = "ExternalGUID")]
        public string ExternalGUID { get; set; }
        [XmlElement(ElementName = "LinkedTxn")]
        public LinkedTxn LinkedTxn { get; set; }
        [XmlElement(ElementName = "ExpenseLineRet")]
        public ExpenseLineRet ExpenseLineRet { get; set; }
        [XmlElement(ElementName = "ItemLineRet")]
        public ItemLineRet ItemLineRet { get; set; }
        [XmlElement(ElementName = "ItemGroupLineRet")]
        public ItemGroupLineRet ItemGroupLineRet { get; set; }
        [XmlElement(ElementName = "DataExtRet")]
        public DataExtRet DataExtRet { get; set; }
    }

    [XmlRoot(ElementName = "ErrorRecovery")]
    public class ErrorRecovery
    {
        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }
        [XmlElement(ElementName = "OwnerID")]
        public string OwnerID { get; set; }
        [XmlElement(ElementName = "TxnID")]
        public string TxnID { get; set; }
        [XmlElement(ElementName = "TxnNumber")]
        public string TxnNumber { get; set; }
        [XmlElement(ElementName = "EditSequence")]
        public string EditSequence { get; set; }
        [XmlElement(ElementName = "ExternalGUID")]
        public string ExternalGUID { get; set; }
    }

    [XmlRoot(ElementName = "CheckAddRs")]
    public class CheckAddRs
    {
        [XmlElement(ElementName = "CheckRet")]
        public CheckRet CheckRet { get; set; }
        [XmlElement(ElementName = "ErrorRecovery")]
        public ErrorRecovery ErrorRecovery { get; set; }
        [XmlAttribute(AttributeName = "statusCode")]
        public string StatusCode { get; set; }
        [XmlAttribute(AttributeName = "statusSeverity")]
        public string StatusSeverity { get; set; }
        [XmlAttribute(AttributeName = "statusMessage")]
        public string StatusMessage { get; set; }
    }
}
