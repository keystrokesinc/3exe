﻿
using DataProcessor.Ref;
using DataProcessor.Sync.ModelRet;
using System;
using System.Xml.Serialization;

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
public partial class BillRet
{

    private string txnIDField;

    private DateTime? timeCreatedField;

    private DateTime? timeModifiedField;

    private string editSequenceField;

    private string txnNumberField;   

    private BillRetVendorRef vendorRefField;
   // private BillRetVendorAddress vendorAddressField;

    private BillRetAPAccountRef aPAccountRefField;

    private DateTime? txnDateField;

    private DateTime? dueDateField;

    private decimal? amountDueField;

    private BillRetCurrencyRef currencyRefField;

    private string exchangeRateField;

    private string amountDueInHomeCurrencyField;

    private string refNumberField;

    private BillRetTermsRef termsRefField;

    private string memoField;

    private bool? isPaidField;

    private string externalGUIDField;

    private BillRetLinkedTxn linkedTxnField;

    private ExpenseLineRet[] expenseLineRetField;

    private ItemLineRet[] itemLineRetField;

    private BillRetItemGroupLineRet itemGroupLineRetField;

    private string openAmountField;

    private BillRetDataExtRet dataExtRetField;

    /// <remarks/>
    public string TxnID
    {
        get
        {
            return this.txnIDField;
        }
        set
        {
            this.txnIDField = value;
        }
    }

    /// <remarks/>
    public DateTime? TimeCreated
    {
        get
        {
            return this.timeCreatedField;
        }
        set
        {
            this.timeCreatedField = value;
        }
    }

    /// <remarks/>
    public DateTime? TimeModified
    {
        get
        {
            return this.timeModifiedField;
        }
        set
        {
            this.timeModifiedField = value;
        }
    }

    /// <remarks/>
    public string EditSequence
    {
        get
        {
            return this.editSequenceField;
        }
        set
        {
            this.editSequenceField = value;
        }
    }

    /// <remarks/>
    //public int? TxnNumber
    //{
    //    get
    //    {
    //        return this.txnNumberField;
    //    }
    //    set
    //    {
    //        this.txnNumberField = value;
    //    }
    //}
    public string TxnNumber
    {
        get
        {
            return this.txnNumberField;
        }
        set
        {
            this.txnNumberField = value;
        }
    }


    /// <remarks/>
    public BillRetVendorRef VendorRef
    {
        get
        {
            return this.vendorRefField;
        }
        set
        {
            this.vendorRefField = value;
        }
    }
    //public BillRetVendorAddress VendorAddress
    //{
    //    get
    //    {
    //        return this.vendorAddressField;
    //    }
    //    set
    //    {
    //        this.vendorAddressField = value;
    //    }
    //}


    /// <remarks/>
    public BillRetAPAccountRef APAccountRef
    {
        get
        {
            return this.aPAccountRefField;
        }
        set
        {
            this.aPAccountRefField = value;
        }
    }

    /// <remarks/>
    public DateTime? TxnDate
    {
        get
        {
            return this.txnDateField;
        }
        set
        {
            this.txnDateField = value;
        }
    }

    /// <remarks/>
    public DateTime? DueDate
    {
        get
        {
            return this.dueDateField;
        }
        set
        {
            this.dueDateField = value;
        }
    }

    /// <remarks/>
    public decimal? AmountDue
    {
        get
        {
            return this.amountDueField;
        }
        set
        {
            this.amountDueField = value;
        }
    }

    /// <remarks/>
    public BillRetCurrencyRef CurrencyRef
    {
        get
        {
            return this.currencyRefField;
        }
        set
        {
            this.currencyRefField = value;
        }
    }

    /// <remarks/>
    public string ExchangeRate
    {
        get
        {
            return this.exchangeRateField;
        }
        set
        {
            this.exchangeRateField = value;
        }
    }

    /// <remarks/>
    public string AmountDueInHomeCurrency
    {
        get
        {
            return this.amountDueInHomeCurrencyField;
        }
        set
        {
            this.amountDueInHomeCurrencyField = value;
        }
    }

    /// <remarks/>
    public string RefNumber
    {
        get
        {
            return this.refNumberField;
        }
        set
        {
            this.refNumberField = value;
        }
    }

    /// <remarks/>
    public BillRetTermsRef TermsRef
    {
        get
        {
            return this.termsRefField;
        }
        set
        {
            this.termsRefField = value;
        }
    }

    /// <remarks/>
    public string Memo
    {
        get
        {
            return this.memoField;
        }
        set
        {
            this.memoField = value;
        }
    }

    /// <remarks/>
    public bool? IsPaid
    {
        get
        {
            return this.isPaidField;
        }
        set
        {
            this.isPaidField = value;
        }
    }

    /// <remarks/>
    public string ExternalGUID
    {
        get
        {
            return this.externalGUIDField;
        }
        set
        {
            this.externalGUIDField = value;
        }
    }

    /// <remarks/>
    public BillRetLinkedTxn LinkedTxn
    {
        get
        {
            return this.linkedTxnField;
        }
        set
        {
            this.linkedTxnField = value;
        }
    }

    [XmlElement("ExpenseLineRet")]
    public ExpenseLineRet[] ExpenseLineRet
    {
        get
        {
            return this.expenseLineRetField;
        }
        set
        {
            this.expenseLineRetField = value;
        }
    }

    /// <remarks/>
    public ItemLineRet [] ItemLineRet
    {
        get
        {
            return this.itemLineRetField;
        }
        set
        {
            this.itemLineRetField = value;
        }
    }

    /// <remarks/>
    public BillRetItemGroupLineRet ItemGroupLineRet
    {
        get
        {
            return this.itemGroupLineRetField;
        }
        set
        {
            this.itemGroupLineRetField = value;
        }
    }

    /// <remarks/>
    public string OpenAmount
    {
        get
        {
            return this.openAmountField;
        }
        set
        {
            this.openAmountField = value;
        }
    }

    /// <remarks/>
    public BillRetDataExtRet DataExtRet
    {
        get
        {
            return this.dataExtRetField;
        }
        set
        {
            this.dataExtRetField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillRetVendorRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}
[XmlType(AnonymousType = true)]
public partial class BillRetVendorAddress
{

    private string addr1Field;

    private string addr2Field;

    private string addr3Field;

    private string addr4Field;

    private string addr5Field;

    private string cityField;

    private string stateField;

    private string postalCodeField;

    private string countryField;

    private string noteField;

    /// <remarks/>
    public string Addr1
    {
        get
        {
            return addr1Field;
        }
        set
        {
            addr1Field = value;
        }
    }

    /// <remarks/>
    public string Addr2
    {
        get
        {
            return addr2Field;
        }
        set
        {
            addr2Field = value;
        }
    }

    /// <remarks/>
    public string Addr3
    {
        get
        {
            return addr3Field;
        }
        set
        {
            addr3Field = value;
        }
    }

    /// <remarks/>
    public string Addr4
    {
        get
        {
            return addr4Field;
        }
        set
        {
            addr4Field = value;
        }
    }

    /// <remarks/>
    public string Addr5
    {
        get
        {
            return addr5Field;
        }
        set
        {
            addr5Field = value;
        }
    }

    /// <remarks/>
    public string City
    {
        get
        {
            return cityField;
        }
        set
        {
            cityField = value;
        }
    }

    /// <remarks/>
    public string State
    {
        get
        {
            return stateField;
        }
        set
        {
            stateField = value;
        }
    }

    /// <remarks/>
    public string PostalCode
    {
        get
        {
            return postalCodeField;
        }
        set
        {
            postalCodeField = value;
        }
    }

    /// <remarks/>
    public string Country
    {
        get
        {
            return countryField;
        }
        set
        {
            countryField = value;
        }
    }

    /// <remarks/>
    public string Note
    {
        get
        {
            return noteField;
        }
        set
        {
            noteField = value;
        }
    }
}
/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillRetAPAccountRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillRetCurrencyRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillRetTermsRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillRetLinkedTxn
{

    private string txnIDField;

    private string txnTypeField;

    private string txnDateField;

    private string refNumberField;

    private string linkTypeField;

    private string amountField;

    /// <remarks/>
    public string TxnID
    {
        get
        {
            return this.txnIDField;
        }
        set
        {
            this.txnIDField = value;
        }
    }

    /// <remarks/>
    public string TxnType
    {
        get
        {
            return this.txnTypeField;
        }
        set
        {
            this.txnTypeField = value;
        }
    }

    /// <remarks/>
    public string TxnDate
    {
        get
        {
            return this.txnDateField;
        }
        set
        {
            this.txnDateField = value;
        }
    }

    /// <remarks/>
    public string RefNumber
    {
        get
        {
            return this.refNumberField;
        }
        set
        {
            this.refNumberField = value;
        }
    }

    /// <remarks/>
    public string LinkType
    {
        get
        {
            return this.linkTypeField;
        }
        set
        {
            this.linkTypeField = value;
        }
    }

    /// <remarks/>
    public string Amount
    {
        get
        {
            return this.amountField;
        }
        set
        {
            this.amountField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ExpenseLineRet
{

    private string txnLineIDField;

    private ExpenseLineRetAccountRef accountRefField;

    private decimal? amountField;

    private string memoField;

    private ExpenseLineRetCustomerRef customerRefField;

    private ExpenseLineRetClassRef classRefField;

    private string billableStatusField;

    /// <remarks/>
    public string TxnLineID
    {
        get
        {
            return this.txnLineIDField;
        }
        set
        {
            this.txnLineIDField = value;
        }
    }

    /// <remarks/>
    public ExpenseLineRetAccountRef AccountRef
    {
        get
        {
            return this.accountRefField;
        }
        set
        {
            this.accountRefField = value;
        }
    }

    /// <remarks/>
    public decimal? Amount
    {
        get
        {
            return this.amountField;
        }
        set
        {
            this.amountField = value;
        }
    }

    /// <remarks/>
    public string Memo
    {
        get
        {
            return this.memoField;
        }
        set
        {
            this.memoField = value;
        }
    }

    /// <remarks/>
    public ExpenseLineRetCustomerRef CustomerRef
    {
        get
        {
            return this.customerRefField;
        }
        set
        {
            this.customerRefField = value;
        }
    }

    /// <remarks/>
    public ExpenseLineRetClassRef ClassRef
    {
        get
        {
            return this.classRefField;
        }
        set
        {
            this.classRefField = value;
        }
    }

    /// <remarks/>
    public string BillableStatus
    {
        get
        {
            return this.billableStatusField;
        }
        set
        {
            this.billableStatusField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ExpenseLineRetAccountRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ExpenseLineRetCustomerRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ExpenseLineRetClassRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ItemLineRet
{

    private string txnLineIDField;

    private ItemRef itemRefField;

    private ItemLineRetInventorySiteRef inventorySiteRefField;

    private string serialNumberField;

    private string lotNumberField;

    private string descField;

    private string quantityField;

    private string unitOfMeasureField;

    private ItemLineRetOverrideUOMSetRef overrideUOMSetRefField;

    private string costField;

    private string amountField;

    private ItemLineRetCustomerRef customerRefField;

    private DataProcessor.Sync.ModelRet.ClassRef classRefField;

    private string billableStatusField;

    /// <remarks/>
    public string TxnLineID
    {
        get
        {
            return this.txnLineIDField;
        }
        set
        {
            this.txnLineIDField = value;
        }
    }

    /// <remarks/>
    public ItemRef ItemRef
    {
        get
        {
            return this.itemRefField;
        }
        set
        {
            this.itemRefField = value;
        }
    }

    /// <remarks/>
    public ItemLineRetInventorySiteRef InventorySiteRef
    {
        get
        {
            return this.inventorySiteRefField;
        }
        set
        {
            this.inventorySiteRefField = value;
        }
    }

    /// <remarks/>
    public string SerialNumber
    {
        get
        {
            return this.serialNumberField;
        }
        set
        {
            this.serialNumberField = value;
        }
    }

    /// <remarks/>
    public string LotNumber
    {
        get
        {
            return this.lotNumberField;
        }
        set
        {
            this.lotNumberField = value;
        }
    }

    /// <remarks/>
    public string Desc
    {
        get
        {
            return this.descField;
        }
        set
        {
            this.descField = value;
        }
    }

    /// <remarks/>
    public string Quantity
    {
        get
        {
            return this.quantityField;
        }
        set
        {
            this.quantityField = value;
        }
    }

    /// <remarks/>
    public string UnitOfMeasure
    {
        get
        {
            return this.unitOfMeasureField;
        }
        set
        {
            this.unitOfMeasureField = value;
        }
    }

    /// <remarks/>
    public ItemLineRetOverrideUOMSetRef OverrideUOMSetRef
    {
        get
        {
            return this.overrideUOMSetRefField;
        }
        set
        {
            this.overrideUOMSetRefField = value;
        }
    }

    /// <remarks/>
    public string Cost
    {
        get
        {
            return this.costField;
        }
        set
        {
            this.costField = value;
        }
    }

    /// <remarks/>
    public string Amount
    {
        get
        {
            return this.amountField;
        }
        set
        {
            this.amountField = value;
        }
    }

    /// <remarks/>
    public ItemLineRetCustomerRef CustomerRef
    {
        get
        {
            return this.customerRefField;
        }
        set
        {
            this.customerRefField = value;
        }
    }

    /// <remarks/>
    public DataProcessor.Sync.ModelRet.ClassRef ClassRef
    {
        get
        {
            return this.classRefField;
        }
        set
        {
            this.classRefField = value;
        }
    }

    /// <remarks/>
    public string BillableStatus
    {
        get
        {
            return this.billableStatusField;
        }
        set
        {
            this.billableStatusField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillRetItemLineRetItemRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ItemLineRetInventorySiteRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ItemLineRetOverrideUOMSetRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ItemLineRetCustomerRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillRetItemLineRetClassRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillRetItemGroupLineRet
{

    private string txnLineIDField;

    private BillRetItemGroupLineRetItemGroupRef itemGroupRefField;

    private string descField;

    private string quantityField;

    private string unitOfMeasureField;

    private BillRetItemGroupLineRetOverrideUOMSetRef overrideUOMSetRefField;

    private string totalAmountField;

    private BillRetItemGroupLineRetItemLineRet itemLineRetField;

    /// <remarks/>
    public string TxnLineID
    {
        get
        {
            return this.txnLineIDField;
        }
        set
        {
            this.txnLineIDField = value;
        }
    }

    /// <remarks/>
    public BillRetItemGroupLineRetItemGroupRef ItemGroupRef
    {
        get
        {
            return this.itemGroupRefField;
        }
        set
        {
            this.itemGroupRefField = value;
        }
    }

    /// <remarks/>
    public string Desc
    {
        get
        {
            return this.descField;
        }
        set
        {
            this.descField = value;
        }
    }

    /// <remarks/>
    public string Quantity
    {
        get
        {
            return this.quantityField;
        }
        set
        {
            this.quantityField = value;
        }
    }

    /// <remarks/>
    public string UnitOfMeasure
    {
        get
        {
            return this.unitOfMeasureField;
        }
        set
        {
            this.unitOfMeasureField = value;
        }
    }

    /// <remarks/>
    public BillRetItemGroupLineRetOverrideUOMSetRef OverrideUOMSetRef
    {
        get
        {
            return this.overrideUOMSetRefField;
        }
        set
        {
            this.overrideUOMSetRefField = value;
        }
    }

    /// <remarks/>
    public string TotalAmount
    {
        get
        {
            return this.totalAmountField;
        }
        set
        {
            this.totalAmountField = value;
        }
    }

    /// <remarks/>
    public BillRetItemGroupLineRetItemLineRet ItemLineRet
    {
        get
        {
            return this.itemLineRetField;
        }
        set
        {
            this.itemLineRetField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillRetItemGroupLineRetItemGroupRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillRetItemGroupLineRetOverrideUOMSetRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillRetItemGroupLineRetItemLineRet
{

    private string txnLineIDField;

    private BillRetItemGroupLineRetItemLineRetItemRef itemRefField;

    private BillRetItemGroupLineRetItemLineRetInventorySiteRef inventorySiteRefField;

    private string serialNumberField;

    private string lotNumberField;

    private string descField;

    private string quantityField;

    private string unitOfMeasureField;

    private BillRetItemGroupLineRetItemLineRetOverrideUOMSetRef overrideUOMSetRefField;

    private string costField;

    private string amountField;

    private BillRetItemGroupLineRetItemLineRetCustomerRef customerRefField;

    private BillRetItemGroupLineRetItemLineRetClassRef classRefField;

    private string billableStatusField;

    /// <remarks/>
    public string TxnLineID
    {
        get
        {
            return this.txnLineIDField;
        }
        set
        {
            this.txnLineIDField = value;
        }
    }

    /// <remarks/>
    public BillRetItemGroupLineRetItemLineRetItemRef ItemRef
    {
        get
        {
            return this.itemRefField;
        }
        set
        {
            this.itemRefField = value;
        }
    }

    /// <remarks/>
    public BillRetItemGroupLineRetItemLineRetInventorySiteRef InventorySiteRef
    {
        get
        {
            return this.inventorySiteRefField;
        }
        set
        {
            this.inventorySiteRefField = value;
        }
    }

    /// <remarks/>
    public string SerialNumber
    {
        get
        {
            return this.serialNumberField;
        }
        set
        {
            this.serialNumberField = value;
        }
    }

    /// <remarks/>
    public string LotNumber
    {
        get
        {
            return this.lotNumberField;
        }
        set
        {
            this.lotNumberField = value;
        }
    }

    /// <remarks/>
    public string Desc
    {
        get
        {
            return this.descField;
        }
        set
        {
            this.descField = value;
        }
    }

    /// <remarks/>
    public string Quantity
    {
        get
        {
            return this.quantityField;
        }
        set
        {
            this.quantityField = value;
        }
    }

    /// <remarks/>
    public string UnitOfMeasure
    {
        get
        {
            return this.unitOfMeasureField;
        }
        set
        {
            this.unitOfMeasureField = value;
        }
    }

    /// <remarks/>
    public BillRetItemGroupLineRetItemLineRetOverrideUOMSetRef OverrideUOMSetRef
    {
        get
        {
            return this.overrideUOMSetRefField;
        }
        set
        {
            this.overrideUOMSetRefField = value;
        }
    }

    /// <remarks/>
    public string Cost
    {
        get
        {
            return this.costField;
        }
        set
        {
            this.costField = value;
        }
    }

    /// <remarks/>
    public string Amount
    {
        get
        {
            return this.amountField;
        }
        set
        {
            this.amountField = value;
        }
    }

    /// <remarks/>
    public BillRetItemGroupLineRetItemLineRetCustomerRef CustomerRef
    {
        get
        {
            return this.customerRefField;
        }
        set
        {
            this.customerRefField = value;
        }
    }

    /// <remarks/>
    public BillRetItemGroupLineRetItemLineRetClassRef ClassRef
    {
        get
        {
            return this.classRefField;
        }
        set
        {
            this.classRefField = value;
        }
    }

    /// <remarks/>
    public string BillableStatus
    {
        get
        {
            return this.billableStatusField;
        }
        set
        {
            this.billableStatusField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillRetItemGroupLineRetItemLineRetItemRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillRetItemGroupLineRetItemLineRetInventorySiteRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillRetItemGroupLineRetItemLineRetOverrideUOMSetRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillRetItemGroupLineRetItemLineRetCustomerRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillRetItemGroupLineRetItemLineRetClassRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillRetDataExtRet
{

    private string ownerIDField;

    private string dataExtNameField;

    private string dataExtTypeField;

    private string dataExtValueField;

    /// <remarks/>
    public string OwnerID
    {
        get
        {
            return this.ownerIDField;
        }
        set
        {
            this.ownerIDField = value;
        }
    }

    /// <remarks/>
    public string DataExtName
    {
        get
        {
            return this.dataExtNameField;
        }
        set
        {
            this.dataExtNameField = value;
        }
    }

    /// <remarks/>
    public string DataExtType
    {
        get
        {
            return this.dataExtTypeField;
        }
        set
        {
            this.dataExtTypeField = value;
        }
    }

    /// <remarks/>
    public string DataExtValue
    {
        get
        {
            return this.dataExtValueField;
        }
        set
        {
            this.dataExtValueField = value;
        }
    }
}

