﻿using System;
using System.Xml.Serialization;

namespace DataProcessor.Sync.ModelRet
{

    /// <remarks/>
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public partial class CustomerMsgRet
    {

        private string listIDField;

        private string timeCreatedField;

        private DateTime timeModifiedField;

        private string editSequenceField;

        private string nameField;

        private bool isActiveField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string TimeCreated
        {
            get
            {
                return timeCreatedField;
            }
            set
            {
                timeCreatedField = value;
            }
        }

        /// <remarks/>
        public DateTime TimeModified
        {
            get
            {
                return timeModifiedField;
            }
            set
            {
                timeModifiedField = value;
            }
        }

        /// <remarks/>
        public string EditSequence
        {
            get
            {
                return editSequenceField;
            }
            set
            {
                editSequenceField = value;
            }
        }

        /// <remarks/>
        public string Name
        {
            get
            {
                return nameField;
            }
            set
            {
                nameField = value;
            }
        }

        /// <remarks/>
        public bool IsActive
        {
            get
            {
                return isActiveField;
            }
            set
            {
                isActiveField = value;
            }
        }
    }


}
