﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;
using DataProcessor.QB.XMLProcessor;
using DataProcessor.Sync.ModelRet;

namespace DataProcessor.Sync
{
    /// <summary>
    /// Note: Do not change enum names, it is using to generate ganaric xml
    /// </summary>

    public enum QueryXML
    {
        [Description(SyncConstant.QUERY_XML_PREFIX + "<CustomerQueryRq>\r\n<ActiveStatus >" + SyncConstant.RECORD_ACTIVE_STATUS + "</ActiveStatus>\r\n" + SyncConstant.PARAMETER_CAPTION + " </CustomerQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        Customer,
        [Description(SyncConstant.QUERY_XML_PREFIX + "<TemplateQueryRq>\r\n" + SyncConstant.PARAMETER_CAPTION + "</TemplateQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        Template,
        [Description(SyncConstant.QUERY_XML_PREFIX + "<CustomerQueryRq>" + SyncConstant.PARAMETER_CAPTION + "</CustomerQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        CustomerSearch,
        [Description(SyncConstant.QUERY_XML_PREFIX + "<JournalEntryRq>\r\n" + SyncConstant.PARAMETER_CAPTION + "<IncludeLineItems >true</IncludeLineItems></JournalEntryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        JournalEntry,
        [Description(SyncConstant.QUERY_XML_PREFIX + "<BillQueryRq>\r\n" + SyncConstant.PARAMETER_CAPTION + "<IncludeLineItems >true</IncludeLineItems></BillQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        Bill,
        [Description(SyncConstant.QUERY_XML_PREFIX + "<VendorCreditAddRq>\r\n" + SyncConstant.PARAMETER_CAPTION + "<IncludeLineItems >true</IncludeLineItems></VendorCreditAddRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        VendorCredit,
        [Description(SyncConstant.QUERY_XML_PREFIX + "<ItemNonInventoryQueryRq>\r\n<ActiveStatus >" + SyncConstant.RECORD_ACTIVE_STATUS + "</ActiveStatus>\r\n" + SyncConstant.PARAMETER_CAPTION + "</ItemNonInventoryQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        ItemNonInventory,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<ItemInventoryQueryRq>\r\n<ActiveStatus >" + SyncConstant.RECORD_ACTIVE_STATUS + "</ActiveStatus>\r\n" + SyncConstant.PARAMETER_CAPTION + "</ItemInventoryQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        ItemInventory,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<ItemServiceQueryRq>\r\n<ActiveStatus >" + SyncConstant.RECORD_ACTIVE_STATUS + "</ActiveStatus>\r\n" + SyncConstant.PARAMETER_CAPTION + "</ItemServiceQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        ItemService,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<ItemDiscountQueryRq>\r\n<ActiveStatus >" + SyncConstant.RECORD_ACTIVE_STATUS + "</ActiveStatus>\r\n" + SyncConstant.PARAMETER_CAPTION + "</ItemDiscountQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        ItemDiscount,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<ItemFixedAssetQueryRq>\r\n<ActiveStatus >" + SyncConstant.RECORD_ACTIVE_STATUS + "</ActiveStatus>\r\n" + SyncConstant.PARAMETER_CAPTION + "</ItemFixedAssetQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        ItemFixedAsset,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<ItemInventoryAssemblyQueryRq>\r\n<ActiveStatus >" + SyncConstant.RECORD_ACTIVE_STATUS + "</ActiveStatus>\r\n" + SyncConstant.PARAMETER_CAPTION + "</ItemInventoryAssemblyQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        ItemInventoryAssembly,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<ItemOtherChargeQueryRq>\r\n<ActiveStatus >" + SyncConstant.RECORD_ACTIVE_STATUS + "</ActiveStatus>\r\n" + SyncConstant.PARAMETER_CAPTION + "</ItemOtherChargeQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        ItemOtherCharge,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<ItemPaymentQueryRq>\r\n<ActiveStatus >" + SyncConstant.RECORD_ACTIVE_STATUS + "</ActiveStatus>\r\n" + SyncConstant.PARAMETER_CAPTION + "</ItemPaymentQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        ItemPayment,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<SalesTaxCodeQueryRq><ActiveStatus >" + SyncConstant.RECORD_ACTIVE_STATUS + "</ActiveStatus>\r\n" + SyncConstant.PARAMETER_CAPTION + "</SalesTaxCodeQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        SalesTaxCode,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<ItemSalesTaxQueryRq><ActiveStatus >" + SyncConstant.RECORD_ACTIVE_STATUS + "</ActiveStatus>\r\n" + SyncConstant.PARAMETER_CAPTION + "</ItemSalesTaxQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        ItemSalesTax,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<CustomerAdd>\r\n" + SyncConstant.PARAMETER_CAPTION + "<IncludeLineItems >true</IncludeLineItems></CustomerAdd>" + SyncConstant.QUERY_XML_POSTFIX)]
        CustomerAdd,
        [Description(SyncConstant.QUERY_XML_PREFIX + "<CustomerMod>\r\n" + SyncConstant.PARAMETER_CAPTION + "<IncludeLineItems >true</IncludeLineItems></CustomerMod>" + SyncConstant.QUERY_XML_POSTFIX)]
        CustomerMod,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<InvoiceQueryRq>\r\n" + SyncConstant.PARAMETER_CAPTION + "<IncludeLineItems >true</IncludeLineItems>\r\n</InvoiceQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        Invoice,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<InvoiceAddRq>\r\n" + SyncConstant.PARAMETER_CAPTION + "<IncludeLineItems >true</IncludeLineItems></InvoiceAddRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        InvoiceAdd,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<SalesReceiptAddRq>\r\n" + SyncConstant.PARAMETER_CAPTION + "<IncludeLineItems >true</IncludeLineItems></SalesReceiptAddRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        SalesReceiptAdd,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<PaymentAddRq>\r\n" + SyncConstant.PARAMETER_CAPTION + "<IncludeLineItems >true</IncludeLineItems></PaymentAddRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        PaymentAdd,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<SalesReceiptQueryRq>" + SyncConstant.PARAMETER_CAPTION + "<IncludeLineItems >true</IncludeLineItems></SalesReceiptQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        SalesReceipt,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<PaymentMethodQueryRq  requestID=\"4\">\r\n <ActiveStatus >" + SyncConstant.RECORD_ACTIVE_STATUS + "</ActiveStatus>\r\n" + SyncConstant.PARAMETER_CAPTION + " </PaymentMethodQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        PaymentMethod,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<PreferencesQueryRq>\r\n" + SyncConstant.PARAMETER_CAPTION + "</PreferencesQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        PreferencesReportQuery,
        [Description(SyncConstant.QUERY_XML_PREFIX + "<AgingReportQueryRq>\r\n" + SyncConstant.PARAMETER_CAPTION + "</AgingReportQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        AgingReportQuery,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<AccountQueryRq>\r\n" + SyncConstant.PARAMETER_CAPTION + "</AccountQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        Account,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<ClassQueryRq>\r\n" + SyncConstant.PARAMETER_CAPTION + "</ClassQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        Class,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<CompanyQueryRq>\r\n" + SyncConstant.PARAMETER_CAPTION + "</CompanyQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        Company,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<VendorQueryRq><ActiveStatus >" + SyncConstant.RECORD_ACTIVE_STATUS + "</ActiveStatus>\r\n" + SyncConstant.PARAMETER_CAPTION + "</VendorQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        Vendor,
        [Description(SyncConstant.QUERY_XML_PREFIX + "<VendorQueryRq>" + SyncConstant.PARAMETER_CAPTION + "</VendorQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        VendorQuery,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<CustomerMsgQueryRq>\r\n<ActiveStatus >" + SyncConstant.RECORD_ACTIVE_STATUS + "</ActiveStatus>\r\n" + SyncConstant.PARAMETER_CAPTION + "</CustomerMsgQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        CustomerMsg,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<ReceivePaymentQueryRq>\r\n" + SyncConstant.PARAMETER_CAPTION + "<IncludeLineItems >true</IncludeLineItems></ReceivePaymentQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        ReceivePayment,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<CreditMemoQueryRq>\r\n" + SyncConstant.PARAMETER_CAPTION + "<IncludeLineItems >true</IncludeLineItems></CreditMemoQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        CreditMemo,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<AccountQueryRq></AccountQueryRq><ClassQueryRq></ClassQueryRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        AccountClass,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<CheckAddRq>\r\n" + SyncConstant.PARAMETER_CAPTION + "</CheckAddRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        Check,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<BillPaymentCheckAddRq>\r\n" + SyncConstant.PARAMETER_CAPTION + "<IncludeLineItems >true</IncludeLineItems></BillPaymentCheckAddRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        BillPaymentCheck,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<BillPaymentCheckQueryRq  >\r\n" + SyncConstant.PARAMETER_CAPTION + "<IncludeLineItems >true</IncludeLineItems> </BillPaymentCheckQueryRq >" + SyncConstant.QUERY_XML_POSTFIX)]
        BillPaymentCheckQuery,

        [Description(SyncConstant.QUERY_XML_PREFIX + "<CreditCardChargeAddRq>\r\n" + SyncConstant.PARAMETER_CAPTION + "</CreditCardChargeAddRq>" + SyncConstant.QUERY_XML_POSTFIX)]
        CreditCardCharge,
    }

    public static class SyncConstant
    {
        internal const string QUERY_XML_PREFIX = "<?xml version=\"1.0\" ?>\r\n<?qbxml version=\"9.0\"?>\r\n<QBXML>\r\n<QBXMLMsgsRq onError=\"stopOnError\">\r\n";
        internal const string PARAMETER_CAPTION = "PARAMETER";

        public const int TOTAL_ENTITY_COUNT = 10;

        public const string QUERY_XML_POSTFIX = "\r\n</QBXMLMsgsRq>\r\n</QBXML>";
        public const string RECORD_ACTIVE_STATUS = "All";

        public static bool IsInsertingInQuickBooks { get; set; }


        public static string EnumDescription(this Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes.Length > 0)
                return attributes[0].Description;
            return value.ToString();
        }
        public static string EnumName(this Enum value)
        {

            return value.ToString();
        }
        /// <summary>
        /// Serialize Xml to List
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xml"></param>
        /// <param name="query"></param>
        /// <param name="includeRootElement"></param>
        /// <returns></returns>
        public static List<T> SerializeXml<T>(string xml, QueryXML query, bool includeRootElement = true)
        {

            string entityName = query.EnumName();
            if (entityName == "VendorQuery") entityName = "Vendor";
            if (entityName == "InvoiceAdd") entityName = "Invoice";
            if (entityName == "CustomerSearch") entityName = "Customer";
            if (entityName == "PreferencesReportQuery") entityName = "Preferences";
            if (entityName == "BillPaymentCheckQuery") entityName = "BillPaymentCheck";
            if (entityName == "AgingReportQuery") entityName = "AgingReport";
            XmlSerializer x;
            if (includeRootElement)
            {
                XmlRootAttribute xRoot = new XmlRootAttribute { ElementName = entityName + "QueryRs", IsNullable = true };
                x = new XmlSerializer(typeof(List<T>), xRoot);
            }
            else
            {
                x = new XmlSerializer(typeof(T));
            }


            try
            {
                object deserializedObject = x.Deserialize(new StringReader(xml));
                if (includeRootElement)
                {
                    List<T> ret = (List<T>)deserializedObject;
                    return ret;
                }
                else
                {
                    T ret = (T)deserializedObject;
                    List<T> t = new List<T> { ret };
                    return t;
                }
            }
            catch (Exception e)
            {
                GlobalValue.Eventlogupdate(e, null, "Error");
                return new List<T>();
            }

        }

        /// <summary>
        /// Generate XML String
        /// </summary>
        /// <param name="xmlString"></param>
        /// <returns></returns>
        public static string GenerateXML(string xmlString)
        {
            string xml = "";

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlString);
            XmlNode selectSingleNode = xmlDoc.SelectSingleNode("QBXML");
            if (selectSingleNode != null)
            {
                XmlNode singleNode = selectSingleNode.SelectSingleNode("QBXMLMsgsRs");
                if (singleNode != null)
                    xml = singleNode.InnerXml;
            }

            return xml;
        }

        private static List<PaymentMethodRet> _syncPaymentMethod;

    }
}
