﻿using DataProcessor.Ref;
using System.Xml.Serialization;

namespace DataProcessor.Sync.ModelAdd
{
    [XmlTypeAttribute(AnonymousType = true)]
    [XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class CustomerAdd
    {

        public string ListID { get; set; }
        public string EditSequence { get; set; }        
        public string Name { get; set; }
        public string IsActive { get; set; }
        public ClassRef ClassRef { get; set; }
        public ParentRef ParentRef { get; set; }

        public string CompanyName { get; set; }

        public string Salutation { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string JobTitle { get; set; }

        public BillAddress BillAddress { get; set; }

        public ShipAddress ShipAddress { get; set; }

        public ShipToAddress ShipToAddress { get; set; }
        public string Email { get; set; }
        
    }
}