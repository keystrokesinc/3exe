﻿using DataProcessor.Ref;

namespace DataProcessor.Sync.ModelAdd
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class VendorAdd
    {

        private string nameField;

        private bool isActiveField;

        private ClassRef classRefField;

        private string companyNameField;

        private string salutationField;

        private string firstNameField;

        private string middleNameField;

        private string lastNameField;

        private string jobTitleField;

        private BillAddress vendorAddressField;

        private ShipAddress shipAddressField;

        private string phoneField;

        private string altPhoneField;

        private string faxField;

        private string emailField;

        private string ccField;

        private string contactField;

        private string altContactField;

        //private VendorAddAdditionalContactRef additionalContactRefField;

        //private VendorAddContacts contactsField;

        private string nameOnCheckField;

        private string accountNumberField;

        private string notesField;

        //private VendorAddAdditionalNotes additionalNotesField;

        //private VendorAddVendorTypeRef vendorTypeRefField;

        //private VendorAddTermsRef termsRefField;

        private string creditLimitField;

        private string vendorTaxIdentField;

        private string isVendorEligibleFor1099Field;

        private string openBalanceField;

        private string openBalanceDateField;

        //private VendorAddBillingRateRef billingRateRefField;

        private string externalGUIDField;

        //private VendorAddPrefillAccountRef prefillAccountRefField;

        //private VendorAddCurrencyRef currencyRefField;

        /// <remarks/>
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public bool IsActive
        {
            get
            {
                return this.isActiveField;
            }
            set
            {
                this.isActiveField = value;
            }
        }

        /// <remarks/>
        public ClassRef ClassRef
        {
            get
            {
                return this.classRefField;
            }
            set
            {
                this.classRefField = value;
            }
        }

        /// <remarks/>
        public string CompanyName
        {
            get
            {
                return this.companyNameField;
            }
            set
            {
                this.companyNameField = value;
            }
        }

        /// <remarks/>
        public string Salutation
        {
            get
            {
                return this.salutationField;
            }
            set
            {
                this.salutationField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string MiddleName
        {
            get
            {
                return this.middleNameField;
            }
            set
            {
                this.middleNameField = value;
            }
        }

        /// <remarks/>
        public string LastName
        {
            get
            {
                return this.lastNameField;
            }
            set
            {
                this.lastNameField = value;
            }
        }

        /// <remarks/>
        public string JobTitle
        {
            get
            {
                return this.jobTitleField;
            }
            set
            {
                this.jobTitleField = value;
            }
        }

        /// <remarks/>
        public BillAddress VendorAddress
        {
            get
            {
                return this.vendorAddressField;
            }
            set
            {
                this.vendorAddressField = value;
            }
        }

        /// <remarks/>
        public ShipAddress ShipAddress
        {
            get
            {
                return this.shipAddressField;
            }
            set
            {
                this.shipAddressField = value;
            }
        }

        /// <remarks/>
        public string Phone
        {
            get
            {
                return this.phoneField;
            }
            set
            {
                this.phoneField = value;
            }
        }

        /// <remarks/>
        public string AltPhone
        {
            get
            {
                return this.altPhoneField;
            }
            set
            {
                this.altPhoneField = value;
            }
        }

        /// <remarks/>
        public string Fax
        {
            get
            {
                return this.faxField;
            }
            set
            {
                this.faxField = value;
            }
        }

        /// <remarks/>
        public string Email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }

        /// <remarks/>
        public string Cc
        {
            get
            {
                return this.ccField;
            }
            set
            {
                this.ccField = value;
            }
        }

        /// <remarks/>
        public string Contact
        {
            get
            {
                return this.contactField;
            }
            set
            {
                this.contactField = value;
            }
        }

        /// <remarks/>
        public string AltContact
        {
            get
            {
                return this.altContactField;
            }
            set
            {
                this.altContactField = value;
            }
        }

        /// <remarks/>
        //public VendorAddAdditionalContactRef AdditionalContactRef
        //{
        //    get
        //    {
        //        return this.additionalContactRefField;
        //    }
        //    set
        //    {
        //        this.additionalContactRefField = value;
        //    }
        //}

        /// <remarks/>
        //public VendorAddContacts Contacts
        //{
        //    get
        //    {
        //        return this.contactsField;
        //    }
        //    set
        //    {
        //        this.contactsField = value;
        //    }
        //}

        /// <remarks/>
        public string NameOnCheck
        {
            get
            {
                return this.nameOnCheckField;
            }
            set
            {
                this.nameOnCheckField = value;
            }
        }

        /// <remarks/>
        public string AccountNumber
        {
            get
            {
                return this.accountNumberField;
            }
            set
            {
                this.accountNumberField = value;
            }
        }

        /// <remarks/>
        public string Notes
        {
            get
            {
                return this.notesField;
            }
            set
            {
                this.notesField = value;
            }
        }

        /// <remarks/>
        //public VendorAddAdditionalNotes AdditionalNotes
        //{
        //    get
        //    {
        //        return this.additionalNotesField;
        //    }
        //    set
        //    {
        //        this.additionalNotesField = value;
        //    }
        //}

        /// <remarks/>
        //public VendorAddVendorTypeRef VendorTypeRef
        //{
        //    get
        //    {
        //        return this.vendorTypeRefField;
        //    }
        //    set
        //    {
        //        this.vendorTypeRefField = value;
        //    }
        //}

        /// <remarks/>
        //public VendorAddTermsRef TermsRef
        //{
        //    get
        //    {
        //        return this.termsRefField;
        //    }
        //    set
        //    {
        //        this.termsRefField = value;
        //    }
        //}

        /// <remarks/>
        public string CreditLimit
        {
            get
            {
                return this.creditLimitField;
            }
            set
            {
                this.creditLimitField = value;
            }
        }

        /// <remarks/>
        public string VendorTaxIdent
        {
            get
            {
                return this.vendorTaxIdentField;
            }
            set
            {
                this.vendorTaxIdentField = value;
            }
        }

        /// <remarks/>
        public string IsVendorEligibleFor1099
        {
            get
            {
                return this.isVendorEligibleFor1099Field;
            }
            set
            {
                this.isVendorEligibleFor1099Field = value;
            }
        }

        /// <remarks/>
        public string OpenBalance
        {
            get
            {
                return this.openBalanceField;
            }
            set
            {
                this.openBalanceField = value;
            }
        }

        /// <remarks/>
        public string OpenBalanceDate
        {
            get
            {
                return this.openBalanceDateField;
            }
            set
            {
                this.openBalanceDateField = value;
            }
        }

        /// <remarks/>
        //public VendorAddBillingRateRef BillingRateRef
        //{
        //    get
        //    {
        //        return this.billingRateRefField;
        //    }
        //    set
        //    {
        //        this.billingRateRefField = value;
        //    }
        //}

        /// <remarks/>
        public string ExternalGUID
        {
            get
            {
                return this.externalGUIDField;
            }
            set
            {
                this.externalGUIDField = value;
            }
        }

        /// <remarks/>
        //public VendorAddPrefillAccountRef PrefillAccountRef
        //{
        //    get
        //    {
        //        return this.prefillAccountRefField;
        //    }
        //    set
        //    {
        //        this.prefillAccountRefField = value;
        //    }
        //}

        /// <remarks/>
        //public VendorAddCurrencyRef CurrencyRef
        //{
        //    get
        //    {
        //        return this.currencyRefField;
        //    }
        //    set
        //    {
        //        this.currencyRefField = value;
        //    }
        //}
    }
}