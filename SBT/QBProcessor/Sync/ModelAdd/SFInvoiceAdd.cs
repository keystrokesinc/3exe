﻿using DataProcessor.Ref;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace DataProcessor.Sync.ModelAdd
{    
    public partial class SFInvoiceAdd
    {
        public class Item
        {
            public string OpportunityStage { get; set; }
            public string Opportunity { get; set; }
            public string Name { get; set; }
            public string Id { get; set; }
            public string GeneralAccountingUnit { get; set; }
            public string DateOfDonation { get; set; }
            public string ClassName { get; set; }
            public double Amount { get; set; }
        }

        public class Invoice
        {
            public object TxnID { get; set; }
            public string TxnDate { get; set; }
            public string SFReferance { get; set; }
            public string RefNumber { get; set; }
            public string PaidInstallments { get; set; }
            public string PaidAmount { get; set; }
            public List<Item> Items { get; set; }
            public string ItemRefFullName { get; set; }
            public string IitemLIstID { get; set; }
            public string DueDate { get; set; }
            public object CustomerRefListID { get; set; }
            public string CustomerRefFullName { get; set; }
            public string ClassRefListID { get; set; }
            public string ClassRefFullName { get; set; }
            public string Amount { get; set; }
        }

        public class RootObject
        {
            public string Message { get; set; }
            public bool isSuccess { get; set; }
            public List<Invoice> invoices { get; set; }
        }

        public class Customer
        {
            public string SFReferance { get; set; }
            public string Salutation { get; set; }
            public string Name { get; set; }
            public string LastName { get; set; }
            public string FirstName { get; set; }
            public string BillAddressState { get; set; }
            public string BillAddressPostalCode { get; set; }
            public string BillAddressCountry { get; set; }
            public string BillAddressCity { get; set; }
            public string BillAddressAddr1 { get; set; }
        }
    }
}