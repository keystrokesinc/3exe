﻿using DataProcessor.Ref;
using DataProcessor.Sync.ModelRet;
using System;
using System.Xml.Serialization;

namespace DataProcessor.Sync.ModelAdd
{

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]

    public partial class SalesReceiptAdd
    {

        private string txnIDField;

        //private DateTime timeCreatedField;

        //private DateTime timeModifiedField;

        private string editSequenceField;

        private string txnNumberField;
  
        private InvoiceRetCustomerRef customerRefField;
  
        private InvoiceRetClassRef classRefField;
  
        private SalesReceiptRetDepositToAccountRef depositToAccountRefField;
   
   

        private InvoiceRetARAccountRef aRAccountRefField;

        private InvoiceRetTemplateRef templateRefField;

        private string txnDateField;

        private string refNumberField;

        private BillAddress billAddressField;

        private BillAddressBlock billAddressBlockField;

        private ShipAddress shipAddressField;

        private ShipAddressBlock shipAddressBlockField;

        private string isPendingField;

        private string isFinanceChargeField;

        private string pONumberField;

        private TermsRef termsRefField;

        private string dueDateField;

        private InvoiceRetSalesRepRef salesRepRefField;

        private string fOBField;

        private string shipDateField;

        private InvoiceRetShipMethodRef shipMethodRefField;

        private string subtotalField;

        private InvoiceRetItemSalesTaxRef itemSalesTaxRefField;

        private string salesTaxPercentageField;

        private string salesTaxTotalField;

        private string appliedAmountField;

        private string balanceRemainingField;

        private InvoiceRetCurrencyRef currencyRefField;

        private string exchangeRateField;

        private string balanceRemainingInHomeCurrencyField;

        private string memoField;

        private string isPaidField;

        private InvoiceRetCustomerMsgRef customerMsgRefField;

        private string isToBePrintedField;

        private string isToBeEmailedField;

        private InvoiceRetCustomerSalesTaxCodeRef customerSalesTaxCodeRefField;

        private string suggestedDiscountAmountField;

        private string suggestedDiscountDateField;

        private string otherField;

        private string externalGUIDField;

        private InvoiceRetLinkedTxn linkedTxnField;

        //private InvoiceRetInvoiceLineRet[] invoiceLineRetField;
        private SalesReceiptRetSalesReceiptLineRet[] salesReceiptLineRetField;
        private SalesReceiptRetSalesReceiptLineGroupRet invoiceLineGroupRetField; 
        //private InvoiceRetInvoiceLineGroupRet invoiceLineGroupRetField;

        private InvoiceRetDataExtRet dataExtRetField;
        private string checkNumber;
        private SalesReceiptPaymentMethodRef paymentMethodRef;


        [XmlElement(Order = 51)]
        /// <remarks/>
        public string TxnID
        {
            get
            {
                return txnIDField;
            }
            set
            {
                txnIDField = value;
            }
        }

        /// <remarks/>
        //public DateTime TimeCreated
        //{
        //    get
        //    {
        //        return timeCreatedField;
        //    }
        //    set
        //    {
        //        timeCreatedField = value;
        //    }
        //}

        ///// <remarks/>
        //public DateTime TimeModified
        //{
        //    get
        //    {
        //        return timeModifiedField;
        //    }
        //    set
        //    {
        //        timeModifiedField = value;
        //    }
        //}
        [XmlElement(Order = 52)]
        /// <remarks/>
        public string EditSequence
        {
            get
            {
                return editSequenceField;
            }
            set
            {
                editSequenceField = value;
            }
        }
        [XmlElement(Order = 53)]
        /// <remarks/>
        public string TxnNumber
        {
            get
            {
                return txnNumberField;
            }
            set
            {
                txnNumberField = value;
            }
        }

        /// <remarks/>
        [XmlElement( Order = 1)]
        public InvoiceRetCustomerRef CustomerRef
        {
            get
            {
                return customerRefField;
            }
            set
            {
                customerRefField = value;
            }
        }
       
        /// <remarks/>
        [XmlElement(Order = 2)]
        public InvoiceRetClassRef ClassRef
        {
            get
            {
                return classRefField;
            }
            set
            {
                classRefField = value;
            }
        }
        [XmlElement(Order = 54)]
        /// <remarks/>
        public InvoiceRetARAccountRef ARAccountRef
        {
            get
            {
                return aRAccountRefField;
            }
            set
            {
                aRAccountRefField = value;
            }
        }
        [XmlElement(Order = 4)]
        /// <remarks/>
        public InvoiceRetTemplateRef TemplateRef
        {
            get
            {
                return templateRefField;
            }
            set
            {
                templateRefField = value;
            }
        }
        [XmlElement(Order = 5)]
        /// <remarks/>
        public string TxnDate
        {
            get
            {
                return txnDateField;
            }
            set
            {
                txnDateField = value;
            }
        }
        [XmlElement(Order = 6)]
        /// <remarks/>
        public string RefNumber
        {
            get
            {
                return refNumberField;
            }
            set
            {
                refNumberField = value;
            }
        }
        [XmlElement(Order = 7)]
        /// <remarks/>
        public BillAddress BillAddress
        {
            get
            {
                return billAddressField;
            }
            set
            {
                billAddressField = value;
            }
        }
        [XmlElement(Order = 8)]
        /// <remarks/>
        public BillAddressBlock BillAddressBlock
        {
            get
            {
                return billAddressBlockField;
            }
            set
            {
                billAddressBlockField = value;
            }
        }
        [XmlElement(Order = 9)]
        /// <remarks/>
        public ShipAddress ShipAddress
        {
            get
            {
                return shipAddressField;
            }
            set
            {
                shipAddressField = value;
            }
        }
        [XmlElement(Order = 10)]
        /// <remarks/>
        public ShipAddressBlock ShipAddressBlock
        {
            get
            {
                return shipAddressBlockField;
            }
            set
            {
                shipAddressBlockField = value;
            }
        }
        [XmlElement(Order = 11)]
        /// <remarks/>
        public string IsPending
        {
            get
            {
                return isPendingField;
            }
            set
            {
                isPendingField = value;
            }
        }
        [XmlElement(Order = 12)]
        /// <remarks/>
        public string IsFinanceCharge
        {
            get
            {
                return isFinanceChargeField;
            }
            set
            {
                isFinanceChargeField = value;
            }
        }
        [XmlElement(Order = 13)]
        /// <remarks/>
        public string PONumber
        {
            get
            {
                return pONumberField;
            }
            set
            {
                pONumberField = value;
            }
        }
        [XmlElement(Order = 14)]
        /// <remarks/>
        public TermsRef TermsRef
        {
            get
            {
                return termsRefField;
            }
            set
            {
                termsRefField = value;
            }
        }
        [XmlElement(Order = 15)]
        /// <remarks/>
        public string DueDate
        {
            get
            {
                return dueDateField;
            }
            set
            {
                dueDateField = value;
            }
        }
        [XmlElement(Order = 16)]
        /// <remarks/>
        public InvoiceRetSalesRepRef SalesRepRef
        {
            get
            {
                return salesRepRefField;
            }
            set
            {
                salesRepRefField = value;
            }
        }
        [XmlElement(Order = 17)]
        /// <remarks/>
        public string FOB
        {
            get
            {
                return fOBField;
            }
            set
            {
                fOBField = value;
            }
        }
        [XmlElement(Order = 18)]
        /// <remarks/>
        public string ShipDate
        {
            get
            {
                return shipDateField;
            }
            set
            {
                shipDateField = value;
            }
        }
        [XmlElement(Order = 55)]
        /// <remarks/>
        public InvoiceRetShipMethodRef ShipMethodRef
        {
            get
            {
                return shipMethodRefField;
            }
            set
            {
                shipMethodRefField = value;
            }
        }

        [XmlElement(Order = 56)]
        /// <remarks/>
        public string Subtotal
        {
            get
            {
                return subtotalField;
            }
            set
            {
                subtotalField = value;
            }
        }
        [XmlElement(Order = 19)]
        /// <remarks/>
        public InvoiceRetItemSalesTaxRef ItemSalesTaxRef
        {
            get
            {
                return itemSalesTaxRefField;
            }
            set
            {
                itemSalesTaxRefField = value;
            }
        }
        [XmlElement(Order = 20)]
        /// <remarks/>
        public string SalesTaxPercentage
        {
            get
            {
                return salesTaxPercentageField;
            }
            set
            {
                salesTaxPercentageField = value;
            }
        }
        [XmlElement(Order = 21)]
        /// <remarks/>
        public string SalesTaxTotal
        {
            get
            {
                return salesTaxTotalField;
            }
            set
            {
                salesTaxTotalField = value;
            }
        }
        [XmlElement(Order = 22)]
        /// <remarks/>
        public string AppliedAmount
        {
            get
            {
                return appliedAmountField;
            }
            set
            {
                appliedAmountField = value;
            }
        }
        [XmlElement(Order = 23)]
        /// <remarks/>
        public string BalanceRemaining
        {
            get
            {
                return balanceRemainingField;
            }
            set
            {
                balanceRemainingField = value;
            }
        }
        [XmlElement(Order = 24)]
        /// <remarks/>
        public InvoiceRetCurrencyRef CurrencyRef
        {
            get
            {
                return currencyRefField;
            }
            set
            {
                currencyRefField = value;
            }
        }
        [XmlElement(Order = 25)]
        /// <remarks/>
        public string ExchangeRate
        {
            get
            {
                return exchangeRateField;
            }
            set
            {
                exchangeRateField = value;
            }
        }
        [XmlElement(Order = 26)]
        /// <remarks/>
        public string BalanceRemainingInHomeCurrency
        {
            get
            {
                return balanceRemainingInHomeCurrencyField;
            }
            set
            {
                balanceRemainingInHomeCurrencyField = value;
            }
        }
        [XmlElement(Order = 27)]
        public string CheckNumber
        {
            get
            {
                return checkNumber;
            }
            set
            {
                checkNumber = value;
            }
        }
        [XmlElement(Order = 28)]
        public SalesReceiptPaymentMethodRef PaymentMethodRef
        {
            get
            {
                return paymentMethodRef;
            }
            set
            {
                paymentMethodRef = value;
            }
        }
        [XmlElement(Order = 29)]
        public string Memo
        {
            get
            {
                return memoField;
            }
            set
            {
                memoField = value;
            }
        }

        [XmlElement(Order = 30)]
        /// <remarks/>
        public string IsPaid
        {
            get
            {
                return isPaidField;
            }
            set
            {
                isPaidField = value;
            }
        }

        [XmlElement(Order = 31)]
        public InvoiceRetCustomerMsgRef CustomerMsgRef
        {
            get
            {
                return customerMsgRefField;
            }
            set
            {
                customerMsgRefField = value;
            }
        }
        [XmlElement(Order = 32)]
        public SalesReceiptRetDepositToAccountRef DepositToAccountRef
        {
            get
            {
                return depositToAccountRefField;
            }
            set
            {
                depositToAccountRefField = value;
            }
        }
        //[XmlElement(Order = 32)]
        //public string IsToBePrinted
        //{
        //    get
        //    {
        //        return isToBePrintedField;
        //    }
        //    set
        //    {
        //        isToBePrintedField = value;
        //    }
        //}

        [XmlElement(Order = 33)]
        public string IsToBeEmailed
        {
            get
            {
                return isToBeEmailedField;
            }
            set
            {
                isToBeEmailedField = value;
            }
        }

        [XmlElement(Order = 34)]
        public InvoiceRetCustomerSalesTaxCodeRef CustomerSalesTaxCodeRef
        {
            get
            {
                return customerSalesTaxCodeRefField;
            }
            set
            {
                customerSalesTaxCodeRefField = value;
            }
        }

        [XmlElement(Order = 35)]
        public string SuggestedDiscountAmount
        {
            get
            {
                return suggestedDiscountAmountField;
            }
            set
            {
                suggestedDiscountAmountField = value;
            }
        }

        [XmlElement(Order = 36)]
        public string SuggestedDiscountDate
        {
            get
            {
                return suggestedDiscountDateField;
            }
            set
            {
                suggestedDiscountDateField = value;
            }
        }

        [XmlElement(Order = 37)]
        public string Other
        {
            get
            {
                return otherField;
            }
            set
            {
                otherField = value;
            }
        }

        [XmlElement(Order = 38)]
        public string ExternalGUID
        {
            get
            {
                return externalGUIDField;
            }
            set
            {
                externalGUIDField = value;
            }
        }

        [XmlElement(Order = 39)]
        public InvoiceRetLinkedTxn LinkedTxn
        {
            get
            {
                return linkedTxnField;
            }
            set
            {
                linkedTxnField = value;
            }
        }
   
        //[XmlElement(ElementName ="SalesReceiptLineAdd", Order =40)]
        //public InvoiceRetInvoiceLineRet[] InvoiceLineAdd
        //{
        //    get
        //    {
        //        return invoiceLineRetField;
        //    }
        //    set
        //    {
        //        invoiceLineRetField = value;
        //    }
        //}
        [XmlElement(ElementName = "SalesReceiptLineAdd", Order = 40)]
        public SalesReceiptRetSalesReceiptLineRet[] SalesReceiptLineAdd
        {
            get
            {
                return salesReceiptLineRetField;
            }
            set
            {
                salesReceiptLineRetField = value;
            }
        }
        [XmlElement(Order = 41)]
        /// <remarks/>
        public SalesReceiptRetSalesReceiptLineGroupRet SalesReceiptGroupLineAdd
        {
            get
            {
                return invoiceLineGroupRetField;
            }
            set
            {
                invoiceLineGroupRetField = value;
            }
        }
        [XmlElement(Order = 42)]
        /// <remarks/>
        public InvoiceRetDataExtRet DataExtRet
        {
            get
            {
                return dataExtRetField;
            }
            set
            {
                dataExtRetField = value;
            }
        }
    
        [XmlType(AnonymousType = true)]

        public class SalesReceiptPaymentMethodRef
        {

            private string listIDField;

            private string fullNameField;
            [XmlElement(Order = 1)]
            /// <remarks/>
            public string ListID
            {
                get
                {
                    return listIDField;
                }
                set
                {
                    listIDField = value;
                }
            }
            [XmlElement(Order = 2)]
            /// <remarks/>
            public string FullName
            {
                get
                {
                    return fullNameField;
                }
                set
                {
                    fullNameField = value;
                }
            }
        }


        /// <remarks/>

    }


}
