﻿
using DataProcessor.Ref;
using System;
using System.Xml.Serialization;

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
public partial class BillAdd
{

    private BillAddVendorRef vendorRefField;

    private BillAddVendorAddress vendorAddressField;

    private BillAddAPAccountRef aPAccountRefField;

    private string txnDateField;

    private string dueDateField;

    private string refNumberField;

    private BillAddTermsRef termsRefField;

    private string memoField;

    private string exchangeRateField;

    private string externalGUIDField;

    private string linkToTxnIDField;

    private BillAddExpenseLineAdd[] expenseLineAddField;

    private BillAddItemLineAdd itemLineAddField;

    private BillAddItemGroupLineAdd itemGroupLineAddField;

    private string defMacroField;

    /// <remarks/>
    public BillAddVendorRef VendorRef
    {
        get
        {
            return this.vendorRefField;
        }
        set
        {
            this.vendorRefField = value;
        }
    }

    /// <remarks/>
    public BillAddVendorAddress VendorAddress
    {
        get
        {
            return this.vendorAddressField;
        }
        set
        {
            this.vendorAddressField = value;
        }
    }

    /// <remarks/>
    public BillAddAPAccountRef APAccountRef
    {
        get
        {
            return this.aPAccountRefField;
        }
        set
        {
            this.aPAccountRefField = value;
        }
    }

    /// <remarks/>
    public string TxnDate
    {
        get
        {
            return this.txnDateField;
        }
        set
        {
            this.txnDateField = value;
        }
    }

    /// <remarks/>
    public string DueDate
    {
        get
        {
            return this.dueDateField;
        }
        set
        {
            this.dueDateField = value;
        }
    }

    /// <remarks/>
    public string RefNumber
    {
        get
        {
            return this.refNumberField;
        }
        set
        {
            this.refNumberField = value;
        }
    }

    /// <remarks/>
    public BillAddTermsRef TermsRef
    {
        get
        {
            return this.termsRefField;
        }
        set
        {
            this.termsRefField = value;
        }
    }

    /// <remarks/>
    public string Memo
    {
        get
        {
            return this.memoField;
        }
        set
        {
            this.memoField = value;
        }
    }

    /// <remarks/>
    public string ExchangeRate
    {
        get
        {
            return this.exchangeRateField;
        }
        set
        {
            this.exchangeRateField = value;
        }
    }

    /// <remarks/>
    public string ExternalGUID
    {
        get
        {
            return this.externalGUIDField;
        }
        set
        {
            this.externalGUIDField = value;
        }
    }

    /// <remarks/>
    public string LinkToTxnID
    {
        get
        {
            return this.linkToTxnIDField;
        }
        set
        {
            this.linkToTxnIDField = value;
        }
    }

     [XmlElement("ExpenseLineAdd")]
    public BillAddExpenseLineAdd[] ExpenseLineAdd
    {
        get
        {
            return this.expenseLineAddField;
        }
        set
        {
            this.expenseLineAddField = value;
        }
    }

    /// <remarks/>
    public BillAddItemLineAdd ItemLineAdd
    {
        get
        {
            return this.itemLineAddField;
        }
        set
        {
            this.itemLineAddField = value;
        }
    }

    /// <remarks/>
    public BillAddItemGroupLineAdd ItemGroupLineAdd
    {
        get
        {
            return this.itemGroupLineAddField;
        }
        set
        {
            this.itemGroupLineAddField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string defMacro
    {
        get
        {
            return this.defMacroField;
        }
        set
        {
            this.defMacroField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillAddVendorRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillAddVendorAddress
{

    private string addr1Field;

    private string addr2Field;

    private string addr3Field;

    private string addr4Field;

    private string addr5Field;

    private string cityField;

    private string stateField;

    private string postalCodeField;

    private string countryField;

    private string noteField;

    /// <remarks/>
    public string Addr1
    {
        get
        {
            return this.addr1Field;
        }
        set
        {
            this.addr1Field = value;
        }
    }

    /// <remarks/>
    public string Addr2
    {
        get
        {
            return this.addr2Field;
        }
        set
        {
            this.addr2Field = value;
        }
    }

    /// <remarks/>
    public string Addr3
    {
        get
        {
            return this.addr3Field;
        }
        set
        {
            this.addr3Field = value;
        }
    }

    /// <remarks/>
    public string Addr4
    {
        get
        {
            return this.addr4Field;
        }
        set
        {
            this.addr4Field = value;
        }
    }

    /// <remarks/>
    public string Addr5
    {
        get
        {
            return this.addr5Field;
        }
        set
        {
            this.addr5Field = value;
        }
    }

    /// <remarks/>
    public string City
    {
        get
        {
            return this.cityField;
        }
        set
        {
            this.cityField = value;
        }
    }

    /// <remarks/>
    public string State
    {
        get
        {
            return this.stateField;
        }
        set
        {
            this.stateField = value;
        }
    }

    /// <remarks/>
    public string PostalCode
    {
        get
        {
            return this.postalCodeField;
        }
        set
        {
            this.postalCodeField = value;
        }
    }

    /// <remarks/>
    public string Country
    {
        get
        {
            return this.countryField;
        }
        set
        {
            this.countryField = value;
        }
    }

    /// <remarks/>
    public string Note
    {
        get
        {
            return this.noteField;
        }
        set
        {
            this.noteField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillAddAPAccountRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillAddTermsRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillAddExpenseLineAdd
{

    private BillAddExpenseLineAddAccountRef accountRefField;

    private decimal amountField;

    private string memoField;

    private BillAddExpenseLineAddCustomerRef customerRefField;

    private ClassRef classRefField;

    private string billableStatusField;

    private BillAddExpenseLineAddSalesRepRef salesRepRefField;

    private BillAddExpenseLineAddDataExt dataExtField;

    private string defMacroField;

    /// <remarks/>
    public BillAddExpenseLineAddAccountRef AccountRef
    {
        get
        {
            return this.accountRefField;
        }
        set
        {
            this.accountRefField = value;
        }
    }

    /// <remarks/>
    public decimal Amount
    {
        get
        {
            return this.amountField;
        }
        set
        {
            this.amountField = value;
        }
    }

    /// <remarks/>
    public string Memo
    {
        get
        {
            return this.memoField;
        }
        set
        {
            this.memoField = value;
        }
    }

    /// <remarks/>
    public BillAddExpenseLineAddCustomerRef CustomerRef
    {
        get
        {
            return this.customerRefField;
        }
        set
        {
            this.customerRefField = value;
        }
    }

    /// <remarks/>
    public ClassRef ClassRef
    {
        get
        {
            return this.classRefField;
        }
        set
        {
            this.classRefField = value;
        }
    }

    /// <remarks/>
    public string BillableStatus
    {
        get
        {
            return this.billableStatusField;
        }
        set
        {
            this.billableStatusField = value;
        }
    }

    /// <remarks/>
    public BillAddExpenseLineAddSalesRepRef SalesRepRef
    {
        get
        {
            return this.salesRepRefField;
        }
        set
        {
            this.salesRepRefField = value;
        }
    }

    /// <remarks/>
    public BillAddExpenseLineAddDataExt DataExt
    {
        get
        {
            return this.dataExtField;
        }
        set
        {
            this.dataExtField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string defMacro
    {
        get
        {
            return this.defMacroField;
        }
        set
        {
            this.defMacroField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillAddExpenseLineAddAccountRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillAddExpenseLineAddCustomerRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillAddExpenseLineAddSalesRepRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillAddExpenseLineAddDataExt
{

    private string ownerIDField;

    private string dataExtNameField;

    private string dataExtValueField;

    /// <remarks/>
    public string OwnerID
    {
        get
        {
            return this.ownerIDField;
        }
        set
        {
            this.ownerIDField = value;
        }
    }

    /// <remarks/>
    public string DataExtName
    {
        get
        {
            return this.dataExtNameField;
        }
        set
        {
            this.dataExtNameField = value;
        }
    }

    /// <remarks/>
    public string DataExtValue
    {
        get
        {
            return this.dataExtValueField;
        }
        set
        {
            this.dataExtValueField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillAddItemLineAdd
{

    private BillAddItemLineAddItemRef itemRefField;

    private BillAddItemLineAddInventorySiteRef inventorySiteRefField;

    private BillAddItemLineAddInventorySiteLocationRef inventorySiteLocationRefField;

    private string serialNumberField;

    private string lotNumberField;

    private string descField;

    private string quantityField;

    private string unitOfMeasureField;

    private string costField;

    private string amountField;

    private BillAddItemLineAddCustomerRef customerRefField;

    private BillAddItemLineAddClassRef classRefField;

    private string billableStatusField;

    private BillAddItemLineAddOverrideItemAccountRef overrideItemAccountRefField;

    private BillAddItemLineAddLinkToTxn linkToTxnField;

    private BillAddItemLineAddSalesRepRef salesRepRefField;

    private BillAddItemLineAddDataExt dataExtField;

    /// <remarks/>
    public BillAddItemLineAddItemRef ItemRef
    {
        get
        {
            return this.itemRefField;
        }
        set
        {
            this.itemRefField = value;
        }
    }

    /// <remarks/>
    public BillAddItemLineAddInventorySiteRef InventorySiteRef
    {
        get
        {
            return this.inventorySiteRefField;
        }
        set
        {
            this.inventorySiteRefField = value;
        }
    }

    /// <remarks/>
    public BillAddItemLineAddInventorySiteLocationRef InventorySiteLocationRef
    {
        get
        {
            return this.inventorySiteLocationRefField;
        }
        set
        {
            this.inventorySiteLocationRefField = value;
        }
    }

    /// <remarks/>
    public string SerialNumber
    {
        get
        {
            return this.serialNumberField;
        }
        set
        {
            this.serialNumberField = value;
        }
    }

    /// <remarks/>
    public string LotNumber
    {
        get
        {
            return this.lotNumberField;
        }
        set
        {
            this.lotNumberField = value;
        }
    }

    /// <remarks/>
    public string Desc
    {
        get
        {
            return this.descField;
        }
        set
        {
            this.descField = value;
        }
    }

    /// <remarks/>
    public string Quantity
    {
        get
        {
            return this.quantityField;
        }
        set
        {
            this.quantityField = value;
        }
    }

    /// <remarks/>
    public string UnitOfMeasure
    {
        get
        {
            return this.unitOfMeasureField;
        }
        set
        {
            this.unitOfMeasureField = value;
        }
    }

    /// <remarks/>
    public string Cost
    {
        get
        {
            return this.costField;
        }
        set
        {
            this.costField = value;
        }
    }

    /// <remarks/>
    public string Amount
    {
        get
        {
            return this.amountField;
        }
        set
        {
            this.amountField = value;
        }
    }

    /// <remarks/>
    public BillAddItemLineAddCustomerRef CustomerRef
    {
        get
        {
            return this.customerRefField;
        }
        set
        {
            this.customerRefField = value;
        }
    }

    /// <remarks/>
    public BillAddItemLineAddClassRef ClassRef
    {
        get
        {
            return this.classRefField;
        }
        set
        {
            this.classRefField = value;
        }
    }

    /// <remarks/>
    public string BillableStatus
    {
        get
        {
            return this.billableStatusField;
        }
        set
        {
            this.billableStatusField = value;
        }
    }

    /// <remarks/>
    public BillAddItemLineAddOverrideItemAccountRef OverrideItemAccountRef
    {
        get
        {
            return this.overrideItemAccountRefField;
        }
        set
        {
            this.overrideItemAccountRefField = value;
        }
    }

    /// <remarks/>
    public BillAddItemLineAddLinkToTxn LinkToTxn
    {
        get
        {
            return this.linkToTxnField;
        }
        set
        {
            this.linkToTxnField = value;
        }
    }

    /// <remarks/>
    public BillAddItemLineAddSalesRepRef SalesRepRef
    {
        get
        {
            return this.salesRepRefField;
        }
        set
        {
            this.salesRepRefField = value;
        }
    }

    /// <remarks/>
    public BillAddItemLineAddDataExt DataExt
    {
        get
        {
            return this.dataExtField;
        }
        set
        {
            this.dataExtField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillAddItemLineAddItemRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillAddItemLineAddInventorySiteRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillAddItemLineAddInventorySiteLocationRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillAddItemLineAddCustomerRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillAddItemLineAddClassRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillAddItemLineAddOverrideItemAccountRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillAddItemLineAddLinkToTxn
{

    private string txnIDField;

    private string txnLineIDField;

    /// <remarks/>
    public string TxnID
    {
        get
        {
            return this.txnIDField;
        }
        set
        {
            this.txnIDField = value;
        }
    }

    /// <remarks/>
    public string TxnLineID
    {
        get
        {
            return this.txnLineIDField;
        }
        set
        {
            this.txnLineIDField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillAddItemLineAddSalesRepRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillAddItemLineAddDataExt
{

    private string ownerIDField;

    private string dataExtNameField;

    private string dataExtValueField;

    /// <remarks/>
    public string OwnerID
    {
        get
        {
            return this.ownerIDField;
        }
        set
        {
            this.ownerIDField = value;
        }
    }

    /// <remarks/>
    public string DataExtName
    {
        get
        {
            return this.dataExtNameField;
        }
        set
        {
            this.dataExtNameField = value;
        }
    }

    /// <remarks/>
    public string DataExtValue
    {
        get
        {
            return this.dataExtValueField;
        }
        set
        {
            this.dataExtValueField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillAddItemGroupLineAdd
{

    private BillAddItemGroupLineAddItemGroupRef itemGroupRefField;

    private string quantityField;

    private string unitOfMeasureField;

    private BillAddItemGroupLineAddInventorySiteRef inventorySiteRefField;

    private BillAddItemGroupLineAddInventorySiteLocationRef inventorySiteLocationRefField;

    private BillAddItemGroupLineAddDataExt dataExtField;

    /// <remarks/>
    public BillAddItemGroupLineAddItemGroupRef ItemGroupRef
    {
        get
        {
            return this.itemGroupRefField;
        }
        set
        {
            this.itemGroupRefField = value;
        }
    }

    /// <remarks/>
    public string Quantity
    {
        get
        {
            return this.quantityField;
        }
        set
        {
            this.quantityField = value;
        }
    }

    /// <remarks/>
    public string UnitOfMeasure
    {
        get
        {
            return this.unitOfMeasureField;
        }
        set
        {
            this.unitOfMeasureField = value;
        }
    }

    /// <remarks/>
    public BillAddItemGroupLineAddInventorySiteRef InventorySiteRef
    {
        get
        {
            return this.inventorySiteRefField;
        }
        set
        {
            this.inventorySiteRefField = value;
        }
    }

    /// <remarks/>
    public BillAddItemGroupLineAddInventorySiteLocationRef InventorySiteLocationRef
    {
        get
        {
            return this.inventorySiteLocationRefField;
        }
        set
        {
            this.inventorySiteLocationRefField = value;
        }
    }

    /// <remarks/>
    public BillAddItemGroupLineAddDataExt DataExt
    {
        get
        {
            return this.dataExtField;
        }
        set
        {
            this.dataExtField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillAddItemGroupLineAddItemGroupRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillAddItemGroupLineAddInventorySiteRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillAddItemGroupLineAddInventorySiteLocationRef
{

    private string listIDField;

    private string fullNameField;

    /// <remarks/>
    public string ListID
    {
        get
        {
            return this.listIDField;
        }
        set
        {
            this.listIDField = value;
        }
    }

    /// <remarks/>
    public string FullName
    {
        get
        {
            return this.fullNameField;
        }
        set
        {
            this.fullNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BillAddItemGroupLineAddDataExt
{

    private string ownerIDField;

    private string dataExtNameField;

    private string dataExtValueField;

    /// <remarks/>
    public string OwnerID
    {
        get
        {
            return this.ownerIDField;
        }
        set
        {
            this.ownerIDField = value;
        }
    }

    /// <remarks/>
    public string DataExtName
    {
        get
        {
            return this.dataExtNameField;
        }
        set
        {
            this.dataExtNameField = value;
        }
    }

    /// <remarks/>
    public string DataExtValue
    {
        get
        {
            return this.dataExtValueField;
        }
        set
        {
            this.dataExtValueField = value;
        }
    }
}

