﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DataProcessor.Sync.ModelAdd
{
    [XmlRoot(ElementName = "APAccountRef")]
    public class APAccountRef
    {
        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }
        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }

    }

    [XmlRoot(ElementName = "BankAccountRef")]
    public class BankAccountRef
    {
        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }
        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }

    }

    [XmlRoot(ElementName = "AppliedToTxnAdd")]
    public class AppliedToTxnAdd
    {
        [XmlElement(ElementName = "TxnID")]
        public string TxnID { get; set; }
        [XmlElement(ElementName = "PaymentAmount")]
        public string PaymentAmount { get; set; }
        [XmlElement(ElementName = "DiscountAmount")]
        public string DiscountAmount { get; set; }
        public SetCredit SetCredit { get; set; }
    }
    public class SetCredit
    {

        [XmlElement(ElementName = "CreditTxnID")]
        public string CreditTxnID { get; set; }

        [XmlElement(ElementName = "AppliedAmount")]
        public string AppliedAmount { get; set; }
    }

    [XmlRoot(ElementName = "BillPaymentCheckAdd")]
    public class BillPaymentCheckAdd
    {
        [XmlElement(ElementName = "PayeeEntityRef")]
        public PayeeEntityRef PayeeEntityRef { get; set; }
        [XmlElement(ElementName = "APAccountRef")]
        public APAccountRef APAccountRef { get; set; }
        [XmlElement(ElementName = "TxnDate")]
        public string TxnDate { get; set; }
        [XmlElement(ElementName = "BankAccountRef")]
        public BankAccountRef BankAccountRef { get; set; }
        [XmlElement(ElementName = "IsToBePrinted")]
        public string IsToBePrinted { get; set; }
        [XmlElement(ElementName = "RefNumber")]
        public string RefNumber { get; set; }
        [XmlElement(ElementName = "Memo")]
        public string Memo { get; set; }
        [XmlElement(ElementName = "ExchangeRate")]
        public string ExchangeRate { get; set; }
        [XmlElement(ElementName = "ExternalGUID")]
        public string ExternalGUID { get; set; }
        [XmlElement(ElementName = "AppliedToTxnAdd")]
        public AppliedToTxnAdd AppliedToTxnAdd { get; set; }

        //[XmlElement(ElementName = "ApplyCheckToTxnAdd")]
        //public ApplyCheckToTxnAdd ApplyCheckToTxnAdd { get; set; }
        //[XmlElement(ElementName = "ExpenseLineAdd")]
        //public ExpenseLineAdd[] ExpenseLineAdd { get; set; }
        //[XmlElement(ElementName = "ItemLineAdd")]
        //public ItemLineAdd ItemLineAdd { get; set; }
        //[XmlElement(ElementName = "ItemGroupLineAdd")]
        //public ItemGroupLineAdd ItemGroupLineAdd { get; set; }
        //[XmlAttribute(AttributeName = "defMacro")]
        public string DefMacro { get; set; }
    }

    [XmlRoot(ElementName = "BillPaymentCheckAddRq")]
    public class BillPaymentCheckAddRq
    {
        [XmlElement(ElementName = "BillPaymentCheckAdd")]
        public BillPaymentCheckAdd BillPaymentCheckAdd { get; set; }
        [XmlElement(ElementName = "IncludeRetElement")]
        public string IncludeRetElement { get; set; }
    }
}
