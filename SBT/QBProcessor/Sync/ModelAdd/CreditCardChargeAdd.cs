﻿using DataProcessor.Ref;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DataProcessor.Sync.ModelAdd
{
    [XmlRoot(ElementName = "CreditCardChargeAdd")]
    public class CreditCardChargeAdd
    {
        [XmlElement(ElementName = "AccountRef")]
        public AccountRef AccountRef { get; set; }
        [XmlElement(ElementName = "PayeeEntityRef")]
        public PayeeEntityRef PayeeEntityRef { get; set; }
        [XmlElement(ElementName = "TxnDate")]
        public string TxnDate { get; set; }
        [XmlElement(ElementName = "RefNumber")]
        public string RefNumber { get; set; }
        [XmlElement(ElementName = "Memo")]
        public string Memo { get; set; }
        [XmlElement(ElementName = "Address")]
        public Address Address { get; set; }
        [XmlElement(ElementName = "IsToBePrinted")]
        public string IsToBePrinted { get; set; }
        [XmlElement(ElementName = "ExchangeRate")]
        public string ExchangeRate { get; set; }
        [XmlElement(ElementName = "ExternalGUID")]
        public string ExternalGUID { get; set; }
        [XmlElement(ElementName = "ApplyCheckToTxnAdd")]
        public ApplyCheckToTxnAdd ApplyCheckToTxnAdd { get; set; }
        [XmlElement(ElementName = "ExpenseLineAdd")]
        public ExpenseLineAdd[] ExpenseLineAdd { get; set; }
        [XmlElement(ElementName = "ItemLineAdd")]
        public ItemLineAdd ItemLineAdd { get; set; }
        [XmlElement(ElementName = "ItemGroupLineAdd")]
        public ItemGroupLineAdd ItemGroupLineAdd { get; set; }
        [XmlAttribute(AttributeName = "defMacro")]
        public string DefMacro { get; set; }
    }

    [XmlRoot(ElementName = "CreditCardChargeAddRq")]
    public class CreditCardChargeAddRq
    {
        [XmlElement(ElementName = "CreditCardChargeAdd")]
        public CreditCardChargeAdd CreditCardChargeAdd { get; set; }
        [XmlElement(ElementName = "IncludeRetElement")]
        public string IncludeRetElement { get; set; }
    }
}
