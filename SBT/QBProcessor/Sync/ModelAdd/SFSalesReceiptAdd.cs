﻿using DataProcessor.Ref;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace DataProcessor.Sync.ModelAdd
{    
    public partial class SFSalesReceiptAdd
    {
        public class SalesReceipt
        {
            public string TxnID { get; set; }
            public string TxnDate { get; set; }
            public string TotalAmount { get; set; }
            public string SFReferance { get; set; }
            public string RefNumber { get; set; }
            public string RecordtypeName { get; set; }
            public string OpportunityStage { get; set; }
            public string DepositToAccountRefListID { get; set; }
            public string DepositToAccountRefFullName { get; set; }
            public string DateOfDonation { get; set; }
            public string CustomerRefListID { get; set; }
            public string CustomerRefFullName { get; set; }
            public string closeDate { get; set; }
            public string DonationDescription { get; set; }
            public string donationImportbatch { get; set; }
            public string paymentMethod { get; set; }
            public Customer customer { get; set; }
            public string ClassRefListID { get; set; }
            public string ClassRefFullName { get; set; }
            public string CheckNumber { get; set; }
        }


        public class InvoiceLinkedTxn
        {
            public string TxnID { get; set; }
            public string TxnDate { get; set; }
            public string TotalAmount { get; set; }
            public string SFReferance { get; set; }
            public object RefNumber { get; set; }
            public string RecordtypeName { get; set; }
            public string OpportunityStage { get; set; }
            public string LinktoTxnID1 { get; set; }
            public string DateOfDonation { get; set; }
            public string CustomerRefListID { get; set; }
            public string CustomerRefFullName { get; set; }
            public Customer customer { get; set; }
            public string ClassRefListID { get; set; }
            public string ClassRefFullName { get; set; }
            public string ARAccountRefListID { get; set; }
            public string ARAccountRefFullName { get; set; }
            public string InvoiceId { get; set; }
            public string QuickbooksmemoId { get; set; }
            public string paymentMethod { get; set; }            
        }
        public class Customer
        {
            public string SFReferance { get; set; }
            public string Salutation { get; set; }
            public string Name { get; set; }
            public string LastName { get; set; }
            public string FirstName { get; set; }
            public string BillAddressState { get; set; }
            public string BillAddressPostalCode { get; set; }
            public string BillAddressCountry { get; set; }
            public string BillAddressCity { get; set; }
            public string BillAddressAddr1 { get; set; }
        }
        public class RootObject1
        {
            public List<SalesReceipt> salesReceipts { get; set; }
            public string Message { get; set; }
            public bool isSuccess { get; set; }
            public List<InvoiceLinkedTxn> invoiceLinkedTxns { get; set; }
        }
    }
}