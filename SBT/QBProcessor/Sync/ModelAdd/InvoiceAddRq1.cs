﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DataProcessor.Sync.ModelAdd
{
    [XmlRoot(ElementName = "InvCustomerRef")]
    public class InvCustomerRef
    {
        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }
        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }

    }
    [XmlRoot(ElementName = "InvItemRef")]
    public class InvItemRef
    {
        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }
        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }

    }
    [XmlRoot(ElementName = "InvoiceAddRq")]
    public class InvoiceAddRq1
    {
        [XmlElement(ElementName = "InvCustomerRef")]
        public InvCustomerRef InvCustomerRef { get; set; }
        [XmlElement(ElementName = "InvItemRef")]
        public InvItemRef InvItemRef { get; set; }
       
        public decimal Amount { get; set; }
    }

}
