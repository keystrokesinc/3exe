﻿using DataProcessor.Ref;
using DataProcessor.Sync.ModelRet;

namespace DataProcessor.Sync.ModelAdd
{
    // using System.Xml.Serialization;
    // XmlSerializer serializer = new XmlSerializer(typeof(VendorCreditAdd));
    // using (StringReader reader = new StringReader(xml))
    // {
    //    var test = (VendorCreditAdd)serializer.Deserialize(reader);
    // }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    //public class VendorRef
    //{

        
    //    public string ListID { get; set; }

       
    //    public string FullName { get; set; }
    //}
 
    //public class APAccountRef
    //{

       
    //    public string ListID { get; set; }

       
    //    public string FullName { get; set; }
    //}

   
    //public class AccountRef
    //{

       
    //    public string ListID { get; set; }

        
    //    public string FullName { get; set; }
    //}

    
    public class CustomerRef
    {

        
        public string ListID { get; set; }

        
        public string FullName { get; set; }
    }

    
    //public class ClassRef
    //{

       
    //    public string ListID { get; set; }

        
    //    public string FullName { get; set; }
    //}

    
    public class SalesRepRef
    {

       
        public string ListID { get; set; }

        
        public string FullName { get; set; }
    }

    
    //   public class ExpenseLineAdd
    //{

        
    //    public AccountRef AccountRef { get; set; }

        
    //    public string Amount { get; set; }

        
    //    public string Memo { get; set; }

        
    //    public CustomerRef CustomerRef { get; set; }
 
    //    public ClassRef ClassRef { get; set; }

        
    //    public string BillableStatus { get; set; }

        
    //    public SalesRepRef SalesRepRef { get; set; }

        
    //    public DataExt DataExt { get; set; }

    //    public string DefMacro { get; set; }

        
    //    public string Text { get; set; }
    //}

    
    //public class ItemRef
    //{

        
    //    public string ListID { get; set; }

        
    //    public string FullName { get; set; }
    //}

    
    //public class InventorySiteRef
    //{

        
    //    public string ListID { get; set; }

        
    //    public string FullName { get; set; }
    //}

    
    //public class InventorySiteLocationRef
    //{

        
    //    public string ListID { get; set; }

        
    //    public string FullName { get; set; }
    //}

    
    //public class OverrideItemAccountRef
    //{

        
    //    public string ListID { get; set; }

        
    //    public string FullName { get; set; }
    //}

    
    //public class ItemLineAdd
    //{

        
    //    public ItemRef ItemRef { get; set; }

       
    //    public InventorySiteRef InventorySiteRef { get; set; }

        
    //    public InventorySiteLocationRef InventorySiteLocationRef { get; set; }

        
    //    public string SerialNumber { get; set; }

       
    //    public string LotNumber { get; set; }

       
    //    public string Desc { get; set; }

        
    //    public string Quantity { get; set; }

        
    //    public string UnitOfMeasure { get; set; }

        
    //    public string Cost { get; set; }

        
    //    public string Amount { get; set; }

       
    //    public CustomerRef CustomerRef { get; set; }

        
    //    public ClassRef ClassRef { get; set; }

        
    //    public string BillableStatus { get; set; }

        
    //    public OverrideItemAccountRef OverrideItemAccountRef { get; set; }

        
    //    public SalesRepRef SalesRepRef { get; set; }

       
    //    public DataExt DataExt { get; set; }
    //}

    
    //public class ItemGroupRef
    //{

       
    //    public string ListID { get; set; }

        
    //    public string FullName { get; set; }
    //}

    
    //public class ItemGroupLineAdd
    //{

        
    //    public ItemGroupRef ItemGroupRef { get; set; }

        
    //    public string Quantity { get; set; }

        
    //    public string UnitOfMeasure { get; set; }

        
    //    public InventorySiteRef InventorySiteRef { get; set; }

        
    //    public InventorySiteLocationRef InventorySiteLocationRef { get; set; }

       
    //    public DataExt DataExt { get; set; }
    //}

    
    public partial class VendorCreditAdd
    {

        
        public VendorRef VendorRef { get; set; }

        
        public APAccountRef APAccountRef { get; set; }

        
        public string TxnDate { get; set; }

        
        public string RefNumber { get; set; }

        
        public string Memo { get; set; }

        
        public string ExchangeRate { get; set; }

       
        public string ExternalGUID { get; set; }

        
        public ExpenseLineAdd ExpenseLineAdd { get; set; }

        
        public ItemLineAdd ItemLineAdd { get; set; }

       
        public ItemGroupLineAdd ItemGroupLineAdd { get; set; }

        
        public string DefMacro { get; set; }

        
        public string Text { get; set; }
    }


}
