﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DataProcessor.Sync.ModelAdd
{
    [XmlTypeAttribute(AnonymousType = true)]
    [XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class PaymentAdd
    {
        private ReceivePaymentRetCustomerRef customerRefField;
        public ReceivePaymentRetCustomerRef CustomerRef
        {
            get
            {
                return customerRefField;
            }
            set
            {
                customerRefField = value;
            }
        }

        private ReceivePaymentRetARAccountRef aRAccountRefField;
        public ReceivePaymentRetARAccountRef ARAccountRef
        {
            get
            {
                return aRAccountRefField;
            }
            set
            {
                aRAccountRefField = value;
            }
        }

        private string txnDateField;
        public string TxnDate
        {
            get
            {
                return txnDateField;
            }
            set
            {
                txnDateField = value;
            }
        }

        private string refNumberField;
        public string RefNumber
        {
            get
            {
                return refNumberField;
            }
            set
            {
                refNumberField = value;
            }
        }
        [DefaultValue("0.00")]
        private string totalAmountField;
        public string TotalAmount
        {
            get
            {
                return totalAmountField;
            }
            set
            {
                totalAmountField = value;
            }
        }

        private string exchangeRateField;
        public string ExchangeRate
        {
            get
            {
                return exchangeRateField;
            }
            set
            {
                exchangeRateField = value;
            }
        }

        private ReceivePaymentMethodRef paymentMethodRefField;
        public ReceivePaymentMethodRef PaymentMethodRef
        {
            get
            {
                return paymentMethodRefField;
            }
            set
            {
                paymentMethodRefField = value;
            }
        }

        private string memoField;
        public string Memo
        {
            get
            {
                return memoField;
            }
            set
            {
                memoField = value;
            }
        }

        private ReceivePaymentRetDepositToAccountRef depositToAccountRefField;
        public ReceivePaymentRetDepositToAccountRef DepositToAccountRef
        {
            get
            {
                return depositToAccountRefField;
            }
            set
            {
                depositToAccountRefField = value;
            }
        }

        private ReceivePaymentRetCreditCardTxnInfo creditCardTxnInfoField;
        public ReceivePaymentRetCreditCardTxnInfo CreditCardTxnInfo
        {
            get
            {
                return creditCardTxnInfoField;
            }
            set
            {
                creditCardTxnInfoField = value;
            }
        }

        private string externalGUIDField;
        public string ExternalGUID
        {
            get
            {
                return externalGUIDField;
            }
            set
            {
                externalGUIDField = value;
            }
        }

        //private bool isAutoApply;
        //public bool IsAutoApply
        //{
        //    get { return this.isAutoApply; }
        //    set { this.isAutoApply = value; }
        //}

        private AppliedToTxnAdd[] appliedToTxnRetField;
        [XmlElement("AppliedToTxnAdd")]
        public AppliedToTxnAdd[] AppliedToTxnAdd
        {
            get
            {
                return appliedToTxnRetField;
            }
            set
            {
                appliedToTxnRetField = value;
            }
        }
   

   
    }

}
