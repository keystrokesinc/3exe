﻿using DataProcessor.Ref;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DataProcessor.Sync.ModelAdd
{
   
    [XmlRoot(ElementName = "PayeeEntityRef")]
    public class PayeeEntityRef
    {
        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }
        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }

    [XmlRoot(ElementName = "Address")]
    public class Address
    {
        [XmlElement(ElementName = "Addr1")]
        public string Addr1 { get; set; }
        [XmlElement(ElementName = "Addr2")]
        public string Addr2 { get; set; }
        [XmlElement(ElementName = "Addr3")]
        public string Addr3 { get; set; }
        [XmlElement(ElementName = "Addr4")]
        public string Addr4 { get; set; }
        [XmlElement(ElementName = "Addr5")]
        public string Addr5 { get; set; }
        [XmlElement(ElementName = "City")]
        public string City { get; set; }
        [XmlElement(ElementName = "State")]
        public string State { get; set; }
        [XmlElement(ElementName = "PostalCode")]
        public string PostalCode { get; set; }
        [XmlElement(ElementName = "Country")]
        public string Country { get; set; }
        [XmlElement(ElementName = "Note")]
        public string Note { get; set; }
    }

    [XmlRoot(ElementName = "TxnID")]
    public class TxnID
    {
        [XmlAttribute(AttributeName = "useMacro")]
        public string UseMacro { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "ApplyCheckToTxnAdd")]
    public class ApplyCheckToTxnAdd
    {
        [XmlElement(ElementName = "TxnID")]
        public TxnID TxnID { get; set; }
        [XmlElement(ElementName = "Amount")]
        public string Amount { get; set; }
    }

    [XmlRoot(ElementName = "DataExt")]
    public class DataExt
    {
        [XmlElement(ElementName = "OwnerID")]
        public string OwnerID { get; set; }
        [XmlElement(ElementName = "DataExtName")]
        public string DataExtName { get; set; }
        [XmlElement(ElementName = "DataExtValue")]
        public string DataExtValue { get; set; }
    }

    [XmlRoot(ElementName = "ExpenseLineAdd")]
    public class ExpenseLineAdd
    {
        [XmlElement(ElementName = "AccountRef")]
        public AccountRef AccountRef { get; set; }
        [XmlElement(ElementName = "Amount")]
        public string Amount { get; set; }
        [XmlElement(ElementName = "Memo")]
        public string Memo { get; set; }
        [XmlElement(ElementName = "CustomerRef")]
        public CustomerRef CustomerRef { get; set; }
        [XmlElement(ElementName = "ClassRef")]
        public ClassRef ClassRef { get; set; }
        [XmlElement(ElementName = "BillableStatus")]
        public string BillableStatus { get; set; }
        [XmlElement(ElementName = "SalesRepRef")]
        public SalesRepRef SalesRepRef { get; set; }
        [XmlElement(ElementName = "DataExt")]
        public DataExt DataExt { get; set; }
        [XmlAttribute(AttributeName = "defMacro")]
        public string DefMacro { get; set; }
    }

    [XmlRoot(ElementName = "ItemRef")]
    public class ItemRef
    {
        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }
        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }

    [XmlRoot(ElementName = "InventorySiteRef")]
    public class InventorySiteRef
    {
        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }
        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }

    [XmlRoot(ElementName = "InventorySiteLocationRef")]
    public class InventorySiteLocationRef
    {
        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }
        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }

    [XmlRoot(ElementName = "OverrideItemAccountRef")]
    public class OverrideItemAccountRef
    {
        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }
        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }

    [XmlRoot(ElementName = "ItemLineAdd")]
    public class ItemLineAdd
    {
        [XmlElement(ElementName = "ItemRef")]
        public ItemRef ItemRef { get; set; }
        [XmlElement(ElementName = "InventorySiteRef")]
        public InventorySiteRef InventorySiteRef { get; set; }
        [XmlElement(ElementName = "InventorySiteLocationRef")]
        public InventorySiteLocationRef InventorySiteLocationRef { get; set; }
        [XmlElement(ElementName = "SerialNumber")]
        public string SerialNumber { get; set; }
        [XmlElement(ElementName = "LotNumber")]
        public string LotNumber { get; set; }
        [XmlElement(ElementName = "Desc")]
        public string Desc { get; set; }
        [XmlElement(ElementName = "Quantity")]
        public string Quantity { get; set; }
        [XmlElement(ElementName = "UnitOfMeasure")]
        public string UnitOfMeasure { get; set; }
        [XmlElement(ElementName = "Cost")]
        public string Cost { get; set; }
        [XmlElement(ElementName = "Amount")]
        public string Amount { get; set; }
        [XmlElement(ElementName = "CustomerRef")]
        public CustomerRef CustomerRef { get; set; }
        [XmlElement(ElementName = "ClassRef")]
        public ClassRef ClassRef { get; set; }
        [XmlElement(ElementName = "BillableStatus")]
        public string BillableStatus { get; set; }
        [XmlElement(ElementName = "OverrideItemAccountRef")]
        public OverrideItemAccountRef OverrideItemAccountRef { get; set; }
        [XmlElement(ElementName = "SalesRepRef")]
        public SalesRepRef SalesRepRef { get; set; }
        [XmlElement(ElementName = "DataExt")]
        public DataExt DataExt { get; set; }
    }

    [XmlRoot(ElementName = "ItemGroupRef")]
    public class ItemGroupRef
    {
        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }
        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }

    [XmlRoot(ElementName = "ItemGroupLineAdd")]
    public class ItemGroupLineAdd
    {
        [XmlElement(ElementName = "ItemGroupRef")]
        public ItemGroupRef ItemGroupRef { get; set; }
        [XmlElement(ElementName = "Quantity")]
        public string Quantity { get; set; }
        [XmlElement(ElementName = "UnitOfMeasure")]
        public string UnitOfMeasure { get; set; }
        [XmlElement(ElementName = "InventorySiteRef")]
        public InventorySiteRef InventorySiteRef { get; set; }
        [XmlElement(ElementName = "InventorySiteLocationRef")]
        public InventorySiteLocationRef InventorySiteLocationRef { get; set; }
        [XmlElement(ElementName = "DataExt")]
        public DataExt DataExt { get; set; }
    }

    [XmlRoot(ElementName = "CheckAdd")]
    public class CheckAdd
    {
        [XmlElement(ElementName = "AccountRef")]
        public AccountRef AccountRef { get; set; }
        [XmlElement(ElementName = "PayeeEntityRef")]
        public PayeeEntityRef PayeeEntityRef { get; set; }
        [XmlElement(ElementName = "RefNumber")]
        public string RefNumber { get; set; }
        [XmlElement(ElementName = "TxnDate")]
        public string TxnDate { get; set; }
        [XmlElement(ElementName = "Memo")]
        public string Memo { get; set; }
        [XmlElement(ElementName = "Address")]
        public Address Address { get; set; }
        [XmlElement(ElementName = "IsToBePrinted")]
        public string IsToBePrinted { get; set; }
        [XmlElement(ElementName = "ExchangeRate")]
        public string ExchangeRate { get; set; }
        [XmlElement(ElementName = "ExternalGUID")]
        public string ExternalGUID { get; set; }
        [XmlElement(ElementName = "ApplyCheckToTxnAdd")]
        public ApplyCheckToTxnAdd ApplyCheckToTxnAdd { get; set; }
        [XmlElement(ElementName = "ExpenseLineAdd")]
        public ExpenseLineAdd[] ExpenseLineAdd { get; set; }
        [XmlElement(ElementName = "ItemLineAdd")]
        public ItemLineAdd ItemLineAdd { get; set; }
        [XmlElement(ElementName = "ItemGroupLineAdd")]
        public ItemGroupLineAdd ItemGroupLineAdd { get; set; }
        [XmlAttribute(AttributeName = "defMacro")]
        public string DefMacro { get; set; }
    }

    [XmlRoot(ElementName = "CheckAddRq")]
    public class CheckAddRq
    {
        [XmlElement(ElementName = "CheckAdd")]
        public CheckAdd CheckAdd { get; set; }
        [XmlElement(ElementName = "IncludeRetElement")]
        public string IncludeRetElement { get; set; }
    }
}
