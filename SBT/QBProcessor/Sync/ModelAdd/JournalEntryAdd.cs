﻿using DataProcessor.Ref;
using DataProcessor.Sync.ModelRet;
using System.Collections.Generic;
using System.Xml.Serialization;
using AccountRef = DataProcessor.Sync.ModelRet.AccountRef;
using ClassRef = DataProcessor.Sync.ModelRet.ClassRef;

namespace DataProcessor.Sync.ModelAdd
{
    [XmlTypeAttribute(AnonymousType = true)]
    [XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class JournalEntryAdd
    {
        public string TxnDate { get; set; }
        public string RefNumber { get; set; }
        public string IsAdjustment { get; set; }
        public string IsHomeCurrencyAdjustment { get; set; }
        public string IsAmountsEnteredInHomeCurrency { get; set; }
        public CurrencyRef CurrencyRef { get; set; }
        public string ExchangeRate { get; set; }
        public string ExternalGUID { get; set; }

        //public <List>JournalDebitLine JournalDebitLine { get; set; }     
        [XmlElement("")]
        public List<JournalDebitLine> JournalDebitLine { get; set; }
        [XmlElement("")]
        public List<JournalCreditLine> JournalCreditLine { get; set; }
    }
    
    public partial class JournalDebitLine
    {
        public string TxnLineID { get; set; }
        public AccountRef AccountRef { get; set; }
        public string Amount { get; set; }
        public string Memo { get; set; }
        public EntityRef EntityRef { get; set; }
        public ClassRef ClassRef { get; set; }
        public string BillableStatus { get; set; }       
    }
    public partial class JournalCreditLine
    {
        public string TxnLineID { get; set; }
        public AccountRef AccountRef { get; set; }
        public string Amount { get; set; }
        public string Memo { get; set; }
        public EntityRef EntityRef { get; set; }
        public ClassRef ClassRef { get; set; }
        public string BillableStatus { get; set; }
       
    }

    //public partial class ClassRef
    //{

    //    private string listIDField;

    //    private string fullNameField;

    //    /// <remarks/>
    //    public string ListID
    //    {
    //        get
    //        {
    //            return listIDField;
    //        }
    //        set
    //        {
    //            listIDField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string FullName
    //    {
    //        get
    //        {
    //            return fullNameField;
    //        }
    //        set
    //        {
    //            fullNameField = value;
    //        }
    //    }
    //}

    /// <remarks/>
  
    public partial class EntityRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

}