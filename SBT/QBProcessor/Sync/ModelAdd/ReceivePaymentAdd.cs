﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DataProcessor.Sync.ModelAdd
{
    [XmlTypeAttribute(AnonymousType = true)]
    [XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class ReceivePaymentAdd
    {
        private ReceivePaymentRetCustomerRef customerRefField;
        public ReceivePaymentRetCustomerRef CustomerRef
        {
            get
            {
                return customerRefField;
            }
            set
            {
                customerRefField = value;
            }
        }

        private ReceivePaymentRetARAccountRef aRAccountRefField;
        public ReceivePaymentRetARAccountRef ARAccountRef
        {
            get
            {
                return aRAccountRefField;
            }
            set
            {
                aRAccountRefField = value;
            }
        }

        private string txnDateField;
        public string TxnDate
        {
            get
            {
                return txnDateField;
            }
            set
            {
                txnDateField = value;
            }
        }

        private string refNumberField;
        public string RefNumber
        {
            get
            {
                return refNumberField;
            }
            set
            {
                refNumberField = value;
            }
        }
        [DefaultValue("0.00")]
        private string totalAmountField;
        public string TotalAmount
        {
            get
            {
                return totalAmountField;
            }
            set
            {
                totalAmountField = value;
            }
        }

        private string exchangeRateField;
        public string ExchangeRate
        {
            get
            {
                return exchangeRateField;
            }
            set
            {
                exchangeRateField = value;
            }
        }

        private ReceivePaymentMethodRef paymentMethodRefField;
        public ReceivePaymentMethodRef PaymentMethodRef
        {
            get
            {
                return paymentMethodRefField;
            }
            set
            {
                paymentMethodRefField = value;
            }
        }

        private string memoField;
        public string Memo
        {
            get
            {
                return memoField;
            }
            set
            {
                memoField = value;
            }
        }

        private ReceivePaymentRetDepositToAccountRef depositToAccountRefField;
        public ReceivePaymentRetDepositToAccountRef DepositToAccountRef
        {
            get
            {
                return depositToAccountRefField;
            }
            set
            {
                depositToAccountRefField = value;
            }
        }

        private ReceivePaymentRetCreditCardTxnInfo creditCardTxnInfoField;
        public ReceivePaymentRetCreditCardTxnInfo CreditCardTxnInfo
        {
            get
            {
                return creditCardTxnInfoField;
            }
            set
            {
                creditCardTxnInfoField = value;
            }
        }

        private string externalGUIDField;
        public string ExternalGUID
        {
            get
            {
                return externalGUIDField;
            }
            set
            {
                externalGUIDField = value;
            }
        }

        private bool isAutoApply;
        public bool IsAutoApply
        {
            get { return this.isAutoApply; }
            set { this.isAutoApply = value; }
        }

        private AppliedToTxnAdd[] appliedToTxnRetField;
        [XmlElement("AppliedToTxnAdd")]
        public AppliedToTxnAdd[] AppliedToTxnAdd
        {
            get
            {
                return appliedToTxnRetField;
            }
            set
            {
                appliedToTxnRetField = value;
            }
        }
    }

    [XmlTypeAttribute(AnonymousType = true)]
    public partial class ReceivePaymentRetCustomerRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    [XmlTypeAttribute(AnonymousType = true)]
    public partial class ReceivePaymentRetARAccountRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    [XmlTypeAttribute(AnonymousType = true)]
    public partial class ReceivePaymentMethodRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    [XmlTypeAttribute(AnonymousType = true)]
    public partial class ReceivePaymentRetDepositToAccountRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    [XmlTypeAttribute(AnonymousType = true)]
    public partial class ReceivePaymentRetCreditCardTxnInfo
    {

        private ReceivePaymentRetCreditCardTxnInfoCreditCardTxnInputInfo creditCardTxnInputInfoField;

        private ReceivePaymentRetCreditCardTxnInfoCreditCardTxnResultInfo creditCardTxnResultInfoField;

        /// <remarks/>
        public ReceivePaymentRetCreditCardTxnInfoCreditCardTxnInputInfo CreditCardTxnInputInfo
        {
            get
            {
                return creditCardTxnInputInfoField;
            }
            set
            {
                creditCardTxnInputInfoField = value;
            }
        }

        /// <remarks/>
        public ReceivePaymentRetCreditCardTxnInfoCreditCardTxnResultInfo CreditCardTxnResultInfo
        {
            get
            {
                return creditCardTxnResultInfoField;
            }
            set
            {
                creditCardTxnResultInfoField = value;
            }
        }
    }

    [XmlTypeAttribute(AnonymousType = true)]
    public partial class ReceivePaymentRetCreditCardTxnInfoCreditCardTxnInputInfo
    {

        private string creditCardNumberField;

        private string expirationMonthField;

        private string expirationYearField;

        private string nameOnCardField;

        private string creditCardAddressField;

        private string creditCardPostalCodeField;

        private string commercialCardCodeField;

        private string transactionModeField;

        private string creditCardTxnTypeField;

        /// <remarks/>
        public string CreditCardNumber
        {
            get
            {
                return creditCardNumberField;
            }
            set
            {
                creditCardNumberField = value;
            }
        }

        /// <remarks/>
        public string ExpirationMonth
        {
            get
            {
                return expirationMonthField;
            }
            set
            {
                expirationMonthField = value;
            }
        }

        /// <remarks/>
        public string ExpirationYear
        {
            get
            {
                return expirationYearField;
            }
            set
            {
                expirationYearField = value;
            }
        }

        /// <remarks/>
        public string NameOnCard
        {
            get
            {
                return nameOnCardField;
            }
            set
            {
                nameOnCardField = value;
            }
        }

        /// <remarks/>
        public string CreditCardAddress
        {
            get
            {
                return creditCardAddressField;
            }
            set
            {
                creditCardAddressField = value;
            }
        }

        /// <remarks/>
        public string CreditCardPostalCode
        {
            get
            {
                return creditCardPostalCodeField;
            }
            set
            {
                creditCardPostalCodeField = value;
            }
        }

        /// <remarks/>
        public string CommercialCardCode
        {
            get
            {
                return commercialCardCodeField;
            }
            set
            {
                commercialCardCodeField = value;
            }
        }

        /// <remarks/>
        public string TransactionMode
        {
            get
            {
                return transactionModeField;
            }
            set
            {
                transactionModeField = value;
            }
        }

        /// <remarks/>
        public string CreditCardTxnType
        {
            get
            {
                return creditCardTxnTypeField;
            }
            set
            {
                creditCardTxnTypeField = value;
            }
        }
    }

    [XmlTypeAttribute(AnonymousType = true)]
    public partial class ReceivePaymentRetCreditCardTxnInfoCreditCardTxnResultInfo
    {

        private string resultCodeField;

        private string resultMessageField;

        private string creditCardTransIDField;

        private string merchantAccountNumberField;

        private string authorizationCodeField;

        private string aVSStreetField;

        private string aVSZipField;

        private string cardSecurityCodeMatchField;

        private string reconBatchIDField;

        private string paymentGroupingCodeField;

        private string paymentStatusField;

        private string txnAuthorizationTimeField;

        private string txnAuthorizationStampField;

        private string clientTransIDField;

        /// <remarks/>
        public string ResultCode
        {
            get
            {
                return resultCodeField;
            }
            set
            {
                resultCodeField = value;
            }
        }

        /// <remarks/>
        public string ResultMessage
        {
            get
            {
                return resultMessageField;
            }
            set
            {
                resultMessageField = value;
            }
        }

        /// <remarks/>
        public string CreditCardTransID
        {
            get
            {
                return creditCardTransIDField;
            }
            set
            {
                creditCardTransIDField = value;
            }
        }

        /// <remarks/>
        public string MerchantAccountNumber
        {
            get
            {
                return merchantAccountNumberField;
            }
            set
            {
                merchantAccountNumberField = value;
            }
        }

        /// <remarks/>
        public string AuthorizationCode
        {
            get
            {
                return authorizationCodeField;
            }
            set
            {
                authorizationCodeField = value;
            }
        }

        /// <remarks/>
        public string AVSStreet
        {
            get
            {
                return aVSStreetField;
            }
            set
            {
                aVSStreetField = value;
            }
        }

        /// <remarks/>
        public string AVSZip
        {
            get
            {
                return aVSZipField;
            }
            set
            {
                aVSZipField = value;
            }
        }

        /// <remarks/>
        public string CardSecurityCodeMatch
        {
            get
            {
                return cardSecurityCodeMatchField;
            }
            set
            {
                cardSecurityCodeMatchField = value;
            }
        }

        /// <remarks/>
        public string ReconBatchID
        {
            get
            {
                return reconBatchIDField;
            }
            set
            {
                reconBatchIDField = value;
            }
        }

        /// <remarks/>
        public string PaymentGroupingCode
        {
            get
            {
                return paymentGroupingCodeField;
            }
            set
            {
                paymentGroupingCodeField = value;
            }
        }

        /// <remarks/>
        public string PaymentStatus
        {
            get
            {
                return paymentStatusField;
            }
            set
            {
                paymentStatusField = value;
            }
        }

        /// <remarks/>
        public string TxnAuthorizationTime
        {
            get
            {
                return txnAuthorizationTimeField;
            }
            set
            {
                txnAuthorizationTimeField = value;
            }
        }

        /// <remarks/>
        public string TxnAuthorizationStamp
        {
            get
            {
                return txnAuthorizationStampField;
            }
            set
            {
                txnAuthorizationStampField = value;
            }
        }

        /// <remarks/>
        public string ClientTransID
        {
            get
            {
                return clientTransIDField;
            }
            set
            {
                clientTransIDField = value;
            }
        }
    }

    [XmlTypeAttribute(AnonymousType = true)]
    public partial class AppliedToTxnRetDiscountAccountRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    [XmlTypeAttribute(AnonymousType = true)]
    public partial class AppliedToTxnRetDiscountClassRef
    {

        private string listIDField;

        private string fullNameField;

        /// <remarks/>
        public string ListID
        {
            get
            {
                return listIDField;
            }
            set
            {
                listIDField = value;
            }
        }

        /// <remarks/>
        public string FullName
        {
            get
            {
                return fullNameField;
            }
            set
            {
                fullNameField = value;
            }
        }
    }

    [XmlTypeAttribute(AnonymousType = true)]
    public partial class AppliedToTxnRetLinkedTxn
    {

        private string txnIDField;

        private string txnTypeField;

        private string txnDateField;

        private string refNumberField;

        private string linkTypeField;

        private string amountField;

        /// <remarks/>
        public string TxnID
        {
            get
            {
                return txnIDField;
            }
            set
            {
                txnIDField = value;
            }
        }

        /// <remarks/>
        public string TxnType
        {
            get
            {
                return txnTypeField;
            }
            set
            {
                txnTypeField = value;
            }
        }

        /// <remarks/>
        public string TxnDate
        {
            get
            {
                return txnDateField;
            }
            set
            {
                txnDateField = value;
            }
        }

        /// <remarks/>
        public string RefNumber
        {
            get
            {
                return refNumberField;
            }
            set
            {
                refNumberField = value;
            }
        }

        /// <remarks/>
        public string LinkType
        {
            get
            {
                return linkTypeField;
            }
            set
            {
                linkTypeField = value;
            }
        }

        /// <remarks/>
        public string Amount
        {
            get
            {
                return amountField;
            }
            set
            {
                amountField = value;
            }
        }
    }

}
