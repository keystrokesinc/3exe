﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using DataProcessor.Properties;
using Microsoft.VisualBasic;
using System.Diagnostics;

namespace DataProcessor
{
    public static class GlobalValue
    {
        public const string AppVersion = "1.0.0001.16"; // 1.0.MMdd.0
       public const string UpdateXmlURL = "http://dev.simplebilltracking.com/SyncLogApp/SBTUpdate.xml"; // Dev
       //  public const string UpdateXmlURL = "http://172.16.1.2/SBT/SyncApp/SBTUpdate.xml";  // Dev Internal 
          //public const string UpdateXmlURL = "https://clients.simplebilltracking.com/SyncLogApp/SBTUpdate.xml"; //live Sync App log 

        public struct Reponse
        {
            public bool IsError;
            public string Message;
            public int RecordCount;
            public object EntityObject;
        }

        #region "wtrite log event"

        public static string StrModule = "";
        public static string Tab = "      ";
        public static bool IsErrorLog = true;
        public static string UpdateDownloadPath;

        public static void Eventlogupdate(string logString, TextBox textbox, string type = null)
        {
            if (textbox == null) textbox = new TextBox();
            try
            {
                if (StrModule.Length > 17)
                {
                    StrModule = StrModule.Substring(0, 17);
                }
                DateTime todaydate = DateTime.Now.AddMilliseconds(9);
                string datePart = todaydate.Date.ToString();
                string timePart = todaydate.TimeOfDay.ToString();
                string hourOnly = todaydate.Hour.ToString();
                string dayonly = todaydate.Day.ToString();
                string monthOnly = todaydate.Month.ToString();
                string yearOnly = todaydate.Year.ToString();

                if (dayonly.Length == 1)
                {
                    dayonly = "0" + dayonly;
                }

                if (monthOnly.Length == 1)
                {
                    monthOnly = "0" + monthOnly;
                }

                string ldir = yearOnly + "-" + monthOnly + "-" + dayonly + " " + timePart;
                string message = ldir + Tab + StrModule + Tab + logString;

                int length = textbox.Text.Length;

                if (type == null)
                {
                    if (IsErrorLog)
                    {
                        var with1 = textbox;
                        with1.Text = textbox.Text + message + Environment.NewLine;
                        with1.SelectionStart = length;
                        with1.SelectionLength = message.Length;
                        with1.ForeColor = Color.Red;
                        with1.SelectionStart = Strings.Len(with1.Text) - 1;
                        with1.ScrollToCaret();
                        with1.Refresh();
                    }
                }
                else
                {
                    if (Settings.Default.ERRORLOGENABLE)
                    {
                        var with2 = textbox;
                        with2.ForeColor = Color.Black;
                        with2.Text = textbox.Text + message + Environment.NewLine;
                        with2.SelectionStart = Strings.Len(with2.Text) - 1;
                        with2.ScrollToCaret();
                        with2.Refresh();
                    }
                }

                string logtime = yearOnly + "-" + monthOnly + "-" + dayonly + ".txt";
                string logDirPath = AppDomain.CurrentDomain.BaseDirectory + "LOG\\ERRORLOG\\" + logtime;
                if (!Directory.Exists(Directory.GetParent(logDirPath).FullName))
                    Directory.CreateDirectory(Directory.GetParent(logDirPath).FullName);
                StreamWriter sw = new StreamWriter(logDirPath, true);
                sw.WriteLine(message);
                sw.Close();
            }
            catch (Exception ex)
            {
                string excep = ex.Message;
            }
        }

        public static void Eventlogupdate(Exception exception, TextBox textbox, string type = null)
        {
            if (textbox == null) textbox = new TextBox();
            try
            {
                if (StrModule.Length > 17)
                {
                    StrModule = StrModule.Substring(0, 17);
                }
                DateTime todaydate = DateTime.Now.AddMilliseconds(9);
                string datePart = todaydate.Date.ToString();
                string timePart = todaydate.TimeOfDay.ToString();
                string hourOnly = todaydate.Hour.ToString();
                string dayonly = todaydate.Day.ToString();
                string monthOnly = todaydate.Month.ToString();
                string yearOnly = todaydate.Year.ToString();

                if (dayonly.Length == 1)
                {
                    dayonly = "0" + dayonly;
                }

                if (monthOnly.Length == 1)
                {
                    monthOnly = "0" + monthOnly;
                }

                string ldir = yearOnly + "-" + monthOnly + "-" + dayonly + " " + timePart;
                string message = ldir + Tab + StrModule + Tab + GetAllFootprints(exception);

                int length = textbox.Text.Length;

                if (type == "Error")
                {
                    if (IsErrorLog)
                    {
                        var with1 = textbox;
                        with1.Text = textbox.Text + message + Environment.NewLine;
                        with1.SelectionStart = length;
                        with1.SelectionLength = message.Length;
                        with1.ForeColor = Color.Red;
                        with1.SelectionStart = Strings.Len(with1.Text) - 1;
                        with1.ScrollToCaret();
                        with1.Refresh();
                    }
                }
                else
                {
                    if (Settings.Default.ERRORLOGENABLE)
                    {
                        var with2 = textbox;
                        with2.ForeColor = Color.Black;
                        with2.Text = textbox.Text + message + Environment.NewLine;
                        with2.SelectionStart = Strings.Len(with2.Text) - 1;
                        with2.ScrollToCaret();
                        with2.Refresh();
                    }
                }

                string logtime = yearOnly + "-" + monthOnly + "-" + dayonly + ".txt";
                string logDirPath = AppDomain.CurrentDomain.BaseDirectory + "LOG\\ERRORLOG\\" + logtime;
                if (!Directory.Exists(Directory.GetParent(logDirPath).FullName))
                    Directory.CreateDirectory(Directory.GetParent(logDirPath).FullName);
                StreamWriter sw = new StreamWriter(logDirPath, true);
                sw.WriteLine(message);
                sw.Close();
            }
            catch (Exception ex)
            {
                string excep = ex.Message;
            }
        }

        public static string GetAllFootprints(Exception x)
        {
            var st = new StackTrace(x, true);
            var frames = st.GetFrames();
            var traceString = "";
            foreach (var frame in frames)
            {
                if (frame.GetFileLineNumber() < 1)
                    continue;

                traceString += string.Format("File: {0}, Method: {1}, LineNumber: {2}  -->" + Environment.NewLine,
                    frame.GetFileName(), frame.GetMethod().Name, frame.GetFileLineNumber());
            }

            return traceString;
        }
        #endregion
    }
}